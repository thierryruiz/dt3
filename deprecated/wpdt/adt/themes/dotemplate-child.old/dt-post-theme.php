<?php
/*
Template Name Posts: ThemePost
*/
?>
<?php get_header(); ?>
<div  id="page">
	<div class="layout-m">
		<div  id="main">
			
			<?php if (have_posts()) : while (have_posts()) : the_post();?>
			<div class="post" id="post-<?php the_ID(); ?>">

			<?php
				$theme_image =  content_url() . "/dtthemes/" . get_post_meta($post->ID, "_dt_themeId", true) . "/img.jpg" ;		
			?>
			
			<img src="<?php echo $theme_image ?>">
			<div class="theme-image-shade"></div>  

			<div style="text-align:center">
				<input type="button" class="button medium style2" style="font-weight:bold;border-radius: 25px;" value="Edit  /  Download" />
			</div>

			<h1>
				Template Features
			</h1>
	
			<div class="entry">
				<?php the_content(); ?>
				<div style="clear:both"></div>
			</div>
	
			<div class="entry-footer">
			</div>
	
			
			<?php comments_template(); ?> 
			<?php endwhile; endif; ?>
		
		</div>					
		<div class="clear" style="height:60px"></div>
	</div>
</div>
<?php get_footer(); ?>
