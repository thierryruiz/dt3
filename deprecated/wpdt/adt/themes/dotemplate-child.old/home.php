<?php
/*
Template Name : doTemplate home page
*/
?>
<?php get_header(); ?>

<style>
<!--
.centered {margin:0 auto}
.feature-bloc { text-align:cendered; padding: 100px 20px;}
.feature-bloc p, .feature-thumb figcaption { font-size:16px;}
#main .feature-bloc h1 { font-size:50px;text-align:center}
.feature-thumb {float:left;margin:30px 30px 0 0 ;width:280px;padding:0px;text-align:center;overflow:hidden;position:relative;}
.feature-thumb.last{margin-right:0} 
div.shade { background-color:#F5F5F5;border-top:1px solid #E4E4E4;border-bottom:1px solid #E4E4E4;}

-->
</style>


<div  id="page">

	<div class="layout-m">
		
		<div id="main" style="margin-top:20px;" >
		
			<div class="sequence-theme">
				<div id="sequence">
				
					<img class="sequence-prev" src="<?php bloginfo('stylesheet_directory'); ?>/images/bt-prev.png" alt="Previous Frame" />
					<!-- <img class="sequence-pause" src="<?php bloginfo('stylesheet_directory'); ?>/images/pause-icon.png" alt="Pause Frame" /> -->					
					<img class="sequence-next" src="<?php bloginfo('stylesheet_directory'); ?>/images/bt-next.png" alt="Next Frame" />
	
					<ul class="sequence-canvas">
						<li class="animate-in">
							<div class="title">Dude ! Create Beautiful Website Templates</div>
							<img class="model" src="<?php bloginfo('stylesheet_directory'); ?>/images/slide1.png" alt="Create Beatiful Website Templates" />
						</li>
						<li>
							<div class="title">Free <br/>Online Template Builder</div>
							<img class="model" src="<?php bloginfo('stylesheet_directory'); ?>/images/slide2.png" alt="Free Online Template Edidtor" />
						</li>
					</ul>
		
				</div>
			</div>			
		
			<div id="themes" class="centered">
			
				<?php query_posts('tag=featured'); ?>
				<?php get_template_part( 'themes-grid' ); ?> 
				
				<div class="clear"></div>
	
			</div>
	
			<div  style="margin:100px 0 60px 0;text-align:center">
				
				<a href="<?php bloginfo('url'); ?>/?tag=theme" title="Browse all templates">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/browse.png" alt="browse all templates"></img>
				</a>
				
			</div>
	
	
			<section id="features">
				
				<div class="feature-bloc"> 
				
					<div class="centered">				
						
						<article>
							
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/screen-1.jpg" alt="theme screen" style="float:right;margin:40px 0 0 40px" />
								<header style="padding-top:160px;">
									<h1>Web Design Made Easy</h1>
								</header>
								<p>
									Making your own Web template has never been easier. Our <strong>online editor</strong> makes it a matter of minutes.
									No software to install. No Photoshop needed. No tricky PSD to HTML conversion required. 
									All you need is there to create a stunning custom Web design ready for download. It's quick, easy and fun !
								</p>
							<div class="clear"></div>
						</article>
					
					</div>
				
				</div>

				<div class="feature-bloc"> 
				
					<div class="centered">				
						
						<article>
							<header>
								<h1>Hundreds of settings to make your very unique template<h1>
							</header>
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/hundreds-of-settings.jpg" alt="template settings" />
							<p style="width:500px;margin:30px auto;text-align:center">
								All parts of your template can be customized. From header to footer, access hundreds of advanced settings to make a fully personalized 
								Website template.
							</p>
						</article>
					
					</div>
				
				</div>

				<div class="feature-bloc"> 
				
					<div class="centered">				
						
						<article>
							<header>
								<h1>Design your header like a pro<h1>
							</header>
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/header-editor.jpg" alt="theme screen" width="100%" style="margin:0 auto" />
							
							<p style="width:500px;margin:30px auto;text-align:center">
								Customize your header is a breeze with the advanced <strong>header editor</strong>, featuring great text effects, classy fonts and premium photos
								library.
							</p>	
							
							
						</article>
					
					</div>
				
				</div>


				<div class="feature-bloc"> 
				
					<div class="centered">				
						
						<article>
							<header>
								<h1>Premium photos library<h1>
							</header>
							
							<img src="<?php bloginfo('stylesheet_directory'); ?>/images/premium-photos.jpg" alt="premium photos" />
							
							<p style="width:500px;margin:30px auto;text-align:center">
								Did you ever wonder what makes a Web design looks good ? <br/><strong>Premium photos !</strong><br/>For less than the price of a single premium photo, 
								you can build your own custom template and choose from a large collection of professionnal shoutings. 
							</p>								
							
						</article>
					
					</div>
				
				</div>
				<div class="feature-bloc"> 
				
					<div class="centered">				
						
						<article>
							<header>
								<h1>Choose from hundreds of design resources<h1>
							</header>
							<div class="feature-thumb">
								<figure>
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/gradients.jpg" alt="background gradients collection" />
									<figcaption>Background gradients</figcaption>							
								</figure>
								
								
							</div>

							<div class="feature-thumb">
								<figure>
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/textures-patterns.jpg" alt="texures and patterns collection" />
									<figcaption>Texture and patterns</figcaption>						
								</figure>
								
							</div>
							
							<div class="feature-thumb last">
								<figure>
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/color-schemes.jpg" alt="Apply color schemes on your theme in one click" />
									<figcaption>"One-click" color schemes</figcaption>								
								</figure>
							</div>				
						
						
							<div class="clear"></div>							

							<div class="feature-thumb">
								<figure>
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/fonts.jpg" alt="fonts" />
									<figcaption>400+ fonts</figcaption>						
								</figure>
								
							</div>

							<div class="feature-thumb">
								<figure>
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/text-fx.jpg" alt="Apply text styles" />
									<figcaption>Stunning text styles</figcaption>						
								</figure>
								
							</div>
							
							<div class="feature-thumb last">
								<figure>
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/logo-gallery.jpg" alt="Logo gallery" />
									<figcaption>Logo gallery</figcaption>								
								</figure>
							</div>									

							<div class="clear"></div>	

						</article>
					
					</div>
				
				</div>
				<div class="feature-bloc"> 
				
					<div class="centered">				
						
						<article>
						
							<header>
								<h1>Wordpress compatible templates</h1>
							</header>
						
							<div>
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/wordpress-export.jpg" alt="Export your template as Wordpress theme" style="float:left;margin:40px 40px 0 40px" />
								
							
								<p style="padding-top:150px;text-align:justify">
									You can export your custom template as <strong>Wordpress theme</strong>, the most popular Content Management System. 
									Publish your own Wordpress blog or Website today. 
								</p>
								<div class="clear"></div>
							</div>
						</article>
					
					</div>
				
				</div>
				
				
				<div class="feature-bloc"> 
				
					<div class="centered">				
						
						<article>
									
									
							<header>
								<h1>Clean HTML code<br/>Easy to update, easy to adapt !</h1>
							</header>
							<div>
										
								<img src="<?php bloginfo('stylesheet_directory'); ?>/images/code.jpg" alt="theme screen" style="float:right;margin:30px 0 0 60px;box-shadow:0 0 3px #555" />
								
								<p style="padding-top:50px;margin:30px;">
									Most of the website templates you will find on the market, are complex to modify and adapt to your branding and needs. 
									The templates you will create with our tool not only will be customized but the resulted HTML code will be easy to adapt even if you are not an expert in HTML. 
									Try our free templates and see how easy it is to create a Website from our templates.
					
								</p>
							
								<div class="clear"></div>
							</div>
							
						
						
													
						</article>
					
					</div>
				
				</div>				
						

			</section>
			
	
	
			<div  style="margin:100px 0 60px 0;text-align:center">
				
				<a id="try-now-btn" href="#themes" title="Browse all templates">
					<img src="<?php bloginfo('stylesheet_directory'); ?>/images/try-now.jpg" alt="Try template editor. It's free"></img>
				</a>
				
			</div>
	
			
			
		</div>
				
			
		<div class="clear" style="height:60px"></div>
	</div>
</div>
<?php get_footer(); ?>
