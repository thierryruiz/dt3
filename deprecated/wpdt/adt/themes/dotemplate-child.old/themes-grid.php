

<?php if (have_posts()) : ?>


	<?php echo $dotemplate_url ; ?>
	<?php $counter = 1; ?>

	<?php while (have_posts()) : the_post(); ?>
		
		<?php
		
			$thumbnail_image =  content_url() . "/dtthemes/" . get_post_meta($post->ID, "_dt_themeId", true) . "/th.jpg" ;
			
		?>
		
		<?php $last = ( ( $counter % 3 == 0 ) ? ' last' : '' ) ?>
		
		<div class="theme-thumb <?php echo $last; ?>"  id="post-<?php the_ID(); ?>">
			<div class="viewport">
				<div class="theme-actions">
					<img class="theme-thumb-img" src="<?php echo $thumbnail_image ?>" />
					<div>
						<a href="<?php echo DOTEMPLATE_URL; ?>/dt/createTheme?id=<?php echo get_post_meta($post->ID, "_dt_themeId", true); ?>" rel="nofollow" class="dteditor">Customize in template editor...</a>  
						<!-- <a href="<?php the_permalink() ?>">Details</a> -->
					</div>
				</div>		
			</div>				
			<div class="theme-thumb-shade"></div>
			
		</div>
		
		<?php $counter++; //Update Counter ?>
				
	<?php endwhile; ?>
	
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } // paginate using pagenavi plugin ?>

	
<?php endif; ?>
