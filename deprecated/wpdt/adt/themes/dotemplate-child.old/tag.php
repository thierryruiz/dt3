<?php get_header(); ?>
<div  id="page">
	<div class="layout-ms">
		<div  id="main">
		 	<?php
		 	 
		 		if ( is_tag('theme') ) { 
					get_template_part( 'themes-grid' );
				} else {
					get_template_part( 'loop' );
				}
				
				
			?>
		</div>
		
		<?php get_sidebar(); ?>				
		
		<div class="clear" style="height:60px"></div>
	</div>
</div>
<?php get_footer(); ?>
