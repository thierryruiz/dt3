/**
 *  Drop down menu fix for IE6 - 
 *  credits to www.waterfallwaydesigns.com
 *   
 **/
$.fn.ie6hover = function() {
	this.each(function(){
		this.onmouseover=function(){
			$(this).addClass("ie6hover") ;
			//this.className+=" ie6hover";
		}
		this.onmouseout=function() {
			$(this).removeClass("ie6hover") ;
			//this.className=this.className.replace(new RegExp(" ie6hover\\b"), "" );
		}
	});
};
