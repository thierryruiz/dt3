/* Add your custom Javascrit here */


(function($){
	$.fn.juizScrollTo = function(speed, v_indent){
		
		if(!speed) var speed = 'slow';
		if(!v_indent) var v_indent = 0;
		
		return this.each(function(){
			$(this).click(function(){
				
				var goscroll = false;
				var the_hash = $(this).attr("href");
				var regex = new RegExp("\#(.*)","gi");

				if(the_hash.match("\#(.+)")) {

					the_hash = the_hash.replace(regex,"$1");

					if($("#"+the_hash).length>0) {
						the_element = "#" + the_hash;
						goscroll = true;
					}
					else if($("a[name=" + the_hash + "]").length>0) {
						the_element = "a[name=" + the_hash + "]";
						goscroll = true;
					}
				
					if(goscroll) {
						var container = 'html';
						if ($.browser.webkit) container = 'body';
						
						$(container).animate({
							scrollTop:$(the_element).offset().top + v_indent
						}, speed, 
							function(){$(the_element).attr('tabindex','0').focus().removeAttr('tabindex');});
						return false;
					}
				}
			});
		});
	};
})(jQuery)



$(document).ready( function() {

	$('#try-now-btn').juizScrollTo('slow');	


	// sequence slider
	var options = {
		nextButton: true,
		prevButton: true,
		pagination: true,
		animateStartingFrameIn: true,
		autoPlay: true,
		autoPlayDelay: 3000,
		preloader: true,
		preloadTheseFrames: [1]
    };
    
    var mySequence = $("#sequence").sequence(options).data("sequence");


	// theme thumbnail hover fx
 	$('.viewport').mouseenter( function(e) {
        $(this).children('.theme-actions').children('img').animate({ height: '300', left: '0', top: '0', width: '300'}, 100);
        $(this).children('.theme-actions').children('div').fadeIn(200);
    }).mouseleave(function(e) {
        $(this).children('.theme-actions').children('img').animate({ height: '340', left: '-20', top: '-20', width: '340'}, 100);
        $(this).children('.theme-actions').children('div').fadeOut(200);
   	});
	 

	
	// edit theme window
	var editorWin ;
	$('.dteditor').click(function (event) {
	    event.preventDefault();
	    var windowName = $(this).attr("name");
        var w = screen.width - 100 ;
        var h = screen.height - 40 ;
        var left = 50 ; 
        var top = 20 ;
        var windowArgs = 'height=' + h + ',width=' + w + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes,location=no';		    
        editorWin = window.open( $(this).attr("href"), "editorWindow", windowArgs);
        editorWin.focus() ;
	});
	

});



