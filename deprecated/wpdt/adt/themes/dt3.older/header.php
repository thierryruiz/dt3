<!DOCTYPE html>
<html lang="en">
<head>

 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
	<title>doTemplate - Free web templates builder</title>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css" type="text/css" media="screen" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />	

	<script src="<?php bloginfo('template_directory'); ?>/js/jquery-1.8.1.min.js"></script>

	<!--[if lte IE 7]>
	<script src="<?php bloginfo('template_directory'); ?>/js/ie-fix.js"></script>
	<![endif]-->

	<!--[if lt IE 9]>
	<script src="dist/html5shiv.js"></script>
	<![endif]-->
		
	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.sequence-min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>

	<?php if (get_option('dt_custom_css') ) { ?>
		<!-- CUSTOM CSS -->
		<style type="text/css">
			<?php echo stripslashes(get_option('dt_custom_css')); ?>
		</style>
	<?php } ?>
		
	<?php wp_head(); ?>

	</head>

<body>


<div  id="top">
		<div  id="logo"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png"></img></a></div>
	
		<nav>
		
			<?php
				wp_nav_menu(array(
					'theme_location' => 'dt-top-menu',
					'menu' => 'hmenu', /* Menu name as defined admin panel Appearance > Menus */
					'container_id' => 'hmenu',
					'menu_class' => 'dtmenu',
					'menu_id' => '',    
					'walker' => new DT_Menu_Walker()
				)); 
			?>
								
		</nav>
			
		<div class="clear"></div> 
</div>
	

<div id="wrapper">		
	<div class="content">
		<div  id="container">

						
		
								
			

									
				