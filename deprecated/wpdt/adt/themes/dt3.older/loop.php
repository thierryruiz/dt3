	
			
		<!-- MAIN COLUMN --> 	
		
								
			<div  id="main"    >

						
							
			
<?php if (have_posts()) : ?>
			
	<?php while (have_posts()) : the_post(); ?>
			
		<div class="post" id="post-<?php the_ID(); ?>">
			<h1>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
					<?php the_title(); ?>
				</a>
			</h1>
		
			<div class="entry">
				<?php if ( has_post_thumbnail() ) : ?>
					<?php if ( $dtPostLayout == "Image on top" ) : ?>
						<div class="wp-post-image-full">
							<?php the_post_thumbnail( 'full' ); ?>
						</div>
					<?php the_content(__('Read more &raquo;')); ?>
					<?php endif; ?>
					<?php if ( $dtPostLayout == "Image left" ) : ?>
						<div class="wp-post-image-left">
							<?php the_post_thumbnail( 'medium' ); ?>
						</div>
					<?php the_content(__('Read more &raquo;')); ?>
					<?php endif; ?>
					<?php if ( $dtPostLayout == "Image right" ) : ?>
						<div class="wp-post-image-right">
							<?php the_post_thumbnail( 'medium' ); ?>
						</div>
					<?php the_content(__('Read more &raquo;')); ?>
					<?php endif; ?>
				<?php else : ?>
					<?php the_content(__('Read more &raquo;')); ?>
				<?php endif; ?>				

				<div style="clear:both"></div>				
			</div>

	
			<div class="entry-footer">
				<span class="authormetadata">
					Posted by <?php the_author_posts_link(); ?>, on <?php the_date($dateFormat); ?>
				</span> | 
	
				<span class="postmetadata">
					<?php if (get_the_tags()){?>
						Tags: <?php the_tags('') ?>
					<?php } ?>
				</span> 
			</div>
	
			
				
<?php comments_template(); ?>
	
				
			
			
		</div>

	<?php endwhile; ?>

	
	
	<div id="navigation">
			<div class="fleft"><?php next_posts_link(__('&laquo; Older Entries')) ?></div>
			<div class="fright"> <?php previous_posts_link(__('Newer Entries &raquo;')) ?></div>
	</div>
			
<?php else : ?>
	
	<div class="post">
		<div class="entry">
			<h2><?php _e('Not Found'); ?></h2>
			<p><?php _e("Sorry, you are looking for something that isn't there."); ?></p>
		</div>
	</div>	
		
<?php endif; ?>
				
	
</div>
					
		<!-- END MAIN -->
		
			