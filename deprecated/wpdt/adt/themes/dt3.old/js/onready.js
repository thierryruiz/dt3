$(document).ready( function() {
//  #hmenu responsive script
/*  Responsive #hmenu
Flaunt.js v1.0.0
by Todd Motto: http://www.toddmotto.com
Latest version: https://github.com/toddmotto/flaunt-js

Copyright 2013 Todd Motto
Licensed under the MIT license
http://www.opensource.org/licenses/mit-license.php

Flaunt JS, stylish responsive navigations with nested click to reveal.
*/
$('#hmenu').append($('<div id="hmenu-mobile"></div>'));

$('#hmenu ul li').has('ul').prepend('<span class="nav-click"><i class="nav-arrow"></i></span>');

$('#hmenu-mobile').click( function(){
$('#hmenu>ul').toggle();
});

$('#hmenu>ul').on('click', '.nav-click', function(){

// Toggle the nested nav
$(this).siblings('ul').toggle();

// Toggle the arrow using CSS3 transforms
$(this).children('.nav-arrow').toggleClass('nav-rotate');

});

//  End #hmenu responsive script
});