

<?php

add_theme_support( 'post-thumbnails' );






/* Drop down menu Wordpress support - thanks to cssmenumaker.com */
class DT_Menu_Walker extends Walker {

  	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	function start_lvl( &$output, $depth = 0, $args = array() ) {
	    $indent = str_repeat("\t", $depth);
	    $output .= "\n$indent<ul>\n";
	}
  
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}
  
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
  
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = ''; 
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
    
    	/* Add active class */
		if(in_array('current-menu-item', $classes)) {
	  		$classes[] = 'active';
	  	unset($classes['current-menu-item']);
		}
    
		/* Check for children */
		$children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
		if (!empty($children)) {
		  $classes[] = 'has-sub';
		}
		
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
		
		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
		
		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
		
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
  
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}
  
}








function initDtLayout() {
	global	$dtLayoutClass  ;
	global	$dtLayout  ;
	global	$dtPostLayout  ;
	
	$dtLayout = get_settings( "dt_page_layout" );
	$dtPostLayout = get_settings( "dt_post_layout" );	
	$dtLayoutClass = "m" ;
	
	if ( $dtLayout == "Full width" ){ $dtLayoutClass = "m"; } 
	if ( $dtLayout == "Sidebar left" ){ $dtLayoutClass = "sm"; }
	if ( $dtLayout == "Sidebar right" ){ $dtLayoutClass = "ms"; }
	if ( $dtLayout == "Sidebars split" ){ $dtLayoutClass = "sms"; }
	if ( $dtLayout == "Two sidebars right" ){ $dtLayoutClass = "mss"; }
}


add_action( 'init', 'initDtLayout' );


if ( function_exists('register_sidebar') ) {

	register_sidebar( array( 
		'name' 			=> 'Sidebar 1',
		'id' 			=> 'sidebar-1',
		'before_widget' => '<div id="%1\$s" class="widget %2\$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' => '</h2>')
	);
	
	register_sidebar( array( 
		'name' 			=> 'Sidebar 2',
		'id' 			=> 'sidebar-2',
		'before_widget' => '<div id="%1\$s" class="widget %2\$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' => '</h2>')
	);
}


function get_sidebar2() {
	get_sidebar('2');
}




// Fist full of comments
function custom_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
                 
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
    
    	<a name="comment-<?php comment_ID() ?>"></a>
      	
      	<div class="comment-container">
      	
	      	<div class="comment-head">
	      	
	    		<?php if(get_comment_type() == "comment"){ ?>
	      		
	  	  			<div class="avatar">
	  	  				<?php the_commenter_avatar($args) ?>
	  	  			</div>
	      	
	 	     	<?php } ?>
	        	
	        	<span class="name"><?php the_commenter_link() ?></span>
	        	
	        	<?php if(get_comment_type() == "comment"){ ?>
	        	
	        		<span class="date"><?php echo get_comment_date($GLOBALS['date']) ?> <?php _e('at', ''); ?> <?php echo get_comment_time(); ?></span>
	        		<span class="edit"><?php edit_comment_link('Edit', '', ''); ?></span>
	        		<span class="perma"><a href="<?php echo get_comment_link(); ?>" title="<?php _e('Direct link to this comment', ''); ?>">#</a></span>
	        	
	        	<?php }?>
	        	
	        	<div class="fix"></div>
	          	
			</div><!-- /.comment-head -->
	      
	   		<div class="comment-entry"  id="comment-<?php comment_ID(); ?>">
			
				<?php comment_text() ?>
	            
	            <?php if ($comment->comment_approved == '0') { ?>
	            	<p class='unapproved'><?php _e('Your comment is awaiting moderation.', ''); ?></p>
	            <?php } ?>
				
				<div class="reply">
	            	<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	            </div><!-- /.reply -->
	
			</div><!-- /comment-entry -->
		
		</div><!-- /.comment-container -->
		
		<?php }
		
		 // PINGBACK / TRACKBACK OUTPUT
			
			function list_pings($comment, $args, $depth) {
      		$GLOBALS['comment'] = $comment; ?>
      	
			<li id="comment-<?php comment_ID(); ?>">
				<span class="author"><?php comment_author_link(); ?></span> - 
				<span class="date"><?php echo get_comment_date($GLOBALS['date']) ?></span>
				<span class="pingcontent"><?php comment_text() ?></span>

		<?php } 
		
		
function the_commenter_link() {
    $commenter = get_comment_author_link();
    if ( ereg( ']* class=[^>]+>', $commenter ) ) {$commenter = ereg_replace( '(]* class=[\'"]?)', '\\1url ' , $commenter );
    } else { $commenter = ereg_replace( '(<a )/', '\\1class="url "' , $commenter );}
    echo $commenter ;
}

function the_commenter_avatar($args) {
    $email = get_comment_author_email();
    $avatar = str_replace( "class='avatar", "class='photo avatar", get_avatar( "$email",  $args['avatar_size']) );
    echo $avatar;
}



// Footer sidebars  
if ( function_exists('register_sidebar') ){

	register_sidebar( array( 
		'name' 			=> 'Footer Column 1',
		'id' 			=> 'sidebar-footer1',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' => '</h2>')
	);
	

	register_sidebar( array( 
		'name' 			=> 'Footer Column 2',
		'id' 			=> 'sidebar-footer2',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' => '</h2>')
	);
	

	register_sidebar( array( 
		'name' 			=> 'Footer Column 3',
		'id' 			=> 'sidebar-footer3',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' => '</h2>')
	);
	
}	


add_action('init','register_custom_dt_menus' );


function register_custom_dt_menus(){
	register_nav_menus( array (
				'dt-top-menu' => __('Top horizontal menu' ),
  
	));
}





// theme admin settings
$dt_options = array (
array(
	"name" => "Footer copyright text",
	"desc" => "Enter your copyright text",
	"id" => "dt_footer_copyright",
	"type" => "text",
	"std" => "",

),
array(
	"name" => "Page layout",
	"desc" => "Choose theme page layout",
	"id" => "dt_page_layout",
	"type" => "select",
	"std" => "",
	"options" => array("Full width","Sidebar left","Sidebar right","Sidebars split","Two sidebars right",),
),
array(
	"name" => "Post layout",
	"desc" => "Choose theme post layout",
	"id" => "dt_post_layout",
	"type" => "select",
	"std" => "",
	"options" => array("Image on top","Image left","Image right",),
),
array(
	"name" => "Custom CSS",
	"desc" => "Add your custom CSS code to extend or override the default theme stylesheet",
	"id" => "dt_custom_css",
	"type" => "textarea",
	"std" => "",

),
);


add_action( 'admin_init', 'dt_admin_init' );
add_action( 'admin_menu', 'dt_admin_menu' );


function dt_admin_init() {
	
	// admin panel css
	wp_enqueue_style( "functions", get_bloginfo( 'template_directory' ) . "/functions.css", false, "1.0", "all" );
	
	// upload resources
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_register_script('dt-upload', get_bloginfo( 'template_directory' ) . "/js/admin.js", array('jquery', 'media-upload', 'thickbox' ) );
	wp_enqueue_script('dt-upload');
	wp_enqueue_style('thickbox');

	
}



function dt_admin_menu() {
	
	global $dt_options;

	if ( $_GET['page'] == basename( __FILE__ ) ) {

		if ( 'save' == $_REQUEST['action'] ) {
			
			foreach ( $dt_options as $value ) {
				update_option( $value[ 'id' ], $_REQUEST[ $value[ 'id' ] ] );
			}

			foreach ($dt_options as $value) {
				if( isset( $_REQUEST[ $value['id'] ] ) ) { 
					update_option( $value[ 'id' ], $_REQUEST[ $value[ 'id' ] ] ); 
				} else { 
					delete_option( $value[ 'id' ] ); 
				}
			}

			header("Location: admin.php?page=functions.php&saved=true");
			die;

		} else if( 'reset' == $_REQUEST['action'] ) {
			foreach ($dt_options as $value) {
				delete_option( $value['id'] ); 
			}

			header("Location: admin.php?page=functions.php&reset=true");
			die;
		}
	}

    add_menu_page( 'doTemplate', 'doTemplate', 'administrator',basename(__FILE__), 'display_admin_form' );

}



function display_admin_form() {
	global $dt_options;
	?>
	<div class="wrap dt_wrap">
	<h2>doTemplate Theme Settings</h2>

	<div class="dt_opts">
	
		<form  method="post" style="margin:20px;">
			
			<?php foreach( $dt_options as $value ) {
			switch ( $value[ 'type' ] ) {
			
			case "text":
			?>
				<div class="dt_input dt_text">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes(get_option( $value['id'])  ); } else { echo $value['std']; } ?>" />
					<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
				</div>
				
			<?php
			break;
			case 'textarea':
			?>
	
			<div class="dt_input dt_textarea">
				<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
			 		<textarea name="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" cols="" rows="">
			 			<?php if ( get_settings( $value['id'] ) != "") { echo stripslashes(get_settings( $value['id']) ); } else { echo $value['std']; } ?>
			 	</textarea>
			 	<small><?php echo $value['desc']; ?></small><div class="clearfix"></div> 
	 		</div>
	  
			<?php
			break;
			case 'media':
			?>
			
				<div class="dt_input dt_upload">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<span>
						<input style="visibility:hidden;width:0" id="<?php echo $value['id']; ?>" type="text" size="0" name="<?php echo $value['id']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes( get_option( $value['id'])  ); } else { echo stripslashes( bloginfo('template_directory') . '/' . $value['std'] ) ; } ?>" />
						<input id="<?php echo $value['id']; ?>_button" class="dt_upload_button" name="<?php echo $value['id']; ?>" type="button" value="Choose slide image..." style="vertical-align:top" />
						<img id="<?php echo $value['id']; ?>_thumbnail" src="<?php if ( get_option( $value['id'] ) != "") { echo stripslashes( get_option( $value['id'])  ); } else { echo stripslashes( bloginfo('template_directory') . '/' . $value['std'] ) ; } ?>" style="width:100px;margin-left:20px;" />
					</span>
					<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
		 		</div>
	  	  
			<?php
			break;		
			case 'select':
			?>
			
				<div class="dt_input dt_select">
					<label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
					<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
						<?php foreach ($value['options'] as $option) { ?>
								<option <?php if (get_option( $value['id'] ) == $option) { echo 'selected="selected"'; } ?>><?php echo $option; ?></option><?php } ?>
					</select>
					<small><?php echo $value['desc']; ?></small><div class="clearfix"></div>
				</div>
			
			<?php
			break;
					
			}
			}
			?>
			
			<input type="hidden" name="action" value="save" />  
				<div style="text-align:right">
			<input name="save" type="submit" value="Save changes" class="button-primary" />
		
		</div>	
		
		</form>
	</div>
	</div>
	<?php
	
}

