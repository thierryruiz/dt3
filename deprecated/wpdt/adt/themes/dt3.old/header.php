	
	
<!DOCTYPE html>
<html lang="en">

<head>


	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	
	<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>	
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Condensed:300|Roboto+Condensed:300|Roboto+Condensed" type="text/css" />

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />	

	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.8.1.min.js"></script>
		
	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/js/addon.js"></script>
	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/js/onready.js"></script>
	<script type="text/javascript" language="javascript" src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>
	
	<?php if (get_option('dt_custom_css') ) { ?>
		<!-- CUSTOM CSS -->
		<style type="text/css">
			<?php echo stripslashes(get_option('dt_custom_css')); ?>
		</style>
	<?php } ?>
	
	<?php wp_head(); ?>
	
</head>

<body>

						
			<header>
			

									
			

<div  id="top"    >

							
			
<div class="overlay"></div>


				
	<div class="content">
		
								
						
<div  id="logo"   >

						
		
				
	
</div>
						
								
			 

<nav>
<?php
	wp_nav_menu(array(
		'theme_location' => 'dt-top-menu',
		'menu' => 'hmenu', /* Menu name as defined admin panel Appearance > Menus */
		'container_id' => 'hmenu',
		'menu_class' => 'dl-menu',
		'menu_id' => '',    
		'walker' => new DT_Menu_Walker()
	)); 
?>
</nav>
					
		<div class="clear"></div> 
		
	</div>

</div>




												
						
<div  id="header-wrapper"   >

							
			
<div class="overlay"></div>


				
		
								
			

									
						
<div  id="header"   >

						
		
				
	
</div>
					

				
	
</div>
					

	</header>
			
						
			<div id="wrapper">

							
			
<div class="overlay"></div>


<div class="border-bottom"></div>
				
							
						
<div  id="container"   >

						
		
								
			

									
				