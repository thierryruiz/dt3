<script type="text/javascript" language="javascript">

openEditor = function( url ) {
	newTab = window.open( url, '_blank');
	newTab.focus() ; 
}


$(document).ready(function() {
    //On mouse over those thumbnail
    $('.theme-thumb').hover(function() {                  
        //Display the caption
        $(this).find('div.caption').stop(false,true).fadeIn(200);
    },
    function() { 
        //Hide the caption
        $(this).find('div.caption').stop(false,true).fadeOut(200);
    });
 
});

</script>	




<?php if (have_posts()) : ?>


	<?php echo $dotemplate_url ; ?>
	<?php $counter = 1; ?>

	<?php while (have_posts()) : the_post(); ?>
		
		<?php
		
			$thumbnail_image =  content_url() . "/dtthemes/" . get_post_meta($post->ID, "_dt_themeId", true) . "/th.jpg" ;
			
		?>
		
		<?php $last = ( ( $counter % 3 == 0 ) ? ' last' : '' ) ?>
		
		<div class="theme-thumb <?php echo $last; ?>"  id="post-<?php the_ID(); ?>">
			<a href="<?php the_permalink() ?>">
				<img class="theme-thumb-img" src="<?php echo $thumbnail_image ?>" >
			</a>
			<div class="theme-thumb-shade"></div>
			<div class="caption">
				<img style="cursor:pointer" src="<?php bloginfo('stylesheet_directory'); ?>/images/edit-btn.png" onclick="openEditor('<?php echo DOTEMPLATE_URL; ?>/dt/createTheme?id=theme<?php echo get_post_meta($post->ID, "_dt_themeId", true); ?>&exportWP=true')" />
				<br/>
				<img style="margin-top:10px;cursor:pointer" src="<?php bloginfo('stylesheet_directory'); ?>/images/more-info-btn.png"  onclick="self.location='<?php the_permalink() ?>'" />			
			</div>
		</div>
		
		<?php $counter++; //Update Counter ?>
				
	<?php endwhile; ?>
	
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } // paginate using pagenavi plugin ?>

	
<?php endif; ?>
