<?php get_header(); ?>
<div  id="page">
	<div class="layout-m">
		<div  id="main">
		 	<?php 
		 	
			 	if ( is_tag('theme') ) { 
					
						get_template_part( 'themes-grid' );
					
				} else {
					
						get_template_part( 'loop' );
					
				}
			
			?>
		</div>					
		<div class="clear" style="height:60px"></div>
	</div>
</div>
<?php get_footer(); ?>
