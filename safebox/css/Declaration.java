package com.dotemplate.theme.server.css;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;


public class Declaration implements Serializable {
	
	private static final long serialVersionUID = 6862445009823535862L;

	private String property ;
	
	private String values ; // comma separated values

	private String jsProperty ;
	
	@SuppressWarnings("unused")
	private Declaration(){} ;
	
	
	public Declaration( String property, String value ) {
		
		this.property = property.trim() ;
		addValue( value ) ;
		
		// properties like background-image to become backgroundImage 
		int i = property.indexOf( "-" ) ;
		
		if ( i > 0 ) {
					
			String[] tokens = StringUtils.split( property , "-" ) ;
			char c = Character.toUpperCase( tokens[1].charAt( 0 ) ) ;
			this.jsProperty = new StringBuffer( tokens[0] ).append( c ).append(  tokens[ 1 ].substring( 1 ) ).toString() ;
				
		} else {
			
			this.jsProperty = property ;		
		
		}

	
	}
	
	
	public String getProperty () {
		return property;
	}
	
	
	public String[] getValues () {
		return StringUtils.split(values, ";" ) ;
	}
	
	protected String getRawValue() {
		return values ;
	}

	public void addValue ( String value ) {
		value = value.trim();
		if ( values == null  ){
			values = value ;
		} else {
			this.values = new StringBuffer(values).append(';').append( value ).toString() ; 
		}
	}

	public String getJsProperty() {
		return jsProperty;
	}
	
	
	public boolean equals( Declaration d ) {
		return d.getProperty().equals( property ) & values.equals( d.getRawValue() );
	}
		
	
}
