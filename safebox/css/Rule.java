package com.dotemplate.theme.server.css;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;



public class Rule implements Serializable {

	private static final long serialVersionUID = 7608165258780285967L;
	
	protected LinkedHashMap< String, Declaration> declarations ;
	
	protected String selector ;	
	
	@SuppressWarnings("unused")
	private Rule() {} ;
	
	public Rule( String selector, LinkedHashMap< String, Declaration> declarations  ) {
		this.selector = selector ;
		this.declarations = declarations ;
	}
	
	public Rule( String selector ) {
		this.selector = selector ;
		this.declarations = new LinkedHashMap< String, Declaration> () ;
	}

	/*
	public void setSelector(String selector) {
		this.selector = selector;
	}*/
	
	public String getSelector() {
		return selector;
	}
	
	
	public void add ( Declaration d ){
		
		String property = d.getProperty() ;
		
		Declaration existing = getDeclaration( property ) ;
		
		if ( existing != null ){
			existing.addValue( d.getRawValue() ) ;
		} else {
			declarations.put( d.getProperty(), d ) ;
		}
		
	}
	
	
	public void add( Collection< Declaration > ds ) {
		
		for ( Declaration d : ds ){
			add( d ) ;
		}
		
	}
	
	
	public Collection< Declaration > getDeclarations() {
		return declarations.values() ;
	}
	

	/*
	public HashMap< String, Declaration> getDeclarations() {
		return declarations;
	}

	public void setDeclarations( HashMap<String, Declaration> declarations ) {
		this.declarations = declarations;
	}*/
	

	
	
	public Declaration getDeclaration( String name ){
		return declarations.get( name ) ;
	}

	public boolean isEmpty() {
		return declarations.isEmpty() ;
	}

	public void replace( Declaration newDeclaration ) {
		declarations.put( newDeclaration.getProperty() , newDeclaration ) ;
	}
	
}
