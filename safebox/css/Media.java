package com.dotemplate.theme.server.css;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashMap;


public class Media implements Serializable {

	private static final long serialVersionUID = 7608165258780285967L;
	
	protected LinkedHashMap< String, Rule> rules ;
	
	protected String selector ;	

	
	public Media() {
		rules = new LinkedHashMap< String, Rule>() ;
	}
	

	public String getSelector() {
		return selector;
	}
	
	public void setSelector(String selector) {
		this.selector = selector;
	}
	
	public void add ( Rule rule ){
		
		Rule existingRule = rules.get( rule.getSelector() ) ; 
		
		if ( existingRule == null ){
			rules.put( rule.getSelector() , rule ) ;
			return ;
		}
		
		existingRule.add ( rule.getDeclarations() ) ;
		
		//rules.put( rule.getSelector(), rule ) ;
	}
	
	public LinkedHashMap< String, Rule> getRules() {
		return rules;
	}

	public void setRules( LinkedHashMap<String, Rule> rules) {
		this.rules = rules;
	}
	
	public void append( Collection< Rule > rs ) {
		
		for ( Rule r : rs ){
			add ( r ) ;
			//rules.put( r.getSelector(), r ) ;
		}
		
	}

	public boolean isEmpty() {
		return rules.isEmpty() ;
	}
	
}
