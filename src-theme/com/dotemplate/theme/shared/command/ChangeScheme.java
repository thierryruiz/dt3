package com.dotemplate.theme.shared.command;


public class ChangeScheme extends ThemeCommand< ChangeSchemeResponse > {
	
	private static final long serialVersionUID = -8469662875224275483L;

	private String schemeName ;

	
	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	
	
}
