package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.theme.shared.Theme;


public class GetThemeResponse implements Response {

	private static final long serialVersionUID = 863633796977605456L;
	
	private Theme theme ;
	
	public Theme getTheme() {
		return theme;
	}
	
	public void setTheme(Theme theme) {
		this.theme = theme;
	}
	
}
