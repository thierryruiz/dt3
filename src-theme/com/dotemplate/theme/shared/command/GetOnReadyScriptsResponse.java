package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.frwk.Response;


public class GetOnReadyScriptsResponse implements Response {

	private static final long serialVersionUID = 1L;

	private String js ;

	public String getJs() {
		return js;
	}

	public void setJs(String js) {
		this.js = js;
	}
	
	
	
	
}
