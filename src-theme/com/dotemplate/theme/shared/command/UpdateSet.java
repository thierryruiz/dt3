package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.properties.PropertySet;


public class UpdateSet extends ThemeCommand< UpdateSetResponse > {

	private static final long serialVersionUID = -7004502255867307196L;

	private PropertySet themePropertySet ;

	public PropertySet getThemePropertySet () {
		return themePropertySet;
	}

	public void setThemePropertySet ( PropertySet themePropertySet ) {
		this.themePropertySet = themePropertySet;
	}

	
}
