package com.dotemplate.theme.shared.command;

import com.dotemplate.core.shared.frwk.Response;


public class ChangeSchemeResponse implements Response {

	private static final long serialVersionUID = 5918315063431829117L;
	
	private String stylesheet ;
	
	public String getStylesheet() {
		return stylesheet;
	}
	
	public void setStylesheet(String stylesheet) {
		this.stylesheet = stylesheet;
	} 
	
	/*
	private ThemeUpdate clientRefresh ;
	
	public ThemeUpdate getClientRefresh () {
		return clientRefresh;
	}
	
	public void setClientRefresh ( ThemeUpdate clientRefresh ) {
		this.clientRefresh = clientRefresh;
	}*/
	
	
	
}
