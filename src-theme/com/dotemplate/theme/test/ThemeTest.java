package com.dotemplate.theme.test;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;


public abstract class ThemeTest implements Callable< Void >{
	
	protected FirefoxDriver driver ;
	
	protected String host  ;
	
	protected String themeId ;
		
	private static Random random = new Random ();

	
	public ThemeTest( String host, String themeId ) {
		this.host = host ;
		this.themeId = themeId ;

	}
	
	
	public void NEW_THEME() {
		driver = new FirefoxDriver() ;
		driver.manage().timeouts().implicitlyWait( 20, TimeUnit.SECONDS ) ;		
		driver.get("http://" + host + "/dt/createTheme?id=" + themeId ) ;
	}
	
	
	public void SELECT_TAB ( String name ){
		driver.findElement( By.xpath( 
				"//li[contains(@id,'grp_" + name + "')]"
		)).click();
		
		waitLoading( 3 ) ;
	}
	

	public void CHANGE_SYMBOL ( String control, String symbol ){
		
		driver.findElement( By.xpath(
			"//table[@id='" + control + "_ctrl']/tbody/tr[2]/td[2]/em/button" 
		)).click();
	
		waitLoading( 7 ) ;
		
		driver.findElement( 
				By.cssSelector("#" + symbol + " > img"  
		)).click();
		
		waitLoading( 10 ) ;
		
	}
	
	
	public void close() {
		driver.quit();
	}
	
	
	public void waitLoading( long s  ){
		try {
			Thread.sleep( 1000 * s ) ;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public Void call() throws Exception {
		
		try {
			Thread.sleep( 1000 * random.nextInt( 10 ) ) ;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			run() ;
		} finally {
			close() ;
		}
		
		return null;
	}
	
	
	protected abstract void run() ;

	
}
