package com.dotemplate.theme.server;

import com.dotemplate.core.server.DesignPreprocessor;
import com.dotemplate.theme.shared.Theme;



public class ThemePreprocessor extends DesignPreprocessor< Theme > {

	private static final long serialVersionUID = -8214136625441565587L;

	
	
	public ThemePreprocessor( ThemeContext context ) {
		super( context );
	}
	

	
	@Override
	public void execute() {
		
		super.execute();
		
		ThemeUtils.buildDependencies( ( ThemeContext ) designContext ) ;
	
	}
		

}
