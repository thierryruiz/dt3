package com.dotemplate.theme.server;

import java.awt.image.BufferedImage;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.CachedImage;



public class CssImage extends CachedImage {

	private static final long serialVersionUID = 7408902902057473962L;

	private static String[] _charsToReplace = new String[] {".", "#" , " " } ;
	
	private static String[] _charsReplacement = new String[] {"", "" ,"-" } ;
	
	public final static int CSS = 1 ;
	
	
	public CssImage( String id, BufferedImage image, Format format ) {
		super( clean( id ) +  ( ( Format.PNG == format ) ? ".png" : ".jpg" ) , image, format ) ;
	}
	
	
	
	@Override
	public int getType() {
		return CSS ;
	}
	
	
	/* Simplifies image name associated with selector */
	public static String clean ( String name ){
				
		name = StringUtils.replaceEachRepeatedly( name, _charsToReplace, _charsReplacement ) ;		
		
		return name ;
		
	}
	

}
