package com.dotemplate.theme.server.cms.blogger;

import java.io.File;
import java.io.FilenameFilter;


import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.cms.AbstractThemeArchiver;

public class BloggerArchiver extends AbstractThemeArchiver {

	private final static Log log = LogFactory.getLog ( BloggerArchiver.class );
	
	
	protected void beforeArchive () {

		super.beforeArchive ();

		// add blogger-README.html to archive
		try {
			FileUtils.copyFile ( 
					new Path ( ThemeApp.realPath ( "WEB-INF" ) ).append ("res") .append( "blogger-README_FIRST.html" ).asFile () , 
					new Path ( ThemeApp.get().getExportDirectoryRealPath () ).append( "README_FIRST.html" ).asFile () ) ;
			
		} catch ( Exception e ) {
			
			log.warn ( "Failed to copy README file", e ) ;
		}
	}
	
	
	
	protected FilenameFilter getArchiveFileFilter () {
		return new FilenameFilter () {
			public boolean accept ( File dir, String fileName ) {
	        	if ( 

	        		fileName.endsWith ( ".svg" ) || 
	        		fileName.equals ( "upload" ) ||
	        		fileName.equals ( "site.xml" ) || 
	        		fileName.equals ( "theme.xml" ) ) {
	        		
	        		return false ;
	        	
	        	}
	        	
	        	return true ;
	        	
			}
		};	
	}
	
	
}
