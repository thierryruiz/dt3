package com.dotemplate.theme.server.cms.blogger;

import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.core.server.util.ConcurrentTaskExecutor;
import com.dotemplate.core.server.util.ConcurrentTaskPool;

public class PicasaUploadExecutor extends ConcurrentTaskExecutor<PicasaUpload> {

	public PicasaUploadExecutor () {
		super( 50 ) ;
	}
	
	
	@Override
	protected ConcurrentTaskPool<PicasaUpload> createPool () {
		return new ConcurrentTaskPool< PicasaUpload >( ) {
			protected ConcurrentTask<PicasaUpload> createTask () throws Exception {
				return new PicasaUploadTask() ;
			}
		} ;
	}

}
