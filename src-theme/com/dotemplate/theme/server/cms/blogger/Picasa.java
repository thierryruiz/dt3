package com.dotemplate.theme.server.cms.blogger;


import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.ConcurrentTaskContext;
import com.dotemplate.theme.server.ThemeApp;
import com.google.gdata.client.photos.PicasawebService;
import com.google.gdata.data.Link;
import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.photos.AlbumEntry;
import com.google.gdata.data.photos.AlbumFeed;
import com.google.gdata.data.photos.UserFeed;
import com.google.gdata.util.AuthenticationException;



/* 
 * 
 * This class is only used in dev mode to deploy Blogger css 
 * images in Picasa to test the template  generated.
 *
 */
class Picasa {

	private static Log log = LogFactory.getLog( Picasa.class ) ;
	
	private PicasawebService picasa ;
	
	private AlbumEntry currentAlbum ;
	
	public Picasa() {
		connect() ;
	}
	
	private synchronized void connect() {
		
		log.info ( "Initializing Picasa connection..." ) ;
		
		picasa = new PicasawebService( "dotemplate" );
		
		try {
			picasa.setUserCredentials( "dotemplate@gmail.com", "saysayboomak" );
		} catch ( AuthenticationException e ) {
			throw new AppRuntimeException( "Failed to login in Picasa", e ) ;
		}
	}
	
	
	synchronized String uploadImages( Iterator<File> images, String bloggerCode ) {
		
		try {
			// check connection
			picasa.getFeed( new URL( "http://picasaweb.google.com/data/feed/api/user/dotemplate?kind=album" ), 
									UserFeed.class );
		} catch ( Exception e ) {
			log.warn ( "Failed to connect Picasa > renew connection ", e ) ;
			connect() ;
		}
		
	
		List< ConcurrentTaskContext > uploads = new ArrayList< ConcurrentTaskContext >() ;
		
		PicasaUpload upload ;
		
		URL uploadAlbum = null ;
		
		try {
		
			uploadAlbum = getCurrentAlbumUrl () ;
			
		} catch ( Exception e ) {
			
			throw new AppRuntimeException( "Failed to upload images in Picasa", e ) ;
		
		}
		
		while ( images.hasNext () ){
			upload = new PicasaUpload() ;
			upload.setPicasaService ( picasa ) ;
			upload.setImage ( images.next () ) ;
			upload.setUploadAlbum ( uploadAlbum ) ;
			
			uploads.add ( upload ) ;
		}
		
		PicasaUploadExecutor uploader = ( PicasaUploadExecutor ) ThemeApp.getSingleton ( PicasaUploadExecutor.class ) ;
		
		
		List<Future< PicasaUpload > > uploadResults ;
		
		try {
			
			uploadResults = uploader.execute ( uploads ) ;
			
			for ( Future< PicasaUpload > result : uploadResults ){
				
				upload = result.get () ;
				bloggerCode = StringUtils.replace ( bloggerCode, 
										"images/css/" + upload.getImage ().getName (), upload.getUploadUrl () ) ;
				
			}
			
			
		} catch ( Exception e ) {

			throw new AppRuntimeException( "Failed to upload template images to Picasa", e ) ;
		}
		
		return bloggerCode ;

		
		
		/*
		try {
			log.info ( "Uploading '" + image.getName () + "' to Picasa..." ) ;
			MediaFileSource myMedia = new MediaFileSource( image, "image/jpeg" );
			PhotoEntry returnedPhoto = picasa.insert( getCurrentAlbumUrl(), PhotoEntry.class, myMedia );
			return returnedPhoto.getMediaContents ().get ( 0 ).getUrl () ;
			
		} catch ( Exception e ) {
			throw new EaseRuntimeException( "Failed to upload image '" + image.getName () + "' in Picasa", e ) ;
		}*/
		
		
		
	}
	

	private URL getCurrentAlbumUrl() throws Exception {
		
		if ( currentAlbum == null ){
			URL dotemplateFeedUrl = new URL("http://picasaweb.google.com/data/feed/api/user/dotemplate?kind=album");
			UserFeed dotemplateUserFeed = picasa.getFeed( dotemplateFeedUrl, UserFeed.class );
			currentAlbum = dotemplateUserFeed.getAlbumEntries().get ( 0 ) ;
		}
		
		
		// load the album ;
		AlbumFeed currentAlbumfeed = picasa.getFeed(
			new URL( currentAlbum.getLink( com.google.gdata.data.Link.Rel.FEED, null  ).getHref () ), AlbumFeed.class );

		log.info ( "Photos left in current albums: "+  currentAlbumfeed.getPhotosLeft () ) ;
		
		
		if ( currentAlbumfeed.getPhotosLeft () < 10 ){
			
			// photo quota reached > create a new album ; 
			
			AlbumEntry newAlbum = new AlbumEntry();
			String name = ""+System.currentTimeMillis () ;
			newAlbum.setTitle(new PlainTextConstruct( name ) );
			newAlbum.setDescription(new PlainTextConstruct( "doTemplate album"));	
			newAlbum.setName ( name ) ;
			URL feedUrl = new URL ("http://picasaweb.google.com/data/feed/api/user/dotemplate" ) ;
			currentAlbum = picasa.insert( feedUrl , newAlbum ) ;
		
		}
		
		
		Link albumFeedLink =
			currentAlbum.getLink(com.google.gdata.data.Link.Rel.FEED, null);
		
			return  new URL( albumFeedLink.getHref() );

		
		/*
		return new URL( "http://picasaweb.google.com/data/feed/api/user/dotemplate/albumid/" 
			+ currentAlbum.getId ()) ;
		*/
	}
	
	
	
	
	public static void main ( String[] args ) {
		
		PicasawebService myPicasa = new PicasawebService( "dotemplate" );

		try {
			myPicasa.setUserCredentials( "dotemplate@gmail.com", "saysayboomak" );
		} catch ( AuthenticationException e ) {
			throw new AppRuntimeException( "Failed to login in Picasa", e ) ;
		}
		
		
		AlbumEntry myAlbum = new AlbumEntry();
		String albumid = "" + System.currentTimeMillis () ;
		myAlbum.setTitle(new PlainTextConstruct( albumid ) );
		myAlbum.setDescription(new PlainTextConstruct("doTemplate album " + albumid));
		

		try {
			URL feedUrl = new URL ("http://picasaweb.google.com/data/feed/api/user/dotemplate" ) ;
			AlbumEntry insertedEntry = myPicasa.insert( feedUrl , myAlbum );
			System.out.println ( "Album " + albumid + " created. Left : " +  insertedEntry.getPhotosLeft () ) ;
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		
		
		try {
			URL feedUrl = new URL("http://picasaweb.google.com/data/feed/api/user/dotemplate?kind=album");

			UserFeed myUserFeed = myPicasa.getFeed(feedUrl, UserFeed.class);

			for (AlbumEntry album : myUserFeed.getAlbumEntries()) {
			    System.out.println(album.getTitle ().getPlainText ());
			}
			
		} catch ( Exception e ) {
			e.printStackTrace();
		}		
	}
	
	
	
}
