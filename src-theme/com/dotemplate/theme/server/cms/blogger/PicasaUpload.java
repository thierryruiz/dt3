package com.dotemplate.theme.server.cms.blogger;

import java.io.File;
import java.net.URL;

import com.dotemplate.core.server.util.ConcurrentTaskContext;
import com.google.gdata.client.photos.PicasawebService;


public class PicasaUpload implements ConcurrentTaskContext {

	private PicasawebService picasaService ;
	
	private File image ;
	
	private URL uploadAlbum ;
	
	private String uploadUrl ;
	
	public boolean isValid () {
		return true ;
	}

	public File getImage () {
		return image;
	}

	public void setImage ( File image ) {
		this.image = image;
	}
	
	public PicasawebService getPicasaService () {
		return picasaService;
	}

	public void setPicasaService ( PicasawebService picasaService ) {
		this.picasaService = picasaService;
	}
	
	public URL getUploadAlbum () {
		return uploadAlbum;
	}
	
	public void setUploadAlbum ( URL uploadAlbum ) {
		this.uploadAlbum = uploadAlbum ;
	}

	public String getUploadUrl () {
		return uploadUrl;
	}

	public void setUploadUrl ( String uploadUrl ) {
		this.uploadUrl = uploadUrl;
	}
	
	
}
