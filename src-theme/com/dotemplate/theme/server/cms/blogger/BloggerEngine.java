package com.dotemplate.theme.server.cms.blogger;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.xhtml.XHTMLEngine;


public class BloggerEngine extends XHTMLEngine {
	
	private static Log log = LogFactory.getLog( BloggerEngine.class ) ;
		
	public BloggerEngine () {
		super() ;
	}
	
	
	@Override
	public void export () {		
		
		if ( log.isDebugEnabled() ){
			log.debug( "Generating Blogger template for download... " ) ;
		}
	
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
	
		CMS.set ( themeContext, CMS.TYPE_BLOGGER ) ;
		
		exportStylesheet ();
		
		
		Writer writer = new StringWriter() ;
		
		try {
			
			
			engine.mergeTemplate( themeContext.getDesign().getTemplate() + "/blogger/blogger.xml.vm", "UTF-8" , themeContext, writer ) ;
			
			String bloggerCode = ( ( StringWriter ) writer ).getBuffer ().toString () ;
			
			if ( ! ThemeApp.getConfig ().isDevMode () ){
				bloggerCode = uploadImagesToPicasa( bloggerCode, writer ) ;
			}
			
			File bloggerXml = new Path ( ThemeApp.get().getExportDirectoryRealPath () ).append ( "blogger.xml" ) .asFile () ;
			
			FileUtils.writeStringToFile ( bloggerXml  , bloggerCode ) ;
			
		} catch ( Exception e ){

			log.error( e ) ;
			throw new AppRuntimeException ( "Failed to export to blogger.", e ) ;
		
		} finally {
			try {
				writer.flush () ;
				writer.close () ;
			} catch ( Exception e ){} 
		}
		
	}
	
	

	public String uploadImagesToPicasa ( String bloggerCode, Writer writer ){
		
		// list all images
		String[] extensions = { "jpg", "png" } ;
		
		// for each image in css folder replace the css local url by its absolute url ( obtained
		// after Picasa upload in download mode and modify the code accordingly
		
		
		Path cssImagesPath = new Path ( ThemeApp.get().getWorkRealPath () ).append ( "images" ).append( "css" ) ;

		@SuppressWarnings("unchecked")
		Iterator<File> cssImages = FileUtils.iterateFiles ( cssImagesPath.asFile() , extensions, false ) ;

		Picasa picasa = ( Picasa ) ThemeApp.getSingleton ( Picasa.class ) ;
		
		
		return picasa.uploadImages ( cssImages, bloggerCode );
		
		
		/*
		while ( cssImages.hasNext () ){
			image = cssImages.next () ;
			url = picasa.uploadImage ( image ) ;
			bloggerCode = StringUtils.replace ( bloggerCode, "images/css/" + image.getName (), url ) ;
		}*/
		
	}
	
	
	
}




