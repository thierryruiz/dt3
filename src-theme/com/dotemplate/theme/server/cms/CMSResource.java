package com.dotemplate.theme.server.cms;

import com.dotemplate.core.server.util.Path;


public abstract class CMSResource {

	private String name ;
	
	private Path path ;
	
	private StringBuffer content ;
	
	private boolean dynamic = false ;
	
	
	protected CMSResource( String name, Path path, boolean dynamic ){
		this.name = name ;
		this.path = path ;
		this.dynamic = dynamic ;
	}
	
	
	public String getName() {
		return name ;
	}
	
	public Path getPath() {
		return path ;
	}
	
	
	public void setDynamic(boolean dynamic) {
		this.dynamic = dynamic;
	}
	
	public boolean isDynamic() {
		return dynamic;
	}
	

	
	public void setContent( String s ) {
		if ( content == null ){
			content =  new StringBuffer() ;
		}
		content.append( s ) ;
	}

	
	
	public void appendContent( String s ) {
		if ( content == null ){
			content =  new StringBuffer() ;
		}
		content.append( "\n\n" ).append( s ) ;
	}
	
	
	public StringBuffer getContent() {
		return content;
	}


	
}
