package com.dotemplate.theme.server.cms;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.theme.server.ThemeApp;





/**
 * A class creating the download zip file for a given target CMS
 * 
 */
public abstract class AbstractThemeArchiver implements ThemeArchiver {
	
	
	private final static Log log = LogFactory.getLog ( AbstractThemeArchiver.class );
	
	
	public void createArchive() throws Exception {
		
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Creating theme download archive..." ) ;
		}
		
		beforeArchive() ;

		String dir = ThemeApp.get().getExportDirectoryRealPath () ;
		
		// create download/doTemplate-<siteUid>.zip
		String zip = new StringBuffer ( "download" )
			.append ( File.separator )
			.append( "doTemplate-" )
			.append( ThemeApp.getDesignUid () )
			.append ( ".zip" )
			.toString() ;
				
		ZipOutputStream zos = null ;
		
	
        // generate the zip file
        try {
        	
    		new File ( ThemeApp.realPath ( zip ) ).deleteOnExit () ;

        	
            zos = new ZipOutputStream( 
            		new FileOutputStream( ThemeApp.realPath ( zip ) ) ) ;
            
            
            zipFolder( dir, zos, getArchiveFileFilter () ) ;
            
            
        } finally {
        	try {
        		zos.close() ;
        	} catch ( Exception e ){
        		log.error( "Failed to close zip output stream",  e ) ;
        	}
        }
        
        if( log.isInfoEnabled() ){
            log.info( zip + "  created" ) ;
        }
        

	}
	
	
	
    private void zipFolder( String folder, ZipOutputStream zos, FilenameFilter filter ) throws Exception {
        
		if ( log.isDebugEnabled () ){
			log.debug ( "Zipping folder " + folder + "..." ) ;
		}
    	        
        String root = ThemeApp.get().getExportDirectoryRealPath () ;
        
        File zipDir = new File( folder );
        String[] dirList = zipDir.list( filter );
        byte[] readBuffer = new byte[ 2156 ];
        int bytesIn = 0;

        String filePath ;
        
        FileInputStream fis = null ;
        File file ;
        
        ZipEntry zipEntry ;
        
        for ( int i = 0; i < dirList.length; i++ ) {
            
        	file = new File( zipDir, dirList[ i ] );
            
        	
        	/* fileName = file.getName () ;
        	
            
        	if ( 
        		fileName.endsWith ( ".svg" ) || 
        		fileName.equals ( "upload" ) ||
        		fileName.equals ( "site.xml" ) || 
        		fileName.equals ( "theme.xml" ) ) {
        		
        		continue ;
        	}*/
        	        	
            
        	
            if ( file.isDirectory() ) {
                filePath = file.getPath();
                zipFolder( filePath, zos, filter );
                
                // loop again 
                continue;
            
            }

            try {
	            fis = new FileInputStream( file );

	            zipEntry = new ZipEntry( file.getCanonicalPath().substring( root.length() + 1 ) ) ;
	            
	            if ( log.isDebugEnabled () ){
	            	log.debug ( "Zipping " + zipEntry.getName () + "..." ) ;
	            }
	            
	            zos.putNextEntry( zipEntry ) ;
	            
	            while ( ( bytesIn = fis.read( readBuffer ) ) != -1 ) {
	                zos.write( readBuffer, 0, bytesIn );
	            }
            
            } finally {
            	try {
            		fis.close() ;
            	} catch ( Exception e ){
            		log.error( "Failed to close file to zip input stream",  e ) ;
            	}
            }
        }
    }
    
    
	protected void beforeArchive() {
	}
    
	protected abstract FilenameFilter getArchiveFileFilter() ;
	
	
}
