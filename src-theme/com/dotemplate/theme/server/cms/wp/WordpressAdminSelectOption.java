package com.dotemplate.theme.server.cms.wp;

import java.util.ArrayList;

public class WordpressAdminSelectOption extends WordpressAdminOption {

	
	private ArrayList<String> options = new ArrayList<String>() ;
	
	public void setOptions(ArrayList<String> options) {
		this.options = options;
	}
	
	public ArrayList<String> getOptions() {
		return options;
	}
	

	@Override
	public String getType() {
		return "select" ;
	}
	
	
	public StringBuffer asPHP(){
		
		StringBuffer sb = super.asPHP()
			.append( "\t\"options\" => array(" ) ; 
		
		for ( String option : options ){
			sb.append( "\"" ).append( option ) .append( "\"," ) ;
		}
		
		sb.append( ")," ) ;
			
		return sb ;
	}

	
}
