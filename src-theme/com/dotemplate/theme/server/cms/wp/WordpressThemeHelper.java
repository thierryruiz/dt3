package com.dotemplate.theme.server.cms.wp;

import com.dotemplate.theme.server.ThemeApp;



public class WordpressThemeHelper {
		
	
	public WordpressResources getExportResources() {
		return ( ( WordpressThemeContext ) ThemeApp.getThemeContext () ).getExportResources() ;
	}
	

	public WordpressAdminOptions getAdminOptions() {
		return ( ( WordpressThemeContext ) ThemeApp.getThemeContext () ).getAdminOptions()  ;
	}
	
	public WordpressCustomMenus getCustomMenus() {
		return ( ( WordpressThemeContext ) ThemeApp.getThemeContext () ).getCustomMenus()  ;
	}
	
	
	
}
