package com.dotemplate.theme.server.cms.wp;

public class WordpressAdminTextOption extends WordpressAdminOption {

	private String regexp ;
	
	public String getRegexp() {
		return regexp;
	}
	
	public void setRegexp(String regexp) {
		this.regexp = regexp;
	}
	
	@Override
	public String getType() {
		return "text" ;
	}
	

	
}
