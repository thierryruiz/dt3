package com.dotemplate.theme.server.cms.wp;

import java.io.File;
import java.io.FilenameFilter;

import com.dotemplate.theme.server.cms.AbstractThemeArchiver;

public class WordpressArchiver extends AbstractThemeArchiver {

	@Override
	protected FilenameFilter getArchiveFileFilter () {
		return new FilenameFilter () {
			public boolean accept ( File dir, String fileName ) {
	        	if ( 

	        		fileName.endsWith ( ".svg" ) || 
	        		fileName.equals ( "upload" ) ||
	        		fileName.equals ( "site.xml" ) || 
	        		fileName.equals ( "theme.xml" ) ) {
	        		
	        		return false ;
	        	
	        	}
	        	
	        	return true ;
	        	
			}
		};	
	}

	
}
