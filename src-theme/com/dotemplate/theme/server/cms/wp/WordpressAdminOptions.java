package com.dotemplate.theme.server.cms.wp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;


public class WordpressAdminOptions {

	private HashMap< String, ArrayList< WordpressAdminOption > > categories ;
	
	
	public void addText( 
			String category, 
			String name,
			String desc,
			String id,
			String regexp,
			String value ){
	
		
		WordpressAdminTextOption option = new WordpressAdminTextOption() ;

		option.setName( name ) ;
		option.setDesc( desc ) ;
		option.setId( id ) ;
		option.setRegexp( regexp ) ;
		option.setValue( value ) ;
		add( category, option ) ;
		
	}
	

	public void addTextarea( 
			String category, 
			String name, 
			String desc,
			String id,
			String value ){
	
		
		WordpressAdminTextareaOption option = new WordpressAdminTextareaOption() ;

		option.setName( name ) ;
		option.setDesc( desc ) ;
		option.setId( id ) ;
		option.setValue( value ) ;
		
		add( category, option ) ;
		
	}

	
	public void addSelect( 
			String category, 
			String name,
			String desc,
			String id,
			ArrayList<String> options,
			String value ){
	
		
		WordpressAdminSelectOption option = new WordpressAdminSelectOption() ;

		option.setName( name ) ;
		option.setDesc( desc ) ;
		option.setId( id ) ;
		option.setValue( value ) ;
		option.setOptions( options ) ;
		add( category, option ) ;
		
	}
	
	
	
	public void addMedia( 
			String category, 
			String name, 
			String desc, 
			String id,
			String value){
		
		WordpressAdminMediaOption option = new WordpressAdminMediaOption() ;

		option.setName( name ) ;
		option.setDesc( desc ) ;
		option.setId( id ) ;
		option.setValue( value ) ;
		
		add( category, option ) ;
		
	}
	
	

	private void add( String category, WordpressAdminOption option ){
		
		if ( categories == null ){
			categories = new HashMap< String, ArrayList< WordpressAdminOption > >() ;
		}
		
		ArrayList< WordpressAdminOption > options = categories.get( category ) ;
		
		if ( options == null ){
			options = new ArrayList< WordpressAdminOption >() ;
			categories.put( category , options ) ;
		}
		
		options.add( option ) ;
		
	}
	
	
	public Collection < WordpressAdminOption > list( String category ){
		return categories.get( category ) ;
		
	}
	
	
	
}
