package com.dotemplate.theme.server.cms.wp;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.ExportThemeContext;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.cms.CMS;
import com.dotemplate.theme.server.cms.xhtml.XHTMLEngine;

public class WordpressEngine extends XHTMLEngine {

	private static Log log = LogFactory.getLog ( WordpressEngine.class );

	private final static File SCREENSHOT  = new File ( ThemeApp.realPath ( "WEB-INF/res/wpscreenshot.png" ) );
	
	private final static String DEV_MODE_WP_DIR = "C:\\Programs\\wamp\\www\\wptest" ;

	
	@Override
	public void export () {

		if ( log.isDebugEnabled () ) {
			
			log.debug ( "Generating Worpress template for download... " );
		
		}

		ThemeContext themeContext = ThemeApp.getThemeContext ();

		CMS.set ( themeContext, CMS.TYPE_WP );

		clearExportFolder() ;
		exportCSSImages() ;
		exportStylesheet ();
		exportJavascript() ;
		
		Writer writer = new StringWriter ();

		try {

			engine.mergeTemplate ( 
					themeContext.getDesign ().getTemplate () + "/wp/index.php.vm", "UTF-8",
					themeContext, writer 
			);

		} catch ( Exception e ) {

			throw new AppRuntimeException ( "Failed to export Wordpress code.", e );

		} finally {
			
			try {
				writer.flush ();
				writer.close ();
			} catch ( Exception e ) {}
		
		}

		String phpCode = ( ( StringWriter ) writer ).getBuffer ().toString ();

		String exportDir = ThemeApp.get().getExportDirectoryRealPath () ;
		
		phpCode = splitWordpressBloc ( phpCode, "header", 		"get_header()", 					exportDir );
		phpCode = splitWordpressBloc ( phpCode, "comments", 	"comments_template()", 				exportDir );
		phpCode = splitWordpressBloc ( phpCode, "loop", 		"get_template_part( 'loop' )" , 	exportDir );	
		phpCode = splitWordpressBloc ( phpCode, "sidebar", 		null, 								exportDir ); // no get_ method replacement for sidebars
		phpCode = splitWordpressBloc ( phpCode, "sidebar-2", 	null, 								exportDir );		
		phpCode = splitWordpressBloc ( phpCode, "footer", 		"get_footer()", 					exportDir );


		
		File index_php = new Path ( ThemeApp.get().getExportDirectoryRealPath () ).append ( "index.php" ).asFile () ;
		
		try {
			
			FileUtils.writeStringToFile ( index_php, phpCode );
				
		} catch ( IOException e ) {
			
			throw new AppRuntimeException ( "Failed to generate the WP template ", e );
		
		}

		
		( ( ExportThemeContext ) themeContext ).exportCMSResources ();

		
		addScreenshot() ;
		
		
		if ( ThemeApp.isDevMode () ) {

			copyToWordpress() ;	
		}
		
		
	}
	
	
	
	public void copyToWordpress() {
		
		// In dev mode copy to local wp for test to DEV_MODE_WP_DIR 
		// http://127.0.0.1/wptest  
		// http://127.0.0.1/wptest/wp-login.php : wptest/wptest
		
		
		
		File wpThemeDir = new File ( DEV_MODE_WP_DIR + "\\wp-content\\themes\\dotemplate" );

		System.out.println( "Copy files to wp wptest..."  + wpThemeDir.getAbsolutePath() ) ;
		
		try {
			
			if ( wpThemeDir.exists() ){
				
				FileUtils.cleanDirectory( wpThemeDir ) ;
			
			}
			
			FileUtils.copyDirectory ( new File ( ThemeApp.get().getExportDirectoryRealPath () ), wpThemeDir , true );
			
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Faile to copy Wordpress theme to local Worpress runtime at " 
					+ DEV_MODE_WP_DIR, e ) ;
		
		}

		
	}


	private void addScreenshot()  {
		
		// TODO Make a Wordpress website capture instead 
		// currently only copy screenshot.png
		
		File screenshop_png = new Path ( ThemeApp.get().getExportDirectoryRealPath () ).append (
			"screenshot.png" ).asFile ();
		
		try {
			
			FileUtils.copyFile ( SCREENSHOT, screenshop_png );
			
		} catch (IOException e) {
			
			throw new AppRuntimeException ( "Failed to create theme capture image", e );
		}
		
	}


	private String splitWordpressBloc ( String phpCode, String blocid, String getMethod, String exportDir ) {

		// splits the code as a wordpress bloc from wp markers
		
		if ( log.isDebugEnabled() ){
			log.debug ( "Spliting bloc " + blocid + "..." );
		}
		
		
		String beginMark = "<!-- wp-" + blocid + "-begin -->";
		String endMark = "<!-- wp-" + blocid + "-end -->";

		String bloc = StringUtils.substringBetween ( phpCode, beginMark, endMark );
		
		if ( bloc == null || bloc.length() == 0 ) return phpCode ;
		
		if ( log.isDebugEnabled () ) {
			
			log.debug ( blocid + "\n\n" + bloc );
		
		}

		try {
			
			// export bloc code
			FileUtils.writeStringToFile ( new File ( exportDir + File.separator + blocid + ".php" ), bloc );

		} catch ( IOException e ) {
			throw new AppRuntimeException ( "Failed to generate " + blocid
									+ ".php of wordpress template", e );
		}

		// replace splited bloc with wp get method
		StringBuffer result = new StringBuffer() ;
		
		if ( getMethod != null ){
			result
				.append( StringUtils.substringBefore ( phpCode, beginMark ) )
				.append ( "\n<?php " )
				.append ( getMethod )
				.append ( "; ?>\n" )
				.append ( StringUtils.substringAfter ( phpCode, endMark ) ) ;
			
			
			
		} else {
			result
				.append( StringUtils.substringBefore ( phpCode, beginMark ) )
				.append ( StringUtils.substringAfter ( phpCode, endMark ) ) ;
		}
		
		return result.toString() ;


	}

}
