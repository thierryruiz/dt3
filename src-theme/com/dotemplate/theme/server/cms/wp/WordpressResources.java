package com.dotemplate.theme.server.cms.wp;

import com.dotemplate.core.server.util.Path;
import com.dotemplate.theme.server.cms.CMSResources;

public class WordpressResources extends CMSResources< WordpressResource > {

	
	public WordpressResources() {

		set ( new WordpressResource ( "functions.php", new Path().append( "functions.php" ), true ) ) ;
		set ( new WordpressResource ( "functions.css", new Path().append( "functions.css" ), false ) ) ;
		set ( new WordpressResource ( "admin.js", new Path().append( "js").append( "admin.js" ), false ) ) ;	
	
	}
	
	
	
	
	
}
