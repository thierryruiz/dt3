package com.dotemplate.theme.server.cms;


public interface ExportEngine {
	
	public void export() throws Exception ;

}
