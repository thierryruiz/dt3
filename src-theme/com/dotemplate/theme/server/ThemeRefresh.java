
package com.dotemplate.theme.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.theme.server.css.CSSHelper;
import com.dotemplate.theme.server.css.Stylesheet;
import com.dotemplate.theme.shared.ThemeUpdate;


/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
public class ThemeRefresh {
	
	private final static Log log = LogFactory.getLog ( ThemeRefresh.class );
	
	private Map < String, String > html = new HashMap< String, String >() ; 
	
	private Map < String, String > js = new LinkedHashMap< String, String >() ; 
		
	private ArrayList< Stylesheet > newRules = new ArrayList< Stylesheet >() ; 
			
	
	public void clear(){
		html.clear() ;
		js.clear() ;
		newRules.clear() ;
	}
	
	public void addHtml( String selector, String content ){
		html.put( selector, content ) ;
	}
	
	
	public void addNewRules( Stylesheet stylesheet) {
		newRules.add( stylesheet );
	}
	

	public boolean containsJs( String id ){
		return js.containsKey( id ) ;
	}

	public void addJs( String id, String s ){
		js.put ( id, s ) ;
	}
	
	
	public ThemeUpdate getDesignUpdate() {
		
		ThemeUpdate update = new ThemeUpdate () ;
		
		StringBuffer sb = new StringBuffer() ;
		for ( String s : js.values() ){
			sb.append ( s ) ;
		}
		
		update.setJs( sb.toString() ) ;

		StringBuffer css = new StringBuffer()  ;
		
		for ( Stylesheet sheet : newRules ){
			
			css.append( CSSHelper.getStylesheetAsString( sheet, false ) )  ;
		
		}
		
		update.setCss( css.toString() ) ;
			
		for ( Entry<String , String > entry : html.entrySet () ){			
			
			update.addHtml( entry.getKey (), entry.getValue () ) ;
			
			if ( log.isDebugEnabled () ){
				log.debug ( "Set + " + entry.getKey () + 
					"will be updated with following HTML content\n" + entry.getValue () ) ;
			}
			
		}
				
		return update ;
	
	}
		
	
	
	public String getCss() {
		
		StringBuffer css = new StringBuffer()  ;
		
		for ( Stylesheet sheet : newRules ){
			css.append( CSSHelper.getStylesheetAsString( sheet, false ) )  ;
		}
		
		return css.toString() ;
		
	}
	
	
	
}
