package com.dotemplate.theme.server;

import java.io.File;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import com.dotemplate.core.server.frwk.AppRuntimeException;

public class Backlinks {
	
	private static Backlinks _instance ;
	
	private PropertiesConfiguration config ; 

	
	public static void load( File file ) {
		try {
			_instance = new Backlinks( file ) ;
		} catch (ConfigurationException e) {
			new AppRuntimeException( e ) ;
		}
	}
	
	
	private Backlinks( File file ) throws ConfigurationException {
		config = new PropertiesConfiguration( file ) ;
		config.setReloadingStrategy( new FileChangedReloadingStrategy() );
	}

	
	public static String generate(){		
		return _instance.doGenerate() ;
				
	}

	
	private String doGenerate( ) {
		
		@SuppressWarnings("unchecked")
		List< Object > phrases = config.getList( "link" ) ;

		String phrase = ( String ) phrases.get( RandomUtils.nextInt( phrases.size() ) ) ;

		String link = StringUtils.substringBetween( phrase, "$", "$" ) ;
		
		phrase = StringUtils.replace( phrase, "$" + link + "$" , "<a href=\"http://www.dotemplate.com\">" + link + "</a>" ) ;
		
		return phrase ;
		
	}
	


	
	

}
