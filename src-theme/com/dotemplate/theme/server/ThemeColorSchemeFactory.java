package com.dotemplate.theme.server;

import java.awt.Color;
import java.util.LinkedHashMap;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.ColorSchemeFactory;
import com.dotemplate.core.server.ColorUtils;
import com.dotemplate.core.server.HSLColor;

import com.dotemplate.core.shared.Scheme;
import com.dotemplate.theme.shared.Theme;


public class ThemeColorSchemeFactory extends ColorSchemeFactory< Theme > {
	
	public Scheme createScheme( String name, String tokens ){
		
		Scheme scheme = new Scheme() ;
		scheme.setName( name ) ;
		
		String[] colors = StringUtils.splitPreserveAllTokens( tokens, ';' ) ;
		
		LinkedHashMap< String, String > hexaMap = new LinkedHashMap< String, String >() ;
		
		
		setColor ( scheme, hexaMap, ThemeColorType.dominant.name(), 				dominant				( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.body.name(), 					body					( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.complement.name(), 				complement				( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.bodyComplement.name(), 			bodyComplement			( colors ) );		
		
		setColor ( scheme, hexaMap, ThemeColorType.dominantBright.name(), 			dominantBright			( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.dominantDark.name(), 			dominantDark			( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.dominantDarkest.name(), 			dominantDarkest			( colors ) );		
		
		
		setColor ( scheme, hexaMap, ThemeColorType.dominantOpposite.name(), 		dominantOpposite		( colors ) );
		
		setColor ( scheme, hexaMap, ThemeColorType.dominantBrightOpposite.name(),	dominantBrightOpposite	( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.dominantDarkOpposite.name(), 	dominantDarkOpposite	( colors ) );
		
		setColor ( scheme, hexaMap, ThemeColorType.bodyOppositeSoft.name(), 		bodyOppositeSoft		( colors ) );
		
		
		setColor ( scheme, hexaMap, ThemeColorType.bodyShade.name(), 				bodyShade				( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.complementOpposite.name(), 		complementOpposite		( colors ) );
		setColor ( scheme, hexaMap, ThemeColorType.complementShade.name(), 			complementShade			( colors ) );	
		
		setColor ( scheme, hexaMap, ThemeColorType.dominantShade.name(), 			dominantShade			( colors ) );	
		setColor ( scheme, hexaMap, ThemeColorType.dominantDarkShade.name(), 		dominantDarkShade		( colors ) );	
		setColor ( scheme, hexaMap, ThemeColorType.dominantDarkestShade.name(), 	dominantDarkestShade	( colors ) );	
		

		
		//scheme.setColor ( ThemeColorType.neutralBright.name(), 		neutralBright( 			colors ) );
		//scheme.setColor ( ThemeColorType.neutralBrighter.name(), 		neutralBrighter( 		colors ) );
		//scheme.setColor ( ThemeColorType.neutralDark.name(), 			neutralDark( 			colors ) );
		//scheme.setColor ( ThemeColorType.neutralDarker.name(), 		neutralDarker( 			colors ) );
		
		String[] colorArray = new String[ hexaMap.size() ] ;
		
		hexaMap.values().toArray( colorArray ) ;
		
		scheme.setColors( colorArray ) ;
		
		return scheme ;
	
	}

	
	
	private void setColor( Scheme scheme, LinkedHashMap<String, String> hexaMap, String type,
			String value ) {
		
		scheme.setColor( type, value ) ;
		hexaMap.put( value, value ) ;
				
	}



	private String dominant( String[] colors ) {
		return colors[ ThemeColorType.dominant.getIndex() ].trim() ;
	}
	
	
	private String body(  String[] colors ) {
		return colors[ ThemeColorType.body.getIndex() ].trim() ;
	}
	
	
	private String bodyComplement(  String[] colors ) {
		String color =  colors[ ThemeColorType.bodyComplement.getIndex() ].trim() ;
		
		if ( color.length() != 0 ){
			return color ;
		}
		
		return dominant( colors ) ;
	
	}

		
	private String complement(  String[] colors ) {
		return colors[ ThemeColorType.complement.getIndex() ].trim() ;
	}
	
	
	private String dominantBright( String[] colors ) {
		
		String color = colors[ ThemeColorType.dominantBright.getIndex() ].trim() ;
		
		if ( color.length() != 0 ){
			return color ;
		}
		
		HSLColor hsl = getHSLColor ( dominant( colors ) ) ;
		return toHexString( hsl.adjustTone( 20 ) );
	
	}
	
	
	private String dominantDark( String[] colors ) {
		
		String color = colors[ ThemeColorType.dominantDark.getIndex() ].trim() ;
		
		if ( color.length() != 0 ){
			return color ;
		}
		
		HSLColor hsl = getHSLColor ( dominant( colors ) ) ;
		return toHexString( hsl.adjustShade( 20 ) );
	
	}
	
	
	private String dominantDarkest( String[] colors ) {
		HSLColor hsl = getHSLColor ( dominant( colors ) ) ;
		return toHexString( hsl.adjustLuminance( 5 ) );
	
	}
	
	
	private String dominantOpposite( String[] colors ){
		return getOpposite( getColor( dominant( colors ) ) ) ;
	}
	
	private String dominantBrightOpposite( String[] colors ){		
		return getOpposite( getColor( dominantBright( colors ) ) ) ;
	}

	private String dominantDarkOpposite( String[] colors ){		
		return getOpposite( getColor( dominantDark( colors ) ) ) ;
	}

	
	private String bodyOppositeSoft( String[] colors ){
		return getOppositeSoft( getColor( body( colors ) ) ) ;
	}
	
	
	private String bodyShade( String[] colors ){	
		return getShadeColor( body( colors ) , 5  ) ;
	}

		
	private String complementShade( String[] colors ){	
		return getShadeColor( complement( colors ) ) ;
	}
	
	
	private String dominantShade( String[] colors ){	
		return getShadeColor( dominant( colors ) ) ;
	}
	
	
	private String dominantDarkShade( String[] colors ){	
		return getShadeColor( dominantDark( colors ) ) ;
	}

	
	private String dominantDarkestShade( String[] colors ){	
		return getShadeColor( dominantDarkest( colors ) ) ;
	}

	
	
	private String getShadeColor( String hex ){
		
		Color color = getColor( hex ) ;
		HSLColor hslColor = getHSLColor ( color ) ;
		
		if ( isDark( color ) ){
			return ColorUtils.brighter( hex, 5 ) ;
		} else {
			return  toHexString( hslColor.adjustShade( 5 ) );
		}
	}
	
	
	private String getShadeColor( String hex, int degree ){
		
		Color color = getColor( hex ) ;
		HSLColor hslColor = getHSLColor ( color ) ;
		
		if ( isDark( color ) ){
			return ColorUtils.brighter( hex, degree ) ;
		} else {
			return  toHexString( hslColor.adjustShade( degree ) );
		}
	}	
	
	
	private String complementOpposite( String[] colors ){
		return getOpposite( getColor( complement( colors ) ) ) ;
	}



	/*
	private String neutralBright( String[] colors ){
		
		String color = colors[ ThemeColorType.neutralBright.getIndex() ].trim() ;
		
		if ( color.length() != 0 ){
			return color ;
		}
		
		return "#C9C9C9" ;
		
	}

	private String neutralBrighter( String[] colors ){
		
		String color = colors[ ThemeColorType.neutralBrighter.getIndex() ].trim() ;
		
		if ( color.length() != 0 ){
			return color ;
		}
		
		return "#F0F0F0" ;
	
	}

	
	private String neutralDark(  String[] colors ){
		
		String color = colors[ ThemeColorType.neutralDark.getIndex() ].trim() ;
		
		if ( color.length() != 0 ){
			return color ;
		}
		
		return "393939" ;
	
	}
	
	
	private String neutralDarker(  String[] colors ){
		
		String color = colors[ ThemeColorType.neutralDarker.getIndex() ].trim() ;
		
		if ( color.length() != 0 ){
			return color ;
		}
		
		return "#101010" ;
	
	}*/
	
	

	

}
