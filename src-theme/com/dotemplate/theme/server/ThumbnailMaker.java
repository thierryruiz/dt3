package com.dotemplate.theme.server;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;

import org.apache.batik.transcoder.ErrorHandler;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;

import com.dotemplate.core.server.frwk.AppException;



/**
 * 
 * Tool class creating banner thumbnails
 * 
 * @author Thierry Ruiz
 * 
 */
public class ThumbnailMaker extends VelocityEngine {

	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main ( String[] args )  {
		
		ThumbnailMaker generator = new ThumbnailMaker() ;
		
		
		Properties properties = new Properties() ;
		properties.setProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH, "WEB-INF/templates" ) ;

		System.out.println ( "Working dir :" +  System.getProperty ( "user.dir" ) ) ;
		
		try {
			generator.init ( properties ) ;
		} catch ( Exception e1 ) {
			e1.printStackTrace();
		}
		
		
		File[] dirs, subDirs, banners  ;
		File dir, subDir, freeDir ;
		
		dirs = new File ( "WEB-INF/templates" ).listFiles( new FilenameFilter() {
			public boolean accept ( File dir, String name ) {
				return name.startsWith ( "tpl" );
			}
		}) ;
		
		
		FileFilter subDirFilter = new FileFilter( ) {
			public boolean accept ( File file ) {
				return file.isDirectory ();
			}
		} ;
		
		FileFilter bannerFilter = new FileFilter( ) {
			public boolean accept ( File file ) {
				return file.getName ().endsWith ( ".png" )  ;
			}
		} ;
		
		
		for ( int i = 0 ; i < dirs.length ; i++ ){
			dir = dirs [ i ] ;
			subDirs = dir.listFiles ( subDirFilter ) ;
			
			for ( int j = 0 ; j < subDirs.length ; j++ ){
				subDir = subDirs [ j ] ;
				freeDir = new File ( subDir.getAbsolutePath () + File.separator + "banners" + File.separator + "free" ) ;
				banners = freeDir.listFiles ( bannerFilter ) ;
				
				for ( int k = 0 ; k < banners.length ; k++ ){
					generator.createBannerThumbnail( dir.getName (), subDir.getName () , "free" , banners [ k ] ) ;
				}
			}
		}
		
		
	}


	private void createBannerThumbnail ( String template, String subdir, String type, File file )  {	
	
    	BufferedImage image ;
    	
        try {
            image = ImageIO.read(  new FileImageInputStream( file ) ) ;
        } catch (IOException e) {
            e.printStackTrace() ;
            throw new RuntimeException ( "Failed to read the image from input stream.", e ) ;
        }
    	    	
    	int width = image.getWidth() ;
    	int height = image.getHeight () ;
    	
    	int scaleWidth = ( width > 450 ) ? 450 : width ;
    	
    	
    	double scale = ( double ) scaleWidth / ( double ) width ;
        int scaleHeight = ( int ) ( height * scale ) ;
        
		String vmTemplate = "banner.svg.vm" ;
	
		Context context = new VelocityContext ();
	
		context.put ( "path", file.getAbsolutePath () ) ;
		context.put ( "width", "" + scaleWidth ) ;
		context.put ( "height", "" + scaleHeight ) ;		
		
		StringWriter writer = null;
	
		// render the layout with nested view
		try {
			writer = new StringWriter ();
			Template vlTemplate = getTemplate ( vmTemplate );
			vlTemplate.merge ( context, writer );
	
			System.out.println ( writer.getBuffer ().toString () ) ;
			String fileName = file.getName () ;
			fileName = fileName.substring ( 0, fileName.indexOf ( '.' ) ) + ".jpg" ;
			
			// render image
			renderAsJpg ( writer.getBuffer ().toString (), "images" + File.separator + "themes" +
					File.separator + template + File.separator + subdir + File.separator +
						"banners" + File.separator + type + File.separator +  fileName , scaleWidth, scaleHeight ) ;
			
			
		} catch ( Exception e ) {
			e.printStackTrace ();
			throw new RuntimeException ( "Cannot render template '" + vmTemplate + "'.", e );
		} finally {
			try {
				writer.flush ();
				writer.close ();
			} catch ( Exception e ) {
				e.printStackTrace ();
			}
		}
	}



	public void renderAsJpg ( String svg, String out, int width, int height ) throws IOException, TranscoderException, AppException {

		FileOutputStream fos = null;
	
		try {
	
			JPEGTranscoder transcoder = new JPEGTranscoder ();
			transcoder.setErrorHandler ( new ErrorHandler () {
	
				public void error ( TranscoderException e ) throws TranscoderException {
					e.printStackTrace ();
	
					throw e;
				}
	
	
				public void fatalError ( TranscoderException e ) throws TranscoderException {
					e.printStackTrace ();
	
					throw e;
				}
	
	
				public void warning ( TranscoderException e ) throws TranscoderException {
					e.printStackTrace ();
	
					throw e;
				}
	
			} );
	
			Dimension d = new Dimension( width, height ) ;
			
			TranscoderOutput output = new TranscoderOutput ();
			TranscoderInput input = new TranscoderInput ();
	
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, new
					Float( d.getWidth() ));
	
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, new
			 Float( d.getHeight() ) );
	
			Rectangle rectangle = new Rectangle( d ) ;
			 transcoder.addTranscodingHint(JPEGTranscoder.KEY_AOI, rectangle);
	
			transcoder.addTranscodingHint ( JPEGTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE );
			transcoder.addTranscodingHint ( JPEGTranscoder.KEY_QUALITY, new Float ( 0.9 ) );
	
			fos = new FileOutputStream ( out );
			input.setInputStream ( new ByteArrayInputStream ( svg.getBytes () ) );
			output.setOutputStream ( fos );
	
			transcoder.transcode ( input, output );
	
		} catch ( Exception e ) {
			e.printStackTrace () ;
		} finally {
			try {
				fos.flush ();
				fos.close ();
			} catch ( Exception e ) {
				e.printStackTrace () ;
			}
		}

	}
	
}
