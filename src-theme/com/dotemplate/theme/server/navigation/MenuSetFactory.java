package com.dotemplate.theme.server.navigation;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;


public class MenuSetFactory {
	
	public static MenuSet create( PropertySet set ){

		MenuSet menuSet = new MenuSet () ;
		
		PropertySet subset ;
		
		for ( Property p : set.getProperties() ){
			
			if( !p.isSet()) continue ;
			
			subset = ( PropertySet ) p ;
			
			if( subset.getParent().equals( "menuset-menu" ) ){
				menuSet.addItem( createMenu ( subset ) ) ;
			}
			
			if( subset.getParent().equals( "menuset-item" ) ){
				menuSet.addItem( createMenuItem ( subset ) ) ;
			}
		}
		
		return menuSet;
	
	}

	
	private static Menu createMenu( PropertySet set ) {
		
		Menu menu = new Menu() ;
		
		menu.setLabel( ( String ) set.get( "label" )  ) ;
		
		PropertySet subset ;
		
		for ( Property p : set.getProperties() ){
			
			if( !p.isSet()) continue ;
			
			subset = ( PropertySet ) p ;
			
			if( subset.getParent().equals( "menuset-menu" ) ){
				menu.addItem( createMenu ( subset ) ) ;
			}
			
			if( subset.getParent().equals( "menuset-item" ) ){
				menu.addItem( createMenuItem ( subset ) ) ;
			}
		}
		
		return menu ;
		
	}
	
	
	private static MenuItem createMenuItem( PropertySet set ) {
		
		PropertySet node = ( PropertySet ) ( ( PropertySet ) ThemeApp.getThemeContext().get( "navigation" ) ) .get( set.getName() ) ;
		
		if( node == null ){
			throw new AppRuntimeException("Failed to create menuset. Unknown navigation node '" + 
					set.getName() + "'" ) ;
		}
				
		return new Item( node ) ;
		
	}
	

}
