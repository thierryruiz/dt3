package com.dotemplate.theme.server.navigation;

import java.util.Collection;

public interface HasItems {
	
	Collection< MenuItem > getItems() ;
	
	void addItem( MenuItem tem ) ;

	
}
