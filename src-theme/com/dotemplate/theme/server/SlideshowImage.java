package com.dotemplate.theme.server;


public class SlideshowImage extends CreativeCommonsImage {
	
	private static final long serialVersionUID = -4845631946627002950L;

	private String picasaUrl ;
	
	public String getPicasaUrl () {
		return picasaUrl;
	}
	
	public void setPicasaUrl ( String picasaUrl ) {
		this.picasaUrl = picasaUrl;
	}
	
	
}
