package com.dotemplate.theme.server;

import java.util.HashMap;

import com.dotemplate.core.shared.properties.PropertySet;

public class BindingsMap extends HashMap<String, Bindings> {

	private static final long serialVersionUID = 5930863240632258823L;

	
	public void bind ( PropertySet set1, PropertySet set2 ){
		
		if ( set1 == null || set2 == null ) return ;
		
		Bindings b = get( set1.getUid () );
		
		if ( b == null  ){

			b = new Bindings( set1 ) ;
			
			put ( set1.getUid () , b ) ;
		} 
		
		b.add ( set2 ) ;
		
	}
	

	
}
