package com.dotemplate.theme.server;

import java.io.StringWriter;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;



/**
 * 
 * Tool class creating banner thumbnails
 * 
 * @author Thierry Ruiz
 * 
 */
public class CatalogHTMLGenerator extends VelocityEngine {

	
	private final static String[] themes = {
		"tpl3a3", "tpl4a4", "tpl1a2","tpl1a4", "tpl5a3", "tpl5a4","tpl5a1", "tpl5a2", "tpl4a1","tpl4a3", "tpl4a2", "tpl3a1","tpl3a2", "tpl1a1", "tpl1a3","tpl1b1", "tpl2a1"
	} ;
	
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main ( String[] args )  {
		
		CatalogHTMLGenerator generator = new CatalogHTMLGenerator() ;
		
		
		Properties properties = new Properties() ;
		properties.setProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH, "WEB-INF/templates" ) ;

		
		try {
			generator.init ( properties ) ;
		} catch ( Exception e1 ) {
			e1.printStackTrace() ;
		}

		Context context = new VelocityContext ();
		
		context.put ( "themes", themes ) ;
		
		StringWriter writer = null;
	
		try {
			
			writer = new StringWriter ();
			Template vlTemplate = generator.getTemplate ( "catalog.html.vm" );
			vlTemplate.merge ( context, writer );
			System.out.println ( writer.getBuffer ().toString () ) ;
			
		} catch( Exception e ){
			throw new RuntimeException( e ) ;
		}

	}
	
}
