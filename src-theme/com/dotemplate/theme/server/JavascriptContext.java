package com.dotemplate.theme.server;

import java.util.ArrayList;
import java.util.Collection;


public class JavascriptContext {
	
	private ArrayList<String> scripts = new ArrayList<String> ();

	private StringBuffer jsBuffer = new StringBuffer();
	
	
	public void addLocalScript ( String url) {
		if ( ! scripts.contains ( url ) ){
			scripts.add ( url ) ;
		}
	}

	public Collection<String> scripts() {
		return scripts;
	}
		
	
	public void addCustomJs ( String js ){
		jsBuffer.append ( js ) ;
	}
	
	
	public String getCustomJs() {
		return jsBuffer.toString () ;
	}
	
	
	public void clear() {
		jsBuffer.setLength ( 0 ) ;
	}
	

	
	
	
}
