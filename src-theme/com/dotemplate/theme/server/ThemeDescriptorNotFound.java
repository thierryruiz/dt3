package com.dotemplate.theme.server;

import com.dotemplate.core.server.frwk.AppException;

public class ThemeDescriptorNotFound extends AppException {
	
	private static final long serialVersionUID = 1L;
	
	public ThemeDescriptorNotFound( String msg ) {
		super(msg);
	}


}
