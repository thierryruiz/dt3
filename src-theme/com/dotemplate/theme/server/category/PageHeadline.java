package com.dotemplate.theme.server.category;

public class PageHeadline {
	
	private String h1;
	
	private String h2 ;
	
	private String tagline ;

	public String getH1() {
		return h1;
	}

	public void setH1(String h1) {
		this.h1 = h1;
	}

	public String getH2() {
		return h2;
	}

	public void setH2(String h2) {
		this.h2 = h2;
	}

	public String getTagline() {
		return tagline;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	
	
}
