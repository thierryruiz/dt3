package com.dotemplate.theme.server.category;

import java.util.List;

public class CategoryContent {
	
	private static String[] LOREM = {
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam rutrum lorem ex, eget iaculis nisl.",
		"In rutrum orci et dapibus aliquam. Morbi sit amet nulla ornare, hendrerit magna vitae, aliquam.",
		"Duis at egestas nisi. Quisque tincidunt dolor vitae ante aliquam laoreet. Nullam ullamcorper a turpis.",
		"Aliquam at faucibus nunc. Aenean id velit quis turpis convallis finibus. Morbi sit amet mauris."};


	private String name;
	
	private String category ;

	private Headline headline;
	
	private PageHeadline pageHeadline ;

	private List<Bloc> blocs;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Headline getHeadline() {
		return headline;
	}

	public void setHeadline(Headline headline) {
		this.headline = headline;
	}

	public List<Bloc> getBlocs() {
		return blocs;
	}

	public void setBlocs(List<Bloc> blocs) {
		this.blocs = blocs;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Bloc getBloc(int i) {
		return blocs.get(i - 1);
	}

	public PageHeadline getPageHeadline() {
		return pageHeadline;
	}

	public void setPageHeadline(PageHeadline pageHeadline) {
		this.pageHeadline = pageHeadline;
	}

	public void init() {
		int i = 0 ;
		
		for ( Bloc bloc : blocs ){
		
			if ( "lorem".equals( bloc.getText() ) ){
				bloc.setText(LOREM[ i ]) ;
			}
			i++;
			bloc.init() ;
		}
		
	}
	
	
	

}
