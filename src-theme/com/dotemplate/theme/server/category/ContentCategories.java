package com.dotemplate.theme.server.category;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs2.FileChangeEvent;
import org.apache.commons.vfs2.FileListener;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.impl.DefaultFileMonitor;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.theme.server.ThemeApp;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


public class ContentCategories {

	private final static Log log = LogFactory.getLog ( ContentCategories.class );
	
	private HashMap< String, CategoryContentMap > categories;
	
	private ArrayList< String > watchedFiles ;
	
	
	private Gson jsonLoader ;
	
	
	public ContentCategories() {
		
		boolean watch =  ThemeApp.getConfig().isDevMode() ;
		
		if ( watch ){
			watchedFiles = new ArrayList< String >() ;
		}	

		jsonLoader = new GsonBuilder().create() ;
		load() ;
	
		if( watch ){
			watchForContentUpdate() ;
		}
	
	}

	private void watchForContentUpdate()  {
		try {
			
			FileSystemManager fsManager = VFS.getManager();
			
			
			DefaultFileMonitor fm = new DefaultFileMonitor(new FileListener() {
				
				@Override
				public void fileDeleted(FileChangeEvent arg0) throws Exception {
				}
				
				@Override
				public void fileCreated(FileChangeEvent arg0) throws Exception {
					
				}
				
				@Override
				public void fileChanged(FileChangeEvent arg0) throws Exception {
					watchedFiles.clear() ;
					load() ;
				}
			});			
			
		
			
			for ( String path : watchedFiles ){
				FileObject listendir = fsManager.resolveFile( path );
				fm.setRecursive(true);
				fm.addFile(listendir);
			}
	   
			fm.start() ;

			
		} catch ( Exception e ){
			throw new AppRuntimeException(e) ;
		}
		
	}
	
	
	
	

	protected void load() {
		
		
		if ( log.isInfoEnabled() ){
			log.info("Loading content categories..." ) ;
		}
		
		if ( categories == null ){
			categories = new HashMap< String, CategoryContentMap >() ;
		} else {
			categories.clear() ;
		}
		
		
		File categoriesFolder =  new File( App.realPath( "categories" )) ;
		
		Collection< File > categoryFolders = listFolders( categoriesFolder ) ;
	
		String name ;
		

		for( File category : categoryFolders ){
						
			name = category.getName() ;
			
			if ( "all".equals( name ) ){
				continue ;
			}
			
			try {
				
				
				categories.put( name, load( name ) ) ;
				
			} catch ( FileNotFoundException e ){

				if ( ThemeApp.isDevMode() ){
					log.warn( "Failed to load category " + name + " content." +  e.getLocalizedMessage() ) ;
				}
				
			} catch ( Exception e ){
				
				log.warn( "Failed to load category " + name + " content.", e ) ;
			
			}
		
		}
	}
	
	
	
	public  ArrayList< File > listFolders( File dir ) {
		
		String[] directories = dir.list( new FilenameFilter() {
		  @Override
		  public boolean accept(File current, String name) {
		    return new File(current, name).isDirectory();
		  }
		});
		
		ArrayList< File > result = new ArrayList< File >() ;
		for ( String s : directories ){
			result.add( new File( s ) ) ;
		}
		return result ;

	}

	
	protected CategoryContentMap load( String categoryName ) throws Exception {
		
		if ( log.isDebugEnabled() ){
			log.debug( "Loading  category " + categoryName ) ;
		}
		
		JsonElement root = new JsonParser().parse( new FileReader ( 
				 App.realPath( "categories/" +  categoryName + "/text/home.json" ) ) );
		
		if ( watchedFiles != null ){
			watchedFiles.add( App.realPath( "categories/" +  categoryName + "/text/" ) ) ;
		}
		
		
		CategoryContentMap contents = jsonLoader.fromJson( root, CategoryContentMap.class ) ;
		contents.setCategoryName( categoryName ) ;
		
		contents.init() ;
		
		return contents ;
		
	}

	
	public CategoryContent getCategoryContent( String category, String name ){
		
		if ( log.isDebugEnabled() ){
			log.debug("get category content " + category + "." + name  ) ;
		}
		
		return  categories.get( category ).get( name ) ;
	}

	
}