package com.dotemplate.theme.server.category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dotemplate.core.server.frwk.AppRuntimeException;


public class CategoryContentMap {
	
	private String categoryName ;
	
	private List< CategoryContent > contents ;
	
	private Map< String, CategoryContent > contentMap ;
	
	public List< CategoryContent > getContents() {
		return contents;
	}
	
	public void setContents( List< CategoryContent > contents ) {
		this.contents = contents;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	public CategoryContent get( String name ){
		if ( contentMap == null ){
			throw new AppRuntimeException( "Contents map not yet initialize call init method first" ) ;
		}
		return contentMap.get( name ) ;
	}
	
	
	public void init(){
		contentMap = new HashMap< String, CategoryContent >() ;
		for ( CategoryContent content : contents ){
			content.init() ;
			content.setCategory(categoryName);
			contentMap.put( content.getName(), content ) ;
			
		}
		
	}
	
	
}
