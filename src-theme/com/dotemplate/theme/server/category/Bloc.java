package com.dotemplate.theme.server.category;

import org.apache.commons.lang.StringUtils;



public class Bloc {


	
	private String title ;

	private String text ;
	
	private String image ;

	private String imageType ;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public void setImageType(String imageType) {
		this.imageType = imageType;
	}
	
	public String getImageType() {
		return ( imageType != null )  ? imageType : StringUtils.substringBefore( getImage() , "-" )  ;
	}

	public void init() {
	}
	
	
}
