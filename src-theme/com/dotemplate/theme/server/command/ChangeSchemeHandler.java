package com.dotemplate.theme.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.command.ChangeScheme;
import com.dotemplate.theme.shared.command.ChangeSchemeResponse;



public class ChangeSchemeHandler extends CommandHandler< ChangeSchemeResponse > {
	
	private static Log log = LogFactory.getLog ( ChangeSchemeHandler.class );
	

	@Override
	public ChangeSchemeResponse handleAction ( Command< ChangeSchemeResponse > command  )
							throws WebException {
			
		String schemeName = ( ( ChangeScheme ) command ).getSchemeName()  ; 

		
		if( log.isDebugEnabled () ){
			log.debug ( "Change color scheme to " + schemeName  ) ;
		}
		
		
		ChangeSchemeResponse response = new ChangeSchemeResponse() ;
	
		
		try {
			
			response.setStylesheet( ThemeApp.getThemeManager ().changeColorScheme ( schemeName ) ) ; 
			
		} catch ( Exception e ) {
			
			throw syserror ( "Sorry. Failed to update scheme " , e ) ;
			
		}

		
		return response ;
		
		
	}
	
				

}
