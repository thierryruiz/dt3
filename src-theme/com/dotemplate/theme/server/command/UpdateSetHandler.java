package com.dotemplate.theme.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.ThemeUpdate;
import com.dotemplate.theme.shared.command.UpdateSet;
import com.dotemplate.theme.shared.command.UpdateSetResponse;



public class UpdateSetHandler extends CommandHandler< UpdateSetResponse > {

	
	private static Log log = LogFactory.getLog ( UpdateSetHandler.class );
	

	
	@Override
	public UpdateSetResponse handleAction ( Command< UpdateSetResponse > action )
							throws WebException {
		
		PropertySet set = ( ( UpdateSet  ) action ).getThemePropertySet () ; 

		if( log.isInfoEnabled () ){
			log.info ( "Update set " + set.getName () + " [uid=" + set.getUid () + "]" ) ;
		}
		
		UpdateSetResponse response = new UpdateSetResponse() ;
	
		try {
			
			ThemeUpdate refresh =  ( ThemeUpdate ) ThemeApp.getThemeManager ().updateProperty ( set ) ; 
			
			response.setClientRefresh ( refresh ) ;
			
			if ( log.isDebugEnabled () ){
			
				StringBuffer sb = new StringBuffer() ;
				
				sb.append ( "\n\nRefresh JS\n" + refresh.getJs () ) ;
				
				
				sb.append ( "\n\nRefresh HTML\n" ) ;
				
				
				for ( String key : refresh.getHtml ().keySet () ){
					sb.append( key ).append( ":\n" ).append ( refresh.getHtml ().get ( key ) ) ;
				}
				
				log.debug ( sb.toString () ) ;

			}
			
			
		} catch ( Exception e ) {
			
			throw syserror ( "Failed to Update. Sorry.", e ) ;
		}

		
		return response ;
		
		
	}
	
				

}
