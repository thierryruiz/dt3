package com.dotemplate.theme.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.shared.command.GetOnReadyScriptsResponse;



public class GetOnReadyScriptsHandler extends CommandHandler< GetOnReadyScriptsResponse > {

	
	private static Log log = LogFactory.getLog ( GetOnReadyScriptsHandler.class );
	

	
	@Override
	public GetOnReadyScriptsResponse handleAction ( Command< GetOnReadyScriptsResponse > action )
							throws WebException {
		
		
		if( log.isInfoEnabled () ){
			log.info ( "Get custom javascript " ) ;
		}
		
		GetOnReadyScriptsResponse response = new GetOnReadyScriptsResponse() ;
	
		try {
		
			
			response.setJs( ThemeApp.getThemeManager().getOnReadyJavascript() ) ;
			
			
		} catch ( Exception e ) {
			
			throw syserror ( "Sorry. Failed to get on ready Javascript " , e ) ;
			
		}

		
		return response ;
		
		
	}
	
				

}
