package com.dotemplate.theme.server.generator;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.CachedImage;
import com.dotemplate.core.server.DesignSession;
import com.dotemplate.core.server.DesignTaskContext;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.svg.JPEGTranscoder;
import com.dotemplate.core.server.svg.PNGTranscoder;
import com.dotemplate.core.server.svg.Slice;
import com.dotemplate.core.server.svg.VoidOutputStream;
import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.theme.server.CssImage;
import com.dotemplate.theme.server.ThemeApp;


public class SliceTask extends ConcurrentTask< Void > {

	private final static Log log = LogFactory.getLog ( SliceTask.class );
		
	
	public Void call () throws Exception {
	
		SliceTask.Context ctx = ( SliceTask.Context ) super.taskContext ;

		DesignSession.set ( ctx.getDesignSession () ) ;
		
		Slice slice =  ctx.getSlice () ;

		if ( App.isDevMode() ){

			System.out.println( "Generate slice " + slice.getName() + " [" + slice.getX() + "," + slice.getY() + "," +  
					slice.getWidth() + "," + slice.getHeight() + "]"  );
		
		} else {
		
		
			if ( log.isDebugEnabled() ){
				log.debug("Generating slice " + slice.getName() + " [" + slice.getX() + "," + slice.getY() + "," +  
						slice.getWidth() + "," + slice.getHeight() + "]"  ) ;
			}
		}
		
		
		try {
			
			CssImage image ;
			
			if ( slice.isPng() ){
				
				image = new CssImage( slice.getName(), PNGTranscoder.transcode ( ctx.getSvg(), slice, 
								new VoidOutputStream() ), CachedImage.Format.PNG ) ;
								
				
			} else {
				
				image = new CssImage( 
						slice.getName(), JPEGTranscoder.transcode ( ctx.getSvg(), slice, slice.getQuality(),
								new VoidOutputStream() ), CachedImage.Format.JPG ) ;
				
			}

			ThemeApp.getThemeContext ().getCssImageCache().setImage( image ) ;
			
			
		} catch ( Exception e ) {

			throw new AppException ( "Failed to execute slice task \n\n\n SVG ----\n" + ctx.getSvg() + "\n\n\n SVG ----\n" , e ) ;
			
		} finally {
			
			DesignSession.clear() ;
		
		}
		
		
		return null;
	
	}
	
	
	
	static class Context extends DesignTaskContext {

		private String svg ;
		
		private String outputDir ;
		
		private Slice slice ;
		
		private String imageName ;
		
		public String getSvg () {
			return svg;
		}

		public void setSvg ( String svg ) {
			this.svg = svg;
		}

		
		public String getOutputDir () {
			return outputDir;
		}

		public void setOutputDir ( String outputDir ) {
			this.outputDir = outputDir;
		}

		public void setImageName ( String imageName ) {
			this.imageName = imageName;
		}
		
		public String getImageName () {
			return imageName;
		}
		
		
		public Slice getSlice () {
			return slice;
		}

		public void setSlice ( Slice slice ) {
			this.slice = slice;
		}

		
		@Override
		public boolean isValid () {
			return true ;
		}
		
	}
	

	
}
