package com.dotemplate.theme.server.generator;


import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.core.server.util.ConcurrentTaskExecutor;
import com.dotemplate.core.server.util.ConcurrentTaskPool;


public class SliceTaskExecutor extends ConcurrentTaskExecutor< Void > {

	public SliceTaskExecutor () {
		super ( 50 ) ;
	}

	
	@Override
	protected ConcurrentTaskPool<Void> createPool () {
		return new ConcurrentTaskPool< Void >( ) {
			protected ConcurrentTask<Void> createTask () throws Exception {
				return new SliceTask() ;
			}
		} ;
	}


}
