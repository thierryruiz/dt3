package com.dotemplate.theme.server.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.SetHierarchy;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;
import com.dotemplate.theme.server.ThemeRefresh;
import com.dotemplate.theme.server.ThemeUtils;
import com.dotemplate.theme.server.css.CSSHelper;
import com.dotemplate.theme.server.css.CSSParser;
import com.dotemplate.theme.server.css.Stylesheet;


public class CSSGeneratorDelegate {
	
	private final static Log log = LogFactory.getLog ( CSSGeneratorDelegate.class );

	
	public static void generate ( PropertySet set ) throws Exception {

		if ( log.isDebugEnabled() ){
			log.debug ( "Generating css for set " + set.getName () );
		}

		
		if ( log.isInfoEnabled() && App.isDevMode() ){
			
			System.out.println ( "\n\ncss		>  " + set.getLongName() ) ;
		
		}		
		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		VelocityContext context = new VelocityContext ( themeContext ) ;
		
		context.put ( "s", set ) ;
				
		String template = DesignUtils.getSymbol ( set.getParent () ).getPath () + "/css.vm" ;
		
		StringWriter writer = new StringWriter() ;
		
		
		// generate css
		try {

			ThemeApp.getRenderEngine ().generate ( context, template, writer );

			ThemeRefresh themeRefresh = themeContext.getThemeRefresh() ;
			
			Stylesheet setRules = CSSParser.parse( writer.getBuffer().toString() )  ;		
			
			themeRefresh.addNewRules( CSSHelper.update( themeContext.getStylesheet(), setRules ) ) ;
			
			
		} catch ( Exception e ) {

			throw new AppException ( "Failed to generate css for set '" + set.getName () + "'", e );
			
			
		}
	}
	

	public static void generate () throws AppException {

		if ( log.isInfoEnabled () ) {
			log.info ( "Generating theme stylesheet..." );
		}

		
		ThemeContext themeContext = ThemeApp.getThemeContext() ;

		String modelName = themeContext.getDesign().getParent() ;
	
		String template = new Path ( "models" ).append ( modelName ).append( "style.css.vm" ).asString() ; 

		VelocityContext context = new VelocityContext ( themeContext ) ;
		
		String out = ( ! themeContext.isExportMode () ) ? 
			new Path ( ThemeApp.get().getWorkRealPath () ).append ( "style.css" ).toString () :
				new Path ( ThemeApp.get().getExportDirectoryRealPath () ).append ( "style.css" ).toString () ;
	
		
		// generate css
		try {
			
			StringWriter writer = new StringWriter() ;
			
			ThemeApp.getRenderEngine ().generate ( context, template, writer ) ;
			
			Stylesheet stylesheet = CSSParser.parse( writer.getBuffer().toString() ) ;			
			
			themeContext.setStylesheet( stylesheet ) ;
			
			CSSHelper.print( stylesheet, new FileOutputStream( new File ( out ) ), 
					themeContext.isExportMode(), ThemeUtils.hasFullFeatures() ) ;
			
			
		} catch ( Exception e ) {

			throw new AppException ( "Failed to generate theme global style sheet", e );

		} finally {
			
			SetHierarchy.clear() ;
		
		}
	
	}
	
	
	
	public static void generate ( Writer writer ) throws AppException {

		if ( log.isInfoEnabled () ) {
			log.info ( "Generating theme stylesheet..." );
		}

		ThemeContext themeContext = ThemeApp.getThemeContext() ;
		
		VelocityContext context = new VelocityContext ( themeContext ) ;
		
		String modelName = themeContext.getDesign().getParent() ;
		
		String template = new Path ( "models" ).append ( modelName ).append( "style.css.vm" ).asString() ; 
		
		// generate css
		try {
						
			ThemeApp.getRenderEngine ().generate ( context, template, writer ) ;
						
		} catch ( Exception e ) {

			throw new AppException ( "Failed to generate theme global style sheet", e );

		} finally {
			
			SetHierarchy.clear() ;
		
		}
		
	
	}

	
	
	
	
	
	 
	
}
