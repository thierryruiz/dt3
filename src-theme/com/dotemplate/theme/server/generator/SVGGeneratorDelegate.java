package com.dotemplate.theme.server.generator;


import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.VelocityContext;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.svg.Slice;
import com.dotemplate.core.server.svg.Slices;
import com.dotemplate.core.server.util.ConcurrentTaskContext;
import com.dotemplate.core.server.util.Duration;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;


public class SVGGeneratorDelegate {

	private final static Log log = LogFactory.getLog ( SVGGeneratorDelegate.class );
	

	
	
	@Duration
	public static void generate ( PropertySet set ) throws Exception {
		
		if ( log.isInfoEnabled() && App.isDevMode() ){
			
			System.out.println ( "\n\nsvg		>  " + set.getLongName() ) ;
		
		}
		
		if ( log.isDebugEnabled() ){
			log.debug( "Generate SVG slices for set " + set.getName() ) ;
		}
		
		VelocityContext context = new VelocityContext( ThemeApp.getThemeContext() ) ;
				
		context.put ( "s", set ) ; 
		
		StringWriter writer = new StringWriter () ;
		
        Slices slices  ;
        
        context.put( ThemeContext.SLICES , slices = new Slices () ) ;
        
        String template  = DesignUtils.getSymbol ( set.getParent() ).getPath () + "/svg.vm" ;
        
        
        // generate svg 
        ThemeApp.getRenderEngine ().generate ( context, template, writer ) ;
		
		// no slices !
		if ( slices.size() == 0  ) return ;
		
		SliceTaskExecutor executor = ( SliceTaskExecutor ) ThemeApp.getSingleton ( SliceTaskExecutor.class ) ;
		
		List< ConcurrentTaskContext > taskContexts = new ArrayList< ConcurrentTaskContext >() ;
		
		SliceTask.Context taskContext ;

		
		if ( log.isDebugEnabled () ){
			log.debug ( "\n" + writer.getBuffer ().toString () ) ;
		}
		
		String output = new Path ( ThemeApp.get().getWorkRealPath () ).append ( "images" )
			.append ( "css" ).toString () ;
	
		
		String random  = ( String ) context.get ( ThemeContext.RANDOM ) ;
	
		if ( random.length() > 1 ){
			random = random.substring ( 1 ) ;
		}
		
		
		String imgName ;
		
		for ( Slice slice : slices.values () ){
			
			imgName = ( slice.isPng () ) ? slice.getName () +  ".png" : 
				 random + "." + slice.getName () +  ".jpg" ;
			
			
			taskContext = new SliceTask.Context() ;
			taskContext.setOutputDir ( output ) ;
			taskContext.setSlice ( slice ) ;
			taskContext.setSvg ( writer.getBuffer ().toString () ) ;
			taskContext.setImageName ( imgName ) ;
			
			taskContexts.add( taskContext ) ;
		
			
		}
		
		
		try {
		
			List< Future < Void> > results = executor.execute ( taskContexts ) ;
			
			for ( Future< Void > future : results ){
				future.get () ; // will rethrow exception raised by a task
			}
		
			
		} catch ( Throwable t ) {
			log.error(  writer.getBuffer ().toString () ) ;
			throw new AppException ( "Failed to slice SVG of set " + set.getLongName () , new Exception ( t )  ) ;
		
		} 
		
	}

	
	
	
}
