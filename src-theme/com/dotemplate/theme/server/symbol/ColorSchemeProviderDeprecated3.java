package com.dotemplate.theme.server.symbol;

import java.io.File;
import java.io.FileInputStream;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.DesignResourceProvider;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.util.Path;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeColorSchemeFactory;

@Deprecated
public class ColorSchemeProviderDeprecated3 extends DesignResourceProvider< Scheme > {
		
	protected ThemeColorSchemeFactory schemeFactory ;
	
	public ColorSchemeProviderDeprecated3() {
		schemeFactory = new ThemeColorSchemeFactory() ;
		load() ;
	}
	
	
	@Override
	public void load() {
		
		// Color schemes are located under WEB-INF/templates/_schemes directory
		
		// load schemes
		
		
		File schemesFile = new Path ( ThemeApp.realPath( "WEB-INF/templates/_schemes/schemes.properties" ) ).asFile() ;
		
		
		Properties schemeProps ;
		LinkedHashMap< String,  Scheme > schemes ;
		
			schemes = new LinkedHashMap< String,  Scheme >() ;
			
			try {
			
				( schemeProps = new Properties() ).load( new FileInputStream ( schemesFile ) );
			
			} catch ( Exception e ) {
				throw new AppRuntimeException ( "Failed to load Schemes", e ) ;
			}
			
			
			Scheme scheme ;
			String name ;

			
			for ( Entry< Object, Object > entry  : schemeProps.entrySet()  ) {
				name = StringUtils.substringBefore( ( String ) entry.getKey() , "." ) ; 
				scheme = schemeFactory.createScheme( name, ( String ) entry.getValue() ) ;
				schemes.put( scheme.getName(), scheme ) ;
				scheme.setFree( true ) ;
				computeThumbnailHtml( scheme ) ;
				
				resourcesMap.get( ALL_RESOURCES ).put( scheme.getName() , scheme ) ;
			}

	}
	
	
	
	
	private void computeThumbnailHtml ( Scheme scheme ){
        String[] colors = scheme.getColors() ;
        StringBuffer html = new StringBuffer ( "<div class='ez-scheme' title='" + scheme.getName () + "'>" )  ;
           
        for ( int i = 0 ; i < colors.length ; i++  ){
            html.append("<div title =\"" + colors[ i ] + 
            		"\" class=\"ez-scheme-color-panel\" style=\"background-color:" + colors[ i ]+ ";\"></div>" ) ;
        }
        
        html.append( "</div>" ) ;
        
        scheme.setThumbnailHtml( html.toString() ) ;
	
	}
	

	
	
	
	@Override
	public boolean isFree( String name ) {
		return true;
	}
	
	
}
