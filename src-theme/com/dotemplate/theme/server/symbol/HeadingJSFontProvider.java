package com.dotemplate.theme.server.symbol;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.StringProperty;
import com.dotemplate.theme.shared.ThemeSymbolType;



public class HeadingJSFontProvider extends ThemeSymbolProvider {
	
	private final static Log log = LogFactory.getLog ( HeadingJSFontProvider.class );
	
	private final static String abcd = " AaBbCcDdEeFfGg" ;
	
	@Override
	protected SymbolType getType () {
		return ThemeSymbolType.HEADINGJSFONT ;
	}

	
	@Override
	protected void prepare ( PropertySet font ) {
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Preparing font " + font.getName () ) ;
		}
				
		StringProperty family  = ( StringProperty ) font.getProperty ( "family" ) ;
		
		if ( family == null ){
			throw new AppRuntimeException( "Set " + font.getName () + " does not look as a heading font. " +
					"Expected property 'family' is missing" ) ;
		}
		
		family.setValue ( StringUtils.substringAfter ( font.getName (), "_" ) ) ;
						
		
		StringProperty info  = ( StringProperty ) font.getProperty ( "info" ) ;
		
		
		( ( StringProperty ) font.getProperty ( "title" ) ).setValue ( family.getValue () ) ;
		
		// get font js file size
		File jsFile = new File ( App.realPath ( "js/cufon/" + family.getValue () + ".js" ) )  ;

		long length = jsFile.length () ;
		int lenghtKb = Math.round ( ( float ) ( length / 1000 ) ) ;
		
		info.setValue ( "Cufon font (" + lenghtKb + " kb)" ) ;
			
		
		if ( log.isInfoEnabled () ){
			log.info ( "Font '" + font.getName () + "' loaded.") ;
		}
		
		
	}
	
	
	
	protected void computeThumbnails ( PropertySet font ){
		
		
		StringBuffer sb = new StringBuffer() ;
		
		String title = ( String ) font.get (  "title" ) ;
		String family = ( String ) font.get (  "family" ) ;
		String info = ( String ) font.get (  "info" ) ;
		
		
		sb.append("<script src=\"js/cufon/" )
			.append ( family )
			.append( ".js\" type=\"text/javascript\"></script>" )
			.append( "<div class=\"ez-headingfont\">")
			.append( "<div class=\"").append( family ).append( "\">" ).append( title ).append( abcd ). append("</div>" )
			.append(  "<div style=\"margin-top:5px\">" ).append( info ).append("</div></div>"  )
			.append( "<script type=\"text/javascript\">")
			.append( "Cufon.replace('.").append( family ) .append("', { fontFamily: '").append( family ).append( "', fontSize: '24px' });" )
			.append( "</script>");

		font.setThumbnailHtml( sb.toString() ) ;
		
		
	}
	
	
}
