package com.dotemplate.theme.server.symbol;

import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.shared.ThemeSymbolType;

public class FontProvider extends ThemeSymbolProvider {
	
	private final static String LOREM = 
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean fermentum fermentum nisl. " +
		"Nullam dolor mi, blandit a, adipiscing sit amet, tempor sit amet, leo. Vivamus ullamcorper varius velit. " +
		"Duis vel sem et nunc pulvinar elementum. Cras pretium lorem a tellus. Vestibulum odio nisl, rhoncus sit amet, " +
		"varius vitae, bibendum mattis, odio." ;

	
	@Override
	protected SymbolType getType () {
		return ThemeSymbolType.FONT ;
	}
	
	
	protected void computeThumbnails ( PropertySet font ){
		
		StringBuffer sb = new StringBuffer() ;

		sb.append("<div style=\"" ) 
			.append( "font-family:" + font.get("fontFamily" ) + ";" ) 
			.append( "font-size:" + font.get("fontSize" ) + ";" )
			.append( "font-style:" + font.get("fontStyle" ) + ";" )	
			.append( "font-weight:" + font.get("fontWeight" ) + ";" )				
			.append( "line-height:" + font.get("lineHeight" ) + ";" )
			.append( "text-transform:" + font.get("textTransform" ) + ";" )
			.append( "letter-spacing:" + font.get("letterSpacing" ) + ";" )			
			.append( "color:black;padding:10px;\">" )
			.append ( "<div style=\"font-size:20px;margin:0 0 10px 0\">" )
			.append ( font.get ( "desc" )  ).append ( "</div>" )
			.append( LOREM ).append("</div>") ;

		
		font.setThumbnailHtml( sb.toString() ) ;
		
		
	}
	
	
}
