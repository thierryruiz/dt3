package com.dotemplate.theme.server.symbol;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.shared.ThemeSymbolType;

public class FillProvider extends ThemeSymbolProvider {

	public final static String PLAIN = "Plain color" ;
	
	public final static String GRADIENTS = "Gradients" ;
	
	public final static String PATTERNS = "Patterns" ;
	
	public final static String TEXTURES = "Textures" ;
	
	public final static String PATTERNS_FADE = "Patterns with fade effect" ;
	
	public final static String TEXTURES_FADE = "Textures with fade effect" ;
	
	
	public FillProvider() {
		
		// ensure sort by precreating tags
		
		resourcesMap.put( PLAIN, new LinkedHashMap<String, PropertySet>() ) ;
		resourcesMap.put( GRADIENTS, new LinkedHashMap<String, PropertySet>() ) ;
		resourcesMap.put( PATTERNS, new LinkedHashMap<String, PropertySet>() ) ;
		resourcesMap.put( PATTERNS_FADE, new LinkedHashMap<String, PropertySet>() ) ;
		resourcesMap.put( TEXTURES, new LinkedHashMap<String, PropertySet>() ) ;
		resourcesMap.put( TEXTURES_FADE, new LinkedHashMap<String, PropertySet>() ) ;
		
	}

	
	
	@Override
	protected SymbolType getType () {
		return ThemeSymbolType.FILL;
	}

	
	/*
	@Override
	protected int getThumbnailWidth( PropertySet symbol ) {
		return 70 ;
	}
	
	
	protected int getThumbnailHeight( PropertySet symbol ) {
		return 50 ;
	}*/
	
	
	protected boolean isFillType( String type, PropertySet symbol ){
		return symbol.getTags().contains( type ) ; 
	}

	
	@Override
	protected void sort() {
		
		Set< Entry< String, LinkedHashMap< String, PropertySet > > > entries = resourcesMap.entrySet() ;
		
		LinkedHashMap< String, PropertySet > sorted ;
		
		ArrayList< PropertySet > list ;
		
		for ( Entry< String, LinkedHashMap< String, PropertySet > >  entry : entries ){
			
			sorted = new  LinkedHashMap< String, PropertySet >() ;
			
			list = new ArrayList< PropertySet > ( entry.getValue().values() ) ;
			
			Collections.sort( list, sortComparator ) ;
			
			for ( PropertySet symbol : list ){
				sorted.put( symbol.getName(), symbol ) ;
			}
			
			resourcesMap.put( entry.getKey(), sorted ) ;
			
		}
	
	}
	

}
