package com.dotemplate.theme.server.symbol;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignResourceProvider;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.Scheme;

@Deprecated
public class ColorSchemeProviderDeprecated extends DesignResourceProvider< Scheme > {

	private final static Log log = LogFactory.getLog ( ColorSchemeProviderDeprecated.class );

	
	public ColorSchemeProviderDeprecated() {
		load();
	}
	
	public void load () {

		if ( log.isDebugEnabled () ){
			log.debug ( "Listing schemes..."  ) ;
		}
		
		// Color schemes are located under WEB-INF/templates/_schemes directory
		
		// load schemes
		
		Properties freeProps = new Properties() ;
		Properties premiumProps = new Properties() ;
		
		try {
			
			freeProps.load( new FileInputStream( App
					.realPath( "WEB-INF/templates/_schemes/free.properties" )));
			
			
			premiumProps.load(new FileInputStream(App
					.realPath("WEB-INF/templates/_schemes/premium.properties")));
			
		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Failed to load Schemes", e ) ;
		}
		
		
		for ( Entry< Object, Object > entry  : freeProps.entrySet()  ) {
			add ( ( String ) entry.getKey(), StringUtils.split( ( String ) entry.getValue(), ',' ), true ) ;			
		}
		
		for ( Entry< Object, Object > entry  : premiumProps.entrySet()  ) {
			add ( ( String ) entry.getKey(), StringUtils.split( ( String ) entry.getValue(), ',' ), false ) ;			
		}
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Schemes loaded."  ) ;
		}
		
		
	}
	
	
	
	protected void add ( String name, String[ ] colors, boolean free ){
		Scheme scheme = new Scheme() ;
		scheme.setFree( free ) ;
		scheme.setName ( name ) ;
		scheme.setColors( colors ) ;
		computeThumbnailHtml ( scheme ) ;
		resourcesMap.get( ALL_RESOURCES ).put  ( name, scheme ) ;
	}
	
	
	
	
	private void computeThumbnailHtml ( Scheme scheme ){
        String[] colors = scheme.getColors() ;
        StringBuffer html = new StringBuffer ( "<div class='ez-scheme' title='" + scheme.getName () + "'>" )  ;
           
        for ( int i = 0 ; i < colors.length ; i++  ){
            html.append("<div title =\"" + colors[ i ] + 
            		"\" class=\"ez-scheme-color-panel\" style=\"background-color:#" + colors[ i ]+ ";\"></div>" ) ;
        }
        
        html.append( "</div>" ) ;
        
        scheme.setThumbnailHtml( html.toString() ) ;
	
	}
		
}
