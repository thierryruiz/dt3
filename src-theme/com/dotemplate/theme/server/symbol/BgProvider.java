package com.dotemplate.theme.server.symbol;


import java.util.List;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.symbol.SymbolFilter;
import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.shared.ThemeSymbolType;


public class BgProvider extends ThemeSymbolProvider {
	
	
	@Override
	protected SymbolType getType () {
		return ThemeSymbolType.BG;
	}

	
	
	public static void main( String[] args ) {
		
		BgProvider bgProvider = ( BgProvider ) DesignUtils.getSymbolProvider ( ThemeSymbolType.BG ) ;
		
		List<PropertySet> bgs = bgProvider.list ( "fluid=true" ) ;
		
		SymbolFilter evaluator = new SymbolFilter( "fluid=true & repeat=repeat-x" ) ;
		
		for ( PropertySet set : bgs ){
			System.out.println ( set.getPath () + " match " + evaluator.match ( set ) ) ;
		}
		
	}

	
	
}
