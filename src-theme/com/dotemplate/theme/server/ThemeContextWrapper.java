package com.dotemplate.theme.server;


import java.util.Collection;
import java.util.LinkedHashMap;


import org.apache.velocity.VelocityContext;


import com.dotemplate.core.server.CanvasImageCache;
import com.dotemplate.core.server.Dependencies;
import com.dotemplate.core.server.PurchaseInfo;
import com.dotemplate.core.server.affiliate.Affiliate;
import com.dotemplate.theme.server.css.Stylesheet;
import com.dotemplate.theme.shared.Theme;


public class ThemeContextWrapper extends VelocityContext implements ThemeContext {

	private ThemeContext innerContext ;
	
	protected ThemeContextWrapper(){}

	protected ThemeContextWrapper ( ThemeContext themeContext ) {
		super( themeContext ) ;	
		innerContext = themeContext ;
	}

	
	@Override
	public Theme getDesign () {
		return innerContext.getDesign ();
	}
		

	@Override
	public void preprocess () {
		innerContext.preprocess () ;
	}


	@Override
	public void addRowScript ( String id, String js ) {
		innerContext.addRowScript ( id, js ) ;
	}


	@Override
	public void clearRowScripts () {
		innerContext.clearRowScripts () ;
		
	}

	@Override
	public Collection< String > getRowScripts () {
		return innerContext.getRowScripts ();
	}


	@Override
	public ThemeRefresh getThemeRefresh () {
		return innerContext.getThemeRefresh ();
	}


	@Override
	public void addScript ( String url ) {
		innerContext.addScript ( url ) ;
		
	}


	@Override
	public Collection<String> getScripts () {
		return innerContext.getScripts ();
	}


	@Override
	public void updateRandom () {
		innerContext.updateRandom () ;
	}


	@Override
	public boolean isExportMode () {
		return innerContext.isExportMode();
	}

	
	@Override
	public CanvasImageCache getCanvasImageCache() {
		return innerContext.getCanvasImageCache();
	}
	
	
	@Override
	public CssImageCache getCssImageCache() {
		return innerContext.getCssImageCache() ;
	}
	
	
	@Override
	public Stylesheet getStylesheet() {
		return innerContext.getStylesheet();
	}

	@Override
	public void setStylesheet( Stylesheet stylesheet ) {
		innerContext.setStylesheet( stylesheet ) ;
	}

	@Override
	public Dependencies getDependencies() {
		return innerContext.getDependencies();
	}
	
	@Override
	public PurchaseInfo getPurchaseInfo() {
		return innerContext.getPurchaseInfo() ;
	}
	
	@Override
	public void setPurchaseInfo(PurchaseInfo purchaseInfo) {
		innerContext.setPurchaseInfo( purchaseInfo ) ;
	}

	@Override
	public LinkedHashMap<String, GoogleFont> getGoogleFonts() {
		return innerContext.getGoogleFonts();
	}

	@Override
	public Affiliate getAffiliate() {
		return innerContext.getAffiliate();
	}
	
	@Override
	public void setAffiliate(Affiliate affiliate) {
		innerContext.setAffiliate( affiliate );
	}

	@Override
	public Collection<String> getPolyfills() {
		return innerContext.getPolyfills();
	}

	@Override
	public void addPolyfill(String url) {
		innerContext.addPolyfill( url ) ;
		
	}

	
}
