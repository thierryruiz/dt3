package com.dotemplate.theme.server;

import org.apache.commons.pool.PoolableObjectFactory;

import com.dotemplate.core.server.DesignDescriptorReaderPool;
import com.dotemplate.theme.shared.Theme;


public class ThemeDescriptorReaderPool extends DesignDescriptorReaderPool< Theme > {
	
	
	@Override
	protected PoolableObjectFactory createFactory() {
		
		return new PoolableObjectFactory () {
			public void activateObject( Object o ) throws Exception {
			}

			public void destroyObject( Object o ) throws Exception {
			}

			public Object makeObject() throws Exception {
				
				ThemeDescriptorReader tr  = new ThemeDescriptorReader() ;
				return tr;
			
			}

			public void passivateObject( Object o ) throws Exception {
				( ( ThemeDescriptorReader ) o ).reset() ; 
			}

			public boolean validateObject( Object o ) {
				return true;
			}
		} ;
	}
	
}
