package com.dotemplate.theme.server.css;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.util.ConcurrentTaskContext;
import com.dotemplate.theme.server.ThemeApp;

public class CSSParser {
	
	private static ParseCssTaskExecutor executor = ( ParseCssTaskExecutor ) 
		ThemeApp.getSingleton ( ParseCssTaskExecutor.class ) ;
	
	private final static Log log = LogFactory.getLog ( CSSParser.class );
	
	
	public static Stylesheet parse ( String css ) throws AppException {
		
		List< ConcurrentTaskContext > taskContexts = new ArrayList< ConcurrentTaskContext >() ;
		
		ParseCSSTask.Context taskContext = new ParseCSSTask.Context() ;

		taskContexts.add( taskContext ) ;
		taskContext.setCss( css ) ;
		
		List < Future< Stylesheet > > results;
		
		try {
			results = executor.execute ( taskContexts );
			return results.get( 0 ).get() ;
		}
		
		
		catch (Exception e) {
			log.debug( "Failed to parse the following css content\n\n" + css + "\n\n" , e ) ;
			throw new AppException ( "Failed to parse stylesheet", e ) ;
		}
		
	}
	

}
