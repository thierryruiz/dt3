package com.dotemplate.theme.server.css;


public class CSSParseException extends RuntimeException {
	
	private static final long serialVersionUID = 7065126482659713471L;

	public CSSParseException( String s ) {
		super( "Failed to parse CSS '" + s + "' ") ; 
	}
	
	
}
