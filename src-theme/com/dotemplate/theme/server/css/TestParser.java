package com.dotemplate.theme.server.css;

import java.io.File;

import org.apache.commons.io.FileUtils;

import com.dotemplate.dt.server.DoTemplateApp;



public class TestParser {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		( new DoTemplateApp() ) .init()   ;
		
		SimpleCSSParser cssParser ;
	
		cssParser = new SimpleCSSParser() ;
		
		try {
			
			Stylesheet s = cssParser.parseStyleSheet( FileUtils.readFileToString( new File( "../testCssParser.css" ) )  ) ;
			
			CSSHelper.print( s, System.out, true, true ) ;
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
				
		
		

	}

}
