package com.dotemplate.theme.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.CachedImage;
import com.dotemplate.core.server.CachedImage.Format;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.svg.JPEGTranscoder;
import com.dotemplate.core.server.svg.PNGTranscoder;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;



public class GetCssImage extends BaseHttpServlet {

	private static final long serialVersionUID = 1L;

	public static final String URL_PREFIX = "/dt/cssImg?src=" ;
       
	private static Log log = LogFactory.getLog ( GetCssImage.class );
	
	
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) 
		throws ServletException, IOException {

		if ( !checkDesignSession( request, response, false ) ){
			response.setStatus( HttpServletResponse.SC_NOT_MODIFIED ) ;
		}
		
		nocache( response ) ;
		
		String img = request.getParameter( "src" ) ;
		
		if ( img == null ) {
			
			response.setStatus( HttpServletResponse.SC_BAD_REQUEST ) ;
			return ;
		}
		
		if ( log.isDebugEnabled () ) {
			log.debug (  "Get CSS image " + img  ) ;
		}
		
		String contentType = StringUtils.endsWith ( img, ".png" ) ? "image/png" : "image/jpeg" ;
		
		// dynamic content so avoid client side caching
		//response.setHeader( "Cache-Control", "no-cache" ); 
		//response.setHeader( "Pragma", "no-cache" ); 
		//response.setHeader( "Expires", "-1" ); 
		
		response.setContentType( contentType );
	
		
		// try to get image from cache 
		CachedImage cachedImage = ThemeApp.getThemeContext ().getCssImageCache ().getImage( img ) ;

		if ( cachedImage == null ){
			
			throw new AppRuntimeException( "No image '" + img + "' found in cache" ) ;
			
		}
		
		if ( cachedImage.isChanged() ){
			
			if ( log.isInfoEnabled() ){
				log.info( "Get image " + img + " from cache (changed)" ) ;
			}
			
			ServletOutputStream os = response.getOutputStream () ;
			
			os.flush () ;
			
			try {
				
				if ( cachedImage.getFormat() == Format.PNG ){
				
					PNGTranscoder.writeImage ( cachedImage.getImage(), os ) ;
				
				} else {
					
					JPEGTranscoder.writeImage ( cachedImage.getImage(), 0.95f, os ) ;
				
				}
				
			} catch (Exception e) {
				
				log.error( "Error while reloading image " + img ) ;
			
			}
		} else {
			
			if ( log.isInfoEnabled() ){
				log.info( "Get image " + img + " (no change)" ) ;
			}
			
			response.setStatus( HttpServletResponse.SC_NOT_MODIFIED ) ;
			
			
		}
	}

}
