package com.dotemplate.theme.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.web.BaseHttpServlet;

public class PageNotFound extends BaseHttpServlet {

	private static final long serialVersionUID = 3765978745696149755L;

	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( PageNotFound.class );

	protected void doGet( HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		nocache( response );
		
		super.forward404( request, response ) ;
		
	}

	
}
