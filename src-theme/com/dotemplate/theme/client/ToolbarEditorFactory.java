package com.dotemplate.theme.client;

import com.dotemplate.core.client.editors.EditorControlStyle;
import com.dotemplate.core.client.editors.EditorFactory;

public class ToolbarEditorFactory extends EditorFactory {

	
	private static ToolbarEditorFactory instance  ;
		
	
	public static ToolbarEditorFactory get() {
		
		if ( instance == null ) {
			instance = new ToolbarEditorFactory() ;
		}
		
		return instance ;
		
	}
	
	
	public EditorControlStyle getEditorControlStyle() {
		return  EditorControlStyle.TOOLBAR_STYLE ;
	}

	
	
}
