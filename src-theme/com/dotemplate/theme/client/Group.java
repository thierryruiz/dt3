package com.dotemplate.theme.client;

import java.util.LinkedHashMap;

import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.client.widgets.GroupTab;


public class Group {

	private String name ;
	
	private LinkedHashMap< String, PropertySet > sets ;
	
	private LinkedHashMap< String, PropertySetEditor > editors ;
	
	private GroupTab widget ;
	
	
	public Group( String name ) {
		this.name = name ;
		sets = new LinkedHashMap< String, PropertySet >() ;
		editors = new LinkedHashMap< String, PropertySetEditor >() ;
		//widget = new GroupTab ( this ) ;
	}
		
	public void addSet( PropertySet set ) {
		sets.put( set.getUid(), set ) ;
	}
	
	public void addEditor( PropertySetEditor editor ) {
		editors.put( editor.getProperty().getUid(), editor ) ;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LinkedHashMap<String, PropertySet> getSets() {
		return sets;
	}

	public void setSets(LinkedHashMap<String, PropertySet> sets) {
		this.sets = sets;
	}

	public LinkedHashMap<String, PropertySetEditor> getEditors() {
		return editors;
	}

	public void setEditors(LinkedHashMap<String, PropertySetEditor> editors) {
		this.editors = editors;
	}
	
	
	public String getLabel(){
		return name ;
	}
	
	public GroupTab getWidget() {
		return widget;
	}
	
	
}
