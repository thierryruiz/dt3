package com.dotemplate.theme.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.editors.MutablePropertySetEditor;
import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertyEditorMap;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.client.events.HasSchemeChangeEventHandlers;
import com.dotemplate.core.client.events.PropertyChangeEvent;
import com.dotemplate.core.client.events.SchemeChangeEvent;
import com.dotemplate.core.client.events.SchemeChangeEventHandler;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.dotemplate.core.shared.properties.BooleanProperty;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.dotemplate.core.shared.properties.ImageProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.client.widgets.ThemeControlPanel;
import com.dotemplate.theme.client.widgets.ThemeTabPanel;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.shared.ThemeUpdate;
import com.dotemplate.theme.shared.command.ChangeScheme;
import com.dotemplate.theme.shared.command.ChangeSchemeResponse;
import com.dotemplate.theme.shared.command.GetOnReadyScripts;
import com.dotemplate.theme.shared.command.UpdateProperty;
import com.dotemplate.theme.shared.command.UpdatePropertyResponse;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;



@SuppressWarnings("unused")
public class ThemeEditor extends SelectableSetEditorCreator {

	private static final long serialVersionUID = -8899846162090045634L;

	protected PropertySetEditor currentEditor ;
	
	protected ArrayList< String  > unlockedImages ; 
	

	public ThemeEditor( Theme theme ) {
		
		controlPanel = new ThemeControlPanel() ; 
		
		allEditors = new PropertyEditorMap() ;
		
		decorator = new ThemeDecorator() ;
		
		unlockedImages = new ArrayList< String >() ;
		
		theme.execute( this ) ;
		
		decorator.addClickHandler( new ClickHandler( ) {
			@Override
			public void onClick( ClickEvent event ) {
				editSet( decorator.getSelected() ) ;
			}
		}) ;
		
		controlPanel.show() ;
	
	}
		
	
	
	@Override
	public boolean startSet( PropertySet set ) {
		
		if ( Property.CANVAS == set.getType() ){
			
			Property property ;
			ImageProperty imageProperty ;
			
			for ( Iterator< Property > it = set.getProperties ().iterator () ; it.hasNext () ; ){

				property = it.next () ;
				
				if ( property.getEnable() != null && !property.getEnable().booleanValue() ){
					continue ;
				}
			
				if ( Graphic.IMAGE == property.getType () ){
					
					imageProperty = ( ImageProperty ) property ;
					
					unlockedImages.add( imageProperty.getValue() ) ;
					Log.debug("Unlocking image " + imageProperty.getValue() ) ;
					
				}
			}
			
		}
		
		return super.startSet( set );
	}

	
	@Override
	public void onPropertyChanged( PropertyChangeEvent e ) {
		
		Property p =  e.getEditor().getProperty() ;
		
		boolean mutable = p.isSet() && ( ( PropertySet ) p ).mutable() ;
		
		if ( mutable ) {
			
			updateMutableProperty( ( PropertySet ) p, 
					e.getMutablePropertyDeletedProperties(), 
						( MutablePropertySetEditor ) e.getEditor() ) ;
			
		} else {
			
			updateProperty( p ) ;
			
		}
		
	}
	
	
	protected void removeEditors( PropertyMap properties ){
		
		PropertyEditor<?> editor ;
		
		for ( Property p : properties.values() ){
			
			editor =  allEditors.remove( p.getUid() ) ;
						
			if ( editor != null ){
				editor.remove() ;
				editor.removeHandler( this ) ;
				editor = null ;	
			}
			
			
			if ( p.isSet() ){
				
				removeEditors( ( ( PropertySet ) p ).getPropertyMap() ) ;
			
			}
			
		}		
	}	
	
	
	public void editSet( PropertySet set ){
		
		if ( set == null ) return ;
	
		if ( set.getType() == Property.CANVAS ){
			
			Boolean isSlide = ( Boolean ) set.get( "isSlide" ) ;
			
			if ( isSlide == null || !isSlide ){
				
				Client.get().editCanvas( ( CanvasProperty ) set ) ;
				return ;
			
			}
			
		}
			
		if ( currentEditor != null ){
			currentEditor.getControl().unselect() ;
		}
		
		controlPanel.getTabPanel().showTab( set ) ;

		currentEditor = ( PropertySetEditor )
			allEditors.get( set.getUid() ) ;

		currentEditor.getControl().select() ;
			

	}

		
	
	
	public void updateProperty ( final Property property  ) {
		
		UpdateProperty updateProperty = new UpdateProperty() ;
		
		updateProperty.setDesignUid( ThemeClient.get().getDesign().getUid() ) ;
		
		updateProperty.setProperty ( property ) ;
		
		( new RPC< UpdatePropertyResponse >( ThemeClient.get().getDesignService() ) ).execute ( 
				updateProperty, new CommandCallback< UpdatePropertyResponse >() {
			
			
			@Override
			public void onSuccess ( UpdatePropertyResponse response ) {
								
				ThemeUpdate update =  response.getClientRefresh () ;
	
				//PropertySetEditor e;

				refresh ( update ) ;

				/* moved to refresh 
				for ( PropertySet set : update.getUpdatedProperties() ){
					
					e = ( PropertySetEditor ) allEditors.get( set.getUid() ) ;
					
					if ( e != null ){
						// update editors's state 
						e.setProperty( set ) ;
					}
				}
				*/
				
				
				for ( PropertyEditor< ? > pe : allEditors.values() ){
					
					if ( pe.getProperty().isSet() ) {
						
						decorator.decorate( ( PropertySet ) pe.getProperty() ) ;
						
					}
					
				}
				
			}
			
		}) ;
		
	}
	
	
	public void updateMutableProperty ( 
			final PropertySet property, 
			final PropertyMap deletedProperties, 
			final MutablePropertySetEditor mutableEditor ) {
		
		
		UpdateProperty updateProperty = new UpdateProperty() ;
		
		updateProperty.setDesignUid( ThemeClient.get().getDesign().getUid() ) ;
		
		updateProperty.setProperty ( property ) ;
		
		
		( new RPC< UpdatePropertyResponse >( ThemeClient.get().getDesignService() ) ).execute ( 
				updateProperty, new CommandCallback< UpdatePropertyResponse >() {
			
					
			@Override
			public void onSuccess ( UpdatePropertyResponse response ) {
				
				ThemeUpdate update =  response.getClientRefresh () ;
				
				refresh ( update ) ;
				
				PropertySetEditor e;
				
				for ( PropertySet set : update.getUpdatedProperties() ){
					
					e = ( PropertySetEditor ) allEditors.get( set.getUid() ) ;
					
					if ( e != null ){
						// update editors's state 
						e.setProperty( set ) ;
					}
					
				}
				
				
				rebuildMutableEditor( mutableEditor, deletedProperties ) ;
			
				// redecorate in case of layout or size changed
				for ( PropertyEditor< ? > pe : allEditors.values() ){
					
					if ( pe.getProperty().isSet() ) {
						decorator.decorate( ( PropertySet ) pe.getProperty() ) ;
					}
					
				}
				
			}
			
		}) ;
		
	}
	
	
	
	protected void rebuildMutableEditor( 
			MutablePropertySetEditor mutableEditor,
			PropertyMap deletedProperties ){
		
		if ( deletedProperties != null  ){
			
			removeEditors( deletedProperties ) ;
		
		}
		
		
		LinkedHashMap< String,  PropertyEditor < ? > > childEditors = 
			new LinkedHashMap< String, PropertyEditor< ? > > () ;
		
		
		for ( Property p : mutableEditor.getProperty().getProperties() ){
			
			if ( p.isSet() && ( ( PropertySet ) p ).selectable()  ){
				( ( PropertySet ) p ).execute( ThemeEditor.this ) ;
			
			} else {
				
				createEditors( p, childEditors, mutableEditor.getEditorId()  ) ;
			
			}
		}
		

		PropertySetEditor rootEditor = mutableEditor.getParentEditor() ;
		
		int index ;
		
		if ( rootEditor == null ) {
			
			rootEditor = mutableEditor ;
			index = 1 ;
		
		} else {
		
			index =  rootEditor.getControl().indexOf( mutableEditor.asWidget() ) + 1  ;
		
		}
		
		
		ThemeTabPanel tabPanel = controlPanel.getTabPanel() ;
		
		for ( PropertyEditor< ? > childEditor : childEditors.values() ){
			
			childEditor.setParentEditor( rootEditor ) ;
			
			tabPanel.insert( rootEditor,  childEditor, index ) ;
			
			index++ ;
			
		}
		
	}
	
	
	
	protected void refresh( ThemeUpdate refresh ){
		
		PropertySetEditor e;
		
		for ( PropertySet set : refresh.getUpdatedProperties() ){
			
			e = ( PropertySetEditor ) allEditors.get( set.getUid() ) ;
			
			if ( e != null ){
				// update editors's state 
				e.setProperty( set ) ;
			}
		}

				
		StyleInjector.inject( refresh.getCss() );
		
		for ( Entry<String, String > entry : refresh.getHtml().entrySet () ) {
			updateHtml( entry.getKey (), entry.getValue () ) ;
		}
		
		Utils.evalScript ( refresh.getJs () ) ;
		
	}
	
	
	
	protected static void updateHtml ( String selector, String html ) {
		Element el = Utils.selectDOM (  selector ).getItem ( 0 ) ;
		// clear it
		DOM.setInnerHTML ( ( com.google.gwt.user.client.Element ) el, html ) ;
	}


	public PropertyEditorMap getPropertyEditors() {
		return allEditors;
	}


	public void redecorate() {
		decorator.redecorate() ;
	}
	
	
	public ArrayList< String > getUnlockedImages() {
		return unlockedImages;
	}
	
	
}

