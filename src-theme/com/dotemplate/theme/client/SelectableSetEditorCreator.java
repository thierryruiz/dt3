package com.dotemplate.theme.client;

import java.util.LinkedHashMap;

import com.dotemplate.core.client.editors.EditorFactory;
import com.dotemplate.core.client.editors.PropertyEditor;
import com.dotemplate.core.client.editors.PropertyEditorMap;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.shared.DesignTraverser;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.theme.client.widgets.ThemeControlPanel;
import com.dotemplate.theme.client.widgets.ThemeTabPanel;


public abstract class SelectableSetEditorCreator implements DesignTraverser, PropertyChangeEventHandler {
	
	private static final long serialVersionUID = -8120466266722908606L;

	protected ThemeDecorator decorator ;
	
	protected PropertyEditorMap allEditors ;
	
	protected ThemeControlPanel controlPanel ;
	
	
	@Override
	public boolean startProperty( Property property ) {	
		return true;
	}
	
	
	@Override
	public boolean startSet( PropertySet set ) {
		
		boolean editable = set.getEditable () != null && set.getEditable () ;

		if ( !editable || !set.selectable() ) return true ;
		
		EditorFactory factory = ToolbarEditorFactory.get() ; 
		
		PropertySetEditor rootEditor = ( PropertySetEditor ) factory.createPropertyEditor( set ) ; 
		
		rootEditor.setEditorId( set.getName().replace( ' ', '_' ) ) ;
						
		allEditors.put( set.getUid(), rootEditor ) ;
		
		rootEditor.addHandler( this ) ;
		
		decorator.decorate( set ) ;
		
		
		LinkedHashMap< String,  PropertyEditor < ? extends Property > > childEditors = new LinkedHashMap< String, PropertyEditor< ? extends Property  > > () ;
		
		for ( Property pp : set.getProperties() ){

			createEditors( pp, childEditors, rootEditor.getEditorId() ) ;
			
		}
		
		buildView( rootEditor, childEditors ) ;
		
		return true;
	
	}


	@Override
	public boolean endSet(PropertySet set) {
		return true;
	}


	@Override
	public boolean endProperty(Property property) {
		return true;
	}
	
	
	protected void buildView ( 
			PropertySetEditor rootEditor,
			LinkedHashMap < String, PropertyEditor< ? extends Property > > childEditors ) {
	
		ThemeTabPanel tabPanel = controlPanel.getTabPanel() ;
	
		tabPanel.add( rootEditor ) ;
		
		for ( PropertyEditor< ? extends Property > editor : childEditors.values() ){
			
			editor.setParentEditor( rootEditor ) ;
			
			tabPanel.add( rootEditor,  editor ) ;

		}
		
	}


	protected void createEditors ( Property p, LinkedHashMap< String,  
			PropertyEditor < ? extends Property > > childEditors, String editorId ){
		
		boolean editable = p.getEditable () != null && p.getEditable () ;

		//boolean enable = p.getEnable() != null &&  p.getEnable () ;
		
		
		EditorFactory factory = ToolbarEditorFactory.get() ; 
		
		if ( !editable /*|| ! enable*/  ) return ;
		
		
		// ignored, will be visited
		if ( p.isSet() && ( ( PropertySet ) p ).selectable() ) return ;
		
		PropertyEditor < ? extends Property > e = factory.createPropertyEditor( p ) ;

		if ( e == null ) return ;
		
		editorId = editorId + '_' + p.getName().replace(' ', '_' ) ;
		e.setEditorId( editorId ) ;
		
		
		allEditors.put( p.getUid(), e ) ;
		
		
		e.addHandler( this ) ;
		
		childEditors.put( p.getUid(), e )   ;

		
		if ( p.isSet() ){
			
			for ( Property pp : ( ( PropertySet ) p ).getProperties() ){
				
				createEditors( pp, childEditors, editorId ) ;
				
			}
		}
	}


	



}
