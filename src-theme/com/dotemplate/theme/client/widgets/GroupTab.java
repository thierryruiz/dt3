package com.dotemplate.theme.client.widgets;


import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.editors.PropertySetEditor;
import com.dotemplate.core.shared.properties.PropertySet;
import com.extjs.gxt.ui.client.widget.Component;


public class GroupTab extends ThemeTabItem {
	
	
	public GroupTab( String label ) {
		super( label ) ;	
	}
	
	
	public void addEditor( PropertySetEditor editor ){
		
		Log.debug( "Add editor " + editor.getProperty().getName() ) ;
		
		if ( extToolbar == null ){
			init() ;
		}
		
		extToolbar.add( ( Component ) editor.asWidget() ) ;
		
		PropertySet set = editor.getProperty() ;
		
		boolean enable = set.getEnable() != null &&  set.getEnable () ;
		
		if( enable ){
			editor.enable() ;
		} else {
			editor.disable() ;
		}
		
		extTabItem.layout( true ) ;		
		extTabItem.show() ;
		
	}
	
	
	
}
