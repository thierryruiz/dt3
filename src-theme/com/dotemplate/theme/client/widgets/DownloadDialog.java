package com.dotemplate.theme.client.widgets;

import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.widgets.Window;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Frame;


public class DownloadDialog extends Window {
	
	private static DownloadDialog _instance ;
	
	private FlowPanel root = new FlowPanel();
	
	
	private DownloadDialog() {
		Log.debug ( "Download dialog" ) ;
		setSize( 800, 500 ) ;
		setPadding ( 0 ) ;
		setHeading ( "Download" ) ;
		setModal( true ) ;
		build() ;
	}

	
    public static DownloadDialog get() {
    	
    	if ( _instance == null ){
    		_instance = new DownloadDialog() ;
    	}
    	
    	return _instance ;
    }

    
	private void build () {
		add (  root ) ;
		
		Frame frame = new Frame() ; 
		frame.setWidth ( "100%" ) ;
		
		// du to FF 3 issue, by default display=none for gwt-Frame class
		
		frame.setUrl ( "dt/download" ) ;
		frame.setStyleName ( "ez-dwld-frame" ) ;
		root.add ( frame ) ;
	
	}
	
}
