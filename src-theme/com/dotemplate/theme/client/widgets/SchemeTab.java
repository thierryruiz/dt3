package com.dotemplate.theme.client.widgets;

import com.dotemplate.core.client.editors.EditorControlStyle;
import com.dotemplate.core.client.editors.PropertyEditorControl;
import com.dotemplate.core.client.widgets.colorpicker.SchemeSelector;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.theme.client.ThemeClient;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.button.ButtonGroup;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;


public class SchemeTab extends ThemeTabItem implements SelectDesignResourceHandler< Scheme >{

	protected ButtonGroup extButtons ;
	
	public SchemeTab() {
		
		super( "Color scheme" ) ;
		
		init() ; 
		
		extTabItem.setIcon( IconHelper.createPath( "client/icons/themetoolbar/scheme-tab.png", 16, 16 ) ) ;

		extButtons = new ButtonGroup( 1 ) ;
		extButtons.setHeading( "Color scheme" ) ;
		
		extToolbar.add( extButtons ) ;
		
		PropertyEditorControl schemeBtn = new PropertyEditorControl( EditorControlStyle.TOOLBAR_STYLE ) ;
		schemeBtn.setText( "Change scheme" ) ;
		schemeBtn.setIcon ( "client/icons/themetoolbar/schemes.png" ) ;

		schemeBtn.addClickHandler( new ClickHandler( ) {
			@Override
			public void onClick( ClickEvent arg0 ) {
				SchemeSelector.show ( SchemeTab.this, ThemeClient.get().getColorScheme() ) ;
			}
		}) ;
		
		extButtons.add( schemeBtn.asWidget() ) ;
					
		extTabItem.layout( true ) ;
		extTabItem.show();
	
	}


	@Override
	public void onDesignResourceSelected( Scheme selectedScheme ) {
		ThemeClient.get().changeColorScheme( selectedScheme );
	}
	
	
}
