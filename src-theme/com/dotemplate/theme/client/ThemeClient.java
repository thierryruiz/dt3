package com.dotemplate.theme.client;


import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.events.SchemeChangeEvent;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.command.GetScheme;
import com.dotemplate.core.shared.command.GetSchemeResponse;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.theme.client.widgets.ThemeControlPanel;
import com.dotemplate.theme.shared.Theme;
import com.dotemplate.theme.shared.command.ChangeScheme;
import com.dotemplate.theme.shared.command.ChangeSchemeResponse;
import com.dotemplate.theme.shared.command.GetOnReadyScripts;
import com.dotemplate.theme.shared.command.GetOnReadyScriptsResponse;
import com.dotemplate.theme.shared.command.GetTheme;
import com.dotemplate.theme.shared.command.GetThemeResponse;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.RootPanel;


public abstract class ThemeClient extends Client< Theme > {
	
	public static ThemeClient instance ;
	
	protected ThemeEditor editor ;
	
	protected ThemeControlPanel controlPanel ;
	
	
	public ThemeClient() {
		instance = this ;
	}
	
	public static ThemeClient get() {
		return ( ThemeClient ) instance ;
	}
		
	
	public ThemeEditor getEditor() {
		return editor ;
	}
	
	
	protected void loadTheme() {
		
		Log.debug( "Loading theme..." ) ;
		
		GetTheme getTheme = new GetTheme() ;
		
		RootPanel.get( "preloader" ).setVisible( false ) ;
		
				
		( new RPC< GetThemeResponse >( designService ) ).execute( getTheme,  
				new CommandCallback< GetThemeResponse >() {
			
			@Override
			public void onSuccess ( GetThemeResponse response ) {
				
				onThemeLoaded( response.getTheme () ) ;
			
			}
		}) ;
		
	}

	
	protected void onThemeLoaded( final Theme theme ){
		
		Log.debug ( "Theme uid[" + theme.getUid () + "] loaded." ) ;
		
		setDesign( theme ) ;
		
		GetScheme getScheme = new GetScheme() ;
		
		
		
		getScheme.setDesignUid( theme.getUid() ) ;
		getScheme.setName( theme.getScheme() ) ;
		
		( new RPC< GetSchemeResponse >( designService ) ).execute( getScheme, new CommandCallback< GetSchemeResponse >() {
			public void onSuccess( GetSchemeResponse response ) {
				Client.get().setColorScheme( response.getScheme() ) ; 
				editor = new ThemeEditor( theme ) ;				
			};
		}); 

	}
	
	/*
	protected static void reloadScript ( String script ) {
		Utils.evalScript ( script ) ;
		// Utils.evalScript ( "Cufon.replace('h1', {fontFamily: 'Liberation Sans',fontSize: '40px'});Cufon.refresh();") ;
	}*/	
	
	
	public static String getThemeURL() {
		return "sites/" + instance.getDesign().getUid ();
	}

	
	public static Theme getTheme() {
		return ( Theme ) instance.getDesign() ;
	}

	
	@Override
	public void updateProperty( Property property ) {
		editor.updateProperty( property ) ;
	}
	
	
	public static native void exportNativeMethods() /*-{
		$wnd.loadPage = function( page,id ){
			@com.dotemplate.theme.client.ThemeClient::loadPage(Ljava/lang/String;Ljava/lang/String;)(page,id);
		};
	}-*/;


	
	public static void loadPage( String page, final String id ){
		
		RequestBuilder loadPageRequest =  new RequestBuilder( RequestBuilder.GET, 
					getThemeURL() + "/" + page + ".html?" + System.currentTimeMillis() );
		
		
		try {
			
			loadPageRequest.sendRequest( null, new RequestCallback() {
				
				@Override
				public void onResponseReceived( Request arg0, Response resp ) {
					
					DOM.setInnerHTML( Utils.selectDOM( id ).getItem( 0 ), resp.getText( ) ) ;
					
					// replay on load js 
					replayOnloadScripts() ;
					
					// redecorate
					ThemeClient.get().getEditor().redecorate() ;
					
				}
				
				@Override
				public void onError(Request arg0, Throwable arg1) {
				}
				
				
			}) ;
		
		} catch ( RequestException e ) {
			
		}
		
	}	

	
	
	private static void replayOnloadScripts(){
		
		GetOnReadyScripts getCustomJs = new GetOnReadyScripts() ;
		
		getCustomJs.setDesignUid( ThemeClient.get().getDesign().getUid() ) ;
		
		( new RPC< GetOnReadyScriptsResponse >( ThemeClient.get().getDesignService() ) ).execute ( 
				getCustomJs, new CommandCallback< GetOnReadyScriptsResponse >() {
					
				public void onSuccess( GetOnReadyScriptsResponse response ) {
					Utils.evalScript ( response.getJs () ) ;
				};
		}) ;
		
	}

	
	
	
	public void changeColorScheme ( final Scheme scheme  ) {
		
		ChangeScheme changeScheme  = new ChangeScheme() ;
		
		changeScheme.setDesignUid( ThemeClient.get().getDesign().getUid() ) ;
		changeScheme.setSchemeName( scheme.getName() ) ;
		
		
		( new RPC< ChangeSchemeResponse >( ThemeClient.get().getDesignService() ) ).execute ( 
				changeScheme, new CommandCallback< ChangeSchemeResponse >() {
			
					
			@Override
			public void onSuccess ( ChangeSchemeResponse response ) {
				
				setColorScheme( scheme ) ;

				//editor.refresh ( response.getClientRefresh () ) ;
				
				//Utils.refrehStyleSheet( ""  +System.currentTimeMillis() ) ;
				
				StyleInjector.inject(  response.getStylesheet() ) ;
				
				fireEvent( new SchemeChangeEvent( scheme )) ;
				
			}
			
		}) ;
		
	}
	
	
	
	@Override
	protected void ensureCanvasEditor() {
		
		super.ensureCanvasEditor();
		
		canvasEditorWindow.setUnlockedImages( editor.getUnlockedImages() ) ;	
		
	}
	
}
