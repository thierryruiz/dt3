<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	String path = request.getContextPath();
	String basePath = "http://"+request.getServerName()+":"+request.getServerPort()+path+"/" ;
%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<head>
	<title>doTemplate</title>
	<base href="<%=basePath%>" />	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
	<meta http-equiv="Cache-Control" content="no-cache"/>
    <meta http-equiv="Expires" content="-1"/>
	<meta http-equiv="pragma" content="no_cache"/>
	<meta http-equiv="Content-Language" content="en-us" />
</head>


<body>
	<style>
	<!--
	body { font-family:"helvetica neue",arial,sans-serif;color:#15428B;font-size:13px;}
	a:link, a:visited {text-decoration:none;color:#33CC33}
	-->
	</style>


	<div style="text-align:center;margin-top:30px">

		<h2>Buy this template</h2>
		
		<form action="${paypal.url}" method="post" target="_top">
			<input type="hidden" name="cmd" value="_s-xclick">
			<input type="hidden" name="hosted_button_id" value="${paypal.buttonId}">
			<input id="notify_url" type="hidden" name="notify_url" value="${paypal.notifyUrl}">
			<input type="image" src="https://www.paypal.com/en_US/FR/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="">
			<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
		</form>
		
		
		
		<!--
		<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="NVE3VXP7XVWWS">
<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.sandbox.paypal.com/fr_XC/i/scr/pixel.gif" width="1" height="1">
</form>
-->


	</div>
</body>
</html>