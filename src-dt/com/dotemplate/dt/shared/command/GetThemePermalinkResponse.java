package com.dotemplate.dt.shared.command;

import com.dotemplate.core.shared.frwk.Response;


public class GetThemePermalinkResponse implements Response {

	private static final long serialVersionUID = 1L;
		
	private String permalink ;
	
	public String getPermalink() {
		return permalink;
	}
	
	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}
	
	
}
