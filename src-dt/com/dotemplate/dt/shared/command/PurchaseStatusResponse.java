package com.dotemplate.dt.shared.command;

import com.dotemplate.core.shared.frwk.Response;


public class PurchaseStatusResponse implements Response {

	private static final long serialVersionUID = 827481250419474943L;

	private int status ;
	
	private String error ;

	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	
}
