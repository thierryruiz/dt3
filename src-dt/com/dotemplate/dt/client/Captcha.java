package com.dotemplate.dt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;

public class Captcha implements EntryPoint {

	private RequestBuilder requestBuilder ;
	
	@Override
	public void onModuleLoad() {
		createTemplate() ;
	}

	public void createTemplate(){
		
		String createUrl = Window.Location.getHref() ;
		
		final String editUrl = createUrl.substring( 0 , createUrl.indexOf("/dt/create" ) )  + "/dt/edit" ;
		
		requestBuilder =  new RequestBuilder( RequestBuilder.POST, createUrl );
		
		try {
			
			requestBuilder.sendRequest( null, new RequestCallback() {
				@Override
				public void onResponseReceived( Request request, Response response ) {
					
					if ( "NOK".equals( response.getText() ) ) {
						displayError();
						return ;
					}
					
					Window.Location.replace( editUrl  );
				}
				
				@Override
				public void onError(Request request, Throwable exception) {
					displayError();
				}
			
			});
			
		} catch ( RequestException e ) {
			Window.alert( "Oups an error occured! Sorry. Please contact us." ) ;
		}
		
		
		
		
	}
	
	
	private void displayError(){
		RootPanel.get( "error" ).setVisible(true) ;
		RootPanel.get( "loader").setVisible( false ) ;
	}
	
}
