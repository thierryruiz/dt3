package com.dotemplate.dt.client.view;

import com.dotemplate.core.client.frwk.View;
import com.google.gwt.event.dom.client.HasClickHandlers;


@Deprecated
public interface PurchaseStatusView extends View {

	HasClickHandlers getOkControl() ;
		
	void open( String userUri ) ;	
	
	void close();
	
}
