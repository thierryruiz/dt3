package com.dotemplate.dt.client.view;

import com.dotemplate.core.client.frwk.View;
import com.google.gwt.event.dom.client.HasClickHandlers;

public interface ConvertView extends View {

	HasClickHandlers getBuyControl() ;
	
	HasClickHandlers getDownloadControl() ;
	
	void open( String heading  ) ;
	
	void open( String heading, String export ) ;

	void close();
	
}
