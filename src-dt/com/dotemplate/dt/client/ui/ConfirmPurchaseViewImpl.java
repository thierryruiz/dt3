package com.dotemplate.dt.client.ui;

import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.dt.client.view.ConfirmPurchaseView;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

public class ConfirmPurchaseViewImpl implements ConfirmPurchaseView {
	
	private Window win ; 
		
	private FlowPanel layout ;
		
	private HorizontalPanel footer ;
	
	private Image continueBtn ;
	
	private static String _HTML = 
			
			"<p style='font-size:1.2em'>You will be redirected to <strong>Paypal (no Paypal account required)</strong>.</p>" +
			"<p style='font-size:1.2em;margin-top:20px'>After payment you can download or continue editing your template.</p>" +
			"<img src='https://www.paypalobjects.com/webstatic/mktg/logo/AM_SbyPP_mc_vs_dc_ae.jpg' border='0' style='height:80px;margin:10px auto'/>" ;
	
	public ConfirmPurchaseViewImpl() {
		
		Log.debug ( "Init confirm purchase dialog" ) ;
		
		win = new Window() ;

		win.setSize( 600, 400 ) ;
		win.setPadding ( 0 ) ;
		win.setHeading ( "" ) ;
		win.setModal( true ) ;
		win.setResizable(false) ;
		
		win.add( layout = new FlowPanel() ) ;
		
		layout.addStyleName ( "ez-dialog" ) ;
		
		HTML message ;
		layout.add( new HTML( "<h4>Purchase your template for <span>$9</span></h4>" ) ) ;
		layout.add(  message = new HTML( _HTML ) ) ;
		Utils.margins( message, "15px auto" ) ;
		
		layout.add( footer = new HorizontalPanel() ) ;
		Utils.margins( footer, "15px auto" ) ; 

		continueBtn = new Image( "images/dotemplate/confirmPurchase.png" ) ;
		Utils.style(continueBtn, "cursor" , "pointer" ) ;
		footer.add( continueBtn ) ;
		
		message.setWidth( "80%" ) ;
		
	}


	
	
	@Override
	public String getId() {
		return "dtConfirmPurchaseDialog" ;
	}

	@Override
	public Widget asWidget() {
		return win;
	}


	@Override
	public void open() {
		win.openCenter() ;
	}

	
	@Override
	public void close() {
		win.close() ;	
	}


	@Override
	public HasClickHandlers getConfirmControl() {
		return continueBtn;
	}


}
