package com.dotemplate.dt.tools;

import java.awt.Rectangle;
import java.io.File;

import org.apache.commons.io.FileUtils;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.svg.PNGTranscoder;
import com.dotemplate.dt.server.DoTemplateApp;

public class TestSVGError {
	
	public static void main( String[] args ) {
		
		try {
		
			( new DoTemplateApp() ).init() ;
			
			
			String svg = FileUtils.readFileToString( new File ( App.realPath( "../tmp/error.svg" ) ) ) ;
			
			PNGTranscoder.transcode( svg, new Rectangle ( 900, 600 ),  App.realPath( "svgerror.png" ) ) ;
		
		} catch ( Exception e ){

			e.printStackTrace() ;
			
		}
		
		
	}
	

}
