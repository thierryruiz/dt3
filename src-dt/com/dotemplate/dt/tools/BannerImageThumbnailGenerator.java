package com.dotemplate.dt.tools ;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;

import com.dotemplate.core.server.ImageResourceProvider;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.dotemplate.dt.server.DoTemplateApp;




/**
 * 
 * 
 * Generate the banner images thumbnail PNG > JPG ( height=100 ) 
 *  
 * @author truiz
 *
 */
public class BannerImageThumbnailGenerator  {
	
	private static Log log = LogFactory.getLog ( BannerImageThumbnailGenerator.class );
	
	private static ImageEngine imageEngine ;
	
	public static void main( String[] args ) {
		
		( new DoTemplateApp() ).init()   ;
		
		boolean all =  ( args.length > 0 && args[ 0 ] .equals( "all" ) ) ;
		
		
		
		ImageResourceProvider provider = (ImageResourceProvider) DoTemplateApp.getSingleton( ImageResourceProvider.class ) ;
		
		createImageEngine() ;
		
		for ( String tag : provider.getResourcesTags() ) {
			for ( ImageResource image : provider.list ( tag ) ){
				createThumbnail( image, all  ) ;
			}			
		}
		
	}



	private static void createThumbnail ( ImageResource image, boolean all ) {
		
		String path = ( image.getPath () ) .replace ( '/', File.separatorChar ) ;
		
		String output =  StringUtils.remove ( path , 
			"WEB-INF" + File.separator + "templates" + File.separator + "_images" + File.separator ) ;
		
		output = "images" + File.separator + output ;
		output = StringUtils.replace ( output, ".png", ".jpg" ) ;
		
		if ( new File ( output ).exists() && !all  ){
			return ;
		}
		
		
		/*
		File outputFolder = new File( output ).getParentFile () ;
		
		
		
		if ( !outputFolder.exists () ){
			try {
				FileUtils.forceMkdir ( outputFolder ) ;
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		} else {
			try {
				//FileUtils.cleanDirectory( outputFolder ) ;
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}*/
	
		
		
		try {
		
			imageEngine.setOutputHeight ( 100 ) ;
			
			imageEngine.generate ( image, path, output ) ;
	
			
		} catch ( Exception e ) {
			e.printStackTrace();
	
		}
	}

	
	private static void createImageEngine(){
		
		log.info( "Creating ImageEngine...") ;
		
		imageEngine = new ImageEngine() ;

		// initializing viewEngine. Absolute path must be set for each vm resource path
		Properties props = new Properties ();

		try {
			
			props.load ( new FileInputStream ( "WEB-INF/conf/render-engine.properties" )  );
		
		} catch ( Exception e ) {
			
			log.fatal ( "Image engine initialization failed ", e );
			return;
		
		}

		String vmPath = props.getProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH );

		props.setProperty ( VelocityEngine.FILE_RESOURCE_LOADER_PATH, vmPath  );
		try {
			
			imageEngine.init ( props );
		
		} catch ( Exception e ) {
			
			log.fatal ( "Image engine initialization failed.", e );
			return;
		
		}
	}


}
