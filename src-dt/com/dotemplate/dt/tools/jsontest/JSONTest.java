package com.dotemplate.dt.tools.jsontest;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.dotemplate.theme.server.category.CategoryContentMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;


public class JSONTest {


	public JSONTest (){
		
	}
	
	
	public static void main ( String[] args ){
		
		try {
			
			String json = FileUtils.readFileToString(new File ( 
					"WebContent/categories/generic/text/home.json") ) ;
			
			
			JsonElement root = new JsonParser().parse( json );
			
			Gson gson = new GsonBuilder().create() ;
	        
			
			CategoryContentMap contents = gson.fromJson( root, CategoryContentMap.class ) ;
	        
			
	        //System.out.println( contents.getContents().get(0).getBlocs().get(2).text ) ;
			
	        contents.init() ;
	        //System.out.println( contents.get("dt1").getBlocs().get(2).text ) ;
			
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
    
    
}


