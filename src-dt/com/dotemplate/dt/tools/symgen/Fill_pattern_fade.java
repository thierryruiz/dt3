package com.dotemplate.dt.tools.symgen;


public class Fill_pattern_fade extends Fill_pattern implements FillGradient {

	
	public Fill_pattern_fade( String patternId, String color, int sortIndex ) {
		super( patternId, color, sortIndex ) ;
	}


	@Override
	protected String getSymbolFolder() {
		return "pattern-fade-" + patternId ;	
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-pattern-fade"	;
	}
	
	
}
