package com.dotemplate.dt.tools.symgen;

public enum Colors {
	
	// from http://www.computerhope.com/htmcolor.htm

	WHITE( "white", "#FFFFFF" ),
	
	LIGHTERBLUE( "lighterblue", "#edf2f8" ),
	LIGHTBLUE( "lightblue", "#b5cce3" ),
	BLUE( "blue", "#90B2D5" ),
	BLUEST( "bluest", "#647A90" ),
	DARKBLUE( "bluegrey", "#4D6680" ),
	DARKERBLUE( "darkerblue", "#3A4754" ),
	
	LIGHTERGREEN ( "lightergreen", "#EDF8ED" ),
	LIGHTGREEN( "lightgreen", "#B5E3B5"),
	GREEN( "green", "#90D590" ),
	GREENER( "greener", "#649064" ),
	DARKGREEN( "darkgreen", "#4F724F" ),
	DARKERGREEN( "darkergreen", "#3A543A" ),
	
	GOLD( "gold", "#FBB948"),
	ORANGE( "orange", "#E14A01" ),
	RED( "red", "#C7141A" ),
	BROWN( "brown", "#964408"),
	DARKBROWN( "darkbrown", "#370000"),
	
	
	WARMGREY( "warmgrey", "#AEA795" ),
	WARMERGREY( "warmergrey", "#878273" ),	
	DARKWARMGREY( "darkwarmgrey", "#69655A" ),	
	LIGHTERGREY( "lightergrey", "#e3e0D4" ),
	LIGHTGREY( "lightgrey", "#cdcdcd" ),
	GREY( "grey", "#8A989B" ),
	DARKGREY( "darkgrey", "#4C5759" ),
	BLACK( "black", "#000000" ),
	
	
	
	/*
	LIGHTBLUE( "lightblue", "#5CB3FF" ), 
	BLUE( "blue", "#357EC7" ),
	DARKBLUE( "darkblue", "#342D7E" ),
	
	LIGHTGREEN( "lightgreen", "#4CC552" ), 
	GREEN( "green", "#4AA02C" ), 
	DARKGREEN( "darkgreen", "#347C2C" ),
	
	RED( "red", "#FF3C2B" ), 
	DARKRED( "darkred", "#C40000" ),
	
	GOLD( "gold", "#FDD017" ),
	
	LIGHTGREY( "lightgrey", "#C5C5C5" ),
	GREY( "grey", "#736F6E" ),
	DARKGREY( "darkgrey", "#25383C" ),
	
	BLACK( "black", "#000000" ),
	*/
	;
	
	
	
	private String name ;
	
	private String code ;
	
	
	private Colors( String name, String code ){
		this.name = name ;
		this.code = code ;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCode() {
		return code;
	}
	
	
	
	

}
