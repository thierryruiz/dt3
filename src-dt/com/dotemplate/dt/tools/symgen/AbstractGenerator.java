package com.dotemplate.dt.tools.symgen;

import java.io.File;
import java.io.FileWriter;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

public abstract class AbstractGenerator {
	
	protected int sortIndex ;
	
	protected VelocityContext context ;
	
	protected abstract String getSymbolFolderRoot() ;
	
	protected abstract String getSymbolFolder() ;
	
	protected abstract String getAbstractSymbolFolder() ;
	
	protected abstract void initContext() ;
	
	
	
	public AbstractGenerator( int sortIndex ) {
		this.sortIndex = sortIndex ;
	}
	
	
	public static String getOutputFolder() {
		return "../tmp/symgen/"  ;
	}
	
	
	protected void initEngine(){
		
		try {
			
			 Properties p = new Properties();
			 
			 String vmPath = "WEB-INF\\templates\\" ;
			 
			 p.setProperty( "file.resource.loader.path", vmPath );
			 
			 System.out.println( "Using path " + vmPath ) ;
			 
			 Velocity.init( p );
		
			 context = new VelocityContext() ;
			 
		
		} catch ( Exception e1 ) {
			
			e1.printStackTrace();
		
		}
		
	}
	
	
	
	protected void generate() throws Exception  {
		
		initEngine() ;
		initContext() ;
			
		File outputFolder = new File ( getOutputFolder() + getSymbolFolder() ) ;
		
		outputFolder.mkdir() ;
		
		// create symbol descriptor
		
		System.out.println( "generate symbol folder " + getSymbolFolder() ) ;
		
		String descriptorTemplate = "_symbols/" + getSymbolFolderRoot() + "/" + getAbstractSymbolFolder() + "/symbol.xml.vm" ;

		FileWriter w = new FileWriter( getOutputFolder() + getSymbolFolder() + "/symbol.xml" ) ;
		
		( Velocity.getTemplate( descriptorTemplate )) .merge( context, w );
		
		w.flush();
		w.close() ;
		
	}
	
	
	protected void generate_css() throws Exception  {
		FileUtils.writeStringToFile( 
				new File(  getOutputFolder() + getSymbolFolder() + "/css.vm" ), "#supercss($s)" ) ;
	}

	protected void generate_js() throws Exception  {
		FileUtils.writeStringToFile( 
				new File(  getOutputFolder() + getSymbolFolder() + "/js.vm" ), "#superjs($s)" ) ;
	}

	
	protected void generate_svgf() throws Exception  {
		FileUtils.writeStringToFile( 
				new File(  getOutputFolder() + getSymbolFolder() + "/svgf.vm" ), "#superdraw($s)" ) ;
	}
	
	
	protected void generate_settings() throws Exception  {
		FileUtils.writeStringToFile( 
				new File(  getOutputFolder() + getSymbolFolder() + "/settings.vm" ), "#supersettings($s)" ) ;
	}

	

}
