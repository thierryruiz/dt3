package com.dotemplate.dt.tools.symgen;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.shared.SymbolType;

public abstract class AbstractPattern extends AbstractGenerator {

	protected String id ;
	
	private int width ;
	
	private int height ;
	
	File imageFile ;
	
	public AbstractPattern( File file, int sortIndex ) {
		super( sortIndex ) ;
		
		imageFile = file ;
		
		id = StringUtils.substringBefore( file.getName(), "." ) ;
		
		try {
			BufferedImage image = ImageIO.read( file ) ;
			width = ( int ) image.getWidth() ;
			height = ( int ) image.getHeight() ;
		} catch (IOException e) {
			throw new RuntimeException( e ) ;
		}
		
	}
	
	
	@Override
	protected String getSymbolFolderRoot() {
		return SymbolType.PATTERN.getFolder();
	}

	


	@Override
	protected void initContext() {
		context.put("fileName", imageFile.getName() ) ;
		context.put("width", width ) ;
		context.put("height", height ) ;
		context.put("sortIndex", sortIndex ) ;
	}

	
	@Override
	protected void generate() throws Exception {
		if ( width % 2 != 0 ) {
			System.out.println ( "Ignore  pattern " + getSymbolFolder() +  ": odd width " + width  ) ;
			return ;
		}
		
		super.generate();
		
		//FileUtils.copyFile( imageFile, new File ( getOutputFolder() + getSymbolFolder() + "\\pattern.png" ) ) ;
		
		//generate_svgf();
	}
	
	
}
