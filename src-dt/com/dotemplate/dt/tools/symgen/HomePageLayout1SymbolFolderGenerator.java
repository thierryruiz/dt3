package com.dotemplate.dt.tools.symgen;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.dotemplate.dt.tools.symgen.home.ContentCategory;
import com.dotemplate.dt.tools.symgen.home.LoopIterator;
import com.dotemplate.theme.server.category.ContentCategories;


public class HomePageLayout1SymbolFolderGenerator {

	
	public static void main( String[] args ) {
		
		try {
			
			// create output symbol folder
			File outputFolder = new File ( AbstractGenerator.getOutputFolder() ) ;
			
			FileUtils.cleanDirectory( outputFolder ) ;
			
			generate( args[ 0 ] ) ;

			
		} catch ( Exception e ) {
			
			e.printStackTrace();
		}

	}


	private static void generate( String category ) {
				
		ContentCategory content ;
		
		try {
			
			content = null ; // ( new ContentCategories()).load( category ) ;
			
		} catch ( Exception e ){
			e.printStackTrace() ;
			return ;
		}
		
		LoopIterator< Map< String, String > > symbols = loadProperties(
				"WEB-INF/templates/_symbols/content/abstract-home-layout-1/symbols.properties" ) ;
		
		for ( int i = 1 ; i < 15 ; i++  ){
			
			try {
				
				new HomePageLayout1Generator( i,  symbols.next(), content ).generate() ;
			
			} catch ( Exception e ) {
				
				throw new RuntimeException ( "Failed the generate symbol " + i, e ) ; 
				
			}
			
		}
		
		
	}
	
	
	
	protected static LoopIterator< Map< String, String > > loadProperties( String file ) {
		
		Properties properties  =  new Properties() ;
		
		try {
			
			properties.load( new FileInputStream( file ) );
		
		} catch (Exception e) {
			
			throw new RuntimeException( e ) ;
		}
	
		Map< String,  Map< String, String > > contents = new LinkedHashMap< String, Map< String, String > > () ;
		Map< String, String > contentMap = new LinkedHashMap< String, String > () ;
				
		String key, value ;
	
		for ( Map.Entry< Object, Object > prop : properties.entrySet() ){
			
			key = ( ( String ) prop.getKey() ).trim() ;
			value = ( ( String ) prop.getValue() ).trim() ;
			
			String index = StringUtils.substringBefore( key, "." ) ; 
			
			contentMap = contents.get( index ) ;
			
			if  ( contentMap == null ){
				contentMap = new LinkedHashMap< String, String > () ;
				contents.put( index, contentMap ) ;
			}
			
			contentMap.put( key, value ) ;
			
		}
		
		return null ; //new LoopIterator< Map< String, String > > ( new ArrayList< Map< String, String > > ( contents.values() ) );
	}



	
}