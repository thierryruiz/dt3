package com.dotemplate.dt.tools.symgen.home;

import java.util.LinkedList;
import java.util.Map;

public class ContentCategory {

	private String name;

	private LinkedList< Map < String, String > > headers;

	private LinkedList<Map<String, String>> columns;

	private ContentCategoryImages images;

	public ContentCategory() {
		images = new ContentCategoryImages();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LinkedList<Map<String, String>> getHeaders() {
		return headers;
	}

	public void setHeaders(LinkedList<Map<String, String>> headers) {
		this.headers = headers;
	}

	public LinkedList< Map<String, String>> getColumns() {
		return columns;
	}

	public void setColumns( LinkedList<Map<String, String>> columns) {
		this.columns = columns;
	}

	public ContentCategoryImages getImages() {
		return images;
	}

	public void setImages(ContentCategoryImages images) {
		this.images = images;
	}

}
