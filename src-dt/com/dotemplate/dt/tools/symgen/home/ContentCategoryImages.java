package com.dotemplate.dt.tools.symgen.home;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ContentCategoryImages extends HashMap< String, Map < String, LinkedList< File > > > {
	
	private static final long serialVersionUID = 1619574124149652669L;

	public ContentCategoryImages() {
			
		put( "100",  new HashMap<String, LinkedList< File > >() ) ;
		put( "50",   new HashMap<String, LinkedList< File > >() ) ;
		put( "30",   new HashMap<String, LinkedList< File > >() ) ;
	
		get( "100" ).put( "standard" ,  new LinkedList< File > ( new ArrayList < File >()) )  ;
		get( "100" ).put( "panoramic" ,  new LinkedList< File > ( new ArrayList < File >() ) )  ;

		get( "50" ).put( "standard" ,  new LinkedList< File > ( new ArrayList < File >() ) )  ;
		get( "50" ).put( "panoramic" ,  new LinkedList< File > ( new ArrayList < File >() ) )  ;
	
		
		get( "30" ).put( "standard" ,  new LinkedList< File > ( new ArrayList < File >() ) )  ;
		get( "30" ).put( "panoramic" ,  new LinkedList< File > ( new ArrayList < File >() ) )  ;
		get( "30" ).put( "square" ,  new LinkedList< File > ( new ArrayList < File >() ) )  ;
		get( "30" ).put( "vertical" ,  new LinkedList< File > ( new ArrayList < File >() ) )  ;
		
	}

}

