package com.dotemplate.dt.tools.symgen.home;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;


public class ContentCategoriesDeprecated {

	private HashMap< String, ContentCategory > categoryMap;
	
	public ContentCategoriesDeprecated() {
		load() ;
	}
	
	
	@SuppressWarnings( "unchecked" )
	protected void load(){
		
		categoryMap = new HashMap< String, ContentCategory >() ;
		
		Collection< File > categoryFolders = 
				FileUtils.listFiles( new File ( "categories" ), null, false ) ;
	
		String name ;
		for( File category : categoryFolders ){
			name = category.getName() ;
			categoryMap.put( name, load( name )  ) ;
		}
		
	}
	
	
	protected ContentCategory load( String categoryName ) {

		ContentCategory category = new ContentCategory() ;
		
		
		category.setName( categoryName ) ;
		
		category.setHeaders( loadProperties(
				"categories/" + categoryName + "/text/headers.properties" ) ) ;

	
		
		category.setColumns( loadProperties(
				"categories/" + categoryName + "/text/columns.properties" ) ) ;
		
		loadImages ( category );
		
		return category ;
		
	}


	@SuppressWarnings("unchecked")
	private  void loadImages( ContentCategory category )  {

		String [] exts = { "png", "jpg" } ; 

		Collection< File > images = 
				FileUtils.listFiles( new File ( 
						"categories/" + category.getName() + "/images" ), exts, true ) ;
		
		for ( File image : images ){
			category.getImages().get( image.getParentFile().getParentFile().getName() ).get( 
					image.getParentFile().getName() ).add( image ) ;			
		}
			
	}
	


	
	protected LinkedList< Map < String, String > > loadProperties( String file ) {
		
		
		Properties properties  =  new Properties() ;
		
		try {
			
			properties.load( new FileInputStream( file ) );
		
		} catch (Exception e) {
			
			throw new RuntimeException( e ) ;
		}
	
		Map< String,  Map< String, String > > contents = new LinkedHashMap< String, Map< String, String > > () ;
		
		Map< String, String > contentMap = new LinkedHashMap< String, String > () ;
				
		String key, value ;
			
		for ( Map.Entry< Object, Object > prop : properties.entrySet() ){
			
			key = ( ( String ) prop.getKey() ).trim() ;
			value = ( ( String ) prop.getValue() ).trim() ;
			
			String index = StringUtils.substringBefore( key, "." ) ; 
			
			contentMap = contents.get( index ) ;
			
			if  ( contentMap == null ){
				contentMap = new LinkedHashMap< String, String > () ;
				
				contents.put( index, contentMap ) ;
			}
			
			contentMap.put( key, value ) ;
			
		}
		
		return new LinkedList< Map< String, String > > ( new ArrayList< Map< String, String > > ( contents.values() ) );
	}


	
}