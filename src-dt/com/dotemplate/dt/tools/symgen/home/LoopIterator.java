package com.dotemplate.dt.tools.symgen.home;

import java.util.Iterator;
import java.util.LinkedList;


public class LoopIterator< T > {

	private LinkedList< T > list ;
	
	private Iterator< T > iterator ;
	
	public LoopIterator( LinkedList< T > list, int index ) {
		
		this.list = list ;
		
		if ( index >= list.size() ){
			index = list.size() - 1 ; 
		}
		
		iterator = list.listIterator( index ) ;
		
	}

	
	public T next() {
		
		if ( !iterator.hasNext() ){
			iterator = list.iterator() ;
		}
		
		return iterator.next() ;		
	
	}

		
	
	
}