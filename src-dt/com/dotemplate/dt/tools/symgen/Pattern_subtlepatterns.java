package com.dotemplate.dt.tools.symgen;

import java.io.File;


public class Pattern_subtlepatterns extends AbstractPattern {
	
	
	public Pattern_subtlepatterns( File file ) {
		super( file, -1 ) ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return "subtlepatterns-" + id.replace('_', '-')  ;
	}
	
	
	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-subtlepatterns" ;
	}
	
}
