package com.dotemplate.dt.tools.symgen;

import java.io.File;


public class Pattern_patterrific extends AbstractPattern {
	
	
	public Pattern_patterrific( File file ) {
		super( file, -1 ) ;
	}
	
	
	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-patterrific" ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return "patterrific-" + id  ;
	}
	
	
}
