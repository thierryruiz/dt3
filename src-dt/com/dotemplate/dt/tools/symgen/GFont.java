package com.dotemplate.dt.tools.symgen;


import com.dotemplate.theme.server.GoogleFont;
import com.dotemplate.theme.shared.ThemeSymbolType;

public class GFont extends AbstractGenerator {

	protected GoogleFont font ;
		
	public GFont( GoogleFont font, int sortIndex ) {
				
		super( sortIndex ) ;
	
		this.font = font ;
			
	}
	
	
	@Override
	protected String getSymbolFolderRoot() {
		return ThemeSymbolType.HEADINGFONT.getFolder();
	}

	
	@Override
	protected void initContext() {
		context.put( "font", font ) ;
	}

	
	
	@Override
	protected void generate() throws Exception {		
		super.generate();
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-google" ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return "google-" + font.getName()  ;
	}
	
	
	
}
