package com.dotemplate.dt.tools.symgen;


import com.dotemplate.theme.shared.ThemeSymbolType;

public abstract class AbstractFill_vgradient extends AbstractGenerator implements FillGradient {
		
	protected int gradient ;
	
	public AbstractFill_vgradient( int gradient, int sortIndex ) {
		super( sortIndex ) ;
		this.gradient = gradient ;
		this.sortIndex = sortIndex ;
	}

	
	@Override
	protected void initContext() {
		context.put( "gradient", gradient ) ;
		context.put( "sortIndex", sortIndex  ) ;
 	}

	@Override
	protected String getSymbolFolderRoot() {
		return ThemeSymbolType.FILL.getFolder();
	}

			
	@Override
	protected void generate() throws Exception {
		super.generate();
		generate_css() ;
		generate_svgf() ;	
	}
	
	
}
