package com.dotemplate.dt.tools.symgen;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.shared.SymbolType;

public class Texture extends AbstractGenerator {

	protected String id ;
	
	private int width ;
	
	private int height ;
	
	File imageFile ;
	
	
	public Texture( File file, int sortIndex ) {
		
		super( sortIndex ) ;
		
		imageFile = file ;
		
		id = StringUtils.substringBefore( file.getName(), "." ).replace( '_', '-' ) ;
		
		
		try {
			BufferedImage image = ImageIO.read( file ) ;
			width = ( int ) image.getWidth() ;
			height = ( int ) image.getHeight() ;
		} catch (IOException e) {
			throw new RuntimeException( e ) ;
		}
		
	}
	
	
	@Override
	protected String getSymbolFolderRoot() {
		return SymbolType.TEXTURE.getFolder();
	}

	


	@Override
	protected void initContext() {
		context.put( "image", imageFile.getName() ) ;
		context.put( "width", width ) ;
		context.put( "height", height ) ;
		context.put( "sortIndex", sortIndex  ) ;
	}

	
	
	@Override
	protected void generate() throws Exception {		
		super.generate();
	}


	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract" ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return id  ;
	}
	
	
}
