package com.dotemplate.dt.tools.symgen;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.dotemplate.dt.tools.symgen.home.LoopIterator;


public class GenericHomePageSymbolFolderGenerator {

	
	public static void main( String[] args ) {
		
		try {
			
			// create output symbol folder
			File outputFolder = new File ( AbstractGenerator.getOutputFolder() ) ;
			FileUtils.cleanDirectory( outputFolder ) ;
			
			generate() ;
			
			
		} catch ( Exception e ) {
			
			e.printStackTrace();
		}

	}


	private static void generate() {

		SynonymDictionary dictionary  = SynonymDictionary.load ( 
				"WEB-INF/templates/_symbols/content/abstract-home-generic/dictionary.properties" ) ;
		
		LoopIterator< Map< String, String > > symbols = loadProperties(
				"WEB-INF/templates/_symbols/content/abstract-home-generic/symbols.properties") ;
		
		LoopIterator< Map< String, String > > headers = loadProperties(
				"WEB-INF/templates/_symbols/content/abstract-home-generic/headers.properties") ;

		LoopIterator< Map< String, String > > columns = loadProperties(
				"WEB-INF/templates/_symbols/content/abstract-home-generic/columns.properties") ;
		
	
		Map< String, LoopIterator< File > > images = loadImages() ;
		
		
		for ( int i = 1 ; i < 10 ; i++  ){
			
			try {
				
				new GenericHomePageGenerator( 
						i, 
						symbols.next(), 
						headers.next(), 
						columns,  
						dictionary, 
						images ).generate() ;
			
			} catch ( Exception e ) {
				
				throw new RuntimeException ( "Failed the generate symbol " + i, e ) ; 
				
			}
			
		}
		
		
	}


	private static Map< String, LoopIterator< File > > loadImages() {
		
		
		String [] exts = { "png", "jpg" } ; 
		String [] folders = { "pano", "standard", "circular", "icon" } ;
		
		Map< String, LoopIterator< File > > imagesMap = new HashMap< String, LoopIterator< File >  >() ;
		
		Iterator< ? > files ;
		ArrayList< File > imageFiles ;
		
		for ( String folder : folders ){
		
			files = FileUtils.iterateFiles ( new File ( 
					"WEB-INF/templates/_images/content/generic/30/" + folder ), exts, true ) ;
			
			imageFiles = new ArrayList< File >() ;
			
			while ( files.hasNext () ){
				
				imageFiles.add( ( File ) files.next () ) ;
				
			}
			
			// imagesMap.put( folder, new LoopIterator < File >( imageFiles ) ) ;
		
		}		
		
		return imagesMap ;
	
	}


	private static LoopIterator< Map< String, String > > loadProperties( String file ) {
		
		Properties properties  =  new Properties() ;
		
		try {
			
			properties.load( new FileInputStream( file ) );
		
		} catch (Exception e) {
			
			throw new RuntimeException( e ) ;
		}
	
		Map< String,  Map< String, String > > contents = new HashMap< String, Map< String, String > > () ;
		Map< String, String > contentMap = new HashMap< String, String > () ;
				
		String key, value ;
	
		for ( Map.Entry< Object, Object > prop : properties.entrySet() ){
			
			key = ( ( String ) prop.getKey() ).trim() ;
			value = ( ( String ) prop.getValue() ).trim() ;
			
			String index = StringUtils.substringBefore( key, "." ) ; 
			
			contentMap = contents.get( index ) ;
			
			if  ( contentMap == null ){
				contentMap = new HashMap< String, String > () ;
				contents.put( index, contentMap ) ;
			}
			
			contentMap.put( key, value ) ;
			
		}
		
		
		return null ; // new LoopIterator< Map< String, String > > ( new ArrayList< Map< String, String > > ( contents.values() ) );
	}


	
}