package com.dotemplate.dt.tools.symgen;

import java.io.File;


public class Pattern_envato_3 extends AbstractPattern {
	
	
	public Pattern_envato_3( File file ) {
		super( file, -1 ) ;
	}
	
	
	@Override
	protected String getAbstractSymbolFolder() {
		return "abstract-envato-3" ;
	}
	
	
	@Override
	protected String getSymbolFolder() {
		return "envato-3-" + id  ;
	}
	
	
}
