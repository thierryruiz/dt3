package com.dotemplate.dt.tools;


import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.batik.transcoder.ErrorHandler;
import org.apache.batik.transcoder.SVGAbstractTranscoder;
import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import com.dotemplate.core.shared.canvas.ImageResource;



public class ImageEngine {

	private static Log log = LogFactory.getLog( ImageEngine.class ) ;
    
    private VelocityEngine engine ;
    
    private Template imageTemplate ;
    
    private PNGTranscoder pngTranscoder ;
    
    private JPEGTranscoder jpegTranscoder ;
    
    private double jpegQuality = 0.9  ;

    private int outputWidth = -1 ;
    
    private int outputHeight = -1 ;

    
    public ImageEngine() {
    	engine = new VelocityEngine() ;
    	pngTranscoder = new PNGTranscoder() ;
    	jpegTranscoder = new JPEGTranscoder() ;
    	
    	pngTranscoder.setErrorHandler( errorHandler ) ;
    	jpegTranscoder.setErrorHandler( errorHandler ) ;
    	
    	
    	pngTranscoder.addTranscodingHint(SVGAbstractTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE) ;
    	
    	jpegTranscoder.addTranscodingHint(SVGAbstractTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE) ;
    	jpegTranscoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, new Float( jpegQuality ) );
    }

    
    
    public void init( Properties props ) throws Exception {
    	
    	if ( log.isDebugEnabled () ){
    		log.debug (  "Initializing image engine..." ) ;
    	}
    	
    	engine.init( props ) ;
    	imageTemplate = engine.getTemplate( "image-thumbnail.svg.vm" );
    	
    	if ( log.isDebugEnabled () ){
    		log.debug (  "Image engine ready." ) ;
    	}
    }
    
    
    public void setOutputWidth ( int outputWidth ) {
		this.outputWidth = outputWidth;
		this.outputHeight = -1 ;
    }


	public void setOutputHeight ( int outputHeight ) {
		this.outputHeight = outputHeight;
		this.outputWidth = -1 ;
	}


	public BufferedImage generate ( ImageResource imageResource, String source, String target ) throws Exception  {
    	
    	System.out.println(  "Generate image : source=" + source + ", target=" + target ) ;
    	
    	BufferedImage image ;
    	
        try {
            image = ImageIO.read( new FileInputStream ( source ) ) ;
        } catch (IOException e) {
            e.printStackTrace() ;
            throw new Exception ( "Failed to read the image from input stream.", e ) ;
        }
    	
    	Dimension d = new Dimension( image.getWidth(), image.getHeight() ) ;
    	
    	
    	double scale ; 

        if ( outputWidth > 0 ){
        	scale = ( double ) outputWidth / ( double ) d.getWidth() ;
        	outputHeight = ( int ) ( d.getHeight() * scale ) ;
        } else if ( outputHeight > 0 ){
        	if ( d.getHeight () < outputHeight ) outputHeight = ( int ) d.getHeight () ;
        	scale = ( double ) outputHeight / ( double ) d.getHeight() ;
            outputWidth = ( int ) ( d.getWidth() * scale ) ;
        } else {
        	throw new RuntimeException ( "Need to set either output width or output height before generate image" ) ;
        }
        
        
        
    	VelocityContext ctx = new VelocityContext() ;
    	
    	ctx.put ( "src" , source ) ;
    	ctx.put ( "dimension", d ) ;
    	ctx.put ( "scale", scale ) ;
    	ctx.put ( "scaleWidth", outputWidth ) ;
    	ctx.put ( "scaleHeight", outputHeight ) ;
    	    	
    	StringWriter writer = new StringWriter () ;
    
    	
    	if ( log.isDebugEnabled () ){
    		log.debug (  "Generating SVG..." ) ;
    	}
    	
    	try {        	
    		imageTemplate.merge( ctx, writer ) ;
		} catch ( Exception e ) {
			throw new Exception( "Failed to create SVG image", e ) ;
		}
		
    	if ( log.isDebugEnabled () ){
    		log.debug (  "Transcoding SVG..." ) ;
    	}
		
    	
		try {
			render ( writer.getBuffer ().toString (), target, new Dimension( outputWidth, outputHeight )  ) ;
		} catch ( Exception e ) {
			throw new Exception( "Failed to transcode SVG image", e ) ;
		}
		
    	if ( log.isDebugEnabled () ){
    		log.debug (  "Result image transcoded to '" + target + "'" ) ;
    	}
    	
    	outputWidth = outputHeight = -1 ;
    	
    	
    	return image ;
    	
    }
    
    
    
    
    public void render( String svg, String out, Dimension d ) throws 
    	IOException, TranscoderException, Exception {
    	
    	if ( log.isDebugEnabled () ){
    		log.debug ( "Transcoding SVG content :\n" + svg ) ;
    	}
    	
    	Transcoder transcoder = ( out.endsWith ( ".png" ) ) ? pngTranscoder : jpegTranscoder  ;    	
    	
    	FileOutputStream fos = null ;
    	
    	try {
    		
		    TranscoderOutput output = new TranscoderOutput();
		    TranscoderInput input = new TranscoderInput();

		    transcoder.addTranscodingHint(SVGAbstractTranscoder.KEY_WIDTH, new Float( d.getWidth() ));
		    transcoder.addTranscodingHint(SVGAbstractTranscoder.KEY_HEIGHT, new Float( d.getHeight() ) );

	        Rectangle rectangle = new Rectangle( d ) ;
	        transcoder.addTranscodingHint(SVGAbstractTranscoder.KEY_AOI, rectangle  );
	        
	        fos = new FileOutputStream( out ) ;
	        input.setInputStream( new ByteArrayInputStream ( svg.getBytes() ) );
	        output.setOutputStream( fos );
	       
	        transcoder.transcode( input, output );
	        	        
    	} catch ( Exception e ){
    		throw new Exception ( "Failed to create JPG image" , e ) ;
    	} finally {
    		try {
    			fos.flush();
    			fos.close();
    		} catch ( Exception e ){
    			log.warn ( e ) ;
    		}
    	}
    }
    
    
    
    private ErrorHandler errorHandler = new ErrorHandler() {
		
        public void error( TranscoderException e ) throws TranscoderException {
            log.error( e ) ;
            throw e ;
        }

        public void fatalError( TranscoderException e ) throws TranscoderException {
            log.error( e ) ;
            throw e ;
        }

        public void warning( TranscoderException e ) throws TranscoderException {
            log.error( e ) ;
            throw e ;
        }
        
    } ;
    

	
}
