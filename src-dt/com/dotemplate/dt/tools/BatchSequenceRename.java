package com.dotemplate.dt.tools;

import java.io.File;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

public class BatchSequenceRename {
	
	
	@SuppressWarnings("unchecked")
	public static void main ( String[] args ){
		
		String path = args[ 0 ] ;
		String prefix = args[ 1 ] ;
		int startIndex = Integer.parseInt( args[ 2 ] ) ;
		String ext, newname ;
		
		for ( File file : ( Collection< File> ) FileUtils.listFiles( new File( path ), null, false ) ) {
		
			ext = StringUtils.substringAfterLast(file.getName(), "." ) ;

			newname = path + "/" + prefix + startIndex +  "." + ext ;
			
			file.renameTo( new File( newname ) ) ;
			
			startIndex++ ;
		}		
		
	}
	
	

}
