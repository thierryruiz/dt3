package com.dotemplate.dt.server;

import com.dotemplate.core.server.AppConfigReader;
import com.dotemplate.theme.server.ThemeApp;

public class DoTemplateApp extends ThemeApp {
	
	public static DoTemplateApp get() {
		return ( DoTemplateApp ) _app ;
	}

	@Override
	protected String getConfigFileName() {
		return "application.dt.xml" ;
	}
	
	
	public void init () {
		this.config = ( new AppConfigReader< DoTemplateConfig >( DoTemplateConfig.class ) ).read(
				getConfigFileName() ) ;
		
		super.init( config ) ;
	
	}
	
	
}
