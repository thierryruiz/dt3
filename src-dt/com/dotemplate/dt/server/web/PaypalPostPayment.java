package com.dotemplate.dt.server.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.web.BaseHttpServlet;


public class PaypalPostPayment extends BaseHttpServlet {
	
	private static final long serialVersionUID = -2513758550307757475L;

	private static Log log = LogFactory.getLog( PaypalPostPayment.class );
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// only for test with PaypalSimulatorServlet 
		doPost( request, response ) ;
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		log.info( "After payment processing." ) ;
		log( request ) ;
		
		
		if (  !checkDesignSession( request, response, true ) ) {
			log.error( "Session expired just after payment !!" ) ;
			return ;
		}
		
		nocache( response ) ;
		
		
		String payerEmail = request.getParameter( "payer_email" ) ;

		if ( payerEmail == null ){
			payerEmail = "" ;
		}
		
		request.getSession().setAttribute( "payerEmail" , payerEmail ) ;
		
		
		/*
		Theme theme = ThemeApp.getTheme() ;
		
		
		if( theme.isPurchased() ){
			
			log.info( "Theme has been purchased. Display thank you page" ) ;
			
			
			String payerEmail = null ;
			
			try {

				payerEmail = PurchaseInfoHelper.load( theme.getUid() ).getPayer() ;
				
			} catch ( AppException e ) {
				
				throw new ServletException( "Failed to get payerEmail",  e ) ;
				
			}
			
			request.getSession().setAttribute( "payerEmail" , payerEmail ) ;
			
		}*/
		
		
		log.info( "Forward to thank you page..." ) ;
		
		forwardTo( "paypal-post-payment.jsp", request, response ) ;
	}

	
	@SuppressWarnings("unchecked")
	public void log( HttpServletRequest req ) {

     	StringBuffer sb = new StringBuffer ()
                .append("\n\n\n\n=========  INCOMING RETURN REQUEST FROM PAYPAL=========================================================\n" )
                .append( "\trequest url : ")
                .append( req.getRequestURL () )                    
                .append( "\n")
                
                .append( "\trequest method : ")
                .append( req.getMethod () )
                .append( "\n")

                .append( "\trequest path info : ")
                .append( req.getPathInfo () )  
                .append( "\n")
                .append( "\trequest uri : ")
                .append( req.getRequestURI () )
                .append( "\n")
                .append( "\trequest query : ")
                .append( req.getQueryString() )
                .append( "\n")                    
                .append( "\trequested sessionId : ")          
                .append( req.getRequestedSessionId () )
    	 		.append( "\n") ;
                
        	String key ;
            for ( Enumeration<String> e = req.getParameterNames ()  ; e.hasMoreElements () ;) {
            	sb.append ( "\t\t").append( key =  e.nextElement () ).append("=").append( 
            			req.getParameter ( key ) ).append("\n") ;
            }
	            
            log.info( "Paypal return url\n" +   sb.toString()  ) ;     
	    	  
        } 
	

}
