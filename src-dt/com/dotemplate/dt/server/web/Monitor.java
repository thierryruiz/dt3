package com.dotemplate.dt.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;
import com.dotemplate.theme.server.ThemeContext;

public class Monitor extends BaseHttpServlet {


	private static final long serialVersionUID = 8463329663864561442L;

	private static Log log = LogFactory.getLog( Monitor.class );

	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		
		nocache( response ) ;
	

		String origin = request.getRemoteHost() ;
		log.info ( "Monitor request coming from " + origin ) ;
		
		
		/*
		if ( ! "localhost".equals( origin ) && ! "127.0.0.1".equals( origin ) ){
			response.setStatus( HttpServletResponse.SC_FORBIDDEN ) ;
			return ;
		}*/
		
		nocache( response );
		response.setContentType( "text/html" );
		

		String themeName = request.getParameter( "id" );
		
		ensureDesignSession( request ) ;
				
		String uid = UIDGenerator.pickId();

		String baseUrl = getBaseUrl(request);
		
		
		ThemeApp.getConfig().setBaseUrl( baseUrl );
				
		

		try {

			ThemeApp.getThemeManager().createTheme( 
					themeName, uid , "dotemplate" );
						
			ThemeContext themeContext = ThemeApp.getThemeContext();	
			themeContext.put( "monitor", true ) ;
			themeContext.put( ThemeContext.BASE_URL, getBaseUrl(request) + "sites/" + uid + "/download/" ) ;
			
			
			themeContext.put( ThemeContext.WATERMARK, false ) ;
			themeContext.getDesign().setPurchased( false ) ;
					
			ThemeApp.getThemeManager().export( "xhtml", false );
			
			String sitePath = "/sites/" + uid + "/download/index.html" ;
			
			//response.sendRedirect("http://127.0.0.1:8888" + request.getContextPath() + sitePath  ) ;
			
			request.getRequestDispatcher( sitePath ).forward(
					request, response );	
			
			
			
		} catch ( Exception e ) {
			log.error(  SYSERROR, e ) ;
			forwardUnexpectedError( request, response ) ;
			return;
		}
	

	}	

	
	
}
