package com.dotemplate.dt.server.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.web.BaseHttpServlet;
import com.dotemplate.theme.server.ThemeApp;


public  class PaypalStatus extends BaseHttpServlet {
	
	private static final long serialVersionUID = -3761303212178158024L;

	private static Log log = LogFactory.getLog( PaypalStatus.class );
	
	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response )
			throws ServletException, IOException {

		log.info( "PaypalStatus invocation..." ) ;
		
		String origin = request.getRemoteHost() ;
		
		// check request is coming from local host IPNServlet
		if ( ! "localhost".equals( origin ) && ! "127.0.0.1".equals( origin ) ){
			response.setStatus( HttpServletResponse.SC_FORBIDDEN ) ;
			log.warn( "Rejected attempt to call Paypal status from remote client ip" + origin + " .Only localhost(ed) IPN servlet is authorized" ) ;
			return ;
		}
		
		log( request ) ;

		if ( !checkDesignSession( request, response, false ) ) {
			
			try {
				
				response.setContentType( "text/plain" ) ;
				response.setHeader( "Content-Length", "KO".getBytes().length + "" ) ;
				response.setStatus( HttpServletResponse.SC_OK ) ;
				response.getWriter().write( "KO" ) ;
				response.getWriter().flush() ;

			} catch ( Exception e ) {
				
				log.error( "Failed to respond back to IPN servlet on checkDesignSession=false", e ) ;
				
			} finally {
				
				try {
					
					response.getWriter().close() ;
					
				} catch ( Exception e ){
					
					log.error( "Failed to flush respond back to IPN servlet on checkDesignSession=false", e ) ;
					
				}

			}
			
			return ;
		}
		
		nocache( response ) ;
		
		boolean paymentOk = "1".equals( request.getParameter( "s" ) ) ;
		
		String themeUid = request.getParameter( "themeUid" ) ;
		String txnId = request.getParameter( "txn_id" ) ;
		String payerEmail = request.getParameter( "payer_email" ) ;
		
		try {
			
			if( paymentOk ){
				
				log.info( "Theme " +  themeUid + " Purchased" ) ;
				
				ThemeApp.getThemeManager().onDesignPurchased( themeUid , 
						"paypal", txnId, payerEmail ) ;
			}
			
			
			response.setContentType( "text/plain" ) ;
			response.setHeader( "Content-Length", "OK".getBytes().length + "" ) ;
			response.setStatus( HttpServletResponse.SC_OK ) ;
			response.getWriter().write( "OK" ) ;
			response.getWriter().flush() ;

		
		} catch ( Exception e ) {
			
			log.error( "Failed to respond back to IPN servlet", e ) ;
			
			
		} finally {
			
			try {
				
				response.getWriter().close() ;
				
			} catch ( Exception e ){
				log.error( e.getMessage(), e ) ;
			}

		}
		
	}
}
	

