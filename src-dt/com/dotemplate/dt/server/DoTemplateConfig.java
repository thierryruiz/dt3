package com.dotemplate.dt.server;

import com.dotemplate.theme.server.ThemeAppConfig;


public class DoTemplateConfig extends ThemeAppConfig {
	
	private String paypalUrl ;
	
	private String paypalNotifyUrl ;
	
	private String paypalReturnUrl ;
	
	private String paypalReceiverEmail;
	
	private String paypalButtonId ;
	
	private String adminEmail ;
	
	private String ports ;


	public String getPaypalUrl() {
		return paypalUrl;
	}

	public void setPaypalUrl(String paypalUrl) {
		this.paypalUrl = paypalUrl;
	}

	public String getPaypalReturnUrl() {
		return paypalReturnUrl;
	}

	public void setPaypalReturnUrl(String paypalReturnUrl) {
		this.paypalReturnUrl = paypalReturnUrl;
	}

	public String getPaypalNotifyUrl() {
		return paypalNotifyUrl;
	}

	public void setPaypalNotifyUrl(String paypalNotifyUrl) {
		this.paypalNotifyUrl = paypalNotifyUrl;
	}

	public String getPaypalReceiverEmail() {
		return paypalReceiverEmail;
	}

	public void setPaypalReceiverEmail(String paypalReceiverEmail) {
		this.paypalReceiverEmail = paypalReceiverEmail;
	}

	public String getPaypalButtonId() {
		return paypalButtonId;
	}

	public void setPaypalButtonId(String paypalButtonId) {
		this.paypalButtonId = paypalButtonId;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getPorts() {
		return ports;
	}

	public void setPorts(String ports) {
		this.ports = ports;
	}
	

}

