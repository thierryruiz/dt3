package com.dotemplate.dt.server.checkout;


public abstract class PaymentContext {
	
	public abstract PaymentGateway getPaymentGateway() ;

	protected PaymentContext() {} ;
	
	
}
