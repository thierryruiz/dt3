package com.dotemplate.dt.server.checkout;


public class PaypalContext extends PaymentContext {
	
	private String url ;

	private String notifyUrl ;
	
	private String returnUrl ;
		
	private String buttonId ;
	

	
	@Override
	public PaymentGateway getPaymentGateway() {
		return PaymentGateway.PAYPAL ;
	}



	public String getUrl() {
		return url;
	}



	public void setUrl( String url ) {
		this.url = url;
	}


	public String getNotifyUrl() {
		return notifyUrl;
	}


	public void setNotifyUrl(String notifyUrl) {
		
		
		this.notifyUrl = notifyUrl;
	}



	public String getButtonId() {
		return buttonId;
	}



	public void setButtonId(String buttonId) {
		this.buttonId = buttonId;
	}



	public String getReturnUrl() {
		return returnUrl;
	}



	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
		
	
	
		
	
	
	
}
