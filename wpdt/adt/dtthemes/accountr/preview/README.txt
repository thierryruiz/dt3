
Thanks for your purchase.

Your template can be be edited and downloaded at this address :

http://127.0.0.1:8888/dt3/dt/u?1501031609rvIv4Uw7

This URL is personal: We limit the number of downloads to 5 times, so do not share this address.


LICENSE

By purchasing this template you are granted to use it to create a unique website for you or for your client. 

If you want to use this template for resale with or without modifications or include it in software or web application 
we provide an extended license for such use. Please contact us. 


For any question about template licensing please contact us.


CREDITS

Our templates may include design resources under Creative Commons license. Please ensure that your final work based on our template respects
this license. This typically requires including credits resource authors:




Thanks again !

doTemplate team.

