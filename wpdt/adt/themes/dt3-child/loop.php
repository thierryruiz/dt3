<?php
			
	global $dtPostLayout;

?>	


<?php if (have_posts()) : ?>
			
	<?php while (have_posts()) : the_post(); ?>
				
		<article>

			<?php if ( has_tag( "theme" , $post ) ) : ?>
		 	
				<?php get_template_part( 'theme' ); ?>
			
			<?php else : ?>
			
				
				<div class="post" id="post-<?php the_ID(); ?>">
					
					<div style="float:right;margin:0 0 0 20px">
						 <?php the_post_thumbnail( 'medium', array( 'title' => ''.get_the_title().''  ) ); ?>
					</div>
					
					<h1>
						<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
							<?php the_title(); ?>
						</a>
					</h1>
			
	
					<div class="entry clearfix">
						
						
						<?php the_content(__('Continue reading &raquo;')); ?>
						
		
					</div>
					
					<div class="entry clearfix">
					
				</div>
				
			<?php endif  ?>	
			
		</article>


	<?php endwhile; ?>

		
	<div id="navigation">
			<div class="fleft"><?php next_posts_link(__('&laquo; Older Entries')) ?></div>
			<div class="fright"> <?php previous_posts_link(__('Newer Entries &raquo;')) ?></div>
	</div>
	
	



<?php else : ?>
	
	<div class="post">
		<div class="entry">
			<h2><?php _e('Not Found'); ?></h2>
			<p><?php _e("Sorry, you are looking for a page that no or not longer exists."); ?></p>
		</div>
	</div>	
		
<?php endif; ?>


			