
<?php if (defined( ENABLE_WIX_ADS  )) : ?>

	<IFRAME SRC="https://ad.doubleclick.net/ddm/adi/N34703.1942300DOTEMPLATE.COM/B8599899.124148156;sz=960x90;ord=[timestamp]?" WIDTH=960 HEIGHT=90 MARGINWIDTH=0 MARGINHEIGHT=0 HSPACE=0 VSPACE=0 FRAMEBORDER=0 SCROLLING=no BORDERCOLOR='#000000'>
		<SCRIPT language='JavaScript1.1' SRC="https://ad.doubleclick.net/ddm/adj/N34703.1942300DOTEMPLATE.COM/B8599899.124148156;abr=!ie;sz=960x90;ord=[timestamp]?">
		</SCRIPT>
		<NOSCRIPT>
			<A HREF="https://ad.doubleclick.net/ddm/jump/N34703.1942300DOTEMPLATE.COM/B8599899.124148156;abr=!ie4;abr=!ie5;sz=960x90;ord=[timestamp]?">
			<IMG SRC="https://ad.doubleclick.net/ddm/ad/N34703.1942300DOTEMPLATE.COM/B8599899.124148156;abr=!ie4;abr=!ie5;sz=960x90;ord=[timestamp]?" BORDER=0 WIDTH=960 HEIGHT=90 ALT="Advertisement"></A>
		</NOSCRIPT>
	</IFRAME>

<?php endif; ?>


<section>


<div class="post" id="post-<?php the_ID(); ?>">

<header>
	<h2>
		<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
			<?php echo get_post_meta($post->ID, "_dt_themeName", true)  ?> website template
		</a>
	</h2>
</header>


<div class="group theme-feat">
	
	<div class="col span_4_of_6">

		<?php 
		
			if ( has_tag( "blank-theme" , $post ) ) : 
			
				$theme_image =  DT_THEMES_URI . "/" . get_post_meta($post->ID, "_dt_themeId", true) . "/th.jpg" ;		
								
			else : 		

				$theme_image =  DT_THEMES_URI . "/" . get_post_meta($post->ID, "_dt_themeId", true) . "/img.jpg" ;		
				
			endif 
		?>		
		
		
		<img src="<?php echo $theme_image ?>" class="theme-view">
	
	</div>

	<div class="col span_2_of_6">
		
		<a href="<?php echo TOMCAT_URL; ?>/dt/createTheme?id=<?php echo get_post_meta($post->ID, "_dt_themeId", true); ?>"
								alt="edit template" rel="nofollow" class="dteditor">
								
			<input type="button" value="CUSTOMIZE IN TEMPLATE EDITOR" class="button medium orange pie customize" />
		
		</a>
				
		
		<a href="<?php echo ( DT_THEMES_URI . "/" . get_post_meta($post->ID, "_dt_themeId", true ) . "/" . get_post_meta($post->ID, "_dt_themeId", true ) . ".zip" ) ?>"
								alt="download template" rel="nofollow">
			<input type="button" value="FREE DOWNLOAD" class="button medium green pie"  style="margin-top:15px;" />
			
		</a>


		
		<?php if ( !has_tag( "blank-theme" , $post ) ) : ?>
		
			<h3 class="t-desc">Template description</h3>
			<p>
			
			
			<?php 
				
				$theme_desc =  ltrim(DT_THEMES_URI, "/" ) . "/" . get_post_meta($post->ID, "_dt_themeId", true) . "/desc.html" ;
				include(  $theme_desc ); 
			?>
			
			</p>
	
			<h3 class="t-cat">Template categories</h3>
			<ul>
				<?php 
				$theme_cat =  ltrim(DT_THEMES_URI, "/" ) . "/" . get_post_meta($post->ID, "_dt_themeId", true) . "/cat.html" ;
				include(  $theme_cat ); 
				?>
			</ul>
			
		<?php endif ?>			

	</div>

</div>

</setction>
