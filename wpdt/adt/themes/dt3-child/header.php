<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title>Free Online Template Builder</title>

	<meta name="verify-v1" content="J39vDqjiZ+Ikdp5/9odQGYjSsaBlh5et5VFfOx5nzpE=" />
	<meta name="google-site-verification" content="x6dKeMC--nNPcLyAjrOl3-Mv_kCw7D5evDB3FORCJWI" />
	<meta name="description" content="Free web templates builder - Make a template in minutes" />
	<meta name="keywords" content="template generator, free templates, web templates, web template, webdesign, custom template, template builder, theme builder, template designer, CSS, web design" />
	<meta name="author" content="Thierry Ruiz" />
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta name="MSSmartTagsPreventParsing" content="true" />

	<link rel="stylesheet" href="<?php echo WPDT_CHILD_THEME_URI ?>/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Condensed:300" type="text/css" />

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />	


	<!--[if IE]>
	<meta http-equiv="imagetoolbar" content="no" />
	<![endif]-->	


	<script src="<?php echo WPDT_THEME_URI ?>/js/jquery-1.8.1.min.js"></script>
	<script src="<?php echo WPDT_THEME_URI ?>/js/scripts.js"></script>
	<script src="<?php echo WPDT_THEME_URI ?>/js/theme.js"></script>
	<script src="<?php echo WPDT_CHILD_THEME_URI ?>/js/custom.js"></script>
	<script src="<?php echo WPDT_CHILD_THEME_URI ?>/js/jquery.sequence-min.js"></script>
	

	<?php if (get_option('dt_custom_css') ) { ?>
		<!-- CUSTOM CSS -->
		<style type="text/css">
			<?php echo stripslashes(get_option('dt_custom_css')); ?>
		</style>
	<?php } ?>
	
	<?php wp_head(); ?>
	
</head>

<body>

	<?php if (defined( DT_PROD )) : ?>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-4680957-1', 'auto');
		  ga('send', 'pageview');
		</script>

		<?php if (defined( ENABLE_WIX_ADS  )) : // WIX REMARKETING ?>
	
			<script type="text/javascript">
				/* <![CDATA[ */
				var google_conversion_id = 1041825691;
				var google_conversion_label = "FYMYCO-T6gMQm__j8AM";
				var google_custom_params = window.google_tag_params;
				var google_remarketing_only = true;
				/* ]]> */
			</script>
			<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
			<noscript>
				<div style="display:inline;">
					<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1041825691/?value=0&amp;label=FYMYCO-T6gMQm__j8AM&amp;guid=ON&amp;script=0"/>
				</div>
			</noscript>

		<?php endif; ?>

	<?php endif; ?>	

	<div id="popup" style="left: 323px; position:absolute; top:0px; z-index:9999; opacity:0; display:none">
        <span class="button b-close"><span>X</span></span>
        <br/><span id="popupTxt"></span>
	</div>


	<header>
		<div id="top">
			<div class="content row">
				<div  id="logo" style="cursor:pointer" onclick="window.location.href=window.location.protocol + '//' +  window.location.host">
				</div>
				<nav>

					<?php
						wp_nav_menu(array(
							'theme_location' => 'dt-top-menu',
							'container_id' => 'hmenu',
							'menu_class' => 'dl-menu',
							'menu_id' => '',    
							'walker' => new DT_Menu_Walker(),
							'fallback_cb' => 'dt_menu_cb'
						));
						
						function dt_menu_cb() {
					 	?>
					    	<div id="hmenu" style="font-size:10px;text-transform:none;font-weight:normal">
					    		Create navigation menu from Wordpress dashboard (Menu appearance &gt; menus)
					    	</div>
					    <?php
						}
						 
						
					?>
				</nav>
				<div class="clear"></div> 		
			</div>
		</div>
		
		<div  id="header-wrapper" <?php if ( !is_home() ) {; ?>  style="display:none" <?php } ?> >				
			<div id="header">
				<div class="sequence-theme">
					<div id="sequence">
						<ul class="sequence-canvas">
							<li class="animate-in">
								<div class="title">Create Beautiful Web Templates</div>
								<img class="model" src="<?php echo WPDT_CHILD_THEME_URI ?>/images/slide1.png" alt="Create Beautiful Web Templates" />
							</li>
							<li>
								<div class="title">Free<br/>Online Template Builder</div>
								<img class="model" src="<?php echo WPDT_CHILD_THEME_URI ?>/images/slide2.png" alt="Easy Online Template Edidtor" />
							</li>
						</ul>
			
					</div>
				</div>
			</div>
		</div>
	</header>
	<div id="wrapper">
		<div  id="container">
				