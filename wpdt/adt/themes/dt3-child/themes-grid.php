



<?php if (have_posts()) : ?>


	<?php if (defined( ENABLE_WIX_ADS  )) : ?>

		<IFRAME SRC="http://ad.doubleclick.net/ddm/adi/N34703.1942300DOTEMPLATE.COM/B8599899.117716295;sz=960x90;ord=[timestamp]?" WIDTH=960 HEIGHT=90 MARGINWIDTH=0 MARGINHEIGHT=0 HSPACE=0 VSPACE=0 FRAMEBORDER=0 SCROLLING=no BORDERCOLOR='#000000'>
			<SCRIPT language='JavaScript1.1' SRC="http://ad.doubleclick.net/ddm/adj/N34703.1942300DOTEMPLATE.COM/B8599899.117716295;abr=!ie;sz=960x90;ord=[timestamp]?">
			</SCRIPT>
			<NOSCRIPT>
				<A HREF="http://ad.doubleclick.net/ddm/jump/N34703.1942300DOTEMPLATE.COM/B8599899.117716295;abr=!ie4;abr=!ie5;sz=960x90;ord=[timestamp]?" rel="nofollow">
					<IMG SRC="http://ad.doubleclick.net/ddm/ad/N34703.1942300DOTEMPLATE.COM/B8599899.117716295;abr=!ie4;abr=!ie5;sz=960x90;ord=[timestamp]?" BORDER=0 WIDTH=960 HEIGHT=90 ALT="Advertisement">
				</A>
			</NOSCRIPT>
		</IFRAME>

	<?php endif; ?>

	<?php $counter = 0; ?>

		<div class="gallery">
		<?php while (have_posts()) : the_post(); ?>
			
			<?php
				$thumbnail_image =  DT_THEMES_URI . "/" . get_post_meta($post->ID, "_dt_themeId", true) . "/th.jpg" ;
				$preview_image =  DT_THEMES_URI . "/" . get_post_meta($post->ID, "_dt_themeId", true) . "/prev.jpg" ;	
			?>
		
			<div class="thumbnail">

				<div class="thumbnail-inner">
			      	

					<?php if ( has_tag( "blank-theme" , $post ) ) : ?>

				      	<a href="<?php echo TOMCAT_URL; ?>/dt/createTheme?id=<?php echo get_post_meta($post->ID, "_dt_themeId", true); ?>"
								alt="edit template" rel="nofollow" class="dteditor">

				      		<img src="<?php echo $thumbnail_image; ?>" />
				 		
				 		</a>
					
					<?php else : ?>				

				      	<a href="<?php the_permalink() ?>" data-preview="<?php echo $preview_image; ?>" class="preview">
				      		<img src="<?php echo $thumbnail_image; ?>" />
				 		</a>

					<?php endif ?>	

			 		<div class="theme-actions">

			 			<div class="theme-action"> 			 				
							<a href="<?php echo TOMCAT_URL; ?>/dt/createTheme?id=<?php echo get_post_meta($post->ID, "_dt_themeId", true); ?>"
								alt="edit template" rel="nofollow" class="dteditor">
								
								<img class="icon" src="<?php echo WPDT_CHILD_THEME_URI ?>/images/edit-icon-16.png" alt="edit template"></img>

							</a>
							<a href="<?php echo TOMCAT_URL; ?>/dt/createTheme?id=<?php echo get_post_meta($post->ID, "_dt_themeId", true); ?>" 
								alt="edit template" rel="nofollow" class="dteditor">
								CUSTOMIZE IN TEMPLATE EDITOR...

							</a>

						</div>

						<?php if ( ! has_tag( "blank-theme" , $post ) ) : ?>

			 			<div class="theme-action"> 
							<a href="<?php the_permalink() ?>"
								alt="preview template" class="dtpreview"> 
								<img class="icon" src="<?php echo WPDT_CHILD_THEME_URI ?>/images/zoom-icon-16.png" alt="more template info"></img>
							</a>					
							<a href="<?php the_permalink() ?>" 
								alt="more template info">
								MORE INFO...
							</a>
						</div>
						<?php endif ?>						

			 			<div class="theme-action">
							<a href="<?php echo ( DT_THEMES_URI . "/" . get_post_meta($post->ID, "_dt_themeId", true ) . "/" . get_post_meta($post->ID, "_dt_themeId", true ) . ".zip" ) ?>"
								alt="download template" rel="nofollow">
								<img class="icon" src="<?php echo WPDT_CHILD_THEME_URI ?>/images/download-icon-16.png" alt="download template"></img>
							</a>						
							<a href="<?php echo ( DT_THEMES_URI . "/" . get_post_meta($post->ID, "_dt_themeId", true ) . "/" . get_post_meta($post->ID, "_dt_themeId", true ) . ".zip" ) ?>" 
								alt="download template" rel="nofollow" >
								FREE DOWNLOAD...
							</a>
						</div>						


					</div>  

				</div>

			</div>
			
			<?php if (defined( ENABLE_WIX_ADS  )) : ?>
				<?php if ( get_post_meta($post->ID, "_dt_wixPlacementId", true) ) : ?>
				<div class="thumbnail">
	
					<div class="thumbnail-inner">
				      	

						<IFRAME SRC="http://ad.doubleclick.net/ddm/adi/N34703.1942300DOTEMPLATE.COM/B8599899.<?php echo get_post_meta($post->ID, "_dt_wixPlacementId", true); ?>;sz=300x300;ord=[timestamp]?" WIDTH=300 HEIGHT=300 MARGINWIDTH=0 MARGINHEIGHT=0 HSPACE=0 VSPACE=0 FRAMEBORDER=0 SCROLLING=no BORDERCOLOR='#000000'>
							<SCRIPT language='JavaScript1.1' SRC="http://ad.doubleclick.net/ddm/adj/N34703.1942300DOTEMPLATE.COM/B8599899.<?php echo get_post_meta($post->ID, "_dt_wixPlacementId", true); ?>;abr=!ie;sz=300x300;ord=[timestamp]?"></SCRIPT>
							<NOSCRIPT>
								<A HREF="http://ad.doubleclick.net/ddm/jump/N34703.1942300DOTEMPLATE.COM/B8599899.<?php echo get_post_meta($post->ID, "_dt_wixPlacementId", true); ?>;abr=!ie4;abr=!ie5;sz=300x300;ord=[timestamp]?">
									<IMG SRC="http://ad.doubleclick.net/ddm/ad/N34703.1942300DOTEMPLATE.COM/B8599899.<?php echo get_post_meta($post->ID, "_dt_wixPlacementId", true); ?>;abr=!ie4;abr=!ie5;sz=300x300;ord=[timestamp]?" BORDER=0 WIDTH=300 HEIGHT=300 ALT="Advertisement">
								</A>
							</NOSCRIPT>
						</IFRAME>
					
					</div>
	
				</div>			
				<?php endif; ?>
			<?php endif; ?>			
		<?php endwhile; ?>	
		</div>
	
	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } // paginate using pagenavi plugin ?>

	
<?php endif; ?>
