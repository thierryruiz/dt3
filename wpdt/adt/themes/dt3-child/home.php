<?php
/*
Template Name : doTemplate home page
*/
?>
<?php get_header(); ?>



<style>
#container { width:100% }
</style>


<div id="page" class="homePage">
	

	<div class="layout-m">
		
		<div id="main">


			<?php 
			// ----------------
			// FEATUREDE THEMES   
			// ----------------
			?>

			<div id="themes" class="row">
				
				<?php query_posts('tag=featured'); ?>
				<?php get_template_part( 'themes-grid' ); ?> 
				
				<div class="clear"></div>
	
			</div>
	

			<div class="bigBtnWpr row">
				
				<a href="<?php bloginfo('url'); ?>/v3/tag/theme" title="Browse all templates">
					<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/browse.png" alt="browse all templates"></img>
				</a>
				
			</div>
	

			<section id="features">

				<div class="feature-bloc shade"> 
						
					<article id="easy-web-design" class="row group">

						<div class="col span_1_of_2">
							<header>
								<h1>Web design made easy</h1>
							</header>
							
							<p>
								Making your own Web template has never been easier. Our <strong>free online editor</strong> makes it a matter of minutes.
								No software to install. No Photoshop needed. No tricky PSD to HTML conversion required. No design service to pay. 
								All you need is there to create a stunning custom Web design ready for download. It's quick, easy and fun !
							</p>
						</div>

						<div class="col span_1_of_2">
							<figure>
								<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/screen-1.jpg" alt="theme screen" />
							</figure>
						</div>								
					
					</article>
					
				
				</div>

				<div class="feature-bloc"> 
						<article id="hundreds-options" class="row group">
							<header>
								<h1>Hundreds of design options</h1>
								<h2>To build your very unique template<h2>
							</header>
							<figure>
								<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/hundreds-of-settings.jpg" alt="template settings" />
							</figure>
							<p>
								All parts of your template can be customized. From header to footer, access hundreds of advanced settings to make
								a fully personalized Website template.
							</p>
						</article>
			
				</div>

				<div class="feature-bloc shade"> 
					<article id="hundreds-options" class="row group">
						<header>
							<h1>Design your header like a pro<h1>
						</header>
						
						<figure>
							<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/header-editor.jpg" alt="theme screen" />
						</figure>

						<p>
							Customize your header is a breeze with the advanced <strong>header editor</strong>, 
							featuring great text effects, classy fonts and premium photos
							library.
						</p>	
							
							
					</article>
				
				</div>


				<div class="feature-bloc"> 
				
					<article id="premium-photos" class="row group">

						<header>
							<h1>Premium photos library<h1>
						</header>
						
						<figure>
							<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/premium-photos.jpg" alt="premium photos"/>
						</figure>
						
						<p>
							<strong>Premium photos make your Web design looks great !</strong> For less than the price of a single premium photo, 
							you can build your own custom template and choose from a large collection of professional shootings. 
						</p>								
						
					</article>
									
				</div>
				<div class="feature-bloc shade"> 
				
		
						
					<article id="hundreds-graphics" class="row">
						<header>
							<h1>Hundreds of graphics to choose from<h1>
						</header>
						<div class="group">

							<div class="col span_1_of_3">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/gradients.jpg" alt="background gradients collection"/>
									<figcaption>Background gradients</figcaption>							
								</figure>								
							</div>
							<div class="col span_1_of_3">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/textures-patterns.jpg" alt="texures and patterns collection" />
									<figcaption>Texture and patterns</figcaption>						
								</figure>								
							</div>
							<div class="col span_1_of_3">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/color-schemes.jpg" alt="Apply color schemes on your theme in one click"/>
									<figcaption>"One-click" color schemes</figcaption>								
								</figure>								
							</div>
							
						</div>


						<div class="group">
							<div class="col span_1_of_3">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/fonts.jpg" alt="fonts" />
									<figcaption>400+ fonts</figcaption>						
								</figure>
							</div>

							<div class="col span_1_of_3">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/text-fx.jpg" alt="Apply text styles"/>
									<figcaption>Stunning text styles</figcaption>						
								</figure>
								
							</div>
						
							<div class="col span_1_of_3">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/logo-gallery.jpg" alt="Logo gallery" />
									<figcaption>Logo gallery</figcaption>								
								</figure>
							</div>

						</div>

					</article>
				
				</div>

				<div class="feature-bloc"> 
										
					<article id="wordpress-compatible" class="row">
					
						<header>
							<h1>Wordpress compatible templates</h1>
						</header>
					
						<div class="group">
							<div class="col span_1_of_2">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/wordpress-export.jpg" alt="Export your template as Wordpress theme" />
								</figure>
							</div>
							<div class="col span_1_of_2">	
								<p>
									You can export your custom template as <strong>Wordpress theme</strong>, the most popular Content Management System. 
									Publish your own Wordpress blog or Website today. 
								</p>
							</div>
						</div>
					</article>
					
				</div>
				

				<div class="feature-bloc shade"> 				
						
					<article id="clean-code" class="row">
								
						<header>
							<h1>Clean HTML5 code</h1>
							<h2>Easy to update, easy to adapt !</h2>
						</header>
						<div class="group">
							<div class="col span_1_of_2">
								<p>
									Most of website templates you will find on the market, are complex to modify to match your branding or to adapt to your content management system. 
									Templates created with our tool not only will be customized but the HTML code will be easy to adapt even if you are not an expert in HTML. 
									Try our free templates and see how easy it is to create a Website from our templates.
					
								</p>
							</div>
							<div class="col span_1_of_2">
								<figure>
									<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/code.jpg" alt="theme screen" />
								</figure>
							</div>								
						</div>
												
					</article>
				
				</div>				
						
			</section>
			
	
	
			<div class="bigBtnWpr row">
				<a id="try-now-btn" href="#themes" title="Browse all templates">
					<img src="<?php echo WPDT_CHILD_THEME_URI ?>/images/try-now.jpg" alt="Try template editor. It's free"></img>
				</a>
			</div>


		</div>
				
			
		<div class="clear" style="height:60px"></div>
	</div>
</div>
<?php get_footer(); ?>
