

// Choose image from Wordpress media manager
jQuery(document).ready(function() {

	window.restore_send_to_editor = window.send_to_editor;
		
	jQuery( '.dt_upload_button' ).click( function() {
		
		mediaName =  jQuery(this).attr( 'name' ) ;
		
		formfield = jQuery('#' + mediaName ).attr( 'name' );
		
		tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
		
		window.send_to_editor = function( html ) {
			 var source = html.match(/src=\".*\" alt/);
			 source = source[0].replace(/^src=\"/, "").replace(/" alt$/, "");
			 jQuery('#' + mediaName ).attr('value', source);
			 jQuery('#' + mediaName + '_thumbnail').attr('src', source);
			 tb_remove();
			 window.send_to_editor = window.restore_send_to_editor;
		}
		
		return false;
	});

});
