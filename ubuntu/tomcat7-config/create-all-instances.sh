#!/bin/sh
# configure 3 tomcat instances for load balancing


CATALINA_BASE=/var/lib/tomcat7

./create-instance.sh tomcat1 8180
./create-instance.sh tomcat2 8280
./create-instance.sh tomcat3 8380

mkdir $CATALINA_BASE/tomcat1/webapps/dt3
ln -s $CATALINA_BASE/tomcat1/webapps/dt3  $CATALINA_BASE/tomcat2/webapps/dt3
ln -s $CATALINA_BASE/tomcat1/webapps/dt3  $CATALINA_BASE/tomcat3/webapps/dt3

chown -R tomcat7:tomcat7 $CATALINA_BASE



