#!/bin/sh
# configure a tomcat instance

CATALINA_BASE=/var/lib/tomcat7

tomcat7-instance-create -p $2 $CATALINA_BASE/$1

cp tomcat7 /etc/init.d/$1

chmod +x /etc/init.d/$1

update-rc.d $1 defaults

ln -s /etc/tomcat7/policy.d/  $CATALINA_BASE/$1/conf/policy.d

sed 's/instance/'$1'/g' ./tomcat7 > /etc/init.d/$1

chmod +x /etc/init.d/$1
update-rc.d $1 defaults


