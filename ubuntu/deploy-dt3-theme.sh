#!/bin/sh
# deploy the content /home/yoman/transfer/wpdt/adt/themes  to /var/www/wpdt/adt/themes


RETVAL=0

echo "Deploying doTemplate wpdt dt3 theme"
echo

rsync -azv /home/yoman/transfer/wpdt/adt/themes/ /var/www/wpdt/adt/themes/
chown www-data:www-data /var/www/wpdt/adt/* -R
chmod -R 700 /var/www/wpdt/adt/themes/

echo
echo "Done."
