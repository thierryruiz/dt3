#!/bin/sh
# deploy the content /home/yoman/transfer/wp/themes to /var/www/wp/zee  

RETVAL=0

echo "Deploying doTemplate wp theme..." 
echo 

rsync -azv /home/yoman/transfer/wp/themes/ /var/www/wpdt/zeee-content/themes/
chown www-data:www-data /var/www/wpdt/zeee-content/* -R
chmod -R 700 /var/www/wpdt/zeee-content/

echo 
echo "Done."
