/** 
A light version of pngfix 
	- fixes only png background and not img tag 
	- allows to pass sizingMethod in parameter crop|scale (default=scale)
**/

(function($) {

jQuery.fn.pngie = function(sizingMethod) {
	
	var ie55 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 5.5") != -1);
	var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);

	if (jQuery.browser.msie && (ie55 || ie6)) {
		if (sizingMethod == null) {sizingMethod = 'scale'} ;
		var bgIMG = jQuery(this).css('background-image');
		if(bgIMG.indexOf(".png")!=-1){
			var iebg = bgIMG.split('url("')[1].split('")')[0];
			jQuery(this).css('background-image', 'none');	
			jQuery(this).get(0).runtimeStyle.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + iebg + "',sizingMethod='"+ sizingMethod + "')";
		}
	}
	
	return jQuery;

};

})(jQuery);
