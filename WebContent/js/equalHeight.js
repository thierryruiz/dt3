function setEqualHeight(blocs) {
	var tallest = 0;
	blocs.each(function() {
		var thisHeight = $(this).height();
		if(thisHeight > tallest) {
			tallest = thisHeight;
		}
	});
	blocs.height(tallest);
}
