

function initDtLayout() {
	global	$dtLayoutClass  ;
	global	$dtLayout  ;
	global	$dtPostLayout  ;
	
	$dtLayout = get_settings( "dt_page_layout" );
	$dtPostLayout = get_settings( "dt_post_layout" );	
	$dtLayoutClass = "m" ;
	
	if ( $dtLayout == "Full width" ){ $dtLayoutClass = "m"; } 
	if ( $dtLayout == "Sidebar left" ){ $dtLayoutClass = "sm"; }
	if ( $dtLayout == "Sidebar right" ){ $dtLayoutClass = "ms"; }
	if ( $dtLayout == "Sidebars split" ){ $dtLayoutClass = "sms"; }
	if ( $dtLayout == "Two sidebars right" ){ $dtLayoutClass = "mss"; }
}


add_action( 'init', 'initDtLayout' );


if ( function_exists('register_sidebar') ) {

	register_sidebar( array( 
		'name' 			=> 'Sidebar 1',
		'id' 			=> 'sidebar-1',
		'before_widget' => '<div id="%1\$s" class="widget %2\$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' => '</h2>')
	);
	
	register_sidebar( array( 
		'name' 			=> 'Sidebar 2',
		'id' 			=> 'sidebar-2',
		'before_widget' => '<div id="%1\$s" class="widget %2\$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<h2>',
		'after_title' => '</h2>')
	);
}


function get_sidebar2() {
	get_sidebar('2');
}

