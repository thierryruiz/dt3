<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body>

	<style>
	<!--
		.button {
			width:50%;
			margin:20px ;
			cursor:pointer;
		}
		
		input {
			width:70%;
			padding:5px;
			border:1px solid #CCC;
			background-color:#EEE;
			text-align:center;
		}
		
		a img {
			border:0;
		}
	
	-->
	</style>

	<script type="text/javascript">
	    function select_all(obj) {
	        var text_val=eval(obj);
	        text_val.focus();
	        text_val.select();
	        if (!document.all) return; // IE only
	        r = text_val.createTextRange();
	        r.execCommand('copy');
	    }
	</script>

	<div style="width:80%;margin:0 auto">
		
		<h3>Thank you for your purchase !</h3>
			
		<p>		
			In a few minutes, please check your email box <strong>${payerEmail}</strong>. 
		</p>
		<p>
			After payment processing you will receive your download instructions.
			<br/>
			<em>If you haven't received this email after few minutes, please verify your spam folder.</em>
		</p>
								
	</div>

</body>
