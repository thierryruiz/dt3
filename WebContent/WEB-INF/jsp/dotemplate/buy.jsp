<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body>

	<p style="font-size:1.2em">
		You are redirected to PayPal, please wait...
	</p>
	
   	<form name="pplForm" action="${paypal.url}" method="post" style="visibility:hidden">
		<input type="hidden" name="cmd" value="_s-xclick">
		<input type="hidden" name="hosted_button_id" value="${paypal.buttonId}">
		<input id="notify_url" type="hidden" name="notify_url" value="${paypal.notifyUrl}">
		<input type="image" src="https://www.paypal.com/en_US/FR/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="">
		<input type="hidden" name="return" value="${paypal.returnUrl}">
		<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
		
	</form>		        

	<script language="javascript">
       setTimeout('document.forms["pplForm"].submit();',2000 );
	</script>
	

</body>
