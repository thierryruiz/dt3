<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<body>

	<c:if test="${devMode == false}"> 
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-4680957-1', 'auto');
		  ga('send', 'pageview');
		</script>
	</c:if>
	
	<script type="text/javascript" language="javascript" src="gwt/dtcaptcha/dtcaptcha.nocache.js?v3.01"></script>
	
	<img src="images/splash.jpg" />
	
	<br/>
	
	<p id="loader">
		<img src="images/ajax-loader.gif" />
	<p>
	
	<p id="error" style="display:none">
		Sorry. The server was unable to process your request.
		<br/>
		It may be a temporary problem. Please retry later or contact us at 
		<a href="mailto:mail@dotemplate.com">mail@dotemplate.com</a> to report us the issue. Thank you.
	</p>

</body>
