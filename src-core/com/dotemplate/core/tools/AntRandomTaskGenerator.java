package com.dotemplate.core.tools;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import java.util.Random;

/**
 * 
 * Generates a random value to use in ant build. Used to foce browser to reload
 * GWT cache when a new version is deployed
 * 
 * 
 */
public class AntRandomTaskGenerator extends Task {

	private String min;
	private String max;
	private String property;

	private Random random = new Random( System.currentTimeMillis() );

	@Override
	public void execute() throws BuildException {
		if (min == null || min.equals(""))
			throw new BuildException("Min property not specified");

		if (max == null || max.equals(""))
			throw new BuildException("Max property not specified");

		int minInt = Integer.parseInt(min);
		int maxInt = Integer.parseInt(max);

		if (minInt > maxInt)
			throw new BuildException("Min is bigger than max");

		int randomInt = calculateRandom(minInt, maxInt);

		getProject().setNewProperty(property, String.valueOf(randomInt));
	}

	protected int calculateRandom(int minInt, int maxInt) {
		return minInt + random.nextInt(maxInt - minInt + 1);
	}

	public void setMin(String min) {
		this.min = min;
	}

	public void setMax(String max) {
		this.max = max;
	}

	public void setProperty(String property) {
		this.property = property;
	}

}