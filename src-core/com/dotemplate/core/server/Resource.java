/**
 * Copyright Thierry Ruiz ( Athesanet ) 2004 - 2008
 * All rights reserved.
 *
 * Created on 4 sept. 08 : 14:46:33
 */
package com.dotemplate.core.server;

import java.io.File;


/**
 * 
 * 
 * @author Thierry Ruiz
 *
 */
public class Resource {

	private String uri ;
	
	private String editedUri ;
		
	
	public Resource ( String uri ){
		
		uri = uri.replace ( '\\', '/' ) ;
		
		this.uri = uri ;
		
		// nnn.ext > .nnn.ext
		// images/ext.jpg > images/.ext.jpg
		
		int i = uri.lastIndexOf  ( '/' ) ;
		if ( i == -1 ) {
			this.editedUri = '.' + uri ;
		} else {
			this.editedUri = uri.substring ( 0, i + 1 ) + '.' + uri.substring ( i + 1  ) ;
		}			
		
	}
	
	
	public String getURI() {
		
		File edited = getEditedFile() ;
		
		if ( DesignSession.get ().isEditing () && edited.exists () ){
			return editedUri ;
		} else {
			return uri ;
		}
	}
	
	
	public File getFile() {
		
		File edited = getEditedFile() ;
		
		if ( DesignSession.get ().isEditing () && edited.exists () ){
			return getEditedFile() ;
		} else {
			return new File ( App.realPath ( File.separator + uri ) ) ;
		}
	}
	
	
	private File getEditedFile() {
		return new File ( App.realPath ( File.separator + editedUri ) ) ;	
	}
	
	
}
