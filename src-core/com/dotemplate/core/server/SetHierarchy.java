package com.dotemplate.core.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.properties.PropertySet;



/**
 * 
 * This bean holds hierarchy of sets used during theme generating and is stored in local thread. 
 * This bean is used by the #superxxx($s) macros 
 * 
 * @author T Ruiz
 *
 */
public class SetHierarchy {
	
	
	private static ThreadLocal<SetHierarchy> threadLocal = new ThreadLocal< SetHierarchy >() ;
	
	
	private Map< String, Iterator< PropertySet > > ancestors = new HashMap< String, Iterator< PropertySet > >() ;
	

	private SetHierarchy buildHirerarchy ( PropertySet set ) {
		
		PropertySet parent = DesignUtils.getSymbol ( set ) ;
		
		if ( parent != null ){
			
			parent = DesignUtils.getSymbol ( DesignUtils.getSymbol ( set ) ) ;
		
		}
			
		ArrayList<PropertySet> list = new ArrayList<PropertySet>() ;
		
		while ( parent != null ) {
			list.add ( parent ) ;
			parent =  DesignUtils.getSymbol ( parent ) ;
		}
		
		ancestors.put ( set.getUid (), list.iterator () ) ;
		
		return this ;
	
	}


	
	private PropertySet nextAncestor( PropertySet set ) throws AppException {
		
		Iterator< PropertySet > ancestorsIt = ancestors.get (  set.getUid () ) ;
		
		if ( ancestorsIt == null  ){
			throw new AppException ( "No hierarchy created for set '" + 
					set.getName () + " ' " + set.getPath() ) ;
		}
		
		if ( ancestorsIt.hasNext () ){
			return ancestorsIt.next () ;
		} else {
			throw new AppException ( "No more ancestor symbol found in set '" + set.getName () + "' hirerarchy." ) ;
		}
		
	}
	

	private static void set( SetHierarchy setInfo ){
		threadLocal.set ( setInfo ) ;
	}

	
	public static SetHierarchy get() {
		return threadLocal.get() ;
	}

	
	public static void clear () {
		threadLocal.remove ();
	}
	
	
	
	public static void create( PropertySet set ){

		SetHierarchy  h = SetHierarchy.get () ;
		
		if ( h == null ){
			h = new SetHierarchy() ;
			SetHierarchy.set( h ) ;
		}
		
		h.buildHirerarchy ( set ) ;
		
	}
	
		
	
	public static PropertySet next( PropertySet set ) throws AppException {
		
		SetHierarchy  h = SetHierarchy.get () ;
		
		if ( h == null ){
			throw new AppRuntimeException ( "No SetHierarchy instance in ThreadLocal" ) ;
		}
			
		return h.nextAncestor ( set ) ;
	}
	
	
}
