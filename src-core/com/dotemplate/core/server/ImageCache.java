package com.dotemplate.core.server;

import java.util.HashMap;


public abstract class ImageCache<IMAGE extends CachedImage> extends HashMap< String, IMAGE > {
	
	private static final long serialVersionUID = 8303798599069467249L;

	
	public void setImage ( IMAGE image ){
	
		IMAGE current = get ( image.getId() ) ;
		
		if( current != null ) {
			current.setImage( image.getImage() ) ;
		} else {
			put ( image.getId(), image ) ;
		}
	}
	
	
	public  IMAGE getImage( String id ) {
		return get( id ) ; 
	}
	
	
	
	public void trackChanges (){
		for ( IMAGE image : values() ){
			image.setChanged( false ) ;
		}
	}
	
}
