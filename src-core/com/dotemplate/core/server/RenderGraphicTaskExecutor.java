package com.dotemplate.core.server;

import com.dotemplate.core.server.util.ConcurrentTask;
import com.dotemplate.core.server.util.ConcurrentTaskExecutor;
import com.dotemplate.core.server.util.ConcurrentTaskPool;


public class RenderGraphicTaskExecutor extends ConcurrentTaskExecutor< Void > {

	public RenderGraphicTaskExecutor () {
		super ( 50 );
	}
	
	
	@Override
	protected ConcurrentTaskPool< Void > createPool () {
		
		return new ConcurrentTaskPool< Void >() {
			
			@Override
			protected ConcurrentTask< Void > createTask () throws Exception {
				return new RenderGraphicTask() ;
			}
		};
	}
	
}