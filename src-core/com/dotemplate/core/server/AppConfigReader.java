/**
 * Copyright Thierry Ruiz - Athesanet 2004-2008. All rights reserved.
 * Created on 3 mars 08 - 21:45:50
 *
 */
package com.dotemplate.core.server;

import java.beans.IntrospectionException;
import java.io.InputStream;

import org.apache.commons.betwixt.io.BeanReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;


/**
 *
 * A Reader parsing the application configuration xml file
 * 
 * 
 * @author Thierry Ruiz
 *
 */
public class AppConfigReader< APPCONFIG extends AppConfig > {
	
	private final static Log log = LogFactory
			.getLog ( AppConfigReader.class );
	
	private BeanReader reader ;

	
	public AppConfigReader( Class< APPCONFIG > clazz ) throws AppRuntimeException {
		
		reader = new BeanReader () ;
		reader.getXMLIntrospector ().getConfiguration ().setAttributesForPrimitives ( true );
		
		try {
			
			reader.registerBeanClass( clazz );
			
		} catch ( IntrospectionException e ) {
			log.fatal (  "Failed to create bean reader",  e ) ;
			throw new AppRuntimeException( e ) ;
		}
		
		reader.getXMLIntrospector().getConfiguration().setAttributesForPrimitives( true );
		
		reader.addSetProperty ( "app/property", "name", "value" );
		
	}

	
	
	@SuppressWarnings( "unchecked" )
	public APPCONFIG read( String xml ) throws AppRuntimeException {
		
		if ( log.isDebugEnabled () ){
			
			log.debug (  "Loadind configuration..." ) ;
		
		}
		
		InputStream is = this.getClass().getClassLoader().getResourceAsStream( xml ) ;
		
		if ( is == null ){
			throw new AppRuntimeException ( "Failed to parse application configuration file " + xml 
					+ ". Is the configuration file in the classpath ?" ) ;
		}
		
		try {
			
			return ( APPCONFIG ) reader.parse ( is ) ;
		
		} catch ( Exception e ) {
				
			log.fatal (  e ) ;
			throw new AppRuntimeException ( "Failed to parse application configuration file " + xml , e ) ;
		
		}
		
		
	}	

}
