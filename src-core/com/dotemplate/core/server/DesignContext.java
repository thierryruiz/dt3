package com.dotemplate.core.server;

import org.apache.velocity.context.Context;

import com.dotemplate.core.server.affiliate.Affiliate;
import com.dotemplate.core.shared.Design;

public interface DesignContext< D extends Design > extends Context {
	
	public final static String RANDOM = "random" ;
	
	public static final String DEV_MODE = "devMode" ;
	
	public final static String TEST_MODE = "testMode" ;

    public static final String HELPER = "Helper" ;
		
	public static final String EXPORT = "export" ;
	
	public static final String WATERMARK = "watermark" ;
	
	public final static String WORK_DIR = "workDir" ;
	
	public static final String SLICES = "slices" ;
	
	public static final String DOSLICE = "doSlice" ;


	D getDesign() ;
	
    void preprocess() ;
	
	void updateRandom() ;
	
	boolean isExportMode() ;
	
	CanvasImageCache getCanvasImageCache() ; 
	
	Dependencies getDependencies() ;

	Affiliate getAffiliate() ;
	
	void setAffiliate( Affiliate affiliate ) ;
	
	PurchaseInfo getPurchaseInfo() ;
	
	void setPurchaseInfo( PurchaseInfo purchaseInfo ) ;
	
}
