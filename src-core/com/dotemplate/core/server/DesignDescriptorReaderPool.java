package com.dotemplate.core.server;


import com.dotemplate.core.server.util.Pool;
import com.dotemplate.core.shared.Design;


public abstract class DesignDescriptorReaderPool<D extends Design > extends Pool< DesignDescriptorReader< D > > {

	public DesignDescriptorReaderPool() {
		super() ;
		innerPool.setMaxActive(-1) ;
	}
	

}
