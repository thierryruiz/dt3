package com.dotemplate.core.server;

import java.awt.Color;

import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.Scheme;

public abstract class ColorSchemeFactory< D extends Design > {

	public abstract Scheme createScheme ( String name, String tokens ) ;
		
	protected HSLColor getHSLColor( String hexa ) {
		return ColorUtils.getHSLColor( hexa ) ;
	}
	
	protected HSLColor getHSLColor( Color color ) {
		return ColorUtils.getHSLColor( color ) ;
	}

	protected  Color getColor( String hexa ) {
		return Color.decode( hexa ) ;
	}
	
	protected String toHexString( Color color ) {
        return ColorUtils.toHexString( color ) ;        
    }

	public boolean isDark( Color c ) {
		return ColorUtils.isDark( c );
	}
    	
	protected String getOpposite( Color c ) {
		return toHexString( ColorUtils.getOpposite( c ) ) ;
	}
	
	
	protected String getOppositeSoft( Color c ){
		return toHexString( ColorUtils.getOppositeSoft( c ) );
	}
		
	
}
