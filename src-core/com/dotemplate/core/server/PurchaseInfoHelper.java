package com.dotemplate.core.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.shared.Design;


public class PurchaseInfoHelper {
	
	private static Log log = LogFactory.getLog( PurchaseInfoHelper.class );

	public static PurchaseInfo load( String designUid ) throws AppException {
		
		PurchaseInfo info = new PurchaseInfo() ;
		
		Properties props = new Properties() ;
		
		File purchaseDesc = new File (  App.get().getWorkRealPath( designUid ) + "/purchase.properties" ) ;
		
		if ( ! purchaseDesc.exists() ){
			return null ;
		}
		
		try {

			props.load( new FileInputStream( purchaseDesc ) )  ;
			
			info.setDownloads( Integer.parseInt( props.getProperty( "downloads" ) ) ) ;
			info.setGateway( props.getProperty( "gateway" ) ) ;
			info.setTransactionId( props.getProperty( "transactionId" ) ) ;
			info.setPayer( props.getProperty( "payer" ) ) ;
			info.setAffiliateId( props.getProperty( "affiliateId" ) ) ;
			
		} catch ( Exception e ){
			
			log.error( "Failed to load purchase info for uid " + designUid ,  e ) ;
			throw new AppException( "Failed to read purchase info from " + purchaseDesc.getAbsolutePath(), e ) ;
			
		}
		
		return info ;
		
	}

	
	
	
	public static void save()  throws AppException {
		
		log.info( "Saving puchased info..." ) ;
		
		Design design = App.getDesign() ;
		
		if ( ! design.isPurchased() ) {
						
			throw new AppException(  "Attempt to save purchased info while design has not been purchased" ) ;
			
		}
		
		PurchaseInfo info = App.getDesignContext().getPurchaseInfo() ; 
		
		if ( info == null ) {
			
			throw new AppException( "No PurchaseInfo instance to save in design context for for uid " + design.getUid() ) ;
		}
		
		
    	// create theme properties
    	Properties props = new Properties() ;
    	props.setProperty( "downloads", "" + info.getDownloads() ) ;
    	props.setProperty( "gateway", info.getGateway() );
    	props.setProperty( "payer", info.getPayer() );
    	props.setProperty( "transactionId", info.getTransactionId() );
    	props.setProperty( "affiliateId", info.getAffiliateId() ) ;
    	
    	
    	FileOutputStream out = null ;
    	
    
    	try {
    		
    		File file = new File( App.get().getWorkRealPath() + "/purchase.properties" ) ;
    		
    		log.info( "Saving puchased info under " + file.getAbsolutePath() ) ;
    		
    		out = new FileOutputStream( file ) ;
    		
    		props.store( out, "" ) ;
    		
    	} catch ( Exception e ){
    		
    		throw new AppException("Failed to save purchase info" ,  e) ;
    		
    	} finally {
    		if ( out != null ){
    			try {
    				out.flush() ;
    			} catch ( Exception e ){
    				log.error(  e ) ;
    			}
    		}
    	}
		
	}

}
