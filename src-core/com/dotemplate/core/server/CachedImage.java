package com.dotemplate.core.server;

import java.awt.image.BufferedImage;
import java.io.Serializable;

public abstract class CachedImage implements Serializable {
	
	private static final long serialVersionUID = -7607301367876879623L;

	public enum Format { PNG, JPG } ;
	
	protected Format format ;
	
	protected BufferedImage image ; 
	
	protected String id ;
	
	protected boolean changed ; 
	
	
	protected CachedImage( String id, BufferedImage image, Format format ){	
		this.id = id ;
		this.image = image ;
		this.format = format ;
		changed = true ;
	}
	
	
	public String getId() {
		return id;
	}
	
	public Format getFormat() {
		return format;
	}

	public BufferedImage getImage() {
		return image;
	}
	
	
	public void setImage( BufferedImage image ) {
		this.image = image;
		changed = true ;
	}
	
	
	public boolean isChanged() {
		return changed;
	}
	
	
	public void setChanged( boolean changed ) {
		this.changed = changed;
	}

	
	public abstract int getType() ;

	
}
