package com.dotemplate.core.server;

import java.io.FileInputStream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Properties;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.SVGFont;


public class SVGFontProvider extends DesignResourceProvider< SVGFont > {

	private final static Log log = LogFactory.getLog ( SVGFontProvider.class );

	
	public void load () {

		if ( log.isDebugEnabled () ){
			log.debug ( "Listing svg fonts..."  ) ;
		}
		
		// SVG fonts are located under WEB-INF/templates/_fonts directory
		// The free.properties and premium.properties files list the fonts
		// and provide font metrics informations ( avoids dynamic metrics
		// calculation ) used at rendering.
		
		// load fonts
		
		Properties freeProps = new Properties() ;
		Properties premiumProps = new Properties() ;
		
		try {
			
			freeProps.load( new FileInputStream( App
					.realPath( "WEB-INF/templates/_fonts/free.properties" )));
			
			premiumProps.load( new FileInputStream( App
					.realPath("WEB-INF/templates/_fonts/premium.properties")));
		
		} catch ( Exception e ) {
			
			throw new AppRuntimeException ( "Failed to load SVG fonts", e ) ;
		
		}


		SVGFont font ;
		
		for ( Entry< Object, Object > entry  : freeProps.entrySet() ) {
			
			font = createSVGFont( ( String ) entry.getKey(), StringUtils.split( ( String ) entry.getValue(), ',' ), true );
			
			createThumbnailHtml ( font ) ;
			
			resourcesMap.get( ALL_RESOURCES ).put  ( font.getFamily(), font ) ;
			
		}

		
		ArrayList< SVGFont > premiumFonts = new ArrayList< SVGFont >( ) ; 
				
				
		for ( Entry< Object, Object > entry  : premiumProps.entrySet()  ) {

			font = createSVGFont( ( String ) entry.getKey(), StringUtils.split( ( String ) entry.getValue(), ',' ), false );
			
			createThumbnailHtml ( font ) ;
			
			premiumFonts.add( font ) ;
			
		}	
		
		
		
		Collections.sort( premiumFonts, new Comparator< SVGFont >() {
			@Override
			public int compare( SVGFont f1, SVGFont f2 ) {
				return f1.getFamily().compareTo( f2.getFamily() ) ;
			}
		});
				
		
		for ( SVGFont f : premiumFonts ){

			resourcesMap.get( ALL_RESOURCES ).put  ( f.getFamily(), f ) ;
			
		}
		
		if ( log.isDebugEnabled () ){
			log.debug ( "SVG fonts loaded."  ) ;
		}
		
	
	}

		
	
	protected SVGFont createSVGFont( String family, String[] metrics, boolean free ){
		
		SVGFont font = new SVGFont() ;
		font.setFamily( family ) ;
		
		font.setAscent( Integer.parseInt( metrics [ 0 ])) ;
		font.setDescent( Integer.parseInt( metrics [ 1 ])) ;
		
		font.setFree( free ) ;
		return font ;
			
	}
	
	
	
	private void createThumbnailHtml ( SVGFont font ){
		if ( font.isFreeResource() ){
			font.setThumbnailHtml ( "<img src=\"images/svgfonts/free/" + font.getFamily() + ".png\" />" ) ;
		} else {
			font.setThumbnailHtml ( "<img src=\"images/svgfonts/premium/" + font.getFamily() + ".png\" />" ) ;
		}
	}
	
	
	


	
}
