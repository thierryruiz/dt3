package com.dotemplate.core.server.util;

import java.lang.reflect.Constructor;
import java.util.Map;

import org.apache.commons.collections.FastHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AppRuntimeException;


public class Singletons {

	private static Log log = LogFactory.getLog ( Singletons.class );
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map<Class, Object> _singletons = new FastHashMap ();
	
	
	public static Object getSingleton ( String className ) {
		if ( className == null ) {
			throw new NullPointerException ( "Cannot get singleton from null class name" );
		}

		if ( log.isDebugEnabled () ) {
			log.debug ( "Get singleton from class '" + className + "'" );
		}

		try {
			return getSingleton ( Class.forName ( className ) );

		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Cannot get singleton instance '" + className + "'", e ) ;
		}
	}


	public static synchronized Object getSingleton ( Class<?> clazz ) {
		Object result = _singletons.get ( clazz );
		if ( result == null ) {
			result = createSingleton ( clazz );
			_singletons.put ( clazz, result );
		}
		return result;
	}


	private static Object createSingleton ( Class<?> clazz ) {
		if ( log.isDebugEnabled () ) {
			log.debug ( "Create singleton for class '" + clazz.getName () + "'" );
		}

		try {
			Constructor<?> c = clazz.getDeclaredConstructor ( new Class[] {} );
			c.setAccessible ( true );
			return c.newInstance ( new Object[] {} );
		} catch ( Exception e ) {
			throw new AppRuntimeException ( "Cannot create singleton instance '" + clazz + "'", e );
		}
	}
	
	
}
