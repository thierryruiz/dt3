/**
 * All information herein is either public information or is the property of and owned solely by Athesanet SARL. who shall
 * have and keep the sole right to file patent applications or any other kind of intellectual property protection in
 * connection with such information.
 * Nothing herein shall be construed as implying or granting to you any rights, by license, grant or otherwise, under any
 * intellectual and/or industrial property rights of or concerning any of Athesanet� information.
 * This document can be used for informational, non-commercial, internal and personal use only provided that:
 * � The copyright notice below, the confidentiality and proprietary legend and this full warning notice appear in all
 * copies.
 * � This document shall not be posted on any network computer or broadcast in any media and no modification of
 * any part of this document shall be made.
 * Use for any other purpose is expressly prohibited and may result in severe civil and criminal liabilities.
 * The information contained in this document is provided �AS IS� without any warranty of any kind. Unless otherwise
 * expressly agreed in writing, Athesanet makes no warranty as to the value or accuracy of information contained herein.
 * The document could include technical inaccuracies or typographical errors. Changes are periodically added to the
 * information herein. Furthermore, Athesanet reserves the right to make any change or improvement in the specifications
 * data, information, and the like described herein, at any time.
 * Athesanet hereby disclaims all warranties and conditions with regard to the information contained herein,
 * including all implied warranties of merchantability, fitness for a particular purpose, title and non-infringement.
 * In no event shall Athesanet be liable, whether in contract, tort or otherwise, for any indirect, special or
 * consequential damages or any damages whatsoever including but not limited to damages resulting from loss of
 * use, data, profits, revenues, or customers, arising out of or in connection with the use or performance of
 * information contained in this document.
 * Athesanet does not and shall not warrant that this product will be resistant to all possible attacks and shall not
 * incur, and disclaims, any liability in this respect. Even if each product is compliant with current security
 * standards in force on the date of their design, security mechanisms' resistance necessarily evolves according to
 * the state of the art in security and notably under the emergence of new attacks. Under no circumstances, shall
 * Athesanet be held liable for any third party actions and in particular in case of any successful attack against
 * systems or equipment incorporating Athesanet products. Athesanet disclaims any liability with respect to security
 * for direct, indirect, incidental or consequential damages that result from any use of its products. It is further
 * stressed that independent testing and verification by the person using the product is particularly encouraged,
 * especially in any application in which defective, incorrect or insecure functioning could result in damage to
 * persons or property, denial of service or loss of privacy.
 */

package com.dotemplate.core.server.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * Use this annotation to trace the time spend in a method  
 * 
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Duration {

	String value() default "" ;
	
}
