package com.dotemplate.core.server.command;


import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.command.GetProperty;
import com.dotemplate.core.shared.command.GetPropertyResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;


public class GetPropertyHandler extends CommandHandler< GetPropertyResponse > {
	
	
	
	@Override
	public GetPropertyResponse handleAction ( Command<GetPropertyResponse> action ) throws WebException {
				
		GetPropertyResponse response = new GetPropertyResponse() ;
		
		response.setProperty( DesignUtils.findPropertyByUid( App.getDesign() , 
				( ( GetProperty ) action ).getUid() ) ) ;
		
		return response ;
		
	}

	
}
