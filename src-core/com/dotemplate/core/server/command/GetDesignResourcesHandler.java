package com.dotemplate.core.server.command;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.command.GetDesignResourcesHandler;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.command.GetDesignResources;
import com.dotemplate.core.shared.command.GetDesignResourcesResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;


public abstract class GetDesignResourcesHandler< RESOURCE extends DesignResource >
		extends CommandHandler< GetDesignResourcesResponse< RESOURCE > > {

	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( GetDesignResourcesHandler.class );

	
	
	@Override
	public GetDesignResourcesResponse< RESOURCE > handleAction(
			Command< GetDesignResourcesResponse< RESOURCE > > action)
			throws WebException {

		
		GetDesignResources< RESOURCE > getDesignResource = 
			( GetDesignResources< RESOURCE > ) action ;
		
		
		GetDesignResourcesResponse< RESOURCE > response = new GetDesignResourcesResponse< RESOURCE >();
		
		
		ArrayList< RESOURCE > resources = getDesignResources( action );
		
		int paging = getDesignResource.getPagingLength() ;

		if ( paging == -1 ) {
			
			// no paging : return all results
			response.setResources( resources );

			
		} else {

			// paging create : return results sub list 
			int start = getDesignResource.getIndex();
			int end = start + paging;
			int size = resources.size();

			boolean hasMore = end < size;

			if (! hasMore ) end = size;

			response.setResources( new ArrayList< RESOURCE >( resources.subList( start, end ) ) );
			response.setHasMoreResults( hasMore );

		}

		
		return response;
	}

	
	
	protected abstract ArrayList< RESOURCE > getDesignResources( 
			Command< GetDesignResourcesResponse< RESOURCE > > action );

	
}
