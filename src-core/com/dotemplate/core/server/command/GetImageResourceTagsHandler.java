package com.dotemplate.core.server.command;


import com.dotemplate.core.server.App;
import com.dotemplate.core.server.ImageResourceProvider;
import com.dotemplate.core.shared.canvas.ImageResource;

public class GetImageResourceTagsHandler extends GetDesignResourcesTagsHandler< ImageResource > {
	
	@Override
	protected String[] getDesignResourcesTags() { 
		return ( ( ImageResourceProvider ) App.getSingleton ( ImageResourceProvider.class ) ).getResourcesTags() ;
	}
	
}
