package com.dotemplate.core.server.command;


import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.command.GetDesignResourcesMapResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.theme.server.symbol.ColorSchemeProvider;


public class GetAllSchemesHandler extends GetDesignResourcesMapHandler< Scheme >{

	private static Log log = LogFactory.getLog ( GetAllSchemesHandler.class );

	
	
	
	@Override
	protected LinkedHashMap<String, LinkedHashMap<String, Scheme > > getDesignResourcesMap(
			Command< GetDesignResourcesMapResponse <Scheme > > action) {
		
		if ( log.isDebugEnabled () ) {
			log.debug ( "Returning all schemes...") ;
		}
		
		return ( ( ColorSchemeProvider ) App.getSingleton(
				ColorSchemeProvider.class ) ).getResourcesMap();
	}
	
	
	
}
