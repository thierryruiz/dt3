package com.dotemplate.core.server.command;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.command.GetSymbolHandler;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.command.GetSymbol;
import com.dotemplate.core.shared.command.GetSymbolResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;


public class GetSymbolHandler extends CommandHandler< GetSymbolResponse > {

	private static Log log = LogFactory.getLog ( GetSymbolHandler.class );

	
	@Override
	public GetSymbolResponse handleAction ( Command<GetSymbolResponse> action ) throws WebException {
				
		GetSymbolResponse response = new GetSymbolResponse() ;
		
		GetSymbol getSymbol = ( GetSymbol ) action ;
		
		if ( log.isDebugEnabled () ) {
			
			log.debug ( "Lookup symbol  '" + getSymbol.getName() + "'"  ) ;
		
		}
		
		try {
			
			response.setSymbol ( DesignUtils.createFromSymbol( getSymbol.getName() ) )  ;
		
		} catch ( Exception e ) {
			
			throw new WebException( "Unable to get symbol copy ", e ) ;
		}
		
		return response ;
		
	}

	
}
