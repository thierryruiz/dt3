package com.dotemplate.core.server.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.command.GetDesignResourcesTagsResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;



public abstract class GetDesignResourcesTagsHandler< RESOURCE extends DesignResource >
		extends CommandHandler< GetDesignResourcesTagsResponse< RESOURCE > > {

	
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog( GetDesignResourcesTagsHandler.class );

	
	@Override
	public GetDesignResourcesTagsResponse< RESOURCE > handleAction(
			Command< GetDesignResourcesTagsResponse< RESOURCE > > action )
			throws WebException {
		
		GetDesignResourcesTagsResponse< RESOURCE > response = new GetDesignResourcesTagsResponse< RESOURCE >();
		
		response.setResourcesTags( getDesignResourcesTags() ) ;
		
		return response;
	
	}

	
	protected abstract String[] getDesignResourcesTags();


	
	
}
