package com.dotemplate.core.server.command;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.AppException;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.dotemplate.core.shared.command.NewGraphicResponse;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.WebException;
import com.dotemplate.core.shared.properties.TextProperty;



public class NewTextGraphicHandler extends CommandHandler< NewGraphicResponse< TextGraphic > > {

	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog ( NewTextGraphicHandler.class );

	
	@Override
	public NewGraphicResponse< TextGraphic > handleAction ( Command< NewGraphicResponse< TextGraphic > > a )
							throws WebException {
		
		NewGraphicResponse< TextGraphic > response = new NewGraphicResponse< TextGraphic >() ;

		TextProperty property;
		
		String textSymbol = "text-basic" ;
		
		property = ( TextProperty ) DesignUtils.createFromSymbol ( textSymbol );
		property.setParent( textSymbol ) ;
		
		//property.setEditable ( true ) ;
		//property.setEnable ( true ) ;
		//property.setFree ( true ) ;
		
		property.setOpacity ( ( float ) 1.0 ) ;
		property.setName ( "text" + RandomStringUtils.randomNumeric ( 5 ) ) ;
		property.setLeft ( 5 ) ;
		property.setTop ( 5 ) ;
		property.setValue ( "Text Sample" ) ;
		property.setLabel( "New Text" ) ;
		//property.setFontSize ( 50 ) ;
		
		try {
		
			App.getDesignManager ().renderGraphic( property ) ;
		
		} catch ( AppException e ) {
			
			throw syserror ( "Unable to create image..." ) ;
		
		}
		
		response.setGraphic ( property ) ; 
		
		return response ;
		
	}
	


}
