package com.dotemplate.core.server;

import org.apache.commons.betwixt.io.read.BeanCreationChain;
import org.apache.commons.betwixt.io.read.ElementMapping;
import org.apache.commons.betwixt.io.read.ReadContext;

import com.dotemplate.core.shared.properties.Property;

public interface PropertyCreator<T extends Property > {
	
	T create ( ElementMapping mapping, ReadContext context,
			BeanCreationChain chain )  ;
	
}
