package com.dotemplate.core.server.web;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;

public class UploadImage extends BaseHttpServlet {

	private static final long serialVersionUID = 1005007861377242929L;

	private static Log log = LogFactory.getLog( UploadImage.class );

	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		
		
		if ( ! checkDesignSession ( request , response, false ) ){
			
			response.getOutputStream ().write( "SESSION_TIMEOUT".getBytes () ) ;
			
			return ;
		}
		
		
	    response.setContentType( "text/plain" );
		response.setStatus ( HttpServletResponse.SC_ACCEPTED ) ;
	    
	    
		DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();
		
		// does not reject files larger than threshold but store in repository file
		fileItemFactory.setSizeThreshold( 400 * 1024 ); //400 KB
		
		File destinationDir = new File (  App.get().getWorkRealPath() + "/upload" ) ;

		// A trick is used to reject files larger that 400 : above threshold commons FileUpload attempt 
		// to store in repository appTmpDir. This file does not exists in purpose and must not be created, 
		// so uploads larger than 400kb will be rejected because repository dir does not exist
		
		File appTmpDir = new File (  App.realPath( "/dummy" ) ) ; // must no exist

		
		fileItemFactory.setRepository( appTmpDir );
		
		
		if ( !destinationDir.exists () ){

			try {
				
				FileUtils.forceMkdir ( destinationDir ) ;
			
			} catch ( IOException e ) {
				
				throw new AppRuntimeException( "Cannot create design upload directory.", e ) ;
			}
		}
		
 
		ServletFileUpload uploadHandler = new ServletFileUpload( fileItemFactory ) ;
		
		
		FileItem uploadItem = null ;
		
		try {

			@SuppressWarnings("unchecked")
			List<FileItem> items = uploadHandler.parseRequest( request );
			
			String uploadId = null ;
			
			
			for ( FileItem item : items ){
				
				if( item.isFormField()) {
					
					if ( "imageName".equals( item.getFieldName() )  ){
						
						uploadId = item.getString() ;
					
					}
					
					log.debug( item.getFieldName()+ " = "+ item.getString() );
				
				} else {
					
					
					
					//Handle Uploaded files.
					log.debug( "Field Name = " + item.getFieldName()+
						", File Name = "+item.getName()+
						", Content type = "+item.getContentType()+
						", File Size = "+item.getSize());
					
					if ( "upload".equals( item.getFieldName() )  ){
						
						uploadItem = item ;
					
					}
				}
			} 
			
			if ( uploadId == null ) {
				log.error( "Upload image failed. Field 'imageName' expected " ) ;
				uploadFailed( request, response ) ;
				return ;
			}

			if ( ! acceptedImage( uploadItem.getContentType().toLowerCase() ) ){
				log.error( "upload content type " + uploadItem.getContentType() + " rejected" ) ;
				uploadFailed( request, response ) ;
				return ;
			}
			
			
			// Write file to the ultimate location.
			File file = new File( destinationDir, uploadId );
			
			uploadItem.write( file ) ;
			
			
		} catch ( FileNotFoundException e ){
			
			log.info( "Upload image failed. File too large" ) ;
			uploadFailed( request, response ) ; 
			
		} catch ( Exception e ) {
			
			log.error( "Upload image failed.", e ) ;
			uploadFailed ( request, response ) ;
		
		} finally {
			try {
				
				uploadItem.getOutputStream().close() ;
				
				response.getOutputStream ().flush ();
				response.getOutputStream ().close () ;
				
			} catch ( Exception e2 ){
				log.error ( "Failed to close Http error response after upload", e2 ) ; 
			}
		}	
 
	}
	
	protected void uploadFailed ( HttpServletRequest request, HttpServletResponse response ) throws IOException {
		
		response.getOutputStream ().write( "UPLOAD_KO".getBytes () ) ;
		
	}
	
		
	protected boolean acceptedImage( String contentType ){
		return 
				"image/png".equals( contentType )	
			||	"image/jpeg".equals( contentType )
			||	"image/jpg".equals( contentType )
			||	"image/pjpeg".equals( contentType )			
			||	"image/gif".equals( contentType ) ;
	}
	

}
