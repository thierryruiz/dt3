package com.dotemplate.core.server.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;




import com.dotemplate.core.server.DesignSession;


public class DesignSessionHelper {
	
	
	
	public static DesignSession ensureDesignSession( HttpServletRequest request ) {
		
		HttpSession session = request.getSession() ;
		
		DesignSession designSession = ( DesignSession ) session.getAttribute( DesignSession.class.getName() ) ;
		
		if ( designSession == null ){
			designSession = new DesignSession() ;
			session.setAttribute( DesignSession.class.getName(), designSession  );
		}
		
		DesignSession.set(designSession);
		
		return designSession ;
		
	}
	
	
	
	public static boolean checkDesignSession( HttpServletRequest request ) {
		
		HttpSession httpSession = request.getSession ( false ) ;
		
		if ( httpSession == null || httpSession.isNew() ){
			return  false ;
		}
		
		DesignSession designSession = ( DesignSession ) httpSession.getAttribute ( DesignSession.class.getName () ) ;
		
		if ( designSession == null ){
			return false ;
		} 
		
		DesignSession.set ( designSession ) ;
		
		return true ; 

	}
	
	


	
	
	
	
	
	@Deprecated
	public static void initDesignSession( HttpServletRequest request ) {
		
		HttpSession httpSession = request.getSession ( false ) ;
		
		if ( httpSession == null ){
			return ;
		}
		
		
		DesignSession designSession = ( DesignSession ) httpSession.getAttribute ( DesignSession.class.getName () ) ;
			
		if ( designSession != null ){
			DesignSession.set ( designSession ) ;
		}

	}
	
	
	
}
