/**
 * Copyright Thierry Ruiz ( Athesanet ) 2004 - 2008
 * All rights reserved.
 *
 * Created on 1 avr. 08 : 15:56:38
 */
package com.dotemplate.core.server;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.frwk.AbstractRemoteService;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.server.frwk.CommandHandler;
import com.dotemplate.core.server.web.DesignSessionHelper;
import com.dotemplate.core.shared.EditService;
import com.dotemplate.core.shared.command.DesignCommand;
import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.core.shared.frwk.WebException;



/**
 * 
 * @author Thierry Ruiz
 * 
 */
public class EditServiceImpl extends AbstractRemoteService implements EditService {

	private static final long serialVersionUID = -717350504565483119L;

	private final static Log log = LogFactory.getLog( EditServiceImpl.class );
	
	
	public EditServiceImpl(){
		if ( log.isDebugEnabled () ){
			log.debug ( "Creating " + getClass ().getName () ) ;
		}
	}
	
	
	
	@Override
	public < RESPONSE extends Response> RESPONSE execute ( Command< RESPONSE > command ) throws WebException {

		if ( log.isInfoEnabled () ) {
			
			log.info ( "Executing action '" + command.getClass ().getSimpleName () + "'" );
		
		}
		
		if ( ! DesignSessionHelper.checkDesignSession (  this.getThreadLocalRequest() ) ){
			
			log.info ( "Session expired or invalid session !"  );
			
			throw new WebException( WebException.SESSION_EXPIRED ) ;
			
		}
				
		
		CommandHandler< RESPONSE > handler  = super.processCommandHandler( command ) ;
		
		
		DesignCommand< RESPONSE > designCommand = null ; 
		
		
		try {
			
			designCommand = ( DesignCommand< RESPONSE >  ) command ;
			
		} catch ( ClassCastException e ){
			
			throw syserror ( new IllegalArgumentException( 
						"A DesignCommand object expected " +
									"as argument of execute method" ) ) ;
		} 
		
		
		String designUid = designCommand.getDesignUid() ;
		
		
		if ( designUid != null ){
			
			DesignSession.get ().setDesignContext ( designUid ) ;
		
		} else {
			
			if ( designCommand.isDesignUidRequired() ){
				
				throw new AppRuntimeException ( "Design uid not set in command object. Check the way the command is created client side." 
						+ command.getClass().getName() ) ;
			
			}
			
		}

		try {
			
			return handler.handleAction ( command ) ;
		
		} catch ( Exception e ){
			
			throw syserror ( e ) ;
		
		}
	}
		
}
