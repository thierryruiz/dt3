package com.dotemplate.core.server;

import java.awt.image.BufferedImage;


public class CanvasImage extends CachedImage {

	private static final long serialVersionUID = 7408902902057473962L;

	public final static int CANVAS = 0 ;
	
	public CanvasImage( String id, BufferedImage image ) {
		super( id + ".png" , image, Format.PNG ) ;
	}
	
	@Override
	public int getType() {
		return CANVAS ;
	}
	
	

}
