package com.dotemplate.core.server;



import org.apache.commons.betwixt.io.read.BeanCreationChain;
import org.apache.commons.betwixt.io.read.ChainedBeanCreator;
import org.apache.commons.betwixt.io.read.ElementMapping;
import org.apache.commons.betwixt.io.read.ReadContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.server.util.Singletons;
import com.dotemplate.core.server.util.UIDGenerator;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;


public class DesignCreator implements ChainedBeanCreator {

	
	private final static Log log = LogFactory.getLog ( DesignCreator.class ) ;
	
	
	
	public Object create( ElementMapping mapping, ReadContext context,
			BeanCreationChain chain ) {
		
		if ( log.isDebugEnabled () ){ 
			trace(  mapping ) ; 
		}


		// encounter property mapping
    	if ( Property.class.equals( mapping.getType() ) ) {

    		Property property ;
    		
    		if ( "size".equals( mapping.getName() ) ){
    			
    			property = ( ( SizePropertyCreator ) Singletons.getSingleton( 
    					SizePropertyCreator.class ) ).
   					
    					create( mapping, context, chain ) ;
    		
    		} else {
    			
    			property =  ( Property ) chain.create( mapping, context );
    		
    		} 
    		
    		property.setUid ( UIDGenerator.pickId () ) ;
    		
    		
    		return property ;
    		
    	}
    	
    	
    	
    	if ( PropertySet.class.equals( mapping.getType() ) ) {
    		
    		PropertySet set = ( PropertySet ) chain.create( mapping, context );
    		
    		set.setUid ( UIDGenerator.pickId () ) ;
    		    		
    		return set  ;
    	}
    		
    	
    	return chain.create( mapping, context ) ;
    	    	
	}

	
	

	private void trace( ElementMapping mapping ) {
		String name = mapping.getAttributes().getValue( "name" ) ;
		
		if ( name != null ){
			log.debug (  "Create " +  mapping.getType() + " " + name ) ;
		} else {
			log.debug (  "Create " +  mapping.getType() ) ;
		}
		
	}
	
	

}
