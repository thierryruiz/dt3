package com.dotemplate.core.server.svg;

import java.io.IOException;
import java.io.OutputStream;

public class VoidOutputStream extends OutputStream {

	@Override
	public void write(int arg0) throws IOException {
	}
	
	public void flush() throws IOException {
	};
	
	@Override
	public void close() throws IOException {
	}
	
	@Override
	public void write(byte[] arg0) throws IOException {
	}
	
	@Override
	public void write(byte[] arg0, int arg1, int arg2) throws IOException {
	}
	
	

}
