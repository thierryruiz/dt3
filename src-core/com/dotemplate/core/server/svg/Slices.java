/*
/*
 * 
 * Created on 16 juil. 2007
 * 
 */
package com.dotemplate.core.server.svg;

import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * 
 * A list of slices built by traversing Slice properties
 * 
 * @author Thierry Ruiz
 *  
 */
public class Slices extends LinkedHashMap<String, Slice>  {
	
	private final static Log log = LogFactory.getLog ( Slices.class );
	
	private static final long serialVersionUID = -2930529165112053987L;
	
	
	public void add ( String name, int left, int top, int width, int height, double quality  ) {
		 add( name, left, top, width, height, quality, false  ) ;
	}
	
	
	public void addPng ( String name, int left, int top, int width, int height, double quality  ) {
		 add( name, left, top, width, height, quality, true  ) ;
	}

	
	
	private void add ( String name, int left, int top, int width, int height, double quality, boolean png ){
		// do not add a slice twice
		
		if ( this.containsKey ( name ) ) ;
		
		if ( log.isDebugEnabled () ){
			log.debug ( "add slice " + name + " " + left +  " " + top + " " + width + " " + height ) ;
		}
				
		Slice slice = new Slice( ) ;
		
		slice.setName ( name );
		slice.setQuality ( ( float) quality );
		slice.setBounds ( left, top, width, height ) ; 
		
		slice.setPng ( png ) ;
		
		put ( slice.getName(), slice ) ;


	}
	

}


