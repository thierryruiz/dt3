package com.dotemplate.core.server.svg;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.DocumentLoader;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgent;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import com.dotemplate.core.server.App;
import com.dotemplate.core.server.frwk.AppRuntimeException;


public class SVGFonts {
	
	private static Log log = LogFactory.getLog( SVGFonts.class );
	
	static Map< ?, ? > fontFamilies ;
	
	
	public static void load () {
		
		String parser = XMLResourceDescriptor.getXMLParserClassName ();
		SAXSVGDocumentFactory factory = new SAXSVGDocumentFactory ( parser );
		UserAgent userAgent = new UserAgentAdapter ();
		BridgeContext bridgeContext = new BridgeContext ( userAgent, new DocumentLoader ( userAgent ) );
		bridgeContext.setDynamicState ( BridgeContext.DYNAMIC );
		GVTBuilder builder = new GVTBuilder ();
		
		if ( log.isDebugEnabled () ){
			log.debug ( "Loading font faces..." ) ;
		}
		
		Document doc = null;
		
		try {
		
			doc = factory.createDocument ( null, new FileReader( 
					App.realPath ( "WEB-INF/templates/_fonts/_fontfaces.svg" ) ) );
		
		} catch ( IOException e ) {
			throw new AppRuntimeException ( "Unable to load font faces", e );
		}
		
		builder.build ( bridgeContext, doc );
		
		if ( log.isDebugEnabled () ){
			log.debug ( "" + bridgeContext.getFontFamilyMap ().size () + " fonts loaded" ) ;
		}
		
		fontFamilies =  bridgeContext.getFontFamilyMap () ; 
	
		
	}
	
	
	public static Map<?, ?> getFontFamilies() {
		
		if ( fontFamilies == null ){
			load() ;
		}
		
		return fontFamilies;
	}
	
	

}
