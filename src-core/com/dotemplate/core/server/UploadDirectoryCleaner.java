package com.dotemplate.core.server;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dotemplate.core.shared.DesignTraverser;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.ImageProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;


/**
 * 
 * 
 * Cleans upload folder from unused images
 * 
 *
 */
public class UploadDirectoryCleaner implements DesignTraverser {

	private static final long serialVersionUID = 379435809793945522L;

	private final static Log log = LogFactory.getLog ( UploadDirectoryCleaner.class );
	
	private ArrayList< String > uploadedImagesUsed = new ArrayList< String >() ; 
	
	private DesignContext< ? > designContext ;
	
	
	public UploadDirectoryCleaner( DesignContext< ? > designContext ) {
		this.designContext = designContext ;
	}
	
	
	public void clean( ) {

		File uploadDir = new File ( App.get().getUploadDirectoryRealPath( designContext.getDesign().getUid() ) ) ;
		
		if ( !uploadDir.exists() ){
			return ;
		}
		
		if ( uploadDir.list().length ==  0 ){
			return ;
		}
		
		designContext.getDesign().execute( this ) ;
		
		
		@SuppressWarnings("unchecked")
		Collection< File > uploadedImages = FileUtils.listFiles( uploadDir, TrueFileFilter.TRUE , TrueFileFilter.TRUE ) ;

		
		System.out.println( uploadedImagesUsed.size() + " used uploaded images" ) ;
		
		if ( uploadedImagesUsed.isEmpty() ) {
			
			try {
				
				FileUtils.cleanDirectory( uploadDir ) ;
				
			} catch (IOException e) {
				
				log.error ( "Failed to purge upload image " + uploadDir.getAbsolutePath(), e ) ;
			
			}
			
			return ;
		}
		
		
		
		for ( File uploadedImage : uploadedImages ){
			
			if ( uploadedImagesUsed.contains( uploadedImage.getName() ) ) continue ;
						
			try {
				
				FileUtils.forceDelete( uploadedImage ) ;
				
			} catch (IOException e) {	
				log.error ( "Failed to purge upload image " + uploadedImage.getAbsolutePath(), e ) ;
			}
			
		}
		
	}


	@Override
	public boolean startSet(PropertySet set) {
		return true;
	}


	@Override
	public boolean endSet(PropertySet set) {
		return true;
	}


	@Override
	public boolean startProperty( Property property ) {
		
		if ( Graphic.IMAGE != property.getType() ){
			return true ;
		}
		
		ImageProperty image = ( ImageProperty ) property ;   
		
		if ( !image.isUploaded() ) {
			return true  ;
		}

		uploadedImagesUsed.add( StringUtils.substringAfterLast( image.getValue(), "/" ) ) ;
		
		return true;
	}


	@Override
	public boolean endProperty(Property property) {
		return true;
	}
	
	
}
