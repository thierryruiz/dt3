package com.dotemplate.core.server.symbol.logic;

public class PropertyExpression extends UnitaryExpression {
	
	private String property ;
	
	public PropertyExpression ( String property ) {
		this.property = property ;
	}
	
	static PropertyExpression parseProperty( String expr  ) {
		return new PropertyExpression( expr ) ;		
	}
	
	
	public String getProperty () {
		return property;
	}
	

}
