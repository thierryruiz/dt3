package com.dotemplate.core.server.symbol.logic;

import org.apache.commons.lang.StringUtils;

import com.dotemplate.core.server.DesignUtils;
import com.dotemplate.core.server.frwk.AppRuntimeException;
import com.dotemplate.core.shared.properties.PropertySet;


public class EqualsExpression extends EqualityExpression {
	

	public EqualsExpression ( PropertyExpression left, ValueExpression right ) {
		super ( left, right ) ;
	}
	
	
	static EqualsExpression parseEquals ( String exp ){
		
		String[] tokens = StringUtils.split ( exp, '=' ) ;
		
		if ( tokens.length != 2 ) {
			return null ;
		}
		
		return new EqualsExpression (
				PropertyExpression.parseProperty ( tokens[ 0 ].trim ()  ), 
				ValueExpression.parseValue ( tokens[ 1 ].trim () ) ) ;
	}
	
	
	
	@Override
	public boolean eval ( PropertySet set )  {	
		try {
			
			return DesignUtils.isPropertyEquals ( set, left.getProperty(), right.getValue () ) ;
		} catch ( Exception e ){
			throw new AppRuntimeException( "Failed to evaluate '" + left.getProperty() + "' " ) ;
		}
	}
	

	

}
