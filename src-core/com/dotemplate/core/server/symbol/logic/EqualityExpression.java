package com.dotemplate.core.server.symbol.logic;



public abstract class EqualityExpression extends BooleanExpression {
	
	PropertyExpression left ;
	
	ValueExpression right ;

	
	public EqualityExpression ( PropertyExpression left, ValueExpression right ) {
		this.left = left ;
		this.right = right ;
	}
	
	
	
	protected static EqualityExpression parseEquality( String expr ) {
		
		EqualsExpression eqExp = EqualsExpression.parseEquals ( expr ) ;
		
		if( eqExp != null ) return eqExp ;
		
		return null ;
	}
	
	
	
	
}
