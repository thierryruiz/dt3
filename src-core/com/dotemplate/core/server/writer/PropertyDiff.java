package com.dotemplate.core.server.writer;

import java.util.ArrayList;
import java.util.Collection;

import java.util.List;

public class PropertyDiff {
	
	private String name ;
	
	private int type ;
	
	private List< Attribute > attributes ;

	
	public PropertyDiff( String name, int type ) {
		this.type = type ;
		this.name = name ;
		attributes = new ArrayList< Attribute > () ;	
	}
	
	public int getType() {
		return type;
	}
	
	
	public String getName() {
		return name;
	}
	

	public void add( Attribute attribute ) {
		attributes.add( attribute ) ;
	}
	
	public Collection< Attribute > getAttributes() {
		return attributes ;
	}
	
	public boolean isEmpty(){
		return attributes.isEmpty() ;
	}
	
	public boolean isSet() {
		return false ;
	}
	
}
