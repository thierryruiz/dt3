package com.dotemplate.core.client;



import com.dotemplate.core.client.canvas.CanvasEditorWindow;
import com.dotemplate.core.client.events.HasSchemeChangeEventHandlers;
import com.dotemplate.core.client.events.SchemeChangeEvent;
import com.dotemplate.core.client.events.SchemeChangeEventHandler;
import com.dotemplate.core.client.frwk.Utils;
import com.dotemplate.core.client.widgets.colorpicker.ColorPickerDialog;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.EditService;
import com.dotemplate.core.shared.EditServiceAsync;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.dotemplate.core.shared.properties.Property;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;


public abstract class Client<D extends Design> implements EntryPoint, HasSchemeChangeEventHandlers {
	
	protected HandlerManager handlerManager ;
	 
	protected ColorPickerDialog colorPickerDialog ;

	protected CanvasEditorWindow canvasEditorWindow ; 
	
	private D design ;

	private Scheme colorScheme ;
	
	protected EditServiceAsync designService ;

	private static Client<? extends Design> instance ; 

	
	public static void systemError ( Object source, String msg ) {
		Utils.alert ( msg );
	}

	public static void systemError ( Throwable t ) {
		Utils.alert ( t.getMessage () );
	}

	
	public Client() {
		instance = this ;
				
		handlerManager = new HandlerManager( this ) ;

		designService = ( EditServiceAsync ) GWT.create ( EditService.class ) ;
				
		// create color editor at start
		colorPickerDialog = new ColorPickerDialog() ;
	
	}
	
	
	public D getDesign() {
		if ( design == null ){
			throw new IllegalStateException( "Undefined design" ) ;
		}
		return design;
	}

	
	public void setDesign( D design ) {
		this.design = design;
	}
	
	
	// --------------------------- STATIC METHODS ---------------------------------------


	public EditServiceAsync getDesignService() {
		return designService;
	}
	
	
	public static Client<? extends Design> get() {
		return instance ;
	}
	
	/*
	
	protected EventBus getEventBus() {
		return eventBus ;
	}
	*/

	public ColorPickerDialog getColorPickerDialog() {
		return colorPickerDialog ;
	}
	
	
	public void editCanvas ( CanvasProperty set ) {
		ensureCanvasEditor() ;
		canvasEditorWindow.edit ( set ) ;
	}

	
	
	protected void ensureCanvasEditor() {
		if ( canvasEditorWindow == null ){
			canvasEditorWindow = new CanvasEditorWindow() ;
		}
	}

	public Scheme getColorScheme() {
		return colorScheme;
	}

	public void setColorScheme(Scheme colorScheme) {
		this.colorScheme = colorScheme;
	}

	public abstract void updateProperty( Property property ) ;

	
	
	public void addUserColor( String color ) {
		// ckeck if color is not in current color scheme ;
		for ( String col : colorScheme.getColors() ){
			if ( col.equals( color ) ) {
				return ;
			}
		}
		
		
		String userScheme = design.getUserScheme() ;
		
		if ( userScheme == null ){
			design.setUserScheme( color ) ;
			return ;
		}
		
		if( userScheme.contains( color ) ){
			return ;
		}
		
		design.setUserScheme( userScheme + "," + color ) ;
		
	}


	
	@Override
	public void fireEvent(GwtEvent<?> event) {
		handlerManager.fireEvent( event ) ;
		
	}


	@Override
	public HandlerRegistration addHandler( SchemeChangeEventHandler h ) {
		return handlerManager.addHandler( SchemeChangeEvent.TYPE , h ) ;
	}

	public abstract void invitePurchase() ;
	


	
	
}
