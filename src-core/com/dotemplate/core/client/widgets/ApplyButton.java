package com.dotemplate.core.client.widgets;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PushButton;


public class ApplyButton extends PushButton {

	public ApplyButton () {
		super( new Image ( "images/gwt/common/check.png" ) ) ;
		DOM.setStyleAttribute ( this.getElement (), "cursor", "pointer" ) ;
	}
	
}
