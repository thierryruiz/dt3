package com.dotemplate.core.client.widgets.upload;


import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.HandlerRegistration;

public interface HasUploadEventHandler extends EventHandler {
	
	public HandlerRegistration addUploadEventHandler(
			UploadEventHandler handler );

}
