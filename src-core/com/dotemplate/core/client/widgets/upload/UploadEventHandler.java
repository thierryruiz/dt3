package com.dotemplate.core.client.widgets.upload;

import com.google.gwt.event.shared.EventHandler;



public interface UploadEventHandler extends EventHandler {

	void onUpload( UploadEvent uploadEvent );
	
	
}
