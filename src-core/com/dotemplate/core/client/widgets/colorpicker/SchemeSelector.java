package com.dotemplate.core.client.widgets.colorpicker;

import com.dotemplate.core.client.widgets.resource.DesignResourceDialog;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourceScrollSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceSelector;
import com.dotemplate.core.client.widgets.resource.SelectDesignResource;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.command.GetAllSchemes;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.google.gwt.event.shared.HandlerRegistration;


public class SchemeSelector extends DesignResourceDialog< Scheme > {

	private static SchemeSelector instance ;
	
	public SchemeSelector() {
		
		super( new DesignResourceProvider< Scheme >() {

			@Override
			protected GetDesignResourcesMap<Scheme> getDesignResourcesMapCommand() {
				return new GetAllSchemes() ;
			}
			
		
		}, 480, 420 ) ;
		
		setHeading( "Choose color scheme" ) ;
	
	}
	
	
	public static void show( SelectDesignResourceHandler< Scheme > handler, Scheme selectedScheme ){
	
		// singleton 
		if ( instance == null ){
			instance = new SchemeSelector() ;
		}
		
		instance.setSelected( selectedScheme ) ;
		instance.open( handler ) ;
		
	}
	
	
	@Override
	protected DesignResourceSelector < Scheme > createResourceSelector(
			DesignResourceProvider< Scheme > loader ) {
		
		return new DesignResourceScrollSelector< Scheme >( loader ) {
			@Override
			public HandlerRegistration addHandler(
					SelectDesignResourceHandler< Scheme > handler ) {
				
				return handlerManager.addHandler( SelectDesignResource.getType( Scheme.class ), handler );
			
			}
			
			
		};
	}
	

}
