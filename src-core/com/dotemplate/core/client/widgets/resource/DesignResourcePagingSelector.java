package com.dotemplate.core.client.widgets.resource;

import java.util.ArrayList;

import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;


public abstract class DesignResourcePagingSelector< RESOURCE extends DesignResource > 
	extends DesignResourceSelector< RESOURCE > {
	
	protected DockPanel layout ;
	
	private Image nextButton ;
	
	private Image previousButton ;
	
	protected SimplePanel top ;
	
	
	public DesignResourcePagingSelector( DesignResourcePageAsyncProvider< RESOURCE > provider ) {
		
		super( provider ) ;
		
		
		layout = new DockPanel() ;
		layout.setHeight( "500px" ) ;
		
		previousButton = new Image( "client/icons/DesignResourcePanel/previous.png" );
		nextButton = new Image( "client/icons/DesignResourcePanel/next.png"  ) ;

		
		DOM.setStyleAttribute( previousButton.getElement(), "cursor", "pointer") ;
		DOM.setStyleAttribute( nextButton.getElement(), "cursor", "pointer") ;
		
		layout.add( top = new SimplePanel(), DockPanel.NORTH ) ;
		layout.add ( previousButton, DockPanel.WEST  ) ;
		layout.add ( nextButton, DockPanel.EAST ) ;
		
		layout.add( list.asWidget(), DockPanel.CENTER ) ;	
		
		layout.setCellVerticalAlignment( previousButton, DockPanel.ALIGN_MIDDLE ) ;
		layout.setCellVerticalAlignment( nextButton, DockPanel.ALIGN_MIDDLE ) ;
		
		layout.setCellHorizontalAlignment( previousButton, DockPanel.ALIGN_CENTER ) ;
		layout.setCellHorizontalAlignment( nextButton, DockPanel.ALIGN_CENTER ) ;
		
		
		layout.setSpacing( 0 ) ;
		layout.setCellWidth( list.asWidget(), "80%" ) ;
		layout.setCellWidth( previousButton, "10%" ) ;
		layout.setCellWidth( nextButton, "10%" ) ;
		
		layout.setCellHeight( top, "50px" ) ;
		
		
		bind() ;
		
	}	
	
	
	@Override
	public void onDesignResourcesLoad( ArrayList< RESOURCE > resources, boolean hasMore) {
		
		setResources( resources, hasMore ) ;
		
		if ( selected != null ){
			select() ;
		}
		
	}
	
	
	
	protected void bind() {
		
		nextButton.addClickHandler ( new ClickHandler() {
			@Override
			public void onClick ( ClickEvent ce ) {
				( ( DesignResourcePageAsyncProvider< RESOURCE > ) provider ).next() ;			
			}
		}) ;
		
		previousButton.addClickHandler ( new ClickHandler() {
			@Override
			public void onClick ( ClickEvent ce ) {
				( ( DesignResourcePageAsyncProvider< RESOURCE > ) provider ).previous() ;
			}
		}) ;
		
	}
	
		
	
	protected void setResources( ArrayList< RESOURCE > resources, boolean hasMore ) {
		
		list.clearList () ;
		
		DesignResourceThumbnail< RESOURCE > el ;
		
		for ( RESOURCE resource : resources  ) {
			
			el = list.add ( resource  );
			
			el.addClickHandler( this )  ;
			DOM.setElementAttribute( el.asWidget().getElement(), "id", resource.getName() ) ;
		}

		
		if ( ( ( DesignResourcePageAsyncProvider< RESOURCE > ) provider ).getIndex() > 0 ) {
			enablePrevious () ;
		} else {
			disablePrevious() ;
		}
		
		if( hasMore ){
			enableNext() ;
		} else {
			disableNext() ;
		}
	
	}
	
	

	@Override
	public Widget asWidget() {
		return layout ;
	}

	
	protected void enableNext () {
		nextButton.removeStyleName ( "disabled" ) ;
	}
	
	protected void enablePrevious ( ) {
		previousButton.removeStyleName ( "disabled" ) ;
	}
	
	
	protected void disableNext () {
		nextButton.addStyleName ( "disabled" ) ;
	}
	
	protected void disablePrevious ( ) {
		previousButton.addStyleName ( "disabled" ) ;
	}
	

	
}
