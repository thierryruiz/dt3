package com.dotemplate.core.client.widgets.resource;

import java.util.HashMap;

import com.dotemplate.core.client.frwk.Event;
import com.dotemplate.core.shared.DesignResource;


public class SelectDesignResource< R extends DesignResource > extends Event< SelectDesignResourceHandler< R > > {
	
	private R resource ;
	
	public SelectDesignResource( R resource ) {
		this.resource = resource ;
	}
	
	public R getResource() {
		return resource;
	}
	
	
	@Override
	protected void dispatch( SelectDesignResourceHandler< R > handler ) {
		handler.onDesignResourceSelected( resource ) ;
	}

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type< SelectDesignResourceHandler< R > > getAssociatedType() {
		 return ( Type ) SelectDesignResource.getType( this.resource.getClass());
	}


	private static HashMap< Class < ? >, Type< SelectDesignResourceHandler< ? > > > TYPES = new
			 HashMap< Class< ? >, Type< SelectDesignResourceHandler < ? > > >();

	 	 
	public static Type< SelectDesignResourceHandler< ? > > getType( Class< ? > clazz ) { 

	     if ( !TYPES.containsKey( clazz )) {
	         TYPES.put( clazz, new Type< SelectDesignResourceHandler< ? > >() );
	     }

	     return TYPES.get( clazz );
	     
	}
	
}
