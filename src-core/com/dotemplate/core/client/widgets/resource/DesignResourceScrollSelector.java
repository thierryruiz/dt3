package com.dotemplate.core.client.widgets.resource;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;


public abstract class DesignResourceScrollSelector< RESOURCE extends DesignResource > 
	extends DesignResourceSelector< RESOURCE > {
	
	protected ScrollPanel wrapper ;
	
	public DesignResourceScrollSelector( DesignResourceProvider< RESOURCE > provider ) {
		
		super( provider ) ;
		
		wrapper = new ScrollPanel() ;	
		wrapper.add( list.asWidget() ) ;
		
	}
	
	
	@Override
	public void onDesignResourcesLoad( LinkedHashMap<String, LinkedHashMap<String, RESOURCE> > tags ) {
		
		setResources( tags ) ;
		
		if ( selected != null ){
			select () ;
		}
		
	}
	

	@Override
	public void onDesignResourcesLoad(ArrayList<RESOURCE> items) {
	}

	
	@Override
	public void onDesignResourcesLoad(ArrayList<RESOURCE> items,
			boolean hasMore) {
		
	}

	@Override
	public void onDesignResourcesLoad(String[] tags) {
		
	}
	
	public void setResources( LinkedHashMap< String, LinkedHashMap< String, RESOURCE > > tags ) {
		
		list.clearList () ;
		
		DesignResourceThumbnail< RESOURCE > el ;
		
		if ( tags.size() == 1 ){
			
			for ( RESOURCE resource : tags.get( "all" ).values() ) {
				
				if ( !isResourceAvailable( resource ) ) continue ;
				
				el = list.add ( resource );
				
				el.addClickHandler( this )  ;
				
				DOM.setElementAttribute( el.asWidget().getElement(), "id", resource.getName() ) ;
			}
			
		} else {
			
			Set< Entry < String, LinkedHashMap< String, RESOURCE > > > entries = tags.entrySet() ; 
			
			
			for ( Entry < String, LinkedHashMap< String, RESOURCE > > entry : entries ){
			
				// do not display 'all' tag resources
				if ( "all".equals( entry.getKey() )  ) continue ;
				
				list.addTagSeparator( entry.getKey() ) ;
				
				for ( RESOURCE resource : entry.getValue().values() ) {
					
					if ( !isResourceAvailable( resource ) ) continue ;
					
					el = list.add ( resource  ) ;
					
					el.addClickHandler( this )  ;
					
					DOM.setElementAttribute( el.asWidget().getElement(), "id", resource.getName() ) ;

				}
				
			}
			
		}
	
	}
	
	
	@Override
	public Widget asWidget() {
		return wrapper ;
	}
	
	
	
}
