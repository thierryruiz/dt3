package com.dotemplate.core.client.widgets.resource;


import com.dotemplate.core.client.Client;
import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;

import com.google.gwt.user.client.ui.IsWidget;


public abstract class DesignResourceSelector< RESOURCE extends DesignResource > 
	implements IsWidget, HasSelectDesignResourceHandlers< RESOURCE >, ClickHandler,
	DesignResourcesLoadHandler< RESOURCE > {
	
	protected HandlerManager handlerManager ;
	
	protected DesignResourceList< RESOURCE > list  ;
	
	protected RESOURCE selected ;
	
	protected DesignResourceProvider< RESOURCE > provider ;
	
	
	public DesignResourceSelector( DesignResourceProvider< RESOURCE > provider ) {
		this.provider = provider ;
		this.handlerManager = new HandlerManager( this ) ;
		list = createResourceList() ;
		provider.setLoadHandler( this  ) ;
	}
	
	
	protected DesignResourceList< RESOURCE > createResourceList() {
		return new DesignResourceList< RESOURCE >() ;
	}
	
	
	public void setSelected( RESOURCE selected ) {
		this.selected = selected;
		select() ;
	}
	
	
	public RESOURCE getSelected() {
		return selected;
	}
	
	
	protected void select() {
		list.select( selected ) ;
	}


	@Override
	public void onClick( ClickEvent e ) {
		
		@SuppressWarnings("unchecked")
		DesignResourceThumbnail< RESOURCE > el = ( DesignResourceThumbnail< RESOURCE > ) e.getSource() ;
		
		if ( el.isLocked() ){
			
			Client.get().invitePurchase() ;
			
			
		} else {
			
			fireEvent ( new SelectDesignResource< RESOURCE >( el.getResource() ) ) ;
		
		}
		
	}
	
	
	@Override
	public void fireEvent(GwtEvent<?> event ) {
		handlerManager.fireEvent( event ) ;
	}

	
	protected boolean isResourceAvailable( RESOURCE resource ){
		return true ;
	}
	
	

}
