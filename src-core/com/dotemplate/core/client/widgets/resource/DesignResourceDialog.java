package com.dotemplate.core.client.widgets.resource;


import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.shared.HandlerRegistration;


public abstract class DesignResourceDialog< RESOURCE extends DesignResource > extends Window 
	implements SelectDesignResourceHandler< RESOURCE > {
	
	protected enum Style { PAGING, SCROLLING } ;
	
	protected DesignResourceSelector< RESOURCE > resourcesPanel ;
	
	protected DesignResourceProvider< RESOURCE > loader ;
	
	protected boolean loaded = false ;
	
	protected SelectDesignResourceHandler < RESOURCE > selectHandler ;
	
	protected HandlerRegistration selectHandlerRegistration ;
	
	private DesignResourceDialog( int width, int height ){
		
		Log.debug ( "DesignResourceDialog" ) ;
		setSize ( width, height ) ;	
		setModal ( true ) ;
		setResizable ( true ) ;
	
	}
	
	
	public void setSelected( RESOURCE selected ) {
		resourcesPanel.setSelected( selected ) ;
	}
	
	
	protected abstract DesignResourceSelector < RESOURCE > createResourceSelector(
			DesignResourceProvider< RESOURCE > loader ) ;

	
	
	public DesignResourceDialog ( DesignResourceProvider< RESOURCE > loader, int width, int height ) {
		
		this( width, height ) ;
		
		this.loader = loader ;
		
		resourcesPanel = createResourceSelector( loader ) ;
		
		resourcesPanel.addHandler( this ) ;
		
		add( resourcesPanel.asWidget() ) ;
		
	}
	


	public void open( SelectDesignResourceHandler< RESOURCE > selectHandler ) {
		
		this.selectHandler = selectHandler ;
		
		if ( selectHandlerRegistration != null ){
			
			selectHandlerRegistration.removeHandler() ;
		
		}
		
		selectHandlerRegistration = resourcesPanel.addHandler( selectHandler ) ;
		
		super.openCenter();
		
		if ( ! loaded ){
			
			loader.loadResourcesMap() ;
			loaded = true ;
		}
		
		
	}

	
	
	@Override
	public void onDesignResourceSelected( RESOURCE resource ) {
		resourcesPanel.setSelected( resource ) ;
		close() ;
	}

	

}
