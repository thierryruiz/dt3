package com.dotemplate.core.client.widgets.resource;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.shared.Design;
import com.dotemplate.core.shared.DesignResource;
import com.extjs.gxt.ui.client.core.XDOM;
import com.extjs.gxt.ui.client.util.Point;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;


public class DesignResourceThumbnail< DR extends DesignResource > implements
	IsWidget, HasClickHandlers  {

	protected DR resource ;
	
	protected boolean locked ; 
	
	protected static Tip extTip = new Tip() ;
	
	protected FlowPanel widget ;
	
	protected HTML thumbnail ;
	
	protected Image lock ;
	
	protected Image check ;
	
	protected HandlerManager handlerManager ;
	
	
	public DesignResourceThumbnail( DR resource ) {
		
		handlerManager = new HandlerManager( this ) ;
		
		widget = new FlowPanel() ;
		widget.setStyleName( "ez-chooser-item" ) ;
		
		thumbnail = new HTML( resource.getThumbnailHtml()  ) ; 
		
		
		lock = new Image( "client/icons/DesignResourcePanel/lock.png" );
		lock.setStyleName( "lock" ) ;
		lock.setTitle( "Premium. Buy template to use it" ) ;

		check = new Image( "client/icons/DesignResourcePanel/check.png" );
		check.setStyleName( "check" ) ;
		
		widget.add( thumbnail ) ;
		
		this.resource = resource ;
		
		Design design = Client.get().getDesign() ;
		
		setLocked( resource.isPremium() && ! design.isPremium() && !design.isPurchased()  ) ;
		
		
		thumbnail.addMouseOverHandler( new MouseOverHandler () {
			public void onMouseOver( MouseOverEvent e ) {
				if ( locked ){
					extTip.show( e.getClientX() + 10 , e.getClientY() + 10, 
							"Premium resource", "Please, buy template if you want to use it") ;
				}
			}			
		}) ;
		
		
		thumbnail.addMouseOutHandler( new MouseOutHandler () {
			public void onMouseOut( MouseOutEvent e ) {
				if ( locked ){
					extTip.hide();
				}
			}
		}) ;
		
		thumbnail.addClickHandler( new ClickHandler() {
			@Override
			public void onClick(ClickEvent e) {
				DesignResourceThumbnail.this.fireEvent( new ClickEvent() {}) ;
			}
		}) ;
	}
	
	
	public boolean isLocked() {
		return locked;
	}
	
	
	public void setLocked( boolean b ){
		locked = b ;
		
		if ( b ) {
			widget.addStyleName( "disabled" ) ;
		} else {
			widget.removeStyleName( "disabled" ) ;
		}
		
		if ( b ){
			widget.add( lock ) ;
		} else {
			if ( lock.isAttached() ){
				lock.removeFromParent() ;
			}
		}
	}
	
	public void setSelected( boolean b ){
		if ( b ) {
			widget.addStyleName("active") ;
		} else {
			widget.removeStyleName("active" ) ;
		}
		
		if ( b ){
			widget.add( check ) ;
		} else {
			if ( check.isAttached() ){
				check.removeFromParent() ;
			}
		}
		
	}
	
	
	public DR getResource() {
		return resource;
	}

	
	
	@Override
	public Widget asWidget() {
		return widget;
	}
	
	
	public static class Tip extends ContentPanel {
		
		  public Tip() {
			    frame = true;
			    baseStyle = "x-tip";
			    shim = true;
			    setAutoHeight(true);
			    setShadow(true);
			    setHeaderVisible(true) ;

		  }
		  
		  
		  public void show(int x, int y, String heading, String text ) {
			    setStyleAttribute( "visibility", "hidden" );
			    
			    if (!isAttached()) {
			      RootPanel.get().add(this);
			    }
			    
			    super.show();
			    Point p = new Point(x, y);
			    setPagePosition( p.x + XDOM.getBodyScrollLeft(), p.y + XDOM.getBodyScrollTop() );
			    setStyleAttribute( "visibility", "visible" );
			    getHeader().setText( heading );
			    getBody().update( text  );
		  }
		
		  
		  @Override
		  public void hide() {
		    super.hide();
		    
		    if (isAttached()) {
		      RootPanel.get().remove(this);
		    }
		  
		  }
	}


	@Override
	public void fireEvent(GwtEvent<?> e) {
		handlerManager.fireEvent( e ) ;
	}


	@Override
	public HandlerRegistration addClickHandler( ClickHandler handler ){
		return handlerManager.addHandler( ClickEvent.getType(), handler );
	}
	
	
	
	
}
