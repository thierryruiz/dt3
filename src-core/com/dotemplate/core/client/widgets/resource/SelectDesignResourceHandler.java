package com.dotemplate.core.client.widgets.resource;


import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.shared.EventHandler;


public interface SelectDesignResourceHandler< R extends DesignResource > extends EventHandler {

	void onDesignResourceSelected( R resource ) ;

}
