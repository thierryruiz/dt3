package com.dotemplate.core.client.widgets.resource;


import com.dotemplate.core.shared.DesignResource;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasSelectDesignResourceHandlers< RESOURCE extends DesignResource > extends HasHandlers {
	
	HandlerRegistration addHandler ( SelectDesignResourceHandler< RESOURCE > h ) ; 
	
}
