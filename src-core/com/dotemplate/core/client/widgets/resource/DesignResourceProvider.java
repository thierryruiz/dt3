package com.dotemplate.core.client.widgets.resource;

import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.shared.DesignResource;
import com.dotemplate.core.shared.command.GetDesignResources;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.dotemplate.core.shared.command.GetDesignResourcesMapResponse;
import com.dotemplate.core.shared.command.GetDesignResourcesResponse;
import com.dotemplate.core.shared.command.GetDesignResourcesTags;
import com.dotemplate.core.shared.command.GetDesignResourcesTagsResponse;


public abstract class DesignResourceProvider< R extends DesignResource > {
	
	protected String tag ;
	
	protected DesignResourcesLoadHandler< R > loadHandler ;
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
	public void setLoadHandler( DesignResourcesLoadHandler<R> loadHandler ) {
		this.loadHandler = loadHandler;
	}
	
	
	
	
	
	public void loadResources () {
		
		Log.debug ( "Loading resources..."  ) ;
		
		GetDesignResources< R > action = getDesignResourcesCommand() ;
		
		action.setTag( tag ) ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
			
		( new RPC< GetDesignResourcesResponse< R > >( Client.get().getDesignService() ) ).execute ( 
				action,
				new CommandCallback< GetDesignResourcesResponse< R > >(){
			
					
			public void onSuccess ( GetDesignResourcesResponse< R > response ) {
				if ( loadHandler != null ){
					loadHandler.onDesignResourcesLoad( response.getResources() ) ;
				}
				
			}
		
		}) ;
		
	}
		
	
	public void loadResourcesMap () {
		
		Log.debug ( "Loading resources map..."  ) ;
		
		GetDesignResourcesMap< R > action = getDesignResourcesMapCommand() ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
			
		( new RPC< GetDesignResourcesMapResponse< R > >( Client.get().getDesignService() ) ).execute ( 
				action,
				new CommandCallback< GetDesignResourcesMapResponse< R > >(){
			
			public void onSuccess ( GetDesignResourcesMapResponse< R > response ) {
			
				
				
				if ( loadHandler != null ){
					loadHandler.onDesignResourcesLoad( response.getResourcesMap() ) ;
				}
				
			}
		
		}) ;
		
	}
	
	
	public void loadResourcesTags () {
		
		Log.debug ( "Loading resources tags..."  ) ;
		Log.debug ( "Loading resources tags 2..."  ) ;
		
		GetDesignResourcesTags< R > action = getDesignResourcesTagsCommand() ;
		
		Log.debug ( "Loading resources tags 3..."  ) ;
		
		action.setDesignUid( Client.get().getDesign().getUid() ) ;
		
	
		( new RPC< GetDesignResourcesTagsResponse< R > >( Client.get().getDesignService() ) ).execute ( 
				action,
				new CommandCallback< GetDesignResourcesTagsResponse< R > >(){
			
			public void onSuccess ( GetDesignResourcesTagsResponse< R > response ) {
				
				if ( loadHandler != null ){	
					loadHandler.onDesignResourcesLoad( response.getResourcesTags() ) ;
				}
				
			}		
		}) ;
		
	}
	
	
	protected GetDesignResources< R > getDesignResourcesCommand() {

		throw new RuntimeException( "Operation not supported" ) ;
	}
	
	protected GetDesignResourcesMap< R > getDesignResourcesMapCommand() {
		throw new RuntimeException( "Operation not supported" ) ;
	}
	
	protected GetDesignResourcesTags< R > getDesignResourcesTagsCommand() {
		Log.debug("Operation not supported" ) ;
		return null ;
	}
	
	
}
