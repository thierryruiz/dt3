package com.dotemplate.core.client.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;



public class TextBox extends com.google.gwt.user.client.ui.TextBox {

	public TextBox () {
		// prevents lost of focus when added to an (Ext) window
		addClickHandler ( new ClickHandler() {
			public void onClick ( ClickEvent ce ) {
				ce.stopPropagation () ;
			}
		});
	}
	
	public void setWidth ( int w ) {
		super.setWidth ( w + "px" ) ;
	}
	
	
	
	//private TextField<String> innerField ;
	
	/*
	public TextBox (){		
		add ( innerField = new MyTextField () ) ;
	}
	
	public String getText() {
		return innerField.getValue () ;
	}
	
	public void setText( String text ) {
		innerField.setValue ( text ) ;
	}
	
	
	public void setWidth ( int w ){
		innerField.setWidth ( w ) ;
	}
	
	
	public void setMaxLength ( int length ){
		innerField.setMaxLength ( length ) ;
	}
	
	
	class MyTextField extends TextField<String> {		
				
		protected void onFocus ( ComponentEvent be ) {
			Log.debug ( "Focus > " ) ;
		}
		
		protected void onBlur ( ComponentEvent be ) {
			Log.debug ( "blur >>" + be.getSource () + " " + be.getComponent ().getId () ) ;
		}
	}
	*/
	
	
}
