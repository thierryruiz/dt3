package com.dotemplate.core.client.widgets;


import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.ModelKeyProvider;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;


public class ListBox< T > implements HasChangeHandlers, IsWidget {

	private ComboBox< BaseModelData > innerCombo ;

	private ListStore< BaseModelData > store ;
	
	private T selected ;

	protected HandlerManager handlerManager;
	
	public ListBox () {
		
		handlerManager = new HandlerManager( this ) ;
		innerCombo = new ComboBox< BaseModelData >() ;
		
		innerCombo.setStore ( store = new ListStore< BaseModelData >() ) ;
		innerCombo.setDisplayField ( "name" ) ;
		
		/*
		innerCombo.addSelectionChangedListener( new SelectionChangedListener<BaseModelData> (){

			@Override
			public void selectionChanged(SelectionChangedEvent<BaseModelData> se) {
				Utils.alert("Changed") ;
			}
			
		});*/
		
		
		/*
		innerCombo.addSelectionChangedListener ( new SelectionChangedListener<BaseModelData> () {
			
			
			@Override
			public void selectionChanged ( SelectionChangedEvent<BaseModelData> se ) {
				selected = se.getSelectedItem ().get ( "value" ) ;
				fireEvent ( new ChangeEvent(){
					@Override
					public Object getSource () {
						return ListBox.this;
					}
				}) ;
			}
		} ) ;*/
		
		
		store.setKeyProvider ( new ModelKeyProvider<BaseModelData>() {
			@Override
			public String getKey ( BaseModelData model ) {
				return model.get ( "value" );
			}
		});
	
		
	}
	
	
	public void add( String name, T value ){
		
		BaseModelData model = new BaseModelData() ;
		model.set ( "name",name ) ;
		model.set ( "value",value ) ;

		store.add ( model ) ;

	}
	
	
	
	@Override
	public HandlerRegistration addChangeHandler ( ChangeHandler handler ) {
		return handlerManager.addHandler ( ChangeEvent.getType (), handler  );
	}
	
	
	public void setSelected( T value ){
		innerCombo.select ( store.findModel ( "value", value ) ) ;
	}
	
	
	public T getSelected() {
		return selected ;
	}
	
	
	public void setWidth ( String width ) {
		innerCombo.setWidth ( width );
	}


	@Override
	public void fireEvent(GwtEvent<?> event ) {
		handlerManager.fireEvent( event ) ;
		
	}


	@Override
	public Widget asWidget() {
		return innerCombo ;
	}
	
	
}
