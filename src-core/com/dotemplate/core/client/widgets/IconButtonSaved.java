/*
/*
 * 
 * Created on 18 d�c. 2007
 * 
 */
package com.dotemplate.core.client.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;



/**
 * 
 * @author Thierry Ruiz
 * 
 */
public class IconButtonSaved extends VerticalPanel implements HasClickHandlers {

    protected Image innerBtn ;
    
    protected Label label;
    
    protected boolean enabled = true ;
    
    protected HandlerManager handlerManager ;
    
    public IconButtonSaved( String lbl, String icon ){
    	
        add( innerBtn = new Image( icon ) ) ;
        setSpacing( 0 ) ;
        
        innerBtn.setStyleName ( "ez-icon-btn" ) ;
        
        if ( lbl != null ){
        	add( label = new Label ( lbl ) ) ;
            setCellHorizontalAlignment( label, ALIGN_CENTER ) ;
        }
                
        setCellHorizontalAlignment( innerBtn, ALIGN_CENTER ) ;
        
        DOM.setStyleAttribute( getElement(), "display", "inline" );
        
        innerBtn.addClickHandler ( new ClickHandler() {
			@Override
			public void onClick ( ClickEvent ce ) {
				if ( enabled ) {
					handlerManager.fireEvent ( new ClickEvent(){}) ;
				}
			}
        }) ;
        
        handlerManager = new HandlerManager(this) ;
        
    }
    
    
    public IconButtonSaved( String lbl, String icon, int width, int height ){
    	this( lbl, icon );
    	innerBtn.setPixelSize ( width, height ) ;
    }

   
    public HandlerRegistration addClickHandler( ClickHandler handler ) {
        return handlerManager.addHandler ( ClickEvent.getType (), handler ) ;
    }
    
    public void enable ( boolean en ){
    	if ( en && enabled || !en && !enabled ) return ;
    	
    	if ( en ) {
    		enabled = true ;
    		removeStyleName ( "disabled" ) ;
    	} else {
    		enabled = false ;
    		addStyleName ( "disabled" ) ;
    	}
    }

}


