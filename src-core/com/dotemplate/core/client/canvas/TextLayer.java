package com.dotemplate.core.client.canvas;


import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.client.canvas.event.TextLayerEvent;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.google.gwt.event.shared.HandlerRegistration;


public class TextLayer extends Layer< TextGraphic > {

	
	public TextLayer () {
		super ();
		makeDraggable () ;
	}
	
	
	public boolean isMoveable () {
		return true;
	}
	
	@Override
	public String getLabel() {
		String label = getGraphic().getLabel() ;
		
		if ( label != null && label.length() != 0 ){
			return label ;
		}
				
		return getGraphic().getText();
		
	}
	
	
	@Override
	public HandlerRegistration addLayerEventHandler ( LayerEventHandler<TextGraphic> h ) {
		return eventHandlerManager.addHandler ( TextLayerEvent.TYPE, h );
	}
	
	
	@Override
	public void fireMoved () {
		fireEvent ( new TextLayerEvent( this, LayerEvent.EventType.MOVED ) ) ;
	}

	@Override
	public void fireResized () {
		fireEvent ( new TextLayerEvent( this, LayerEvent.EventType.RESIZED ) ) ;
		
	}

	@Override
	public void fireSelected () {
		fireEvent ( new TextLayerEvent( this, LayerEvent.EventType.SELECTED ) ) ;
	}

	
}
