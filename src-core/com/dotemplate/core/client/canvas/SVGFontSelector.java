package com.dotemplate.core.client.canvas;

import java.util.LinkedHashMap;

import com.dotemplate.core.client.widgets.resource.DesignResourceDialog;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourceScrollSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceSelector;
import com.dotemplate.core.client.widgets.resource.SelectDesignResource;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.SVGFont;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.dotemplate.core.shared.command.GetSVGFonts;
import com.google.gwt.event.shared.HandlerRegistration;


public class SVGFontSelector extends DesignResourceDialog < SVGFont > {

	private static SVGFontSelector instance ;
	
	private LinkedHashMap < String, SVGFont > fontsMap ;
	
	private String selectedFontFamily ;
	
	public SVGFontSelector() {
		
		super( new DesignResourceProvider< SVGFont >() {
			
			@Override
			protected GetDesignResourcesMap< SVGFont > getDesignResourcesMapCommand() {
				return new GetSVGFonts() ;
			}
		
		}, 600, 500 ) ;
		
		
		setHeading( "Choose font" ) ;
	}
	
	
	public static void show( SelectDesignResourceHandler< SVGFont > handler, String selectedFamily  ){
		
		if ( instance == null ){
			instance = new SVGFontSelector() ;
		}
		
		instance.open( handler, selectedFamily ) ;
		
	}
	
	
	public void open( SelectDesignResourceHandler< SVGFont > selectHandler,
			String selectedFamily ) {
		
		selectedFontFamily = selectedFamily ;
		
		super.open( selectHandler  );
	
	}


	@Override
	protected DesignResourceSelector < SVGFont > createResourceSelector(
			DesignResourceProvider<SVGFont> loader ) {
		
		DesignResourceSelector < SVGFont > selector = new DesignResourceScrollSelector< SVGFont >( loader ) {
			@Override
			public HandlerRegistration addHandler(
					SelectDesignResourceHandler< SVGFont > handler ) {
				return handlerManager.addHandler( SelectDesignResource.getType( SVGFont.class ), handler );
			}
			
			@Override
			public  void setResources(
					LinkedHashMap< String, LinkedHashMap< String, SVGFont > > tags ) {
				
				super.setResources( tags );
				
				fontsMap = tags.values().iterator().next() ;
				
				resourcesPanel.setSelected( fontsMap.get( selectedFontFamily ) ) ;
			
			}
						
		};
		
		selector.asWidget().addStyleName( "ez-svgfont-selector" ) ;
		
		return selector ;
	}
	

	
}
