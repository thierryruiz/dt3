package com.dotemplate.core.client.canvas;



import java.util.ArrayList;
import java.util.LinkedHashMap;
import com.allen_sauer.gwt.log.client.Log;

import com.dotemplate.core.client.widgets.resource.DesignResourcePagingSelector;
import com.dotemplate.core.client.widgets.resource.SelectDesignResource;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;



public class ImageGalleryPanel extends DesignResourcePagingSelector< ImageResource > {
	
	protected HorizontalPanel tagsPanel ;
	
	protected ListBox tagSelector; 
	
	protected String selectedTag = null ;
	
	protected Label tagLabel ;
	
	protected ArrayList< String  > unlockedImages = new ArrayList< String >() ;
	
	
	public ImageGalleryPanel() {
		
		super ( new ImageResourcePagingAsyncProvider() ) ;
		
		top.setWidget( tagsPanel = new HorizontalPanel() ) ;
		top.setStyleName( "ez-toolbar" ) ;
				
		tagsPanel.setHeight( "50px" ) ;
		tagsPanel.setWidth( "100%" ) ;
		tagsPanel.setSpacing( 10 ) ;
		tagsPanel.setStyleName("ez-tag-selector-panel");
		
		tagLabel = new Label( "Image category : ") ;
		tagSelector = new ListBox() ;
		
		tagsPanel.add( tagLabel ) ;
		tagsPanel.add( tagSelector ) ;
		
		tagsPanel.setCellHorizontalAlignment(tagLabel, HorizontalPanel.ALIGN_RIGHT ) ;
		tagsPanel.setCellHorizontalAlignment(tagSelector, HorizontalPanel.ALIGN_LEFT ) ;
		
		tagsPanel.setCellVerticalAlignment(tagLabel, HorizontalPanel.ALIGN_MIDDLE ) ;
		tagsPanel.setCellVerticalAlignment(tagSelector, HorizontalPanel.ALIGN_MIDDLE ) ;
		
		
		tagSelector.addChangeHandler( new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent e) {
				filter ( tagSelector.getItemText( tagSelector.getSelectedIndex() ) );
			}
		} ) ;
		
		
		provider.loadResourcesTags() ;
		provider.loadResources() ;
		
		
	}
	

	protected void filter( String tag ) {
		( ( ImageResourcePagingAsyncProvider ) provider ).setTag( tag ) ;
		( ( ImageResourcePagingAsyncProvider ) provider ).reset() ;
		provider.loadResources() ;
	}


	@Override
	public HandlerRegistration addHandler(
			SelectDesignResourceHandler< ImageResource > handler ) {
		
		return handlerManager.addHandler( SelectDesignResource.getType( ImageResource.class ), handler );
	
	}



	public void setSelectedTag( String tag ) {
		selectedTag = tag ;
		selectTag ( tag ) ;
	}
	
	
	private void selectTag( String tag ){
		
		if ( tagSelector.getItemCount() == 0 ) {
			return ;
		}

		for ( int i = 0 ; i <   tagSelector.getItemCount()  ; i++  ){
			
			if (  tagSelector.getItemText( i ).equals ( tag ) ){
				tagSelector.setSelectedIndex( i ) ;
				filter ( tag );
				return ;
			}
		}
	}


	
	@Override
	public void onDesignResourcesLoad(ArrayList<ImageResource> items) {
	}


	@Override
	public void onDesignResourcesLoad(
			LinkedHashMap< String, LinkedHashMap<String, ImageResource > > tags) {
	}


	@Override
	public void onDesignResourcesLoad(String[] tags) {
		
		for ( String tag : tags ){
			tagSelector.addItem( tag ) ;
		}
		
		if ( selectedTag != null ){
			selectTag ( selectedTag ) ;
		}
		
	}
	
	@Override
	protected void setResources( ArrayList< ImageResource > resources,
			boolean hasMore) {
		
		for( ImageResource ir : resources ){
			for ( String img : unlockedImages ){
				if ( ir.isPremium() && ir.getPath().equals( img ) ){
					Log.debug( "Unlock premium resource " + ir.getPath() + " : " ) ;
					ir.setFree( true ) ;
				}
			}
		}
		
		
		super.setResources(resources, hasMore);
	}
	
	protected void setUnlockedImages( ArrayList< String > unlockedImages ) {
		this.unlockedImages = unlockedImages;
	}
	
}
