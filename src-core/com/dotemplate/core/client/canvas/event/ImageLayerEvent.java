package com.dotemplate.core.client.canvas.event;


import com.dotemplate.core.client.canvas.ImageLayer;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.google.gwt.event.shared.GwtEvent;



public class ImageLayerEvent extends LayerEvent< ImageGraphic > {
	
	
	public static Type<LayerEventHandler<ImageGraphic> > TYPE = new Type< LayerEventHandler<ImageGraphic> >();
	
	
	public ImageLayerEvent ( ImageLayer layer, EventType type ) { 
		super( layer, type ) ;
	}

	
	@Override
	public GwtEvent.Type<LayerEventHandler< ImageGraphic > > getAssociatedType () {
		return TYPE ;
	}

}
