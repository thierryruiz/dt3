package com.dotemplate.core.client.canvas.event;

import com.dotemplate.core.shared.canvas.Graphic;

public class LayerEventAdapter<GRAPHIC extends Graphic> implements LayerEventHandler<GRAPHIC> {
	
	@Override
	public void onLayerMoved(LayerEvent<GRAPHIC> event) {
	}
	
	@Override
	public void onLayerResized(LayerEvent<GRAPHIC> event) {
	}
	
	@Override
	public void onLayerSelected(LayerEvent<GRAPHIC> event) {
	}
	
}
