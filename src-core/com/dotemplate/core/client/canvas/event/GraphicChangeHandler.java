package com.dotemplate.core.client.canvas.event;


import com.google.gwt.event.shared.EventHandler;

public interface GraphicChangeHandler extends EventHandler {
	
	public void onGraphicChange( GraphicChange event ) ;
	
	
}
