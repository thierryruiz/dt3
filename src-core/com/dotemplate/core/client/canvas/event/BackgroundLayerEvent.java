package com.dotemplate.core.client.canvas.event;


import com.dotemplate.core.client.canvas.BackgroundLayer;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;
import com.google.gwt.event.shared.GwtEvent;

public class BackgroundLayerEvent extends LayerEvent< BackgroundGraphic > {
	
	public static Type<LayerEventHandler<BackgroundGraphic> > TYPE = 
			new Type< LayerEventHandler< BackgroundGraphic > >();
	
	
	public BackgroundLayerEvent ( BackgroundLayer layer, EventType type ) { 
		super( layer, type ) ;
	}

	
	@Override
	public GwtEvent.Type<LayerEventHandler< BackgroundGraphic > > getAssociatedType () {
		return TYPE ;
	}

}
