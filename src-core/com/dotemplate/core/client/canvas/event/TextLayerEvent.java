package com.dotemplate.core.client.canvas.event;



import com.dotemplate.core.client.canvas.TextLayer;
import com.dotemplate.core.shared.canvas.TextGraphic;
import com.google.gwt.event.shared.GwtEvent;

public class TextLayerEvent extends LayerEvent< TextGraphic > {
	
	public static Type<LayerEventHandler<TextGraphic> > TYPE = new Type< LayerEventHandler<TextGraphic> >();
	
	
	public TextLayerEvent ( TextLayer layer, EventType type ) { 
		super( layer, type ) ;
	}

	
	@Override
	public GwtEvent.Type<LayerEventHandler< TextGraphic > > getAssociatedType () {
		return TYPE ;
	}

}
