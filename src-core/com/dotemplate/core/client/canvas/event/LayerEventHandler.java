package com.dotemplate.core.client.canvas.event;



import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.event.shared.EventHandler;


public interface LayerEventHandler< GRAPHIC extends Graphic > extends EventHandler {
	
	void onLayerSelected( LayerEvent< GRAPHIC > event ) ;
	
	void onLayerMoved( LayerEvent< GRAPHIC > event ) ;
	
	void onLayerResized( LayerEvent< GRAPHIC > event ) ;
	
	
}
