package com.dotemplate.core.client.canvas.event;


import com.dotemplate.core.shared.canvas.Graphic;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasLayerEventHandlers< GRAPHIC extends Graphic> extends HasHandlers {
	
	HandlerRegistration addLayerEventHandler ( LayerEventHandler< GRAPHIC > h ) ; 
	
	
}
