package com.dotemplate.core.client.canvas.event;

import com.dotemplate.core.client.canvas.Layer;
import com.dotemplate.core.client.frwk.Event;
import com.dotemplate.core.shared.canvas.Graphic;




public abstract class LayerEvent <GRAPHIC extends Graphic> extends Event< LayerEventHandler< GRAPHIC > > {

	public enum EventType { SELECTED, MOVED , RESIZED } ;
	
	private EventType eventType ;
		
	private Layer< GRAPHIC > layer ;
	
	public LayerEvent( Layer< GRAPHIC > layer, EventType type ) {
		this.eventType = type ;
		this.layer = layer ; 
	}
	
	
	public Layer< GRAPHIC > getLayer () {
		return layer;
	}
	

	@Override
	protected void dispatch ( LayerEventHandler< GRAPHIC > handler ) {
		switch ( eventType ){
			case SELECTED : handler.onLayerSelected ( this ) ; return ;
			case MOVED : handler.onLayerMoved ( this ) ; return ;
			case RESIZED : handler.onLayerResized ( this ) ; return ;
			default:return;
		}
	}

	

	
	
}
