package com.dotemplate.core.client.canvas;

import java.util.ArrayList;

import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.widgets.Window;
import com.dotemplate.core.client.widgets.resource.HasSelectDesignResourceHandlers;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.canvas.ImageResource;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.google.gwt.event.shared.HandlerRegistration;




public class AddGalleryImageDialog extends Window implements HasSelectDesignResourceHandlers< ImageResource >  {
	
	protected final static int WIDTH = 950 ;
	
	protected final static int HEIGHT = 630 ;
	
	protected ImageGalleryPanel galleryPanel ;
	
	protected TabPanel extTabs ;
	
	
	public AddGalleryImageDialog () {

		setModal ( true ) ;
		setHeading ( "Add image" ) ;
		setSize ( WIDTH, HEIGHT ) ;
		
		Log.debug ( "Done AddImageDialog..."  ) ;

		galleryPanel = new ImageGalleryPanel() ;
		
		add( galleryPanel.asWidget() ) ;
		
	}
	
	
	@Override
	public HandlerRegistration addHandler( SelectDesignResourceHandler< ImageResource > h ) {
		return galleryPanel.addHandler( h );
	}


	public void selectTag(String tag ) {
		galleryPanel.setSelectedTag( tag ) ;
	}
	
	
	
	public void setUnlockedImages( ArrayList< String > unlockedImages ) {
		galleryPanel.setUnlockedImages( unlockedImages ) ;
	}



	
	
}
