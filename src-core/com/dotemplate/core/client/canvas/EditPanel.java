package com.dotemplate.core.client.canvas;

import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.widgets.ContainerPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;

public class EditPanel implements IsWidget {

	private ContainerPanel container;
	
	private ScrollPanel scroller ;
	
	
	public EditPanel () {
		Log.debug ( "Create edit panel..." ) ;		
		container = new ContainerPanel( "Layer Settings" ) ;
		scroller = new ScrollPanel() ;
		container.setWidget( scroller ) ;
		container.asWidget().setStyleName( "ez-edit-panel" ) ;
		
	}
	
	
	public void show( Widget w ){
		clear () ;
		scroller.add( w ) ;
		asWidget().setVisible( true ) ;
		
	}
	
	
	public void clear() {
		scroller.clear() ;
	}


	@Override
	public Widget asWidget() {
		return container.asWidget() ;
	}
	
	
}
