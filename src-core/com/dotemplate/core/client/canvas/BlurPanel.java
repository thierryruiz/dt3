package com.dotemplate.core.client.canvas;


import com.dotemplate.core.client.widgets.Slider;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;


public class BlurPanel extends VerticalPanel implements HasChangeHandlers {

	private Slider slider ;
	
	private Image icon ;
		

	public BlurPanel ( ) {
				
		setSpacing ( 25 ) ;
		
		add ( icon =  new Image( "client/icons/BlurPanel/blur.jpg" ) ) ;
		setCellHorizontalAlignment ( icon ,  VerticalPanel.ALIGN_CENTER) ;
		setCellVerticalAlignment ( icon ,  VerticalPanel.ALIGN_MIDDLE) ;
		

		Label title = new Label( "Blur effect" ) ;
		
		add ( title ) ;
		add (  slider = new Slider( 0, 60, 130 ) )  ;
		

		setCellHorizontalAlignment ( title,  VerticalPanel.ALIGN_CENTER ) ;		
		setCellHorizontalAlignment ( slider,  VerticalPanel.ALIGN_CENTER ) ;
		
		setWidth ( "250px" ) ;
		
	}
	
	
	public int getBlurDeviation( ) {
		return ( int ) slider.getValue() ;
	}
	
	
	public void setBlurDeviation ( int deviation ){
		slider.setValue ( deviation ) ;
	}
	
	
	
	@Override
	public HandlerRegistration addChangeHandler ( ChangeHandler handler ) {
		return slider.addChangeHandler ( handler ) ;
	}
	
	
}
