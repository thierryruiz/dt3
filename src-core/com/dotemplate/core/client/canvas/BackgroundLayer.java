package com.dotemplate.core.client.canvas;

import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.canvas.event.BackgroundLayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.shared.canvas.BackgroundGraphic;

import com.google.gwt.event.shared.HandlerRegistration;


public class BackgroundLayer extends Layer< BackgroundGraphic > {
	
	public BackgroundLayer () {
		super() ;
	}
	
	
	@Override
	public boolean isMoveable () {
		return false;
	}

	
	@Override
	public String getLabel() {
		String label = getGraphic().getLabel() ;
		
		if ( label != null && label.length() != 0 ){
			return label ;
		}
				
		return "Background" ;
		
	}
	
	
	@Override
	public HandlerRegistration addLayerEventHandler ( LayerEventHandler<BackgroundGraphic> h ) {
		return eventHandlerManager.addHandler ( BackgroundLayerEvent.TYPE, h );
	}

	
	public void refresh ( String url, int rWidth, int rHeight ) {
		img.setUrl ( url );			
	}

	public void select() {
		Log.debug("Select bg") ;
		if ( selected ) return ; 
		selected = true ;
		fireSelected() ;
		selector.select() ;	
	}
	
	
	public void unselect(){
		Log.debug("Unselect bg") ;
		if ( !selected ) return ; 
		selected = false ;
		selector.unselect() ;
	}
	
	
	@Override
	public void fireMoved () {
		fireEvent ( new BackgroundLayerEvent( this, LayerEvent.EventType.MOVED ) ) ;
	}

	@Override
	public void fireResized () {
		fireEvent ( new BackgroundLayerEvent( this, LayerEvent.EventType.RESIZED ) ) ;
		
	}

	@Override
	public void fireSelected () {
		fireEvent ( new BackgroundLayerEvent( this, LayerEvent.EventType.SELECTED ) ) ;
	}

	
}
