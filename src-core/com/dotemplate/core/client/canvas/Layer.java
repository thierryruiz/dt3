package com.dotemplate.core.client.canvas;


import com.dotemplate.core.client.canvas.event.HasLayerEventHandlers;
import com.dotemplate.core.client.editors.GraphicEditor;
import com.dotemplate.core.shared.canvas.Graphic;
import com.extjs.gxt.ui.client.event.DragEvent;
import com.extjs.gxt.ui.client.event.DragListener;
import com.extjs.gxt.ui.client.event.ResizeEvent;
import com.extjs.gxt.ui.client.event.ResizeListener;
import com.extjs.gxt.ui.client.fx.Draggable;
import com.extjs.gxt.ui.client.fx.Resizable;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;


public abstract class Layer< GRAPHIC extends Graphic > implements IsWidget, HasLayerEventHandlers< GRAPHIC > {
		
	protected Image img ;
	
	protected String imgUrl ;
		
	protected Draggable draggable ;
	
	protected Resizable resizable ;
	
	protected boolean selected ;

	protected ContentPanel ui ;

	protected HandlerManager eventHandlerManager ;
	
	protected GraphicEditor< GRAPHIC > editor ;
	
	protected LayerButton < GRAPHIC > selector ;
	
	
	public Layer () {

		eventHandlerManager = new HandlerManager( this ) ;
		selected = false ;

		img = new Image () ;
		
		ui = new ContentPanel() ;
		ui.setHeaderVisible(false) ;
		ui.setBorders ( false );
		ui.setCollapsible ( false );
		ui.add ( img );
		ui.setStyleName ( "ez-layer" ) ;		
		
		img.addClickHandler ( new ClickHandler () {
			public void onClick ( ClickEvent arg0 ) {
				select() ;
			}
		} );
		
		img.addLoadHandler ( new LoadHandler(){
			@Override
			public void onLoad ( LoadEvent arg0 ) {
				
				if ( selected && editor.getGraphic().getType() != Graphic.BG){
					ui.setSize ( img.getWidth () + 4  , img.getHeight () + 4  ) ;
				} else {
					ui.setSize ( img.getWidth () , img.getHeight () ) ;
				}
				
			}
		}) ;
		
	}
	
	public int getLeft() {
		ContentPanel parentPanel = ( ContentPanel ) ui.getParent() ;
		if ( parentPanel == null ) return 0 ;
		return ( !selected ) ? draggable.getDragWidget ().getAbsoluteLeft () - parentPanel.getAbsoluteLeft () + 1 : // parentPanel has 1px border  	
			draggable.getDragWidget ().getAbsoluteLeft () - parentPanel.getAbsoluteLeft () + 1 ; // parentPanel has 1px border  	
	}

	
	public int getTop() {		
		
		ContentPanel parentPanel = ( ContentPanel ) ui.getParent() ;
		if ( parentPanel == null ) return 0 ;
		return ( !selected ) ? draggable.getDragWidget ().getAbsoluteTop () - parentPanel.getAbsoluteTop () + 1 : // parentPanel has 1px border  	
			draggable.getDragWidget ().getAbsoluteTop () - parentPanel.getAbsoluteTop () + 1 ; // parentPanel has 1px border  		
	}
	
	
	public int getWidth() {
		return selected ? ui.getOffsetWidth () - 4 : ui.getOffsetWidth()  ;
	}
	

	public int getHeight () {
		return selected ? ui.getOffsetHeight () - 4 : ui.getOffsetHeight()  ;
	}
	

	public void setImageUrl ( String url ) {
		this.imgUrl = url ;
		img.setUrl ( url + "?" + System.currentTimeMillis () ) ;
	}
	
	public GraphicEditor< GRAPHIC > getEditor() {
		return editor;
	}

	public void setEditor( GraphicEditor<GRAPHIC> editor ) {
		this.editor = editor;
	}
	
	public void setSelector(LayerButton<GRAPHIC> selector) {
		this.selector = selector;
	}
	
	public LayerButton<GRAPHIC> getSelector() {
		return selector;
	}
	
	
	public GRAPHIC getGraphic () {
		return editor.getGraphic() ;
	}
	
	
	public abstract boolean isMoveable () ;
	
	
	protected void makeDraggable() {
		
		draggable = new Draggable(  ui ) ;
		draggable.setUpdateZIndex ( false ) ;
		
		draggable.addDragListener ( new DragListener () {

			@Override
			public void dragStart ( DragEvent de ) {
				select() ;
			}
			
			public void dragEnd( com.extjs.gxt.ui.client.event.DragEvent de ) {				
				fireMoved() ;
			};
		}) ;
		
		draggable.setUseProxy( false ); 
	
	}
	
	
	protected void  makeResizeable() {
		
		resizable = new Resizable ( ui ) ;
		
		resizable.addResizeListener ( new ResizeListener ( ) {	
			
			public void resizeEnd ( ResizeEvent re ) {
				super.resizeEnd ( re );
				
				img.setWidth ( Layer.this.getWidth() + "px" );
				img.setHeight ( Layer.this.getHeight()  +  "px" ) ;
				
				fireResized() ;
				
			}
			
			
			@Override
			public void resizeStart ( ResizeEvent re ) {
				super.resizeStart ( re );
				select() ;
			}
			
		}) ;
		
		resizable.setPreserveRatio ( true ) ;
	}
	
	
	
	@Override
	public void fireEvent ( GwtEvent<?> e ) {
		eventHandlerManager.fireEvent ( e  ) ;
	}
	

	
	@Override
	public Widget asWidget() {
		return ui;
	}
	

	
	public void refresh ( String url, int rWidth, int rHeight ) {

		img.setUrl ( url );			
		img.setWidth ( rWidth + "px" );
		img.setHeight ( rHeight  +  "px" );
		
		ui.setSize ( rWidth , rHeight ) ;
	
		if ( selected ){
			ui.setSize ( rWidth + 4  , rHeight + 4  ) ;
		} else {
			ui.setSize ( rWidth , rHeight ) ;
		}
		
		ui.layout(true) ;	
	
	}
	
	
	
	public void select() {
		
		if ( selected ) return ;
	
		selected = true ;
		img.addStyleName ( "selected" ) ;
		ui.setSize ( img.getWidth () + 4  , img.getHeight () + 4 ) ;
		fireSelected() ;
		selector.select() ;
	}
	
	
	public void unselect(){
		if( !selected ) return ;
		selected = false ;
		img.removeStyleName ( "selected" ) ;
		ui.setSize ( img.getWidth () , img.getHeight () ) ;
		selector.unselect() ;
	}
	
	
	public abstract String getLabel() ;
	
	public abstract void fireSelected() ;
	
	public abstract void fireResized() ; 
	
	public abstract void fireMoved() ;
	
	

}
