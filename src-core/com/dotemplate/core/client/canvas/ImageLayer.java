package com.dotemplate.core.client.canvas;

import com.dotemplate.core.client.canvas.event.ImageLayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEvent;
import com.dotemplate.core.client.canvas.event.LayerEventHandler;
import com.dotemplate.core.shared.canvas.ImageGraphic;
import com.google.gwt.event.shared.HandlerRegistration;


public class ImageLayer extends Layer< ImageGraphic > {
		

	public ImageLayer() {
		makeDraggable () ;
		makeResizeable () ;	
	}

	
	public boolean isMoveable () {
		return true ;
	}


	@Override
	public HandlerRegistration addLayerEventHandler ( LayerEventHandler<ImageGraphic> h ) {
		return eventHandlerManager.addHandler ( ImageLayerEvent.TYPE, h );
	}
	
	
	
	@Override
	public void fireMoved () {
		fireEvent ( new ImageLayerEvent( this, LayerEvent.EventType.MOVED ) ) ;
	}

	@Override
	public void fireResized () {
		fireEvent ( new ImageLayerEvent( this, LayerEvent.EventType.RESIZED ) ) ;
		
	}

	@Override
	public void fireSelected () {
		fireEvent ( new ImageLayerEvent( this, LayerEvent.EventType.SELECTED ) ) ;
	}


	@Override
	public String getLabel() {
		return editor.getGraphic().getLabel() ;
	}

	
}
