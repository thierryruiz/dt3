package com.dotemplate.core.client.editors;

import java.util.LinkedHashMap;

import com.dotemplate.core.shared.properties.Property;


public class PropertyEditorMap extends LinkedHashMap<String, PropertyEditor<? extends Property > > {

	private static final long serialVersionUID = -651953993207616955L;
	
	public void add( PropertyEditor<? extends Property > e ) {
		put( e.getProperty().getUid(), e ) ; 
	}
	
}
