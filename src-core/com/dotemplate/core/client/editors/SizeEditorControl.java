package com.dotemplate.core.client.editors;

public class SizeEditorControl extends PropertyEditorControl {
	
	public SizeEditorControl( EditorControlStyle style ) {
		super ( style ) ;
	}
	
	
	@Override
	public void setText(String text) {
		extButton.setText( text ) ;
		extButton.setWidth( style.getButtonWidth() - 10 ) ;
	}
	
	
}
