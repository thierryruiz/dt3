package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.widgets.NumberInputDialog;
import com.dotemplate.core.client.widgets.NumberInputDialog.Callback;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class SizePropertyEditor implements PropertyEditor<  SizeProperty >, Callback {

	private SizeProperty property ;
	
	private static NumberInputDialog customSizeDialog ;
	
	private EditorSupport support ;
	
	protected PropertyEditorControl control ;
	
	protected EditorControlStyle editorStyle ;
	
	protected PropertySetEditor parentEditor ;
	
	protected String editorId ;
	
	private static int CUSTOM_VALUE = -9000 ;
	
	protected SelectionListener< MenuEvent > menuSelectionListener = new SelectionListener<MenuEvent>() {
		@Override
		public void componentSelected( MenuEvent event ) {
			onMenuSelected( ( SizeMenuItem )  event.getItem() ) ;
		}
	};
	
	
	protected SizePropertyEditor( SizeProperty property, EditorControlStyle style ) {

		this.property = property ;
		
		this.support = new EditorSupport( this ) ;
		
		this.editorStyle = style ;
		
	}
	
	
	
	protected void onMenuSelected( SizeMenuItem item ) {
		
		for ( Component menuitem : item.getParentMenu().getItems() ){
			(( SizeMenuItem ) menuitem ).setSelected( false ) ;
		}
		
		item.setSelected( true ) ;
		
		if ( item.isCustom() ){
			
			if ( customSizeDialog == null ){
				customSizeDialog = new NumberInputDialog() ;
			}
			
			customSizeDialog.setHeading( "Custom " +  property.getLabel().toLowerCase() ) ;
			customSizeDialog.setLabel( "Enter custom " + property.getLabel().toLowerCase() + " (px)"  ) ;
			customSizeDialog.open( property.getValue(), property.getMin(), property.getMax(), this ) ;
						
	
		} else {
			property.setValue( item.value ) ;
			support.firePropertyChange( property ) ; 
		}
		
	}

	
	@Override
	public HandlerRegistration addHandler( PropertyChangeEventHandler h ) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler(PropertyChangeEventHandler h) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public SizeProperty getProperty() {
		return property ;
	}

	
	@Override
	public void setProperty(SizeProperty property) {
		this.property = property ;
	}
	
	
	
	@Override
	public Widget asWidget() {
		if ( control == null ) {
			createWidget() ;
		}
		
		return control.asWidget() ;
	}

	
	
	public PropertySetEditor getParentEditor() {
		return parentEditor;
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {
		this.parentEditor = parentEditor;
	}
	
	
	protected void createWidget() {
		
		control = new SizeEditorControl( editorStyle ) ;
		control.setText( property.getLabel() ) ;
		control.setIcon( "client/icons/themetoolbar/" + property.getIcon() ) ;
		control.setId( editorId ) ;
		
		
		// TODO move GXT menu outside editor class 
    	final Menu menu = new Menu();
    	
    	SizeMenuItem item ;
    	
    	int[] range = property.getAllowedValues() ;
    	
    	boolean custom = true ;
    	
    	for ( int v : range ){
    		
    		menu.add( item = new SizeMenuItem( "" + v  + " px", v ) );  	
    		
    		item.addSelectionListener( menuSelectionListener ) ;
   
    		if ( v == property.getValue() ){
    			item.setSelected( true ) ;
    			custom = false ;
    		}
    	}

    	menu.add( item = new SizeMenuItem( "Custom value...", CUSTOM_VALUE ) )  ;  
    	
    	if ( custom ){
    		item.setSelected( true ) ;
    	}
    	
    	item.addSelectionListener( menuSelectionListener ) ;
    	
    	( ( Button ) control.asWidget() ).setMenu ( menu ) ;
    	
    	
    	
    	
	}
	
	
	
	private class SizeMenuItem extends MenuItem {
		
		private int value ;
		
		public SizeMenuItem( String text,  int value ) {
			setText(text) ;
			this.value = value;
		}
		
		boolean isCustom() {
			return CUSTOM_VALUE == value ;
		}
		
		void setSelected( boolean selected ) {
			this.setStyleAttribute( "fontWeight", 
					selected ? "bold" : "normal" ) ;
		}
	
	}

	


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
		
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		return control.isEnabled();
	}


	@Override
	public void onOk( int value ) {
		property.setValue( value ) ;
		support.firePropertyChange( property ) ;				
	}
	
	
}
