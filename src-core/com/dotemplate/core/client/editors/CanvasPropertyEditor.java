package com.dotemplate.core.client.editors;


import com.dotemplate.core.client.Client;
import com.dotemplate.core.shared.properties.CanvasProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;


public class CanvasPropertyEditor extends PropertySetEditor {
		
	protected EditorControlStyle editorStyle ;
	
	
	public CanvasPropertyEditor( CanvasProperty property, EditorControlStyle style ) {	
		
		super( property );
				
		this.editorStyle = style ;
		
	}

	
	
	@Override
	protected void createWidget() {

		control = new PropertySetEditorControl( property.getLabel() ) ;
		
		PropertyEditorControl editCanvasControl =  new PropertyEditorControl( editorStyle ) ;
		
		editCanvasControl.setText( "Design " + property.getLabel().toLowerCase()  ) ;
		editCanvasControl.setIcon( "client/icons/themetoolbar/canvas-edit.png" ) ;
		
		control.add( editCanvasControl.asWidget() ) ;
		
		control.layout() ;
		
		editCanvasControl.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				Client.get().editCanvas( ( CanvasProperty ) property ) ;
			}
		}) ;
	
	}
	
	

}
