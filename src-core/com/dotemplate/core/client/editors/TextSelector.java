package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourceScrollSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceSelector;
import com.dotemplate.core.client.widgets.resource.SelectDesignResource;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.TextProperty;
import com.google.gwt.event.shared.HandlerRegistration;


public class TextSelector extends SymbolSelector {

	public TextSelector( PropertySet set ) {
		super( set );
	}
	
	
	@Override
	protected DesignResourceSelector < PropertySet > createResourceSelector(
			DesignResourceProvider< PropertySet > loader ) {
		
		return new DesignResourceScrollSelector< PropertySet >( loader ) {
			
			@Override
			public HandlerRegistration addHandler(
					SelectDesignResourceHandler< PropertySet > handler ) {
				
				return handlerManager.addHandler( SelectDesignResource.getType( TextProperty.class ), handler );
			
			}
			
			@Override
			protected boolean isResourceAvailable( PropertySet resource ) {
				return !resource.getName().contains( "abstract" ) ;
			}
			
		};
	
	}

	
}
