package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.frwk.Utils;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;



public class ColorPropertyEditorControl extends PropertyEditorControl {
	
	private String color ;

	
	public ColorPropertyEditorControl( final String color, EditorControlStyle style ) {
		
		super( style ) ;
		
		this.color = color ;
		
		//setIcon( "images/spacer.gif" ) ;

		extButton.setIcon( new ColorImagePrototype() ) ;
		
		/*
		// Color can be set only after creation
		extButton.addHandler( new AttachEvent.Handler() {
			@Override
			public void onAttachOrDetach( AttachEvent ae ) {
				iconElement.getStyle().setBackgroundColor( color ) ;
			}
		} , AttachEvent.getType() ) ;
		*/
	}
	
	
	
	// icon has borders
	
	public void setIcon( String icon  ) {

		
		
		/*
		AbstractImagePrototype imagePrototype =  IconHelper.create( icon , style.getIconWidth()- 2, style.getIconHeight() -2 ) ;
		iconElement = imagePrototype.createElement() ;
		iconElement.getStyle().setProperty( "border", "1px solid #444" ) ;
		iconElement.getStyle().setBackgroundColor( "#FFFFFF" ) ;
		extButton.setIcon( imagePrototype ) ; 

		
		extButton.setIcon( new ClippedImagePrototype( icon , 0, 0,
				style.getIconWidth()- 2, style.getIconHeight() -2  ) {
			
			@Override
			public ImagePrototypeElement createElement() {
				Log.debug ( "create icon element" ) ;
				iconElement = super.createElement();
				iconElement.getStyle().setProperty( "border", "1px solid #444" ) ;
				//iconElement.getStyle().setBackgroundColor( color ) ;
				return iconElement;
			}
		
		}) ;
		
		*/
		
		
	}


	public void setColor( String color ){
		this.color= color ;
		//iconElement.getStyle().setBackgroundColor( color ) ;
		ColorImagePrototype p = new ColorImagePrototype() ;
		extButton.setIcon( new ColorImagePrototype() ) ;
		p.createElement() ;
		extButton.setText( extButton.getText() ) ;
	}
	
	
	
	public class ColorImagePrototype extends AbstractImagePrototype {

		private Image image ;
		
		@Override
		public void applyTo( Image image ) {
		}
	
		@Override
		public Image createImage() {
		    image = new Image( "images/spacer.gif" );
		    image.setPixelSize( style.getIconWidth() - 2, style.getIconHeight() - 2 );
		    Utils.style(image, "backgroundColor" , color ) ;
		    return image ;
		}

		
		public ImagePrototypeElement createElement() {
			AbstractImagePrototype p = IconHelper.create( 
					"images/spacer.gif" , style.getIconWidth()- 2, style.getIconHeight() -2 ) ;
			ImagePrototypeElement el = p.createElement() ;
			el.getStyle().setProperty( "border", "1px solid #444" ) ;
			el.getStyle().setBackgroundColor( color ) ;
			return el ;
		}
		
		@Override
		public String getHTML() {
			return "" ;
		}
		
		
	}
	


}
