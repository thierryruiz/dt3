/*
/*
 * 
 * Created on 2 mars 2007
 * 
 */
package com.dotemplate.core.client.editors;


import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.events.SchemeChangeEvent;
import com.dotemplate.core.client.events.SchemeChangeEventHandler;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.frwk.RPC;
import com.dotemplate.core.client.widgets.colorpicker.ColorPickerDialog;
import com.dotemplate.core.shared.command.GetProperty;
import com.dotemplate.core.shared.command.GetPropertyResponse;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


/**
 * 
 * @author Thierry Ruiz
 * 
 */
public class ColorPropertyEditor implements PropertyEditor< ColorProperty >, 
	ColorPickerDialog.Handler, SchemeChangeEventHandler {
	
	protected ColorProperty property ;
	
	protected EditorSupport support ;

	protected PropertySetEditor parentEditor ;
	
	protected ColorPropertyEditorControl control ;
	
	protected EditorControlStyle editorStyle ;
	
	protected String editorId ;
	
	protected ColorPropertyEditor ( ColorProperty property, EditorControlStyle style ) {
		
		this.property = property ; 
		
		this.support = new EditorSupport( this ) ;
		
		this.editorStyle = style ;
		
	}
	

	protected void openColorPicker() {
		
		ColorPickerDialog colorPicker = Client.get().getColorPickerDialog() ;
				
		colorPicker.setSelectedColor( property.getValue() ) ;
		colorPicker.setScheme( Client.get().getColorScheme() ) ;
		colorPicker.setUserScheme( Client.get().getDesign().getUserScheme() ) ;
		
		colorPicker.open( this ) ;
	}

	
	
	@Override
	public void onColorSelected( String color ) {
		property.setValue( color ) ;
		control.setColor( color ) ;
		
		support.firePropertyChange( property ) ;
	
		Client.get().addUserColor( color ) ;
	
	}

	
	
	@Override
	public HandlerRegistration addHandler(PropertyChangeEventHandler h) {
		return support.addHandler( h );
	}



	@Override
	public void removeHandler( PropertyChangeEventHandler h ) {
		support.removeHandler( h ) ;
	}



	@Override
	public void fireEvent( GwtEvent<?> e ) {
		support.fireEvent( e ) ; 
	}


	@Override
	public ColorProperty getProperty() {
		return property ;
	}
	
	
	@Override
	public void setProperty(ColorProperty property) {
		this.property = property ;
	}
	

	@Override
	public Widget asWidget() {
		
		if ( control == null ) {
			createWidget() ; 
		}
		
		return control.asWidget() ;
	}

	
	public PropertySetEditor getParentEditor() {
	
		return parentEditor;
	
	}


	public void setParentEditor( PropertySetEditor parentEditor ) {	
		this.parentEditor = parentEditor;
	}
	
	
	protected void createWidget() {
		
		control = new ColorPropertyEditorControl( property.getValue(), editorStyle ) ;
		control.setText( property.getLabel() ) ;
		control.setId( editorId ) ;
				
		control.addClickHandler( new ClickHandler() {
			@Override
			public void onClick( ClickEvent e ) {
				openColorPicker() ;
			}
		}) ;
		
	}


	@Override
	public void remove() {
		control.asWidget().removeFromParent() ;
	}


	public String getEditorId() {
		return editorId;
	}


	public void setEditorId(String editorId) {
		this.editorId = editorId;
	}


	@Override
	public void enable() {
		control.enable();
	}


	@Override
	public void disable() {
		control.disable();
	}


	@Override
	public boolean isEnabled() {
		return control.isEnabled();
	}


	@Override
	public void onSchemeChanged( SchemeChangeEvent e ) {
		
		GetProperty getProperty = new GetProperty() ;
		getProperty.setDesignUid( Client.get().getDesign().getUid() ) ;
		getProperty.setUid( property.getUid() ) ;
		
		( new RPC< GetPropertyResponse >( Client.get().getDesignService() ) ).execute ( 
				getProperty, new CommandCallback< GetPropertyResponse >() {
				
			@Override
			public void onSuccess ( GetPropertyResponse response ) {
				
				property.setValue( ( ( ColorProperty ) response.getProperty() ).getValue() )  ;
				
				if ( control == null ) {
					createWidget() ;
				}
				
				control.setColor( property.getValue() ) ;
			
			}
			
		}) ;
		

	}
	
	
}
