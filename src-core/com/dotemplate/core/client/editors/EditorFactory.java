package com.dotemplate.core.client.editors;



import com.allen_sauer.gwt.log.client.Log;
import com.dotemplate.core.client.editors.MutablePropertySetEditor;

import com.dotemplate.core.shared.properties.CanvasProperty;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.dotemplate.theme.client.ThemeClient;



public abstract class EditorFactory  {

	
	public PropertyEditor< ? extends Property > createPropertyEditor( Property property ) {
		
		
		/*
		boolean editable = property.getEditable () != null &&  property.getEditable () ;
		
		boolean enable = property.getEnable() != null &&  property.getEnable () ;
	
		
		if ( !editable || ! enable ) {
			
			Log.debug ( "Property '" + property.getName () + "' ignored because not editable or enabled." ) ;
			
			return null ;
		}*/
		
		
		Log.debug ( "Creating property editor for property '" + property.getType() + "'" + property.getLongName () + "'." ) ;
		
		
		switch ( property.getType () ){
			
			case Property.SET :
				PropertySet set = ( PropertySet ) property ;
				
				if ( set.mutable() ) {
					return new MutablePropertySetEditor( set, getEditorControlStyle() ) ;
				
				} else {
					return new PropertySetEditor( set ) ;				
				}
				
			case ( Property.CANVAS ) :
				return new CanvasPropertyEditor( ( CanvasProperty ) property, getEditorControlStyle() ) ;


			case ( Property.COLOR ) :
				ColorPropertyEditor cpe = new ColorPropertyEditor( ( ColorProperty ) property, getEditorControlStyle() ) ;
				ThemeClient.get().addHandler(  cpe ) ;
				return cpe ;
			
			case ( Property.SIZE ) :
				return new SizePropertyEditor( ( SizeProperty ) property, getEditorControlStyle() ) ;
			
			case ( Property.PERCENT ) :
				return new PercentPropertyEditor( ( PercentProperty ) property, getEditorControlStyle() ) ;	
			
			
			default: return null ;
		
		}
		
	}
	
	
	public abstract EditorControlStyle getEditorControlStyle() ;
	
	

}
