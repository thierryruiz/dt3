package com.dotemplate.core.client.editors;

import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.properties.ColorProperty;
import com.dotemplate.core.shared.properties.PercentProperty;
import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.dotemplate.core.shared.properties.SizeProperty;
import com.dotemplate.core.shared.properties.TextProperty;


public abstract class CanvasEditorFactory extends EditorFactory {
		
	
	@Override
	public PropertyEditor<? extends Property> createPropertyEditor(
			Property property) {
		
		PropertyEditor<? extends Property> editor = super.createPropertyEditor( property );
	
		if ( editor != null ) return editor ;
		
		
		switch ( property.getType () ){
		
			case Property.SET :
			case Graphic.BG :
				PropertySet set = ( PropertySet ) property ;
				
				if ( set.mutable() ) {
					
					return new MutablePropertySetEditor( set, getEditorControlStyle() ) ;
				
				} else {
					return new PropertySetEditor( set ) ;				
				}

			case Graphic.TEXT :
				return new TextStyleEditor( ( TextProperty ) property, getEditorControlStyle() ) ;
	
			case ( Property.COLOR ) :
				return new ColorPropertyEditor( ( ColorProperty ) property, getEditorControlStyle() ) ;
			
			
			case ( Property.SIZE ) :
				return new SizePropertyEditor( ( SizeProperty ) property, getEditorControlStyle() ) ;	
			
			case ( Property.PERCENT ) :
				return new PercentPropertyEditor( ( PercentProperty ) property, getEditorControlStyle() ) ;	
			
			
			default: return null ;
		
		}
		
	
	}
	
	
	
}
