package com.dotemplate.core.client.editors;



public class BackgroundEditorFactory extends CanvasEditorFactory {
	
	private static BackgroundEditorFactory instance  ;
	
	
	public static BackgroundEditorFactory get() {
		
		if ( instance == null ) {
			instance = new BackgroundEditorFactory() ;
		}
		
		return instance ;
	}
	

	@Override
	public EditorControlStyle getEditorControlStyle() {
		return EditorControlStyle.BACKGROUND_EDITOR_STYLE ;
	}
	
	
	
}
