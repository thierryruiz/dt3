package com.dotemplate.core.client.editors;

import com.dotemplate.core.shared.properties.PropertySet;

public class TextStyleEditor extends MutablePropertySetEditor {

	public TextStyleEditor( PropertySet set, EditorControlStyle style ) {
		super( set, style );	
	}
	
	
	@Override
	protected void createWidget() {
		super.createWidget();
		changeControl.setText( "Text styles..." ) ;
	}
	
	
	protected void chooseSymbol() {
		
		if ( symbolSelector == null ){
			
			symbolSelector = new TextSelector( property ) ;
		
		}
		
		symbolSelector.setSelected( property ) ;
		symbolSelector.open( this ) ;
		
		
	}

}
