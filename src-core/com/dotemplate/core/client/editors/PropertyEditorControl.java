package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.editors.EditorControlStyle.Layout;
import com.dotemplate.core.client.editors.EditorControlStyle.Scale;
import com.extjs.gxt.ui.client.Style.ButtonArrowAlign;
import com.extjs.gxt.ui.client.Style.ButtonScale;
import com.extjs.gxt.ui.client.Style.IconAlign;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.AbstractImagePrototype.ImagePrototypeElement;
import com.google.gwt.user.client.ui.impl.ClippedImagePrototype;


public class PropertyEditorControl implements IsWidget, HasClickHandlers {
	
	protected Button extButton ; 
	
	protected EditorControlStyle style ;
	
	protected ImagePrototypeElement iconElement ;
	
	public PropertyEditorControl( EditorControlStyle style ) {
		
		extButton = new Button() ;

		
		this.style = style ;
		
		if ( style.getScale() == Scale.SMALL ){
			extButton.setScale( ButtonScale.SMALL ) ;
		}
		
		if ( style.getScale().equals( Scale.MEDIUM )  ){
			extButton.setScale( ButtonScale.MEDIUM ) ;
		}
		
		if ( style.getScale() == Scale.LARGE ){
			extButton.setScale( ButtonScale.LARGE ) ;
		}
		
		
		if ( style.getLayout() == Layout.ICON_RIGHT ) {
			extButton.setIconAlign( IconAlign.RIGHT ) ;
		} else {
			extButton.setIconAlign( IconAlign.TOP ) ;
			extButton.setArrowAlign( ButtonArrowAlign.BOTTOM );  
		}
		
		if ( style.getButtonWidth() > 0 ){
			extButton.setWidth( style.getButtonWidth() ) ;
		}
		
		if ( style.getButtonHeight() > 0 ){
			extButton.setHeight( style.getButtonHeight() ) ;
		}
		
	}
	
	/*
	public void setIcon( String icon ){
		extButton.setIcon( IconHelper.createPath( icon, style.getIconWidth(), style.getIconHeight() ) ) ;
	}
	*/
	

	
	public void setIcon( String icon ){
		extButton.setIcon( new ClippedImagePrototype( icon, 0, 0,
				style.getIconWidth(), style.getIconHeight() )   {
			
			@Override
			public ImagePrototypeElement createElement() {
				iconElement = super.createElement();
				iconElement.getStyle().setProperty( "backgroundPosition", "center center" ) ;
				return iconElement;
			}
		}) ;
	}	
	
	
	
	
	public void setIcon( String icon, boolean border ){
		if ( ! border ) {
			setIcon( icon ) ;
			return ;
		}
		
		extButton.setIcon( new ClippedImagePrototype( icon, 0, 0,
				style.getIconWidth()- 2, style.getIconHeight() -2  )   {
			
			@Override
			public ImagePrototypeElement createElement() {
				iconElement = super.createElement();
				iconElement.getStyle().setProperty( "border", "1px solid #444" ) ;
				iconElement.getStyle().setProperty( "backgroundPosition", "center center" ) ;
				return iconElement;
			}
		}) ;
		
	}
	
	
		
	
	@Override
	public Widget asWidget() {
		return extButton ;
	}
	
	
	@Override
	public HandlerRegistration addClickHandler( ClickHandler h ) {
		return extButton.addHandler( h , ClickEvent.getType() );
	}

		
	@Override
	public void fireEvent( GwtEvent<?> event ) {
		extButton.fireEvent( event ) ;
	}

	
	public void setText( String text ) {
		if ( style.labelWordWrap() ){
			extButton.setText(  text.replaceFirst(" ", "<br/>" ) ) ;
		} else {
			extButton.setText( text ) ;
		}
	}
	
	public void setTip( String tip ){
		extButton.setToolTip(tip) ;
	}
	
	public void setId( String id ){
		extButton.setId( id + "_ctrl" ) ;
	}
	
	public void enable() {
		extButton.enable() ;
	}
		
	public void disable() {
		extButton.disable() ;
	}
	
	public boolean isEnabled() {
		return extButton.isEnabled() ;
	}
	
}
