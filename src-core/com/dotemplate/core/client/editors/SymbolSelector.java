package com.dotemplate.core.client.editors;


import com.dotemplate.core.client.widgets.resource.DesignResourceDialog;
import com.dotemplate.core.client.widgets.resource.DesignResourceList;
import com.dotemplate.core.client.widgets.resource.DesignResourceProvider;
import com.dotemplate.core.client.widgets.resource.DesignResourceScrollSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceSelector;
import com.dotemplate.core.client.widgets.resource.DesignResourceThumbnail;
import com.dotemplate.core.client.widgets.resource.SelectDesignResource;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.command.GetDesignResourcesMap;
import com.dotemplate.core.shared.command.GetSymbols;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.event.shared.HandlerRegistration;


public class SymbolSelector extends DesignResourceDialog< PropertySet > {
		
	public SymbolSelector( final PropertySet set ) {
		
		super( new DesignResourceProvider< PropertySet >() {
			@Override
			protected GetDesignResourcesMap< PropertySet > getDesignResourcesMapCommand() {
				GetSymbols action = new GetSymbols() ;
				action.setType ( set.getSymbolType () ) ;
				action.setFilter ( set.getFilter () ) ;
				return action ;
			}},  600, 500 ) ;
		
		
		setHeading( set.getLabel() ) ;
		setSelected( set ) ;
	}
	
	
	@Override
	protected DesignResourceSelector < PropertySet > createResourceSelector(
			DesignResourceProvider< PropertySet > loader ) {
		
		return new SymbolScrollSelector( loader ) ;
	
	}
	

	
	private final  class SymbolScrollSelector extends DesignResourceScrollSelector< PropertySet > {

		public SymbolScrollSelector( DesignResourceProvider<PropertySet> provider ) {
			super( provider );
		}

		
		@Override
		public HandlerRegistration addHandler( SelectDesignResourceHandler< PropertySet > h) {
			return handlerManager.addHandler( SelectDesignResource.getType( PropertySet.class ), h );
		}
		
		@Override
		protected DesignResourceList<PropertySet> createResourceList() {
			return new SymbolList() ;
		}
		
		
		@Override
		protected boolean isResourceAvailable(PropertySet resource) {
			return !resource.getName().contains( "abstract" ) ;
		}
				
	}
	
	
	private final class SymbolList extends DesignResourceList< PropertySet > {
		
		@Override
		protected DesignResourceThumbnail<PropertySet> getThumbnail(
				PropertySet set ) {
			return thumbnails.get( set.getParent() );
		}
		
			
	}


}