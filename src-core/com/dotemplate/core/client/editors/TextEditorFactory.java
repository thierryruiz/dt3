package com.dotemplate.core.client.editors;



public class TextEditorFactory extends CanvasEditorFactory {
	
	private static TextEditorFactory instance  ;
	
	
	public static TextEditorFactory get() {
		
		if ( instance == null ) {
			instance = new TextEditorFactory() ;
		}
		
		return instance ;
	}
	
	
	
	@Override
	public EditorControlStyle getEditorControlStyle() {
		return EditorControlStyle.TEXT_EDITOR_STYLE ;
	}
	
	
}
