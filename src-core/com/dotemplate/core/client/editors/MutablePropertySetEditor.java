package com.dotemplate.core.client.editors;

import com.dotemplate.core.client.Client;
import com.dotemplate.core.client.events.PropertyChangeEventHandler;
import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.client.widgets.resource.SelectDesignResourceHandler;
import com.dotemplate.core.shared.command.GetSymbol;
import com.dotemplate.core.shared.command.GetSymbolResponse;
import com.dotemplate.core.shared.properties.PropertyMap;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Widget;


public class MutablePropertySetEditor extends PropertySetEditor 
	implements SelectDesignResourceHandler< PropertySet > {
	
	protected SymbolSelector symbolSelector ;
	
	protected PropertyEditorControl changeControl ;
	
	protected EditorControlStyle editorStyle ;
	
	
	public MutablePropertySetEditor( PropertySet set, EditorControlStyle style ) {	
		
		super( set );
				
		this.editorStyle = style ;

	}
	
	
	protected void chooseSymbol() {
		
		if ( symbolSelector == null ){
			symbolSelector = new SymbolSelector( property ) ;
		}
		
		symbolSelector.setSelected( property ) ;
		symbolSelector.open( this ) ;
	
	}
	
	
	@Override
	public HandlerRegistration addHandler(PropertyChangeEventHandler h) {
		return super.addHandler(h);
	}
	
	
	@Override
	public void onDesignResourceSelected( PropertySet selectedSymbol ) {
				
		// load symbol from server 
		GetSymbol getSymbol = new GetSymbol() ;
		
		getSymbol.setDesignUid( Client.get().getDesign().getUid() ) ;
		
		getSymbol.setName( selectedSymbol.getName() ) ;
		
		
		Client.get().getDesignService().execute( getSymbol , new CommandCallback< GetSymbolResponse >()  {
					
			public void onSuccess( GetSymbolResponse response ) {
				
				PropertySet symbol = response.getSymbol() ;
				
				property.setParent ( symbol.getName () ) ;
				property.setPath ( symbol.getPath () ) ;
				property.setThumbnailHtml ( symbol.getThumbnailHtml () ) ;
				property.setThumbnailUrl( symbol.getThumbnailUrl() ) ;
				property.setFilter ( symbol.getFilter () ) ;
				property.setIcon( symbol.getIcon () ) ;
				
				PropertyMap deletedProperties = property.getPropertyMap() ;
				
				property.setPropertyMap ( symbol.getPropertyMap () ) ;
				
				if ( property.getIcon() != null ){
					
					changeControl.setIcon( "client/icons/themetoolbar/" + property.getIcon() ) ;
				
				} else {
					
					changeControl.setIcon( ( ( PropertySet ) property ).getThumbnailUrl (), true ) ;
				
				}
						
				support.fireSymbolChange( property, symbol, deletedProperties ) ; 
				
			};
		
		}) ;
		
	}
	
	
	
	@Override
	protected void createWidget() {

		String heading =  property.selectable() ? property.getLabel() : null ;

		control = new PropertySetEditorControl( heading ) ;

		changeControl = new PropertyEditorControl( editorStyle ) ;
		
		
		if ( property.getIcon() != null ){
			changeControl.setIcon( "client/icons/themetoolbar/" + property.getIcon() ) ;
		} else {
			changeControl.setIcon( ( ( PropertySet ) property ).getThumbnailUrl (), true ) ;
		}
		
			
		changeControl.setId( editorId ) ;
		
		String lbl = property.getSwappButton() ;
		
		if ( lbl == null ){
			lbl = property.getLabel() ;
			if ( lbl == null ) lbl = "" ;
		}
		
		changeControl.setText( lbl ) ;
		
		
		changeControl.addClickHandler( new ClickHandler() {	
			@Override
			public void onClick( ClickEvent e ) {
				chooseSymbol() ;
			}
		} ) ;
		
		
		control.add( changeControl.asWidget() ) ;
		
	}
	

	public PropertyEditorControl getChangeControl() {
		return changeControl;
	}
	
	
	
	@Override
	public Widget asWidget() {
		
		if( control == null ) {
			
			createWidget() ;
		
		}
		
		return control.asWidget() ;
		
	}

	
}
