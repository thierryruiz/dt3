package com.dotemplate.core.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface SchemeChangeEventHandler extends EventHandler {
	
	void onSchemeChanged( SchemeChangeEvent e ) ;

}
