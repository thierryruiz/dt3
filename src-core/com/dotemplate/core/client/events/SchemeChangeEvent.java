package com.dotemplate.core.client.events;

import com.dotemplate.core.client.frwk.Event;
import com.dotemplate.core.shared.Scheme;


public class SchemeChangeEvent extends Event< SchemeChangeEventHandler > {
				
	private Scheme scheme ;
	
	public SchemeChangeEvent( Scheme scheme ) {
		this.scheme = scheme ;
	}

	
	public Scheme getScheme() {
		return scheme;
	}
	
	
	public static  Type< SchemeChangeEventHandler > TYPE = 
		new Type< SchemeChangeEventHandler >();
	

	@Override
	protected void dispatch( SchemeChangeEventHandler handler ) {
		handler.onSchemeChanged( this ) ;
	}

	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type< SchemeChangeEventHandler > getAssociatedType() {
		return TYPE ;
	}

	
	
}
