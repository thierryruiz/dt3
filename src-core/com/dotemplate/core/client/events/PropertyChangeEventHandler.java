package com.dotemplate.core.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface PropertyChangeEventHandler extends EventHandler {
	
	void onPropertyChanged( PropertyChangeEvent e ) ;

}
