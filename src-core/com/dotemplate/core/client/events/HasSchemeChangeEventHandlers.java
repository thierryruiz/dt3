package com.dotemplate.core.client.events;


import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasSchemeChangeEventHandlers extends HasHandlers {
	
	HandlerRegistration addHandler ( SchemeChangeEventHandler h ) ; 
	
	
}
