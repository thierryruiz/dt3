package com.dotemplate.core.client.events;


import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.HasHandlers;


public interface HasPropertyChangeEventHandlers extends HasHandlers {
	
	HandlerRegistration addHandler ( PropertyChangeEventHandler h ) ; 
	
	void removeHandler ( PropertyChangeEventHandler h ) ;
	
	
}
