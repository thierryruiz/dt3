package com.dotemplate.core.client;


import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
//import com.google.gwt.user.client.ui.PopupPanel;


public class Loader {
	
	private static Image ajaxLoader ;
	
	private static Window waitWnd ;

	private static El _mask ;
	
	
	static {
		
		ajaxLoader = new Image ( "client/images/ajax-loader.gif" ) ;
		ajaxLoader.setStyleName( "ez-loader" ) ;
		ajaxLoader.setPixelSize( 100, 100 ) ;

		waitWnd = new Window() ;
		waitWnd.setBodyBorder ( false ) ;
		waitWnd.setBorders ( false ) ;
		waitWnd.setPlain( false ) ;
		waitWnd.setResizable( false ) ;
		waitWnd.setHeaderVisible ( false ) ;
		waitWnd.setLayout ( new FitLayout() ) ;
		waitWnd.setSize ( 100, 100 ) ;
		
		waitWnd.add ( new Image ( "client/images/ajax-loader.gif" ) ) ;
		
		RootPanel.get().add( ajaxLoader ) ;
	
		_mask = new El("<div class='ext-el-mask'></div>");
		  
	    _mask.setSize( RootPanel.getBodyElement().getOffsetWidth(), RootPanel.getBodyElement().getOffsetHeight());
	    
	    RootPanel.getBodyElement().appendChild( _mask.dom ) ;
		
	}
	
	
	

	
	public static void show( String message, boolean showAjaxLoader ) {
		
		if ( showAjaxLoader ){
			ajaxLoader.setVisible( true ) ;
		}
		
		_mask.setDisplayed(true) ;
		
		
		//if ( message != null )
		
		// TODO 
		//instance.waitImage.show ();
		//instance.waitImage.setPosition ( 20, 20 ) ;
				
		//Point p = instance.waitImage.el().getAlignToXY(XDOM.getBody(), "c-c", null);
		//instance.waitImage.setPagePosition(p.x, p.y);
		
	
	}
	
	public static void hide() {	
		ajaxLoader.setVisible( false ) ;
		_mask.setDisplayed(false) ;
	}
	

}
