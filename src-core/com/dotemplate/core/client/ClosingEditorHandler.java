package com.dotemplate.core.client;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ClosingEvent;
import com.google.gwt.user.client.Window.ClosingHandler;

public class ClosingEditorHandler implements ClosingHandler, CloseHandler< Window > {

	private boolean confirmClosing = true ;
	
	public ClosingEditorHandler() {
		Window.addCloseHandler( this ) ;
		Window.addWindowClosingHandler( this ) ;
	}
	
	
	@Override
	public void onWindowClosing( ClosingEvent closingEvent ) {

		if( confirmClosing ){
			
			closingEvent.setMessage( "Quit editor ?" );			
		
		} else {
			
			closingEvent.setMessage( null );
		
		}

	}
	
	@Override
	public void onClose( CloseEvent <Window > event ) {
	}

	
	public void confirmClosing( boolean confirm ){
		confirmClosing = confirm ;
	}
	
	
}