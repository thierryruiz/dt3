package com.dotemplate.core.client.frwk;

import com.dotemplate.core.shared.frwk.Command;
import com.dotemplate.core.shared.frwk.Response;
import com.google.gwt.user.client.rpc.AsyncCallback;


public interface RemoteServiceAsync {

	<T extends Response> void execute ( Command<T> action , AsyncCallback<T> callback ) ;

}
