package com.dotemplate.core.client.frwk;


public abstract class AbstractPresenter< V extends View > implements Presenter< V > {

	protected V view ;
	
	public AbstractPresenter( V view ) {
		this.view = view ;
		bind() ;
	}
	
	public V getView() {
		return view;
	}
	
}
