package com.dotemplate.core.client.frwk;


public interface Presenter< V extends View > {
	
	void bind() ;

}
