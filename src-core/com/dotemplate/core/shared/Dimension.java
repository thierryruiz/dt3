package com.dotemplate.core.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Dimension implements IsSerializable, Serializable {

	private static final long serialVersionUID = -4459914523519803522L;

	private int width ;
	
	private int height ;

	public int getHeight() {
		return height;
	}

	public void setHeight( int height ) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth( int width ) {
		this.width = width;
	}
	
}
