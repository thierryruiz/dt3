package com.dotemplate.core.shared.properties;


import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.canvas.TextGraphic;


public class TextProperty extends PropertySet implements TextGraphic  {

	private static final long serialVersionUID = -9136737800341132684L;

	public final static int PADDING = 15 ;
	
	private String value;

	private Integer top ;

	private Integer left ;
	
	private Integer width;

	private Integer height;

	private Integer paddingTop ;
	
	private Integer paddingLeft ;
	
	private Integer paddingBottom ;
	
	private Integer paddingRight ;	
	
	private String fontFamily ;
	
	private Integer fontSize ;
	
	private Boolean visible  ;

	private String imgUrl ;
	
	private Float opacity ;
	
	private String workingUri ;
			
	private int renderedWidth ;
	
	private int renderedHeight ;
	
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "enable", "free", "editable", 
				"top", "left", "fontFamily", "fontSize", "opacity" , "visible", 
				"paddingTop", "paddingRight", "paddingBottom", "paddingLeft" } ;
	}
	
	
	public int getRenderedWidth () {
		return renderedWidth;
	}

	public void setRenderedWidth ( int renderedWidth ) {
		this.renderedWidth = renderedWidth;
	}

	public int getRenderedHeight () {
		return renderedHeight;
	}

	public void setRenderedHeight ( int renderedHeight ) {
		this.renderedHeight = renderedHeight;
	}

	public String getWorkingUri () {
		return workingUri;
	}

	public void setWorkingUri ( String workingUri ) {
		this.workingUri = workingUri;
	}

			
	public String getValue () {
		return value;
	}

	public void setValue ( String value ) {
		this.value = value;
	}
	
	public Integer getTop () {
		return top;
	}

	public void setTop ( Integer top ) {
		this.top = top;
	}

	public Integer getLeft () {
		return left;
	}

	public void setLeft ( Integer left ) {
		this.left = left;
	}

	public Integer getWidth () {
		return width;
	}

	public void setWidth ( Integer width ) {
		this.width = width;
	}

	public Integer getHeight () {
		return height;
	}

	public void setHeight ( Integer height ) {
		this.height = height;
	}


	public String getFontFamily () {
		return fontFamily;
	}

	public void setFontFamily ( String fontFamily ) {
		this.fontFamily = fontFamily;
	}
	
	public int getType () {
		return Graphic.TEXT;
	}

	public String getImgUrl () {
		return imgUrl;
	}

	public void setImgUrl ( String imgUrl ) {
		this.imgUrl = imgUrl;
	}

	public Boolean getVisible () {
		return visible;
	}

	public void setVisible ( Boolean visible ) {
		this.visible = visible;
	}
	
	public Float getOpacity () {
		return opacity;
	}

	public void setOpacity ( Float opacity ) {
		this.opacity = opacity;
	}

	public Integer getPaddingTop () {
		return paddingTop;
	}

	public void setPaddingTop ( Integer paddingTop ) {
		this.paddingTop = paddingTop;
	}

	public Integer getPaddingLeft () {
		return paddingLeft;
	}

	public void setPaddingLeft ( Integer paddingLeft ) {
		this.paddingLeft = paddingLeft;
	}

	public Integer getPaddingBottom () {
		return paddingBottom;
	}

	public void setPaddingBottom ( Integer paddingBottom ) {
		this.paddingBottom = paddingBottom;
	}

	public Integer getPaddingRight () {
		return paddingRight;
	}

	public void setPaddingRight ( Integer paddingRight ) {
		this.paddingRight = paddingRight;
	}


	public Integer getFontSize () {
		return fontSize;
	}

	public void setFontSize ( Integer fontSize ) {
		this.fontSize = fontSize;
	}

	public boolean isMoveable() {
		return true;
	}

	public boolean isResizeable() {
		return false;
	}

	public boolean isEmpty () {
		return value == null ;
	}

	public boolean isLocked () {
		return false;
	}

	@Override
	public String getText () {
		return getValue() ;
	}

	@Override
	public void setText ( String text ) {
		setValue( text ) ;
	}

	
	public boolean isText() {
		return true ;
	}
	
	
	@Override
	public Boolean getMutable () {
		return true ;
	}
	
	@Override
	public boolean mutable() {
		return true ;
	}
	
	
}
