package com.dotemplate.core.shared.properties;


public class PercentProperty extends Property {
	
	private static final long serialVersionUID = 1410480331522695661L;
	
	private Float value ;
	
	
	public Float getValue () {
		return value;
	}

	public void setValue ( Float value ) {
		this.value = value;
	}

	public int getType () {
		return PERCENT ;
	}
	
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "enable", "free", "editable" } ;
	}
	
	
}
