/**
 * Copyright Thierry Ruiz ( Athesanet ) 2004 - 2008
 * All rights reserved.
 *
 * Created on 31 mars 08 : 14:54:23
 */

package com.dotemplate.core.shared.properties;

import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.canvas.ImageGraphic;

/**
 * 
 * 
 * @author Thierry Ruiz
 * 
 */
public class ImageProperty extends Property implements ImageGraphic {

	private static final long serialVersionUID = -1230932029947673340L;

	private Integer top = 0;

	private Integer left = 0;

	private Integer width;

	private Integer height;

	private Float scale = (float) 1.0;

	private float opacity = (float) 1.0;

	private Integer blur = 0 ;
	
	private boolean uploadable;

	private Boolean visible;

	private String value;

	private Float leftFading = (float) 0.0;

	private Float rightFading = (float) 0.0;

	private Float topFading = (float) 0.0;

	private Float bottomFading = (float) 0.0;

	private String workingUri;

	private int renderedWidth;

	private int renderedHeight;

	public int getRenderedWidth() {
		return renderedWidth;
	}

	public void setRenderedWidth(int renderedWidth) {
		this.renderedWidth = renderedWidth;
	}

	public int getRenderedHeight() {
		return renderedHeight;
	}

	public void setRenderedHeight(int renderedHeight) {
		this.renderedHeight = renderedHeight;
	}

	public String getWorkingUri() {
		return workingUri;
	}

	public void setWorkingUri(String workingUri) {
		this.workingUri = workingUri;
	}

	public int getType() {
		return Graphic.IMAGE;
	}

	public Integer getTop() {
		return top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	public Integer getLeft() {
		return left;
	}

	public void setLeft(Integer left) {
		this.left = left;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public boolean isUploadable() {
		return uploadable;
	}

	public void setUploadable(boolean uploadable) {
		this.uploadable = uploadable;
	}

	public Float getOpacity() {
		return opacity;
	}

	public void setOpacity(Float opacity) {
		this.opacity = opacity;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setScale(Float scale) {
		this.scale = scale;
	}

	public Float getScale() {
		return scale;
	}

	public boolean isMoveable() {
		return true;
	}

	public boolean isResizeable() {
		return true;
	}

	public Float getLeftFading() {
		return leftFading;
	}

	public void setLeftFading(Float leftFading) {
		this.leftFading = leftFading;
	}

	public Float getRightFading() {
		return rightFading;
	}

	public void setRightFading(Float rightFading) {
		this.rightFading = rightFading;
	}

	public Float getTopFading() {
		return topFading;
	}

	public void setTopFading(Float topFading) {
		this.topFading = topFading;
	}

	public Float getBottomFading() {
		return bottomFading;
	}

	public void setBottomFading(Float bottomFading) {
		this.bottomFading = bottomFading;
	}

	public boolean isEmpty() {
		return value == null;
	}

	@Override
	public boolean isLocked() {
		return false;
	}

	public boolean isImage() {
		return true;
	}

	@Override
	public boolean isUploaded() {
		// not academic yet effective
		return value.indexOf( "upload" ) > 0 ;
	}
	
	@Override
	public Integer getBlur() {
		return this.blur;
	}
	
	
	public void setBlur( Integer blur ) {
		this.blur = blur;
	}
	
	
	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "enable", "free", "editable", 
				"top", "left","opacity", "scale", "blur",
				"topFading", "rightFading", "bottomFading","leftFading", 
				"visible", "uploadable" } ;
	}

	
}
