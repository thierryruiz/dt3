package com.dotemplate.core.shared.properties;


public class StringProperty extends Property {

	private static final long serialVersionUID = 7821094466895011072L;
	
    private String regexp;

    private String value ;
    
	public String getValue () {
		return value;
	}

	public void setValue ( String value ) {
		this.value = value;
	}

	public String getRegexp() {
		return regexp;
	}

	public void setRegexp( String regexp ) {
		this.regexp = regexp;
	}
	
	public int getType() {
		return STRING ;
	}

	@Override
	public String[] getDiffAttributes() {
		return new String[] { "value", "label", "enable", "free", "editable" } ;
	}
	

	
}
