package com.dotemplate.core.shared;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;


import com.dotemplate.core.shared.properties.Property;
import com.dotemplate.core.shared.properties.PropertySet;
import com.google.gwt.user.client.rpc.IsSerializable;


public class Design implements Serializable, IsSerializable {

	private static final long serialVersionUID = 640740019430732704L;
			
	private LinkedHashMap< String, PropertySet > propertySetMap = new LinkedHashMap< String, PropertySet >() ;
		
	private String uid ;
	
	private String name = "" ;
	
	private String template ;
		
	private String parent ;
				
	private String scheme ;
	
	private String userScheme ;
	
	private Boolean purchased = false ;
	
	private Boolean premium = false ;
		
	public String getUid () {
		return uid;
	}

	public void setUid ( String uid ) {
		this.uid = uid;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getParent() {
		return parent;
	}

	public void setParent( String parent ) {
		this.parent = parent;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate( String template ) {
		this.template = template;
	}
	
	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	

	public boolean isPurchased() {
		return purchased.booleanValue() ;
	}

	public void setPurchased( boolean purchased ) {
		this.purchased = purchased;
	}

	public void setPurchased( Boolean purchased ) {
		this.purchased = purchased;
	}

	public Boolean getPurchased() {
		return purchased ;
	}
	
	
	public boolean isPremium() {
		return premium.booleanValue() ;
	}

	public void setPremium( boolean premium ) {
		this.premium = premium;
	}

	public void setPremium( Boolean premium ) {
		this.premium = premium ;
	}

	public Boolean getPremium() {
		return premium ;
	}
	
	public String getUserScheme() {
		return userScheme;
	}
	
	public void setUserScheme( String userScheme ) {
		this.userScheme = userScheme;
	}
	
	
	public Collection< PropertySet > getPropertySets() {
		return propertySetMap.values ();
	}
	
	public LinkedHashMap< String, PropertySet > getPropertySetMap() {
		return propertySetMap;
	}
	
	public void setPropertySetMap( LinkedHashMap<String, PropertySet> propertySetMap) {
		this.propertySetMap = propertySetMap;
	}
	
	public void addPropertySet ( PropertySet set ){
		propertySetMap.put (  set.getName (), set ) ;
	}

	public PropertySet getPropertySet( String name ){
		return  propertySetMap.get( name ) ;
	}
	
	public boolean  execute ( DesignTraverser traversal ){
		
		for ( Iterator<PropertySet> sets = propertySetMap.values ().iterator () ; sets.hasNext() ; ) {
			if ( ! sets.next ().execute( traversal ) ) return false  ;
		}
		
		return true ;
	}

	
	public Property lookupPropertyByUid ( String uid ){
			   
		Property property ;
		
		for ( PropertySet set : getPropertySets () ){
	
			if ( set.getUid().equals( uid ) ) return set ;
			
			property = findPropertyByUid( set, uid ) ;
			
			if ( property != null ) return property ;
			
		}
		
		return null;
	}

	
	private Property findPropertyByUid ( PropertySet set, String uid ){
			    	
	    Property result ;
	    
		for ( Property property : set.getProperties () ){
						
			if ( property.getUid().equals( uid ) ) return property ;
			
			if ( property.isSet () ) {
				result =  findPropertyByUid ( ( PropertySet ) property, uid ) ;
				if ( result != null ) {
					return result ;
				}
			} 
		}
		
		return null;
	}
	
	
}
