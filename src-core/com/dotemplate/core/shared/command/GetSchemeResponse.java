package com.dotemplate.core.shared.command;


import com.dotemplate.core.shared.Scheme;
import com.dotemplate.core.shared.frwk.Response;

public class GetSchemeResponse implements Response {

	private static final long serialVersionUID = -3925982670368521131L;

	private Scheme scheme ;

	
	public Scheme getScheme () {
		return scheme;
	}

	public void setScheme ( Scheme scheme ) {
		this.scheme = scheme;
	}
	
	
	
}
