package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.Scheme;

public class GetAllSchemesResponse extends GetDesignResourcesMapResponse< Scheme > {

	private static final long serialVersionUID = -8427546658068063334L;

}
