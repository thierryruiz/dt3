package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.command.RenderGraphicResponse;
import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.theme.shared.command.ThemeCommand;



public class RenderGraphic extends ThemeCommand< RenderGraphicResponse > {

	private static final long serialVersionUID = -2166408772743428132L;
	
	private Graphic graphic ;
	

	public Graphic getGraphic () {
		return graphic;
	}
	
	
	public void setGraphic ( Graphic graphic ) {
		this.graphic = graphic;
	}

}
