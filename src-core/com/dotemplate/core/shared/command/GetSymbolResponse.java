package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.frwk.Response;
import com.dotemplate.core.shared.properties.PropertySet;

public class GetSymbolResponse implements Response {

	private static final long serialVersionUID = 8582737476139701798L;

	private PropertySet symbol;

	public PropertySet getSymbol() {
		return symbol;
	}

	public void setSymbol( PropertySet symbol ) {
		this.symbol = symbol;
	}

}
