package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.SVGFont;

public class GetSVGFontsResponse extends GetDesignResourcesMapResponse< SVGFont > {

	private static final long serialVersionUID = 2396790215035157080L;
	
}
