package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.DesignResource;


public class GetDesignResourcesMap< R extends DesignResource > extends DesignCommand< GetDesignResourcesMapResponse< R > > {

	private static final long serialVersionUID = 2168466817375207138L;
	
	private String filter ;
	
	public void setFilter(String filter) {
		this.filter = filter;
	}
	
	public String getFilter() {
		return filter;
	}
	
	
}
