package com.dotemplate.core.shared.command;

import com.dotemplate.core.client.frwk.CommandCallback;
import com.dotemplate.core.shared.command.NewGraphicResponse;
import com.dotemplate.core.shared.canvas.Graphic;



public abstract class NewGraphicDone< GRAPHIC extends Graphic > extends CommandCallback< NewGraphicResponse< GRAPHIC > > {

	
}
