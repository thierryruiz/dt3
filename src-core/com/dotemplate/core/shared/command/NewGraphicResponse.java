package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.canvas.Graphic;
import com.dotemplate.core.shared.frwk.Response;


public class NewGraphicResponse<GRAPHIC extends Graphic > implements Response {

	private static final long serialVersionUID = -2787502323676048256L;
	
	private GRAPHIC graphic ;
	
	public void setGraphic ( GRAPHIC graphic ) {
		this.graphic = graphic;
	}
	
	public GRAPHIC getGraphic () {
		return graphic;
	}
	

}
