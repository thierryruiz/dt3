package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.SymbolType;
import com.dotemplate.core.shared.properties.PropertySet;



public class GetSymbols extends GetDesignResourcesMap< PropertySet > {

	private static final long serialVersionUID = -7794071974471084700L ;
	
	private SymbolType type ;
		
	public SymbolType getType () {
		return type;
	}

	public void setType ( SymbolType type ) {
		this.type = type;
	}

	
}
