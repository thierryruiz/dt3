package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.command.GetSymbolResponse;


public class GetSymbol extends DesignCommand< GetSymbolResponse > {


	private static final long serialVersionUID = 6365258605624444984L;

	private String name ;
		
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
}

