package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.command.GetDesignResources;
import com.dotemplate.core.shared.canvas.ImageResource;

public class GetImageResources extends GetDesignResources< ImageResource > {

	private static final long serialVersionUID = 7545505293574351409L;
			
}
