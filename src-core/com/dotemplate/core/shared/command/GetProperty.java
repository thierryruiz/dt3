package com.dotemplate.core.shared.command;


public class GetProperty extends DesignCommand< GetPropertyResponse > {

	private static final long serialVersionUID = 6365258605624444984L;

	private String uid ;
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getUid() {
		return uid;
	}
	
	
	
	
}

