package com.dotemplate.core.shared.command;

import com.dotemplate.core.shared.command.GetGraphicsResponse;



public class GetGraphics extends DesignCommand< GetGraphicsResponse > {

	private static final long serialVersionUID = 3346362849077727869L;
	
	private String drawableId ;
	
		
	public String getDrawableId () {
		return drawableId;
	}

	public void setDrawableId ( String drawableId ) {
		this.drawableId = drawableId;
	}
	
	
}
