package com.dotemplate.core.shared.frwk;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

public interface Response extends Serializable, IsSerializable {

}
