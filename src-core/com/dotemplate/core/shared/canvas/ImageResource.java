package com.dotemplate.core.shared.canvas;

import com.dotemplate.core.shared.DesignResource;

/**
 * 
 * @author Admin
 *
 */
public class ImageResource implements DesignResource {
	
	private static final long serialVersionUID = -3446105127610848469L;
	
	private String name ;
	
	private String thumbnailHtml ;
	
	private String previewHtml ;
	
	private boolean free ;
	
	private String path ;
	
	private int width ;
	
	private int height ;
	
	private String tags ;
	
	@Override	
	public String getName () {
		return name;
	}

	public void setName ( String name ) {
		this.name = name;
	}

	public String getThumbnailHtml() {
		return thumbnailHtml;
	}

	public void setThumbnailHtml(String thumbnailHtml) {
		this.thumbnailHtml = thumbnailHtml;
	}

	public boolean isFreeResource () {
		return free;
	}

	public void setFree ( boolean free ) {
		this.free = free;
	}


	public String getPath () {
		return path;
	}

	public void setPath ( String path ) {
		this.path = path;
	}
	
	public void setWidth ( int width ) {
		this.width = width;
	}
	
	public int getWidth () {
		return width;
	}
	
	public void setHeight ( int height ) {
		this.height = height;
	}
	
	public int getHeight () {
		return height;
	}
	
	public String getPreviewHtml () {
		return previewHtml;
	}

	public void setPreviewHtml ( String previewHtml ) {
		this.previewHtml = previewHtml;
	}

	@Override
	public boolean isPremium () {
		return !free ;
	}

	@Override
	public String getFilter () {
		return null;
	}

	@Override
	public String getTags() {
		return tags;
	}

	
	public void setTags( String tags ) {
		this.tags = tags;
	}
	

}
