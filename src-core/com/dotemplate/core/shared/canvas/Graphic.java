package com.dotemplate.core.shared.canvas;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;


public interface Graphic extends IsSerializable, Serializable {
	
	public final static String BACKGROUND = "background" ;

	//public final static String FGIMG = "fgImg" ;
	
	//public final static String BGIMG = "bgImg" ;
		
	//public final static String LOGO = "logo" ;
	
	public final static int IMAGE = 7 ;
	
	public final static int BG = 9 ;
	
	public final static int TEXT = 14 ;  
				
	int getType() ;
	
	String getUid() ;
	
	String getName() ;
	
	String getWorkingUri() ;
	
	void setWorkingUri( String uri ) ;
		
	int getRenderedWidth() ;
	
	int getRenderedHeight() ;
	
	String getLabel() ;
	
	void setLabel( String label ) ;
	
	void setLeft ( Integer left ) ;	
	
	void setTop( Integer top ) ;
	
	void setWidth( Integer width ) ;
	
	void setHeight( Integer height ) ;

	Integer getLeft () ;
	
	Integer getTop() ;

	Integer getWidth() ;
	
	Integer getHeight() ;
	
	void setVisible( Boolean visible ) ;
	
	Boolean getVisible() ;

	boolean isResizeable() ;
	
	boolean isMoveable() ;
	
	boolean isEmpty() ;

	boolean isLocked ();
	
	
}
