package com.dotemplate.core.shared;


import java.io.Serializable;


import com.google.gwt.user.client.rpc.IsSerializable;


public class DefaultSite implements Site, IsSerializable, Serializable {

	private static final long serialVersionUID = 2860607208358093844L;

	private String uid  ;
		
	private String themeUid ;
	
	private String type ;
	
	
	public String getUid() {
		return uid;
	}

	public void setUid( String uid ) {
		this.uid = uid;
	}

	public String getType () {
		return type;
	}

	public void setType ( String type ) {
		this.type = type;
	}


	public String getThemeUid () {
		return themeUid;
	}

	public void setThemeUid ( String themeUid ) {
		this.themeUid = themeUid;
	}
	
	
}
